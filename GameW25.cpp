// GameW25.cpp: Определяет точку входа для приложения.
//


#include "stdafx.h"


#define MAX_LOADSTRING 100

// Глобальные переменные:

// Текущий экземпляр
HINSTANCE hInst;                                
// Текст строки заголовка
WCHAR szTitle[MAX_LOADSTRING] = L"Engine W25";    
// Имя класса главного окна
WCHAR szWindowClass[MAX_LOADSTRING] = L"Engine W25";

// Отправить объявления функций, включенных в этот модуль кода:
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow){
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);
	MSG msg;
	int screenWidth = 1920;
	int screenHeight = 1080;
	bool isFullScreen = false;
	bool isVSYNC = false;
	hInst = hInstance;
	SystemClass* m_System = SystemClass::GetInstance();
	bool result;
	// Инициализируем и запускаем системный объект
	result = m_System->Initialize(hInst, screenWidth, screenHeight, isFullScreen, isVSYNC);
	if (result == true) {
		// Запуск цикла
		msg = m_System->Run();
	}
	// Освобождаем и выходим из приложения
	m_System->Shutdown();

    return msg.wParam;
}



