////////////////////////////////////////////////////////////////////////////////
// Filename: panelclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _PANELCLASS_H_
#define _PANELCLASS_H_



class PanelClass : public BoxClass{
public:
	bool Update(InputClass*, int, int);
	bool Create(ID3D10Device*, TextureManagerClass*, int, int);
	bool Render(ID3D10Device*, ShaderManagerClass*,D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	void Add(ID3D10Device* device, WidgetClass* widget);
	bool SetText(std::string, std::wstring);
	void Shutdown(void);
	PanelClass();
private:
	MaterialClass m_Material;
	std::vector<WidgetClass*> m_Widget;
	

};

#endif