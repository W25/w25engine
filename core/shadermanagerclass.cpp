////////////////////////////////////////////////////////////////////////////////
// Filename: shadermanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "shadermanagerclass.h"
#include "stdafx.h"

ShaderManagerClass::ShaderManagerClass(){
	m_TextureShader = nullptr;
	m_ParticleShader = nullptr;
	m_FontShader = nullptr;
	m_LightShader = nullptr;
	m_BumpMapShader = nullptr;
	m_MultiTextureShader = nullptr;
	m_TerrainShader = nullptr;
	p_TextureManager = nullptr;
	p_TextureManager = nullptr;
	p_MeshManager = nullptr;
	p_LightManager = nullptr;
}

bool ShaderManagerClass::Initialize(ID3D10Device* device, HWND hwnd, TextureManagerClass* p_TextureMng,
									MeshManagerClass* p_MeshMng, LightManagerClass* p_LightMng){
	bool result;
	p_Device = device;
	// ����������� ���������� � ��������� ��������
	p_TextureManager = p_TextureMng;
	p_MeshManager = p_MeshMng;
	p_LightManager = p_LightMng;

	// Create the texture shader object.
	m_TextureShader = new TextureShaderClass;
	if(!m_TextureShader){
		return false;
	}
	// Initialize the texture shader object.
	result = m_TextureShader->Initialize(device, hwnd);
	if(!result){
		MessageBox(hwnd, "Could not initialize the texture shader object.", "Error", MB_OK);
		return false;
	}
	// Create the particle shader object.
	m_ParticleShader = new ParticleShaderClass;
	if (!m_ParticleShader) {
		return false;
	}
	// Initialize the particle shader object.
	result = m_ParticleShader->Initialize(device, hwnd);
	if (!result) {
		MessageBox(hwnd, "Could not initialize the particle shader object.", "Error", MB_OK);
		return false;
	}
	// Create the font shader object.
	m_FontShader = new FontShaderClass;
	if(!m_FontShader){
		return false;
	}
	// Initialize the font shader object.
	result = m_FontShader->Initialize(device, hwnd);
	if(!result)
	{
		MessageBox(hwnd, "Could not initialize the font shader object.", "Error", MB_OK);
		return false;
	}

	// Create the light shader object.
	m_LightShader = new LightShaderClass;
	if(!m_LightShader)
	{
		return false;
	}
	// Initialize the light shader object.
	result = m_LightShader->Initialize(device, hwnd);
	if(!result)
	{
		MessageBox(hwnd, "Could not initialize the light shader object.", "Error", MB_OK);
		return false;
	}

	// Create the bump map shader object.
	m_BumpMapShader = new BumpMapShaderClass;
	if(!m_BumpMapShader)
	{
		return false;
	}
	// Initialize the bump map shader object.
	result = m_BumpMapShader->Initialize(device, hwnd);
	if(!result)
	{
		MessageBox(hwnd, "Could not initialize the bump map shader object.", "Error", MB_OK);
		return false;
	}

	// Create the multi texture shader object.
	m_MultiTextureShader = new MultiTextureShaderClass;
	if(!m_MultiTextureShader)
	{
		return false;
	}
	// Initialize the multi texture shader object.
	result = m_MultiTextureShader->Initialize(device, hwnd);
	if(!result)
	{
		MessageBox(hwnd, "Could not initialize the multi texture shader object.", "Error", MB_OK);
		return false;
	}

	// Create the terrain shader object.
	m_TerrainShader = new TerrainShaderClass;
	if(!m_TerrainShader){
		return false;
	}
	// Initialize the terrain shader object.
	result = m_TerrainShader->Initialize(device, hwnd);
	if(!result){
		MessageBox(hwnd, "Could not initialize the terrain shader object.", "Error", MB_OK);
		return false;
	}

	return true;
}


void ShaderManagerClass::Shutdown(){
	// Release the multi texture shader object.
	if(m_MultiTextureShader){
		m_MultiTextureShader->Shutdown();
		delete m_MultiTextureShader;
		m_MultiTextureShader = 0;
	}
	// Release the bump map shader object.
	if(m_BumpMapShader)
	{
		m_BumpMapShader->Shutdown();
		delete m_BumpMapShader;
		m_BumpMapShader = 0;
	}

	// Release the light shader object.
	if(m_LightShader)
	{
		m_LightShader->Shutdown();
		delete m_LightShader;
		m_LightShader = 0;
	}

	// Release the texture shader object.
	if(m_TextureShader)
	{
		m_TextureShader->Shutdown();
		delete m_TextureShader;
		m_TextureShader = 0;
	}
	// Release the particle shader object.
	if (m_ParticleShader)
	{
		m_ParticleShader->Shutdown();
		delete m_ParticleShader;
		m_ParticleShader = 0;
	}
	
	// Release the font shader object.
	if(m_FontShader)
	{
		m_FontShader->Shutdown();
		delete m_FontShader;
		m_FontShader = 0;
	}
	// Release the terrain shader object.
	if(m_TerrainShader)
	{
		m_TerrainShader->Shutdown();
		delete m_TerrainShader;
		m_TerrainShader = 0;
	}

	p_TextureManager = nullptr;
	p_MeshManager = nullptr;
	p_LightManager = nullptr;
	return;
}

void ShaderManagerClass::RenderTextureShader(	int countIndex, D3DXMATRIX worldMatrix,
												D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, MaterialClass material){
	ID3D10ShaderResourceView* textureDiffuse = p_TextureManager->GetTexture(material.pathDiffuse);
	// Render the model using the texture shader.
	m_TextureShader->Render(p_Device, countIndex, worldMatrix, viewMatrix, projectionMatrix, textureDiffuse);

	return;
}

void ShaderManagerClass::RenderParticleShader(int countIndex, D3DXMATRIX worldMatrix,
												D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
												MaterialClass material, D3DXVECTOR3 cameraPosition) {

	ID3D10ShaderResourceView* textureDiffuse = p_TextureManager->GetTexture(material.pathDiffuse);
	// Render the model using the texture shader.
	m_ParticleShader->Render(p_Device, countIndex, worldMatrix, viewMatrix, projectionMatrix, textureDiffuse, cameraPosition);

}


void ShaderManagerClass::RenderSkeleton(int, D3DXMATRIX, D3DXMATRIX , D3DXMATRIX ){
}

void ShaderManagerClass::RenderLightShader(int countIndex, D3DXMATRIX worldMatrix,
											D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, 
											D3DXVECTOR3 cameraPosition, MaterialClass material, std::vector<D3DXMATRIX> boneMatrix){
	LightClass *light = p_LightManager->GetLight();
	ID3D10ShaderResourceView* textureDiffuse = p_TextureManager->GetTexture(material.pathDiffuse);
	// Render the model using the light shader.
	m_LightShader->Render(p_Device, countIndex, worldMatrix, viewMatrix, projectionMatrix,
							textureDiffuse, light->GetDirection(), light->GetAmbientColor(), light->GetDiffuseColor(),
							cameraPosition, light->GetSpecularColor(), light->GetSpecularPower(), boneMatrix);

	return;
}


void ShaderManagerClass::RenderBumpMapShader(int countIndex, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
												D3DXMATRIX projectionMatrix, MaterialClass material){

	/*LightClass light = p_LightManager->GetLight(0);
	ID3D10ShaderResourceView* colorTexture;
	ID3D10ShaderResourceView* normalTexture;
	colorTexture = p_TextureManager->GetTexture(material.pathDiffuse);
	normalTexture = p_TextureManager->GetTexture(material.pathNormal);
	// Render the model using the bump map shader.
	m_BumpMapShader->Render(p_Device, countIndex, worldMatrix, viewMatrix, projectionMatrix,
							colorTexture, normalTexture, light.GetDirection(), light.GetDiffuseColor());*/

	return;
}

void ShaderManagerClass::RenderMultiTextureShader(int indexCount, D3DXMATRIX worldMatrix, 
												  D3DXMATRIX viewMatrix,D3DXMATRIX projectionMatrix, 
												  ID3D10ShaderResourceView* texture_1, ID3D10ShaderResourceView* texture_2){
	// Render the model using the multi texture shader.
	ID3D10ShaderResourceView* textureArray[2];
	textureArray[0] = texture_1;
	textureArray[1] = texture_2;
	m_MultiTextureShader->Render(p_Device, indexCount, worldMatrix, viewMatrix, projectionMatrix, textureArray);
	return;
}

// ������ ��������� ���������
void ShaderManagerClass::RenderTerrainShader(int indexCount, D3DXMATRIX worldMatrix, 
											D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
											std::vector<MaterialClass> vecMaterial, LightClass* light){
	// ������ ������ ��������� ������ ��������.
	MaterialClass shaderMaterial[3];
	// ��������� ������, ������ ������������� ����������
	// ��� ������� �������� ���������� �������� � �������
	for (unsigned int i = 0; i < vecMaterial.size(); i++){
		if (i < 3) {
			shaderMaterial[i] = vecMaterial[i];
		}
	}
	// ��� ����� �������� ���������
	ID3D10ShaderResourceView* colorTexture[3];
	ID3D10ShaderResourceView* normalTexture[3];
	colorTexture[0] = p_TextureManager->GetTexture(shaderMaterial[0].pathDiffuse);
	colorTexture[1] = p_TextureManager->GetTexture(shaderMaterial[1].pathDiffuse);
	colorTexture[2] = p_TextureManager->GetTexture(shaderMaterial[2].pathDiffuse);
	normalTexture[0] = p_TextureManager->GetTexture(shaderMaterial[0].pathNormal);
	normalTexture[1] = p_TextureManager->GetTexture(shaderMaterial[1].pathNormal);
	normalTexture[2] = p_TextureManager->GetTexture(shaderMaterial[2].pathNormal);
	m_TerrainShader->Render(p_Device, indexCount, worldMatrix, viewMatrix, projectionMatrix,
							colorTexture, normalTexture, light->GetDirection(),
							light->GetDiffuseColor(), light->GetSpecularPower());
	return;
}

void ShaderManagerClass::RenderFontShader(int indexCount, D3DXMATRIX worldMatrix,
	D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
	ID3D10ShaderResourceView* texture, D3DXVECTOR4 pixelColor){

	// Render the model using the font shader.
	m_FontShader->Render(p_Device, indexCount, worldMatrix, viewMatrix, projectionMatrix, texture, pixelColor);

	return;
}