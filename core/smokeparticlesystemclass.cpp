
////////////////////////////////////////////////////////////////////////////////
// Filename: SmokeParticleSystemClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "smokeparticlesystemclass.h"
#include "stdafx.h"



SmokeParticleSystemClass::SmokeParticleSystemClass(){
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_particleList = 0;
	m_vertices = 0;
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	flagDeath = false;
}

void SmokeParticleSystemClass::SetPosition(D3DXVECTOR3 pos) {

	if (m_Emitter.prevPosition != pos && m_Emitter.prevPosition != m_Emitter.position) {
		m_Emitter.prevPosition = m_Emitter.position;
	}
	m_Emitter.position = pos;
	position = pos;


}
void SmokeParticleSystemClass::SetDirection(D3DXVECTOR3 dir) {
	m_Emitter.direction = dir;
}

D3DXVECTOR3 SmokeParticleSystemClass::GetPosition(void) {
	return position;
}
D3DXVECTOR3 SmokeParticleSystemClass::GetDirection(void) {
	return m_Emitter.direction;
}
bool SmokeParticleSystemClass::InitializeParticleSystem(){

	// ������������� ���������� ��������� �� ��������� ��������
	m_Emitter.particleDeviation = D3DXVECTOR3(0.1250f, 0.1250f, 0.1250f);
	// ������������� �������� � � ���������
	m_Emitter.particleVelocity = 1.5f;
	m_Emitter.particleVelocityVariation = 0.50f;
	// ������������� ���������� ������ ������
	m_Emitter.particleSize = 0.01f;
	// ������������� ���������� ������ � �������
	m_Emitter.particlesPerSecond = 2000.0f;
	// ������������� ������������ ���������� ������
	m_Emitter.limitParticles = 5000;
	// ���������� ��������� ������ ���������
	m_Emitter.currentParticleCount = 0;
	// ����� � ��������� ��������� ������
	m_Emitter.accumulatedTime = 0.0f;
	// ����� ����� ���������� �������
	m_Emitter.particleTimeLife = 2.0f;
	m_Emitter.flagEmitting = true;
	m_Emitter.flagInterpolation = true;
	m_Emitter.prevPosition = m_Emitter.position;
	m_Emitter.direction = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	
	// Create the particle list.
	m_particleList = new ParticleType[m_Emitter.limitParticles];
	if (!m_particleList){
		return false;
	}
	// Initialize the particle list.
	for (int i = 0; i < m_Emitter.limitParticles; i++){
		m_particleList[i].active = false;
	}
	return true;
}

void SmokeParticleSystemClass::EmitParticles(CameraClass *p_Camera, float dTime){
	bool found;
	D3DXVECTOR3 pos;
	D3DXVECTOR3 speed;
	float distance;
	float red, green, blue;
	float energy;
	int index;
	// Increment the frame time.
	m_Emitter.accumulatedTime += dTime;

	// ���������� ������ ������� ����� �������������
	int countCreateParticles = (int)(m_Emitter.accumulatedTime * m_Emitter.particlesPerSecond);

	for (int k = 0; k < countCreateParticles; k++) {
		// If there are particles to emit then emit one per frame.
		if (m_Emitter.currentParticleCount < (m_Emitter.limitParticles - 1)){
			D3DXVECTOR3 pointEmitter = m_Emitter.position;
			if (m_Emitter.flagInterpolation == true) {
				D3DXVECTOR3 stepEmitter = (m_Emitter.position - m_Emitter.prevPosition);
				pointEmitter = m_Emitter.position - ((float)(k) / (float)(countCreateParticles))*stepEmitter;
			}
			D3DXVECTOR3 normal(0.0f, 1.0f, 0.0f);
			D3DXVECTOR3 posRnd;
			D3DXMATRIX rotationMatrix;
			
			D3DXVECTOR3 rotRnd;
			rotRnd.x = (((float)rand() / (float)RAND_MAX) - 0.5f)*(float)D3DX_PI;
			rotRnd.y = (((float)rand() / (float)RAND_MAX) - 0.5f)*(float)D3DX_PI;
			rotRnd.z = (((float)rand() / (float)RAND_MAX) - 0.5f)*(float)D3DX_PI;
			D3DXMatrixRotationYawPitchRoll(&rotationMatrix, rotRnd.x, 0.0f, rotRnd.z);

			D3DXVec3TransformCoord(&normal, &normal, &rotationMatrix);
			D3DXVec3Normalize(&normal, &normal);
			// Now generate the randomized particle properties.
			pos = pointEmitter;
			pos.x += (((float)rand() - (float)rand()) / RAND_MAX) * m_Emitter.particleDeviation.x;
			pos.y += (((float)rand() - (float)rand()) / RAND_MAX) * m_Emitter.particleDeviation.y;
			pos.z += (((float)rand() - (float)rand()) / RAND_MAX) * m_Emitter.particleDeviation.z;
			//position += m_Emitter.direction*25* m_Emitter.accumulatedTime *  (float)k / (float)countCreateParticles;
			D3DXVECTOR3 directionRnd = m_Emitter.direction / 2.5f;
			
			// ������� ������������� ������ 90 ������������ ������� ��������� ��������
			// ��������� ����������� ������� �� ���� ����
			D3DXMatrixRotationYawPitchRoll(&rotationMatrix, 0, (float)D3DX_PI / 2.0f, 0);
			D3DXVec3TransformCoord(&directionRnd, &directionRnd, &rotationMatrix);
			float angleRnd = (float)D3DX_PI * ((float)rand() - (float)rand()) / RAND_MAX;
			D3DXMatrixRotationYawPitchRoll(&rotationMatrix, 0, 0, angleRnd);
			D3DXVec3TransformCoord(&directionRnd, &directionRnd, &rotationMatrix);

			//speed = -m_Emitter.direction*(m_Emitter.particleVelocity + (((float)rand() - (float)rand()) / RAND_MAX) *
			//	m_Emitter.particleVelocityVariation) + directionRnd * ((float)rand() - (float)rand()) / RAND_MAX;
			speed = pos - pointEmitter;
			D3DXVec3Normalize(&speed, &speed);
			speed *= 0.005f;
			red = 1.0f;
			green = 1.0f;
			blue = 1.0f;
			energy = m_Emitter.particleTimeLife;
			D3DXVECTOR3 vec = (p_Camera->GetPosition() - pos);
			distance = D3DXVec3Length(&vec);
			// Now since the particles need to be rendered from back to front for blending we have to sort the particle array.
			// We will sort using Z depth so we need to find where in the list the particle should be inserted.
			// ��������� ������� (���� ��� � ������������� �� �� �����
			index = 0;
			found = false;
			while (!found && index < m_Emitter.limitParticles){
				if ((m_particleList[index].active == false)){
					found = true;
				} else {
					index++;
				}

			}
			// ����� ����� ��� ����� �������
			if (found == true) {
				// Now insert it into the particle array in the correct depth order.
				m_particleList[index].position = position + normal * 5.0f;
				m_particleList[index].color = D3DXVECTOR4(red, green, blue, 0.5f);
				m_particleList[index].speed = speed;
				m_particleList[index].active = true;
				m_particleList[index].energy = energy;
				m_particleList[index].distance = distance;
				m_particleList[index].size = m_Emitter.particleSize;
				m_Emitter.currentParticleCount++;
				// ������������� ���� �� ���� �������
				if (countCreateParticles > 0) {
					m_Emitter.accumulatedTime = 0.0f;
				}
			}
		}
	}


	return;
}

void SmokeParticleSystemClass::UpdateParticles(CameraClass *p_Camera, float dTime){

	// Each frame we update all the particles by making them move downwards using their position, velocity, and the frame time.
	for (int i = 0; i<m_Emitter.limitParticles; i++){
		if (m_particleList[i].active == true) {
			m_particleList[i].position += m_particleList[i].speed;
			// ������ �� ������ ������� �� ����� �������, ���������� �� ������ ������� � 
			// �������� ����� ����� ������� ��� �������� ��� z - ����������
			D3DXVECTOR3 zvector = m_particleList[i].position - p_Camera->GetPosition();
			D3DXVECTOR3 vec = p_Camera->GetView();
			m_particleList[i].distance = D3DXVec3Dot(&vec, &zvector);

			//m_particleList[i].color.x = 0.50f - m_particleList[i].energy / m_Emitter.particleTimeLife / 2.0f;
			//m_particleList[i].color.y = 0.50f - m_particleList[i].energy / m_Emitter.particleTimeLife / 2.0f;	
			//m_particleList[i].color.z = 0.50f - m_particleList[i].energy / m_Emitter.particleTimeLife / 2.0f;

			m_particleList[i].color.w = m_particleList[i].energy / m_Emitter.particleTimeLife;
			m_particleList[i].energy = m_particleList[i].energy - dTime;
			m_particleList[i].size += 0.01f*dTime;
			D3DXVECTOR3 cameraView = p_Camera->GetView();
			m_particleList[i].distance = D3DXVec3Dot(&cameraView, &zvector);
		}
	}
	return;
}



