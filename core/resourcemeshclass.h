////////////////////////////////////////////////////////////////////////////////
// Filename: resourcemeshclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _RESOURCEMECHCLASS_H_
#define _RESOURCEMECHCLASS_H_

class AnimationClass;
class SkeletonClass;
class MeshClass;

class ResourceMeshClass {
public:
	ResourceMeshClass();
	~ResourceMeshClass();
	void Shutdown();
	bool RenderBuffers(ID3D10Device* device, int methodRender);
	std::vector<D3DXMATRIX> GetBoneMatrix(void);
	int countLink;
	int countBone;
	float scale;
	MeshClass *m_Mesh;
	SkeletonClass* m_Skeleton;	
};


#endif