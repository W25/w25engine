////////////////////////////////////////////////////////////////////////////////
// Filename: shotbaseclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _SHOTBASECLASS_MENU_H_
#define _SHOTBASECLASS_MENU_H_


class ShotBaseClass {
public:
	ShotBaseClass(void);
	virtual ~ShotBaseClass();
	virtual void Update(float);
	void Shutdown(void);
	bool flagActive;
	D3DXVECTOR3 position;
	D3DXVECTOR3 direction;	
	D3DXVECTOR3 targetPos;
	float timeLife;
	float speed;
	float lengthPath;
	float damage;
	float miss;
	float lengthTarget;
	int effectID;
	int targetID;
	int sourceID;
	int typeShotID;

};




#endif
