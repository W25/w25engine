////////////////////////////////////////////////////////////////////////////////
// Filename: buttonclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "buttonclass.h"
#include "stdafx.h"

ButtonClass::ButtonClass(){
	// �������������� �������

	this->m_State = type_state_control::NORMAL;
	eventOnClick = NULL;
	sprite.pathDiffuse = "./data/GUI/redSheet.png";

	m_VertexBuffer = NULL;
	m_IndexBuffer = NULL;
	m_Text = NULL;
}
ButtonClass::~ButtonClass() {
}
// ���������� ������ � ������
bool ButtonClass::Create(ID3D10Device* device, TextureManagerClass* p_TextureManager, 
						 int screenWidth, int screenHeight) {
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	// ����� �� ������, �� ���� ��� Widget � ��� ������ ��������� ������� ��������
	// �����
	m_Text = new TextClass; 
	RECT posSize;
	posSize.left = (LONG)this->Left;
	posSize.top = (LONG)this->Top;
	posSize.right = (LONG)(this->Left + this->Width);
	posSize.bottom = (LONG)(this->Top + this->Height);
	m_Text->Initialize(device, posSize, DT_CENTER | DT_VCENTER, 8.0f);


	// �������� ������
	// ���� ������
	p_TextureManager->Add(this->sprite);

	this->InitializeBuffers(device);
	return true;
}
void ButtonClass::SetText(std::wstring inText) {
	this->Text = inText;

}
bool ButtonClass::Render(	ID3D10Device* device, ShaderManagerClass* p_ShaderManager,
							D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
							D3DXMATRIX projectionMatrix) {
	// ������ ���������������� �������������
	this->RenderBuffers(device);
	p_ShaderManager->RenderTextureShader(this->m_IndexCount, worldMatrix, viewMatrix,
												 projectionMatrix, sprite);
	// ����� �� ������
	m_Text->Render(Text);

	return true;
}
// ����������� ��������
bool ButtonClass::UpdateBuffer() {

	bool result = false;
	HRESULT hresult;
	// Update the vertex buffer with the new solution
	VertexMeshClass *v = 0;
	hresult = m_VertexBuffer->Map(D3D10_MAP_WRITE_DISCARD, 0, (void**)&v);
	if (result == S_OK) {
		for (int i = 0; i < m_VertexCount; i++) {
			v[i] = m_Mesh[i];
		}
		result = true;
	}
	m_VertexBuffer->Unmap();
	return result;
}
void ButtonClass::UpdateState(type_state_control state) {

	D3DXVECTOR2 posTextureLeftTop;
	D3DXVECTOR2 posTextureLeftBottom;
	D3DXVECTOR2 posTextureRightTop;
	D3DXVECTOR2 posTextureRightBottom;

	float frameWidth = 1.0f;
	float frameHeight = 1.0f;
	float frameX = 0.0f;
	float frameY = 0.0f;
	//<SubTexture name = "green_button00.png" x = "0" y = "0" width = "190" height = "49" / >
	//<SubTexture name = "green_button01.png" x = "0" y = "49" width = "190" height = "45" / >
	//<SubTexture name = "green_button02.png" x = "0" y = "94" width = "190" height = "49" / >
	//<SubTexture name = "red_button00.png" x = "0" y = "0" width = "190" height = "45" / >
	//	<SubTexture name = "red_button01.png" x = "0" y = "45" width = "190" height = "49" / >
	//	<SubTexture name = "red_button02.png" x = "0" y = "94" width="190" height="45"/>
	switch (state) {
		case type_state_control::NORMAL:
			// 	<SubTexture name="red_button13.png" x="0" y="139" width="190" height="49"/>
			frameX = 0.0f;
			frameY = 139.0f;
			frameWidth = 190.0f;
			frameHeight = 49.0f;
			break;
		case type_state_control::HOVER:
			// <SubTexture name="red_button11.png" x="190" y="0" width="190" height="49"/>
			frameX = 190.0f;
			frameY = 0.0f;
			frameWidth = 190.0f;
			frameHeight = 49.0f;
			break;
		case type_state_control::ACTIVE:
			//<SubTexture name = "red_button12.png" x = "0" y = "188" width = "190" height = "45" / >
			frameX = 0.0f;
			frameY = 188.0f;
			frameWidth = 190.0f;
			frameHeight = 45.0f;
			break;
	}
	posTextureLeftTop = D3DXVECTOR2(frameX, frameY);
	posTextureLeftBottom = D3DXVECTOR2(frameX, frameY + frameHeight);
	posTextureRightTop = D3DXVECTOR2(frameX + frameWidth, frameY);
	posTextureRightBottom = D3DXVECTOR2(frameX + frameWidth, frameY + frameHeight);

	posTextureLeftTop.x /= 512.0f;
	posTextureLeftBottom.x /= 512.0f;
	posTextureRightTop.x /= 512.0f;
	posTextureRightBottom.x /= 512.0f;
	
	posTextureLeftTop.y /= 256.0f;
	posTextureLeftBottom.y /= 256.0f;
	posTextureRightTop.y /= 256.0f;
	posTextureRightBottom.y /= 256.0f;

	m_Mesh[0].texture = posTextureLeftTop;
	m_Mesh[1].texture = posTextureRightBottom;
	m_Mesh[2].texture = posTextureLeftBottom;
	m_Mesh[3].texture = posTextureLeftTop;
	m_Mesh[4].texture = posTextureRightTop;
	m_Mesh[5].texture = posTextureRightBottom;
}

bool ButtonClass::Update(InputClass* p_Input, int mouseX, int mouseY){
	bool result = false;
	// ������ �� ������
	if (this->GetReside(mouseX, mouseY) == true) {
		// ������
		if (this->m_State == type_state_control::NORMAL) {
			this->m_State = type_state_control::HOVER;
			UpdateState(this->m_State);
			UpdateBuffer();
		}
		// �������� ����� �������
		if (p_Input->IsMouseDown(MOUSE_LEFT)) {
			this->m_State = type_state_control::ACTIVE;
			UpdateState(this->m_State);
			UpdateBuffer();
		}
		// ������ ����� ������, ������ �� ������
		if (p_Input->IsMousePress(MOUSE_LEFT)) {
			// �������
			if (this->m_State == type_state_control::ACTIVE) {
				this->m_State = type_state_control::ACTIVE;
				UpdateState(this->m_State);
				UpdateBuffer();
				this->onClick(MOUSE_LEFT, mouseX, mouseY);
			}
		}
		result = true;
	}else {
		// �� ������ ��� ������
		this->m_State = type_state_control::NORMAL;
		UpdateState(this->m_State);
		UpdateBuffer();
	}

	return result;
}

void ButtonClass::Shutdown(void) {
	if (m_Text) {
		m_Text->Shutdown();
		delete m_Text;
		m_Text = 0;
	}
//	this->ShutdownBuffers();
	
}
// ��������� ������� ������ ����� ������� ��������
void ButtonClass::onClick(int mouseKey, int mouseX, int mouseY){
	if (eventOnClick != nullptr) {
		eventOnClick(mouseKey, mouseX, mouseY);
	}
	return;	
}


ButtonSkillClass::ButtonSkillClass() {
}
ButtonSkillClass::~ButtonSkillClass() {
}

bool ButtonSkillClass::Update(InputClass* p_Input, int mouseX, int mouseY) {
	bool result = false;
	// ������ �� ������
	if (this->GetReside(mouseX, mouseY) == true) {
		// ������
		if (this->m_State == type_state_control::NORMAL) {
			this->m_State = type_state_control::HOVER;
			UpdateState(this->m_State);
			UpdateBuffer();
		}
		// �������� ����� �������
		if (p_Input->IsMouseDown(MOUSE_LEFT)) {
			this->m_State = type_state_control::ACTIVE;
			UpdateState(this->m_State);
			UpdateBuffer();
		}
		// ������ ����� ������, ������ �� ������
		if (p_Input->IsMousePress(MOUSE_LEFT)) {
			// �������
			if (this->m_State == type_state_control::ACTIVE) {
				this->m_State = type_state_control::ACTIVE;
				UpdateState(this->m_State);
				UpdateBuffer();
				this->onClick(MOUSE_LEFT, mouseX, mouseY);
			}
		}
		result = true;
	}else {
		// �� ������ ��� ������
		if (this->m_State != type_state_control::ACTIVE) {
			this->m_State = type_state_control::NORMAL;
			UpdateState(this->m_State);
			UpdateBuffer();
		}
	}

	return result;
}

void ButtonSkillClass::UpdateState(type_state_control state) {
	D3DXVECTOR2 posTextureLeftTop;
	D3DXVECTOR2 posTextureLeftBottom;
	D3DXVECTOR2 posTextureRightTop;
	D3DXVECTOR2 posTextureRightBottom;

	float frameWidth = 1.0f;
	float frameHeight = 1.0f;
	float frameX = 0.0f;
	float frameY = 0.0f;
	//<SubTexture name = "green_button00.png" x = "0" y = "0" width = "190" height = "49" / >
	//<SubTexture name = "green_button01.png" x = "0" y = "49" width = "190" height = "45" / >
	//<SubTexture name = "green_button02.png" x = "0" y = "94" width = "190" height = "49" / >
	//<SubTexture name = "red_button00.png" x = "0" y = "0" width = "190" height = "45" / >
	//	<SubTexture name = "red_button01.png" x = "0" y = "45" width = "190" height = "49" / >
	//	<SubTexture name = "red_button02.png" x = "0" y = "94" width="190" height="45"/>
	switch (state) {
	case type_state_control::NORMAL:
		// 	<SubTexture name="red_button13.png" x="0" y="139" width="190" height="49"/>
		frameX = 0.0f;
		frameY = 100.0f;
		frameWidth = 64.0f;
		frameHeight = 64.0f;
		break;
	case type_state_control::HOVER:
		// <SubTexture name="red_button11.png" x="190" y="0" width="190" height="49"/>
		frameX = 100.0f;
		frameY = 100.0f;
		frameWidth = 64.0f;
		frameHeight = 64.0f;
		break;
	case type_state_control::ACTIVE:
		//<SubTexture name = "red_button12.png" x = "0" y = "188" width = "190" height = "45" / >
		frameX = 100.0f;
		frameY = 100.0f;
		frameWidth = 64.0f;
		frameHeight = 64.0f;
		break;
	}
	posTextureLeftTop = D3DXVECTOR2(frameX, frameY);
	posTextureLeftBottom = D3DXVECTOR2(frameX, frameY + frameHeight);
	posTextureRightTop = D3DXVECTOR2(frameX + frameWidth, frameY);
	posTextureRightBottom = D3DXVECTOR2(frameX + frameWidth, frameY + frameHeight);

	posTextureLeftTop.x /= 256.0f;
	posTextureLeftBottom.x /= 256.0f;
	posTextureRightTop.x /= 256.0f;
	posTextureRightBottom.x /= 256.0f;

	posTextureLeftTop.y /= 256.0f;
	posTextureLeftBottom.y /= 256.0f;
	posTextureRightTop.y /= 256.0f;
	posTextureRightBottom.y /= 256.0f;


	m_Mesh[0].texture = posTextureLeftTop;
	m_Mesh[1].texture = posTextureRightBottom;
	m_Mesh[2].texture = posTextureLeftBottom;
	m_Mesh[3].texture = posTextureLeftTop;
	m_Mesh[4].texture = posTextureRightTop;
	m_Mesh[5].texture = posTextureRightBottom;
}
