////////////////////////////////////////////////////////////////////////////////
// Filename: texturemanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "texturemanagerclass.h"
#include "stdafx.h"


bool TextureManagerClass::Initialize(ID3D10Device* device){
	p_Device = device;
	bool result;
	// ��������� �������� ������
	TexturePack createTexture;
	createTexture.countLink = 1;
	createTexture.m_Path = "NULL";
	// ��������� � �������
	createTexture.m_Texture = new TextureClass;
	result = createTexture.m_Texture->Initialize(device, "./data/errorTexture.jpg");
	if (result == false) {
		return false;
	}
	// ��������� ����� ��������
	m_Pack.insert(std::pair<std::string, TexturePack>("NULL", createTexture));
	return true;
}

// ��������� �������, ���� � �������� �������� ���� � �����
std::string TextureManagerClass::Add(std::string path){
	bool result;
	bool flagUnique = false;
	std::map<std::string,TexturePack>::iterator iter;
	// ��������� �� ������
	if (m_Pack.size() == 0) {
		flagUnique = true;

	} else{
		// �������� ������������ �������
		iter = m_Pack.find(path);
		if (iter == m_Pack.end()) {
			// ���������� � ������
			flagUnique = true;
		} 
	}

	// �������� ���������, ��������� � � �������
	if (flagUnique == true){
		// ��������� ���������
		if (path == "NULL") {
			return "ERROR";
		}
		////////////////////////////////////////////////
		TexturePack createTexture;
		createTexture.countLink = 1;
		createTexture.m_Path = path;
		createTexture.m_Texture = new TextureClass;
		result = createTexture.m_Texture->Initialize(p_Device, path);
		if (result == false) {
			return "ERROR";
		}
		// ��������� ����� ��������
		m_Pack.insert(std::pair<std::string, TexturePack>(path, createTexture));
	} else{
		// ����������� ���������� ������ �� ��������
		iter->second.countLink += 1;
	}
	// ���������� ���� ��������
	return path;
}
// ��������� ��������������� ��������
void TextureManagerClass::Add(TexturePack inPackTexture) {
	std::map<std::string, TexturePack>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(inPackTexture.m_Path);
	// ���� ������ �� ����������� ������� ������ �� ��������
	if (iter != m_Pack.end()) {
		// ���������� � ������
		iter->second.countLink = 1;
		iter->second.m_Texture->SetTexture(inPackTexture.m_Texture->GetTexture());
		// ����� ��������� ����� ��������
	}else {
		m_Pack.insert(std::pair<std::string, TexturePack>(inPackTexture.m_Path, inPackTexture));
	}
}
void TextureManagerClass::Add(MaterialClass material) {
	Add(material.pathAmbient);
	Add(material.pathDiffuse);
	Add(material.pathNormal);
	Add(material.pathSpecular);
}
// ������� ��� �������� ���������
void TextureManagerClass::Erase(MaterialClass material) {
	Erase(material.pathAmbient);
	Erase(material.pathDiffuse);
	Erase(material.pathNormal);
	Erase(material.pathSpecular);
}
void TextureManagerClass::Erase(std::string key){
	std::map<std::string,TexturePack>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	// ��������� ������� ������ �� ��������
	if (iter!=m_Pack.end() && key != "ERROR" && key != "NULL"){
		// ������� ��� ��������� ��������, ��� ������� (������, ��� �� ���������
		if (iter->second.countLink == 0){
			MessageBox(NULL, "Texture", "Warhing", MB_OK);
			return;
		}
		iter->second.countLink -=1;
		// ������ �� �������� ����������� ������ ������
		if (iter->second.countLink==0 ){
			iter->second.Shutdown();
			m_Pack.erase(iter);
		}
	}
	return;
}

// ���������� �������� �� �����
ID3D10ShaderResourceView* TextureManagerClass::GetTexture(std::string key){
	std::map<std::string,TexturePack>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		return iter->second.m_Texture->GetTexture();
	// �� ����� ���� �������� ������
	} else {	
		iter = m_Pack.find("NULL");
		if (iter != m_Pack.end()) {
			return iter->second.m_Texture->GetTexture();
		}
	}
	return NULL;
}

// ������� ��� �������� �� ������
void TextureManagerClass::Shutdown(void){
	// ERROR NULL �������� �� ��������� ��������
	std::map<std::string,TexturePack>::iterator iter;
	for ( iter = m_Pack.begin(); iter != m_Pack.end(); ++iter){
		iter->second.Shutdown();
		//m_Pack.erase(iter);
	}
	m_Pack.clear();
}

// ���������� ��������� �������
unsigned int  TextureManagerClass::GetCountPack(void){	
	return m_Pack.size();
}
// ���������� ������������ �������� �� ��������
unsigned int TextureManagerClass::GetCountLink(std::string key){
	std::map<std::string,TexturePack>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		return iter->second.countLink;
	}
	return 0;
}

// ���������� �������� �� ������ � � ����������
ID3D10ShaderResourceView* TextureManagerClass::GetTextureByItem(unsigned int index){
	std::map<std::string,TexturePack>::iterator iter;
	unsigned int count = 0;
	for ( iter = m_Pack.begin(); iter != m_Pack.end(); iter++ ){
		if (index==count){
			return iter->second.m_Texture->GetTexture();
		}
		count++;
	}
	return NULL;
}
std::string TextureManagerClass::GetPathTexture(unsigned int index){
	std::map<std::string,TexturePack>::iterator iter;
	unsigned int count=0;
	for ( iter = m_Pack.begin(); iter != m_Pack.end(); iter++ ){
		if (index==count){
			return iter->second.m_Path;
		}
		count++;
	}
	return "";
}