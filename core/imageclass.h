////////////////////////////////////////////////////////////////////////////////
// Filename: cursorclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _IMAGECLASS_H_
#define _IMAGECLASS_H_


class ImageClass : public BoxClass{
public:
	bool Create(ID3D10Device*, TextureManagerClass*, int, int);
	bool Render(ID3D10Device*, ShaderManagerClass*,
				D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	void SetMaterial(ID3D10Device* device, TextureManagerClass* p_TextureManager, MaterialClass material);
	MaterialClass m_Material;
	ImageClass();

};

#endif