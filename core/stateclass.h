////////////////////////////////////////////////////////////////////////////////
// Filename: statesclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _STATECLASS_H_
#define _STATECLASS_H_


class D3DClass;
class MsgManagerClass;
class MeshManagerClass;
class TextureManagerClass;
class LightManagerClass;
class ShaderManagerClass;
class RenderTextureClass;
class GuiManagerClass;
class CameraClass;
class InputClass;

////////////////////////////////////////////////////////////////////////////////
// Struct name: MsgMode
////////////////////////////////////////////////////////////////////////////////
struct ModeStruct{
	bool pause; // ����� ���������� ����������
	unsigned int itemRender; // ����� ������� ���������� 
	unsigned int itemFrame; // ����� ������� ���������� �����
};

////////////////////////////////////////////////////////////////////////////////
// Class name: State
////////////////////////////////////////////////////////////////////////////////

class StateClass{ 
public:
	StateClass();
	virtual ~StateClass();
	// ��������� ������������ ���������
	virtual bool InitializeState(void);
	// ������������� ���������
	virtual bool Resume();
	// ���������� ��������� � ������ �����
	virtual bool Pause();
	// ����� �� ���������
	virtual bool Exit();
	// ��������� �������� ������� ������ ������� �� �����, ������ ����������
	virtual bool RenderState();
	// ���������� ������
	virtual bool UpdateState(InputClass*, float);
	// ������������� GUI
	virtual bool InitializeComponentGUI( int screenWidth, int screenHeight);
	// ����������� �������� �����������
	virtual bool ShutdownState(void);
	// ���������� ��������� �������
	bool Shutdown(void);
	// ������ ������ ���������
	bool Initialize(HWND*, D3DClass*, MsgManagerClass*, MeshManagerClass*, ModelManagerClass*,
					TextureManagerClass*, LightManagerClass*, ShaderManagerClass*);
	bool Render();
	bool Update(InputClass*, float);
	bool RenderTexture();
	// ���������� �������� ���������
	WCHAR* GetNameState();
	// ���������� ����� ������ ���������
	float GetRunTime();
	// ���������� � ����� ������ ��������
	ModeStruct GetMode();
protected:
	// ����� ���������, �� �������� ���������� �������� ���������
	HWND* p_HWND;
	D3DClass* p_D3D;
	MsgManagerClass* p_MsgManager; 
	MeshManagerClass* p_MeshManager;
	TextureManagerClass* p_TextureManager;
	LightManagerClass* p_LightManager;
	ShaderManagerClass* p_ShaderManager;
	ModelManagerClass* p_ModelManager;
	// ��������� � ��������
	RenderTextureClass* m_RenderTexture;
	// �������� ����������
	GuiManagerClass* m_GuiManager;
	CameraClass* m_Camera;
	// ������� ������ � �������
	FpsClass m_Fps;
	// ������ �������� CPU
	CpuClass m_Cpu;
	// ������� �������
	int m_ScreenWidth;
	int m_ScrenHeight;
	// �������� ���������
	WCHAR* nameState;
	// ����� ������ ���������
	float runTime;
	// ���������� ����������� ������
	int countRenderVertex;
	// ����� ��������� � ������ �����
	ModeStruct mode;
	// ����� ��� ����?
};

#endif