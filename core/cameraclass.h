////////////////////////////////////////////////////////////////////////////////
// Filename: cameraclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _CAMERACLASS_H_
#define _CAMERACLASS_H_

#include <vector3.h>
#include <matrix4x4.h>

struct D3DXVECTOR3;
struct D3DXMATRIX;

class CameraClass{
public:
	bool flagFreez;
	bool flagUpdate;
	CameraClass();
	CameraClass(const CameraClass&);
	~CameraClass();
	void Shutdown(void) {};
	void Initialize(int screenWidth, int screenHeight, float screenDepth, float screenNear);
	void SetPosition(D3DXVECTOR3);
	void SetRotation(D3DXVECTOR3);
	void AddRotation(D3DXVECTOR3);
	void AddRotation(D3DXVECTOR3, D3DXVECTOR3);
	void AddPosition(D3DXVECTOR3);
	void MovePosition(float); // �������� ����������� �������
	void ShiftPosition(float, float); // �������� ��������������� �������
	void AddLength(float);
	void SetLength(D3DXVECTOR3,float);
	void Clone(CameraClass* camera);
	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetRotation();
	D3DXVECTOR3 GetView();
	// �������� ����������� ������� �������
	D3DXVECTOR3 GetCursorView(float MouseX, float MouseY, D3DXVECTOR3 position);

	void Render();
	void GetViewMatrix(D3DXMATRIX*);
	void GetWorldMatrix(D3DXMATRIX*);
	void GetProjectionMatrix(D3DXMATRIX*);
	void GetOrthoMatrix(D3DXMATRIX*);
	void GetMatrixGUI(D3DXMATRIX*);
	void GetMatrixModel(D3DXMATRIX*);
private:
	float m_FieldOfView;
	float m_ScreenAspect;
	float m_ScreenWidth;
	float m_ScreenHeight;
	float m_ScreenNear;
	float m_ScreenDepth;
	float m_Length;
	D3DXVECTOR3 m_Position;
	D3DXVECTOR3 m_Rotation;
	D3DXVECTOR3 m_View;
	D3DXMATRIX m_ViewMatrix;
	D3DXMATRIX m_ProjectionMatrix;
	D3DXMATRIX m_WorldMatrix;
	D3DXMATRIX m_MatrixGUI;
	D3DXMATRIX m_OrthoMatrix;
};

#endif