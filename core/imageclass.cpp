////////////////////////////////////////////////////////////////////////////////
// Filename: imageclass.h
////////////////////////////////////////////////////////////////////////////////
#include "imageclass.h"
#include "stdafx.h"

// ������� ��������
bool ImageClass::Create(ID3D10Device* device, TextureManagerClass* p_TextureManager,
						int screenWidth, int screenHeight) {
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	p_TextureManager->Add(m_Material.pathDiffuse);
	this->InitializeBuffers(device);

	return true;
}

bool ImageClass::Render(ID3D10Device* device, ShaderManagerClass* p_ShaderManager,
						D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
						D3DXMATRIX projectionMatrix) {

	this->RenderBuffers(device);
	
	p_ShaderManager->RenderTextureShader(this->m_IndexCount, worldMatrix, viewMatrix, projectionMatrix,
										 m_Material);
	return true;
}

void ImageClass::SetMaterial(ID3D10Device*, TextureManagerClass* p_TextureManager, MaterialClass material) {
	p_TextureManager->Erase(m_Material);
	p_TextureManager->Add(m_Material.pathDiffuse);
	m_Material = material;

}
ImageClass::ImageClass()
{
	m_Material.pathDiffuse = "NULL";
}

