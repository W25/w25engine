////////////////////////////////////////////////////////////////////////////////
// Filename: unitmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "unitmanagerclass.h"
#include "stdafx.h"



void UnitClass::Shutdown(void) {
	unitModel.Shutdown();
}

void UnitClass::SetMovePoint(D3DXVECTOR3 point) {
	markPoint = point;
	isMove = true;
	isRotation = true;
}



void UnitClass::UpdateMove(float dTime) {
	if (isMove == true) {
		// ������������ ������	
		if (unitModel.GetAnglePoint(markPoint) > D3DX_PI / 64.0f && isRotation == true) {
			D3DXVECTOR3 direction = unitModel.GetView();
			D3DXVECTOR3 markDirection = (markPoint - unitModel.position) - direction;
			D3DXVec3Normalize(&markDirection, &markDirection);
			// ���������� ���� ����� ������������ �������� � �����
			float angle = atan2(direction.x * markDirection.z - direction.z * markDirection.x,
								direction.x * markDirection.x - direction.z * markDirection.z);
			debug = angle;
			if (angle >= 0) {
				unitModel.SetTypeAnimation(TypeAnimation::TURN_R);
				unitModel.AddRotation(D3DXVECTOR3(-SpeedRotation * dTime, 0.0f, 0.0f));
			}
			if (angle < 0) {
				unitModel.SetTypeAnimation(TypeAnimation::TURN_L);
				unitModel.AddRotation(D3DXVECTOR3(SpeedRotation * dTime, 0.0f, 0.0f));
			}		
		} else {
			// ������������ ����������� ��������
			isRotation = false;
			if (unitModel.GetAnglePoint(markPoint) > D3DX_PI / 16.0f) {
				isRotation = true;
			}
			// ���������� ������
			D3DXVECTOR3 vecTarget = (markPoint - unitModel.position);
			float leght = D3DXVec3Length(&vecTarget);
			if (leght > unitModel.radiusModel) {
				unitModel.SetTypeAnimation(TypeAnimation::MOVE_UP);
				unitModel.MovePosition(SpeedMove * dTime);
			} else {
				unitModel.SetTypeAnimation(TypeAnimation::IDLE);
				isMove = false;
			}
		}
	}


}







UnitManagerClass::UnitManagerClass() {

}

bool UnitManagerClass::Initialize(void) {

	return true;
}

void  UnitManagerClass::AddUnit(UnitClass* unit) {
	unitID++;
	unit->ID = unitID;
	m_Units.insert(std::pair<int, UnitClass*>(unitID, unit));
}

UnitClass* UnitManagerClass::GetUnitSelection(D3DXVECTOR3 pos, D3DXVECTOR3 dir) {
	std::map<int, UnitClass*>::iterator iter;
	float distResult = D3D10_FLOAT32_MAX;
	UnitClass* result = nullptr;
	// ������� ��������� ����
	for (iter = m_Units.begin(); iter != m_Units.end(); ++iter) {
		D3DXVECTOR3 vec = iter->second->unitModel.position - pos;
		D3DXVECTOR3 resVec;
		D3DXVec3Cross(&resVec, &vec, &dir);
		float dist = D3DXVec3Length(&resVec) / D3DXVec3Length(&dir);
		if (dist < iter->second->unitModel.radiusModel) {
			distResult = dist;
			result = iter->second;
		}
	}
	return result;
}

void UnitManagerClass::Shutdown() {
	std::map<int, UnitClass*>::iterator iter;
	for (iter = m_Units.begin(); iter != m_Units.end(); ++iter) {
		// ��������� ������ ��������
		iter->second->Shutdown();
		delete iter->second;
		iter->second = nullptr;
	}
	m_Units.clear();
}

void UnitManagerClass::Render(CameraClass* p_Camera) {
	std::map<int, UnitClass*>::iterator iter;
	for (iter = m_Units.begin(); iter != m_Units.end(); ++iter) {
		// ��������� ������ ��������
		iter->second->unitModel.Render(p_Camera);
	}
}

void UnitManagerClass::Update(float dTime, TerrainClass *p_Terrain) {
	std::map<int, UnitClass*>::iterator iter;
	for (iter = m_Units.begin(); iter != m_Units.end(); ++iter) {
		iter->second->UpdateMove(dTime);
		// ��������� ������ ��������
		iter->second->unitModel.Update(dTime* iter->second->SpeedMove, p_Terrain);
	}
}

UnitClass* UnitManagerClass::GetUnit(int ID) {
	std::map<int, UnitClass*>::iterator iter;
	// ���� ���� � ����������
	iter = m_Units.find(ID);
	// ���� ����������
	if (iter != m_Units.end()) {
		return iter->second;
	}
	// �� ������������ ������
	return nullptr;
}