#include "partmodelclass.h"
#include "stdafx.h"

ModelClass::ModelClass() {
	m_Skeleton = nullptr;
	p_HeadBone = nullptr;
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	//D3DXQuaternionIdentity(&rotation);
}

void ModelClass::RefreshAnimation(TypeAnimation animation) {
	m_Skeleton->RefreshAnimation(animation);
}
void ModelClass::PlayAnimation(TypeAnimation animation, double dTime) {
	m_Skeleton->PlayAnimation(animation, dTime);
}

void ModelClass::SkeletonClone(SkeletonClass *skeleton) {
	if (m_Skeleton == nullptr) {
		m_Skeleton = new SkeletonClass();
		m_Skeleton->CopySkeleton(skeleton);
	}
}

BoneTreeClass* ModelClass::GetBoneRoot(TypeBody item) {
	switch (item) {
		case TypeBody::BODY:
			break;
		case TypeBody::HEAD:
			return p_HeadBone;
			break;
		case TypeBody::LEGS:
			break;
		case TypeBody::WEAPON_0:
			break;
		case TypeBody::WEAPON_1:
			break;
		case TypeBody::WEAPON_2:
			break;
		case TypeBody::WEAPON_3:
			break;
	}
}

void ModelClass::Shutdown(void) {
	if (m_Skeleton != nullptr) {
		m_Skeleton->Shutdown();
		delete m_Skeleton;
		m_Skeleton = nullptr;
	}

}
void ModelClass::AddMeshID(int ID){
	MeshID.push_back(ID);
}

std::vector<D3DXMATRIX> ModelClass::GetMatrixBones(void) {
	std::vector<D3DXMATRIX> result;
	if (m_Skeleton != nullptr) {
		result = m_Skeleton->GetMatrixBones();
	}
	return result;
}

