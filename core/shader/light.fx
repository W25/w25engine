////////////////////////////////////////////////////////////////////////////////
// Filename: light.fx
////////////////////////////////////////////////////////////////////////////////


/////////////
// GLOBALS //
/////////////
matrix worldMatrix;
matrix viewMatrix;
matrix projectionMatrix;
Texture2D shaderTexture;
float4 ambientColor;
float4 diffuseColor;
float3 lightDirection;
float3 cameraPosition;
float4 specularColor;
float specularPower;

matrix boneMatrix[100];
///////////////////
// SAMPLE STATES //
///////////////////
SamplerState SampleType{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;

};

/////////////////////
// BLENDING STATES //
/////////////////////
BlendState AlphaBlendingState
{
	BlendEnable[0] = TRUE;
	//SrcBlend = SRC_ALPHA;
	DestBlend = INV_SRC_ALPHA;
	BlendOpAlpha = ADD;
};
//////////////
// TYPEDEFS //
//////////////
struct VertexInputType{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
	uint4 boneID : BLENDINDICES;
	float4 boneWidth : BLENDWEIGHT;
};

struct PixelInputType{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 viewDirection : TEXCOORD1;
};


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelInputType LightVertexShader(VertexInputType input){
	PixelInputType output;
	float4 worldPosition;


	// offset position by bone matrices, using weights to scale
	matrix p = input.boneWidth[0] * boneMatrix[input.boneID[0]];
	p += input.boneWidth[1] *  boneMatrix[input.boneID[1]];
	p += input.boneWidth[2] *  boneMatrix[input.boneID[2]];
	p += input.boneWidth[3] *  boneMatrix[input.boneID[3]];
	input.position.w = 1.0f;
	output.position = mul(input.position, p);
	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.position = mul(output.position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	// Store the texture coordinates for the pixel shader.
	output.tex = input.tex;

	// Calculate the normal vector against the world matrix only.
    output.normal = mul(input.normal, (float3x3)worldMatrix);
    // Normalize the normal vector.
    output.normal = normalize(output.normal);

	// Calculate the position of the vertex in the world.
	worldPosition = mul(input.position, worldMatrix);
    // Determine the viewing direction based on the position of the camera and the position of the vertex in the world.
    output.viewDirection = cameraPosition.xyz - worldPosition.xyz;
    // Normalize the viewing direction vector.
    output.viewDirection = normalize(output.viewDirection);

	return output;
}


////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 LightPixelShader(PixelInputType input) : SV_Target{
	float4 textureColor;
    float3 lightDir;
    float lightIntensity;
    float4 color;
    // Sample the pixel color from the texture using the sampler at this texture coordinate location.
    textureColor = shaderTexture.Sample(SampleType, input.tex);
    // Invert the light direction for calculations.
    lightDir = -lightDirection;
    // Calculate the amount of light on this pixel.
    lightIntensity = saturate(dot(input.normal, lightDir));
    // Determine the final amount of diffuse color based on the diffuse color combined with the light intensity.
    color = saturate(diffuseColor * lightIntensity);
    // Multiply the texture pixel and the final diffuse color to get the final pixel color result.
    color = 0.75f * color * textureColor + textureColor * 0.50f;
	color.a = 1.0f;
	return color;
	
}


////////////////////////////////////////////////////////////////////////////////
// Technique
////////////////////////////////////////////////////////////////////////////////
technique10 LightTechnique
{
    pass pass0
    {
		SetBlendState(AlphaBlendingState, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
        SetVertexShader(CompileShader(vs_4_0, LightVertexShader()));
        SetPixelShader(CompileShader(ps_4_0, LightPixelShader()));
        SetGeometryShader(NULL);
    }
}