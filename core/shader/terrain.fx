////////////////////////////////////////////////////////////////////////////////
// Filename: terrain.fx
////////////////////////////////////////////////////////////////////////////////


/////////////
// GLOBALS //
/////////////
matrix worldMatrix;
matrix viewMatrix;
matrix projectionMatrix;

Texture2D colorTexture_1;
Texture2D colorTexture_2;
Texture2D colorTexture_3;

Texture2D normalTexture_1;
Texture2D normalTexture_2;
Texture2D normalTexture_3;

float3 lightDirection;
float4 lightDiffuseColor;
float colorTextureBrightness;


///////////////////
// SAMPLE STATES //
///////////////////
SamplerState SampleType
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Wrap;
    AddressV = Wrap;
};


//////////////
// TYPEDEFS //
//////////////
struct VertexInputType
{
    	float4 position : POSITION;
    	float2 texcoord : TEXCOORD;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
	float4 mask : MASK;
};

struct PixelInputType
{
    	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
	float4 mask : MASK;
};


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelInputType TerrainVertexShader(VertexInputType input)
{
    PixelInputType output;
    

	// Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;

	// Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(input.position, worldMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
    
	// Store the texture coordinates for the pixel shader.
    output.texcoord = input.texcoord;
    output.mask = input.mask;
    // Calculate the normal vector against the world matrix only and then normalize the final value.
    output.normal = mul(input.normal, (float3x3)worldMatrix);
    output.normal = normalize(output.normal);

	// Calculate the tangent vector against the world matrix only and then normalize the final value.
    output.tangent = mul(input.tangent, (float3x3)worldMatrix);
    output.tangent = normalize(output.tangent);

    // Calculate the binormal vector against the world matrix only and then normalize the final value.
    output.binormal = mul(input.binormal, (float3x3)worldMatrix);
    output.binormal = normalize(output.binormal);


    return output;
}


////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 TerrainPixelShader(PixelInputType input) : SV_Target
{
	float3 lightDir;
	float4 textureMask;
	float4 textureColor_1;
	float4 textureColor_2;
	float4 textureColor_3;
	float4 bumpMap_1;
	float4 bumpMap_2;
	float4 bumpMap_3;

    float3 bumpNormal_1;
	float3 bumpNormal_2;
	float3 bumpNormal_3;

    float lightIntensity_1;
	float lightIntensity_2;
	float lightIntensity_3;

    float4 color_1;
	float4 color_2; 
	float4 color_3;
	float4 resultColor;

	// Sample the texture pixel at this location.
	textureColor_1 = colorTexture_1.Sample(SampleType, input.texcoord);
	textureColor_2 = colorTexture_2.Sample(SampleType, input.texcoord);
	textureColor_3 = colorTexture_3.Sample(SampleType, input.texcoord);
	// Sample the pixel in the bump map.
	bumpMap_1 = normalTexture_1.Sample(SampleType, input.texcoord);
	bumpMap_2 = normalTexture_2.Sample(SampleType, input.texcoord);
	bumpMap_3 = normalTexture_3.Sample(SampleType, input.texcoord);
	// Sample the pixel in the mask
	// Expand the range of the normal value from (0, +1) to (-1, +1).
	bumpMap_1 = (bumpMap_1 * 2.0f) - 1.0f;
	bumpMap_2 = (bumpMap_2 * 2.0f) - 1.0f;
	bumpMap_3 = (bumpMap_3 * 2.0f) - 1.0f;
	// Calculate the normal from the data in the bump map.
	input.normal = normalize(input.normal);
	bumpNormal_1 = input.normal + bumpMap_1.x * (input.tangent) + bumpMap_1.y * (input.binormal);
	bumpNormal_2 = input.normal + bumpMap_2.x * (input.tangent) + bumpMap_2.y * (input.binormal);
	bumpNormal_3 = input.normal + bumpMap_3.x * (input.tangent) + bumpMap_3.y * (input.binormal);
	// Normalize the resulting bump normal.
	bumpNormal_1 = normalize(bumpNormal_1);
	bumpNormal_2 = normalize(bumpNormal_2);
	bumpNormal_3 = normalize(bumpNormal_3);
	// Invert the light direction for calculations.
	lightDir = -lightDirection;
	//
	// Calculate the amount of light on this pixel based on the bump map normal value.
	lightIntensity_1 = saturate(dot(bumpNormal_1, lightDir));
	lightIntensity_2 = saturate(dot(bumpNormal_2, lightDir));
	lightIntensity_3 = saturate(dot(bumpNormal_3, lightDir));
	// Determine the final diffuse color based on the diffuse color and the amount of light intensity.
	color_1 = saturate(lightDiffuseColor * lightIntensity_1);	
	color_2 = saturate(lightDiffuseColor * lightIntensity_2);
	color_3 = saturate(lightDiffuseColor * lightIntensity_3);
	// Combine the final bump light color with the texture color.
		
	resultColor = color_1*textureColor_1*input.mask.x + color_2*textureColor_2*input.mask.y + color_3*textureColor_3*input.mask.z;

	resultColor.a = 1;
	
	return resultColor;
}


////////////////////////////////////////////////////////////////////////////////
// Technique
////////////////////////////////////////////////////////////////////////////////
technique10 TerrainTechnique
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, TerrainVertexShader()));
        SetPixelShader(CompileShader(ps_4_0, TerrainPixelShader()));
        SetGeometryShader(NULL);
    }
}