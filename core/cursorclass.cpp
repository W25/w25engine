////////////////////////////////////////////////////////////////////////////////
// Filename: cursorclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "cursorclass.h"
#include "stdafx.h"

CursorClass::CursorClass() {
	m_Material.pathDiffuse = "./data/GUI/cursor.png";
	this->Width = 32;
	this->Height = 32;
	itemAnimation = 0;
	timeAnimation = 0.0f;
}

// ���������� ������ � ������
bool CursorClass::Create(ID3D10Device* device, TextureManagerClass* p_TextureManager,
						 int screenWidth, int screenHeight) {
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	// �������� ������
	// ���� ������
	this->Left = (float)screenWidth / 2.0f;
	this->Top = (float)screenHeight / 2.0f;
	p_TextureManager->Add(m_Material.pathDiffuse);
	this->InitializeBuffers(device);
	return true;
}
bool CursorClass::Render(ID3D10Device* device, ShaderManagerClass* p_ShaderManager,
						 D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
						 D3DXMATRIX projectionMatrix) {
	D3DXMATRIX translateMatrix;
	D3DXMatrixTranslation(&translateMatrix, this->Left, this->Top, 0.0f);
	// �������� ������ ���� ������
	D3DXMatrixMultiply(&worldMatrix, &worldMatrix, &translateMatrix);
	this->RenderBuffers(device);
	p_ShaderManager->RenderTextureShader(this->m_IndexCount,worldMatrix, viewMatrix, projectionMatrix, m_Material);
	return true;
}

bool CursorClass::Update(InputClass*, int mouseX, int mouseY) {
	// ��� ��� ���������� ��������� �������???
	this->Left =(float) mouseX - (float)m_screenWidth / 2.0f-8.0f;
	this->Top = -(float) mouseY + (float)m_screenHeight / 2.0f+8.0f;
	return true;
}

void CursorClass::Shutdown(void) {
//	this->ShutdownBuffers();
}