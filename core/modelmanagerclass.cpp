////////////////////////////////////////////////////////////////////////////////
// Filename: modelmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "modelmanagerclass.h"
#include "stdafx.h"



int ModelManagerClass::GetCountModel(void) {
	return m_Models.size();
}

int ModelManagerClass::AddModel(ModelClass* model) {
	incrementID++;
	m_Models.insert(std::pair<int, ModelClass*>(incrementID, model));
	return incrementID;
}

void ModelManagerClass::EraseModel(int ID) {
	std::map<int, ModelClass*>::iterator iter;
	// ���� ���� � ����������
	iter = m_Models.find(ID);
	if (iter == m_Models.end()) {
		iter->second->Shutdown();
		delete iter->second;
		iter->second = nullptr;
		m_Models.erase(ID);
	}
}

ModelClass* ModelManagerClass::GetModel(int ID) {
	std::map<int, ModelClass*>::iterator iter;
	// ���� ���� � ����������
	iter = m_Models.find(ID);
	return iter->second;

}

void ModelManagerClass::Shutdown() {
	// ERROR NULL �������� �� ��������� ��������
	std::map<int, ModelClass*>::iterator iter;
	for (iter = m_Models.begin(); iter != m_Models.end(); ++iter) {
		iter->second->Shutdown();
		delete iter->second;
		iter->second = nullptr;
	}
	m_Models.clear();
}
/*void ModelManagerClass::RefreshAnimation(int ID, TypeBody item, TypeAnimation animation) {
	std::map<int, ModelClass*>::iterator iter;
	// ���� ���� � ����������
	iter = m_Models.find(ID);
	if (iter == m_Models.end()) {
		return;
	}
	ModelClass *model = iter->second;
	model->RefreshAnimation(item, animation);
}*/
void ModelManagerClass::Update(int ID, double dTime, TerrainClass* p_Terrain) {
	std::map<int, ModelClass*>::iterator iter;
	// ���� ���� � ����������
	iter = m_Models.find(ID);
	if (iter == m_Models.end()) {
		return;
	}
	ModelClass *model = iter->second;	 
	model->Update(dTime, p_Terrain);
}

bool ModelManagerClass::Initialize(ID3D10Device* device, MeshManagerClass* p_MeshMng, 
									ShaderManagerClass* p_ShaderMng, LightManagerClass* p_LightMng) {
	p_LightManager = p_LightMng;
	p_MeshManager = p_MeshMng;
	p_ShaderManager = p_ShaderMng;
	p_Device = device;
	return true;
}
void ModelManagerClass::Render(int ID, CameraClass* p_Camera, D3DXMATRIX matrixUnit) {
	std::map<int, ModelClass*>::iterator iter;
	// ���� ���� � ����������
	iter = m_Models.find(ID);
	if (iter == m_Models.end()) {
		return;
	}
	ModelClass *p_Model = iter->second;

	MaterialClass material;
	material = p_Model->material;
	LightClass *light = p_LightManager->GetLight(0);	
	std::vector<D3DXMATRIX> boneMatrix = p_Model->GetMatrixBones();

	D3DXMATRIX worldMatrix;
	D3DXMATRIX viewMatrix;
	D3DXMATRIX projectionMatrix;

	D3DXVECTOR3 cameraPos;
	p_Camera->GetWorldMatrix(&worldMatrix);
	p_Camera->GetViewMatrix(&viewMatrix);
	p_Camera->GetProjectionMatrix(&projectionMatrix);
	cameraPos = p_Camera->GetPosition();

	// �������������� �������
	D3DXMATRIX translateMatrix;
	D3DXMATRIX rotationMatrix;
	D3DXMATRIX scaleMatrix;
	// ������� �������� ��������
	D3DXMatrixScaling(&scaleMatrix, p_Model->scale.x, p_Model->scale.y, p_Model->scale.z);
	// ������� �������� ��������
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, p_Model->rotation.x, p_Model->rotation.y, p_Model->rotation.z);
	// ������� ����������� ��������
	D3DXMatrixTranslation(&translateMatrix, p_Model->position.x, p_Model->position.y, p_Model->position.z);
	D3DXMATRIX modelMatrixRoot = p_Model->GetMatrixRoot();
	D3DXMatrixMultiply(&worldMatrix, &worldMatrix, &modelMatrixRoot);
	D3DXMatrixMultiply(&worldMatrix, &worldMatrix, &scaleMatrix);	
	D3DXMatrixMultiply(&worldMatrix, &worldMatrix, &rotationMatrix);
	D3DXMatrixMultiply(&worldMatrix, &worldMatrix, &translateMatrix);
	
	D3DXMatrixMultiply(&worldMatrix, &worldMatrix, &matrixUnit);
	
	
	p_MeshManager->RenderBuffers(p_Model->meshID, p_Model->modeRender);
	int countIndex = p_MeshManager->GetIndexCount(p_Model->meshID);
	if (boneMatrix.size() == 0) {
		p_ShaderManager->RenderTextureShader(countIndex, worldMatrix, viewMatrix, projectionMatrix, material);
	} else {
		p_ShaderManager->RenderLightShader(countIndex, worldMatrix, viewMatrix,
			projectionMatrix, cameraPos, material, boneMatrix);
	}

	
}