#ifndef _STATECLASS_DEBUG_H_
#define _STATECLASS_DEBUG_H_

#include <d3dx10math.h>

class ObjectModelClass;

////////////////////////////////////////////////////////////////////////////////
// ��������� ������ ���������� ��� �������
////////////////////////////////////////////////////////////////////////////////

class DebugState: public StateClass {
public:
	DebugState(WCHAR*);
	//���������� ��������� �� ������ ��� ��������� � �������
	static DebugState* GetInstance() { return &m_DebugState; }
		// ��������� � ���������� ���������
	bool Render(); 
	bool Update(InputClass*, float);
	// �������� ��� ����� � �����(�������� ������)
	bool InitializeState();
	// �������� �� ������ �� ������(�������� �������� ��������)
	bool Exit(); 
	// ������������ ���� ��������
	bool Shutdown();
private:
	ObjectModelClass object;
	bool RenderPrimary(); 
	bool UpdatePrimary(InputClass*, float);
	static DebugState m_DebugState;
	//std::vector<MeshBoxClass*> m_Skeleton;
	// ������ CPU
	CpuClass* m_Cpu;
	// ������ FPS
	FpsClass* m_Fps;
	D3DXVECTOR3 rotationV;
	double frameTime;
	int itemAnimation;
	int itemPeakAnimation;
	MeshLineClass *m_MeshLine;
	ModelManagerClass* m_ModelManager;
	D3DXQUATERNION rotationFix;
	D3DXVECTOR3 translationFix;
};

#endif