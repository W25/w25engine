
////////////////////////////////////////////////////////////////////////////////
// Filename: baseparticlesystemclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "baseparticlesystemclass.h"
#include "stdafx.h"


EmitterClass::EmitterClass() {
	particleVelocity = 0.0f;
	particleTimeLife = 0.0f;
	particleVelocityVariation = 0.0f;
	limitParticles = 1;
	currentParticleCount = 0;
	particleSize = 1;
	particlesPerSecond = 1;
	accumulatedTime = 1;
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	direction = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	particleDeviation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	flagEmitting = true;
}

BaseParticleSystemClass::BaseParticleSystemClass(){
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_particleList = 0;
	m_vertices = 0;
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	flagDeath = false;
}


BaseParticleSystemClass::~BaseParticleSystemClass()
{
}


bool BaseParticleSystemClass::Initialize(void){
	bool result;
	TextureManagerClass* p_TextureManager = TextureManagerClass::GetInstance();
	m_Material.pathDiffuse = "./Data/particle.png";
	p_TextureManager->Add(m_Material);
	// Initialize the particle system.
	result = InitializeParticleSystem();
	if (!result){
		return false;
	}
	// Create the buffers that will be used to render the particles with.
	result = InitializeBuffers();
	if (!result){
		return false;
	}

	return true;
}

void BaseParticleSystemClass::Refresh(void) {
	for (int i = 0; i < m_Emitter.limitParticles; i++) {
		m_particleList[i].active = false;
	}
	m_Emitter.currentParticleCount = 0;
	m_Emitter.accumulatedTime = 0;
	m_Emitter.flagEmitting = true;
	flagDeath = false;
	
}

void BaseParticleSystemClass::Shutdown(){
	// Release the buffers.
	ShutdownBuffers();
	// Release the particle system.
	ShutdownParticleSystem();
	return;
}


bool BaseParticleSystemClass::Update(CameraClass *p_Camera, float dTime){
	bool result;

	// ������� ������ �������
	KillParticles();

	// ���������� �������, ��� ����� ���������
	if (m_Emitter.flagEmitting == true) {
		EmitParticles(p_Camera, dTime);
	}

	// Update the position of the particles.
	UpdateParticles(p_Camera, dTime);

	// ����������� ��������� �� ������� ������
	if (m_Emitter.currentParticleCount == 0 && m_Emitter.flagEmitting == false) {
		// ��������, ��� � ����� �����
		flagDeath = true;
	}
	result = UpdateBuffers(p_Camera);
	if (!result){
		return false;
	}

	return true;
}


void BaseParticleSystemClass::Render(void){
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers();
	return;
}

int BaseParticleSystemClass::GetIndexCount(){
	return m_indexCount;
}
bool BaseParticleSystemClass::GetFlagDeath() {
	return flagDeath;
}
MaterialClass BaseParticleSystemClass::GetMaterial() {
	return m_Material;
}
void BaseParticleSystemClass::SetPosition(D3DXVECTOR3 pos) {
	position = pos;
}
void BaseParticleSystemClass::SetDirection(D3DXVECTOR3 dir) {
	m_Emitter.direction = dir;
}
D3DXVECTOR3 BaseParticleSystemClass::GetPosition(void) {
	return position;
}
D3DXVECTOR3 BaseParticleSystemClass::GetDirection(void) {
	return m_Emitter.direction;
}
void BaseParticleSystemClass::SetEmitting(bool flag) {
	m_Emitter.flagEmitting = flag;
}
bool BaseParticleSystemClass::InitializeParticleSystem(){

	// ������������� ���������� ��������� �� ��������� ��������
	m_Emitter.particleDeviation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// ������������� �������� � � ���������
	m_Emitter.particleVelocity = 0.5f;
	m_Emitter.particleVelocityVariation = 0.50f;
	// ������������� ���������� ������ ������
	m_Emitter.particleSize = 0.05f;
	// ������������� ���������� ������ � �������
	m_Emitter.particlesPerSecond = 1000.0f;
	// ������������� ������������ ���������� ������
	m_Emitter.limitParticles = 5000;
	// ���������� ��������� ������ ���������
	m_Emitter.currentParticleCount = 0;
	// ����� � ��������� ��������� ������
	m_Emitter.accumulatedTime = 0.0f;
	// ����� ����� ���������� �������
	m_Emitter.particleTimeLife = 0.750f;
	m_Emitter.flagEmitting = true;
	// Create the particle list.
	m_particleList = new ParticleType[m_Emitter.limitParticles];
	if (m_particleList == 0){
		return false;
	}
	// Initialize the particle list.
	for (int i = 0; i < m_Emitter.limitParticles; i++){
		m_particleList[i].active = false;
	}
	return true;
}


void BaseParticleSystemClass::ShutdownParticleSystem(){
	// Release the particle list.
	if (m_particleList != 0){
		delete[] m_particleList;
		m_particleList = 0;
	}
	return;
}


bool BaseParticleSystemClass::InitializeBuffers(){
	unsigned long* indices;

	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;


	// Set the maximum number of vertices in the vertex array.
	m_vertexCount = m_Emitter.limitParticles;

	// Set the maximum number of indices in the index array.
	m_indexCount = m_vertexCount;

	// Create the vertex array for the particles that will be rendered.
	m_vertices = new VertexType[m_vertexCount];
	if (!m_vertices){
		return false;
	}

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if (!indices){
		return false;
	}

	// Initialize vertex array to zeros at first.
	memset(m_vertices, 0, (sizeof(VertexType) * m_vertexCount));

	// Initialize the index array.
	for (int i = 0; i<m_indexCount; i++){
		indices[i] = i;
	}

	// Set up the description of the dynamic vertex buffer.
	vertexBufferDesc.Usage = D3D10_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = m_vertices;

	D3DClass* p_D3D = D3DClass::GetInstance();
	// Now finally create the vertex buffer.
	result = p_D3D->GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if (FAILED(result)){
		return false;
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;

	// Create the index buffer.
	result = p_D3D->GetDevice()->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if (FAILED(result)){
		return false;
	}

	// Release the index array since it is no longer needed.
	delete[] indices;
	indices = 0;

	return true;
}


void BaseParticleSystemClass::ShutdownBuffers(){
	// Release the index buffer.
	if (m_indexBuffer){
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if (m_vertexBuffer){
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}


void BaseParticleSystemClass::EmitParticles(CameraClass *p_Camera, float dTime){
	bool found;
	D3DXVECTOR3 pos;
	D3DXVECTOR3 speed;
	float distance;
	float red, green, blue;
	float energy;
	int index;
	// Increment the frame time.
	m_Emitter.accumulatedTime += dTime;

	// ���������� ������ ������� ����� �������������
	int countCreateParticles = (int)(m_Emitter.accumulatedTime * m_Emitter.particlesPerSecond);

	for (int k = 0; k < countCreateParticles; k++) {
		// If there are particles to emit then emit one per frame.
		if (m_Emitter.currentParticleCount < (m_Emitter.limitParticles - 1)){
			// Now generate the randomized particle properties.
			pos.x = m_Emitter.position.x + (((float)rand() - (float)rand()) / RAND_MAX) * m_Emitter.particleDeviation.x;
			pos.y = m_Emitter.position.y + (((float)rand() - (float)rand()) / RAND_MAX) * m_Emitter.particleDeviation.y;
			pos.z = m_Emitter.position.z + (((float)rand() - (float)rand()) / RAND_MAX) * m_Emitter.particleDeviation.z;
			//position += m_Emitter.direction*25* m_Emitter.accumulatedTime *  (float)k / (float)countCreateParticles;
			D3DXVECTOR3 directionRnd = m_Emitter.direction * 2.0f;

			D3DXVECTOR3 posRnd;
			posRnd.x = ((float)rand() / (float)RAND_MAX) - 1.0f;
			posRnd.y = ((float)rand() / (float)RAND_MAX) - 1.0f;
			posRnd.z = ((float)rand() / (float)RAND_MAX) - 1.0f;
			D3DXMATRIX rotationMatrix;
			// ������� ������������� ������ 90 ������������ ������� ��������� ��������
			// ��������� ����������� ������� �� ���� ����
			D3DXMatrixRotationYawPitchRoll(&rotationMatrix, 0, (float)D3DX_PI / 2.0f, 0);
			D3DXVec3TransformCoord(&directionRnd, &directionRnd, &rotationMatrix);
			float angleRnd = (float)D3DX_PI * ((float)rand() - (float)rand()) / RAND_MAX;
			D3DXMatrixRotationYawPitchRoll(&rotationMatrix, 0, 0, angleRnd);
			D3DXVec3TransformCoord(&directionRnd, &directionRnd, &rotationMatrix);

			speed = -m_Emitter.direction*(m_Emitter.particleVelocity + (((float)rand() - (float)rand()) / RAND_MAX) *
				m_Emitter.particleVelocityVariation) + 0.125f*directionRnd * ((float)rand() - (float)rand()) / RAND_MAX;

			red = (((float)rand() - (float)rand()) / RAND_MAX) + 0.5f;
			green =  (((float)rand() - (float)rand()) / RAND_MAX) + 0.5f;
			blue = (((float)rand() - (float)rand()) / RAND_MAX) + 0.5f;
			energy = m_Emitter.particleTimeLife;
			D3DXVECTOR3 CameraPos = (p_Camera->GetPosition() - pos);
			distance = D3DXVec3Length(&CameraPos);
			// Now since the particles need to be rendered from back to front for blending we have to sort the particle array.
			// We will sort using Z depth so we need to find where in the list the particle should be inserted.
			// ��������� ������� (���� ��� � ������������� �� �� �����
			index = 0;
			found = false;
			while (!found && index < m_Emitter.limitParticles) {
				if ((m_particleList[index].active == false)) {
					found = true;
				} else {
					index++;
				}

			}
			// ����� ����� ��� ����� �������
			if (found == true) {
				m_Emitter.currentParticleCount++;
				// Now insert it into the particle array in the correct depth order.
				m_particleList[index].position = position;
				m_particleList[index].color = D3DXVECTOR4(red, green, blue, 1.0f);
				m_particleList[index].speed = speed;
				m_particleList[index].active = true;
				m_particleList[index].energy = energy;
				m_particleList[index].distance = distance;
				m_particleList[index].size = m_Emitter.particleSize;
			}
		}
	}
	// ������������� ���� �� ���� �������
	if (countCreateParticles > 0) {
		m_Emitter.accumulatedTime = 0.0f;
	}

	return;
}
void BaseParticleSystemClass::CamputeParticleSort() {
	int index = 0;
	// ������������ ������� ���� ���� �� ������� (�������� ������������ ����� �� ������ ������)
	while (index < m_Emitter.limitParticles-1) {
		if (m_particleList[index].active == false) {
			m_particleList[index] = m_particleList[index + 1];
		}
		index++;
	}
	if (m_Emitter.currentParticleCount > 0) {
		std::qsort(m_particleList, m_Emitter.currentParticleCount, sizeof(ParticleType),
			(int(*) (const void *, const void *)) CompareParticle);
	}
}

// C�������� ���� ����� ��� QSORT // ������ ��� �� GPU
int BaseParticleSystemClass::CompareParticle(const ParticleType *a, const ParticleType *b){
	// ��������� qsort �� �����������
	// ���� arg1 ������, ��� arg2, �� ������������ �������� -1. 
	// ���� arg1 ����� arg2, �� ������������ 0.
	// ���� arg1 ������, ��� arg2, �� ������������ �������� 1.
	// ��������� �� ��������
	if( (a->distance > b->distance) && a->active == true && b->active == true) {
		return -1;
	}
	return 1;
}

void BaseParticleSystemClass::UpdateParticles(CameraClass *p_Camera, float dTime){

	// Each frame we update all the particles by making them move downwards using their position, velocity, and the frame time.
	for (int i = 0; i<m_Emitter.limitParticles; i++){
		if (m_particleList[i].active == true) {
			m_particleList[i].position += (m_particleList[i].speed * dTime);
			// ������ �� ������ ������� �� ����� �������, ���������� �� ������ ������� � 
			// �������� ����� ����� ������� ��� �������� ��� z - ����������
			D3DXVECTOR3 zvector = m_particleList[i].position - p_Camera->GetPosition();
			D3DXVECTOR3 vecView = p_Camera->GetView();
			m_particleList[i].distance = D3DXVec3Dot(&vecView, &zvector);
			if (m_particleList[i].color.x < 0.75f) {
				m_particleList[i].color.x = 0.75f + (m_particleList[i].energy / m_Emitter.particleTimeLife);
			}
			if (m_particleList[i].color.y < 0.75f) {
				m_particleList[i].color.y = 0.75f + (m_particleList[i].energy / m_Emitter.particleTimeLife);
			}
			m_particleList[i].color.x = m_particleList[i].energy / m_Emitter.particleTimeLife;
			m_particleList[i].color.z = m_particleList[i].energy / m_Emitter.particleTimeLife;

			m_particleList[i].color.w = m_particleList[i].energy / m_Emitter.particleTimeLife;
			m_particleList[i].energy = m_particleList[i].energy - dTime;
			m_particleList[i].size += 0.01f*dTime;

		}
	}
	return;
}

void BaseParticleSystemClass::KillParticles(){

	// Kill all the particles that have gone below a certain height range.
	for (int i = 0; i < m_Emitter.limitParticles; i++){
		if ((m_particleList[i].active == true) && (m_particleList[i].energy < 0.0f)){
			m_particleList[i].active = false;
			if (m_Emitter.currentParticleCount > 0) {
				m_Emitter.currentParticleCount--;
			} else {
				m_Emitter.currentParticleCount = 0;
			}
		}
	}

	return;
}


bool BaseParticleSystemClass::UpdateBuffers(CameraClass *p_Camera){
	void* verticesPtr;
	HRESULT result;
	int index, i;

	// Initialize vertex array to zeros at first.
	memset(m_vertices, 0, (sizeof(VertexType) * m_vertexCount));

	// Now build the vertex array from the particle list array.  Each particle is a quad made out of two triangles.
	index = 0;
	D3DXMATRIX viewMatrix;

	for (i = 0; i < m_Emitter.limitParticles; i++){
		if (m_particleList[i].active == true) {
			m_vertices[index].position = m_particleList[i].position;
			m_vertices[index].color = m_particleList[i].color;
			m_vertices[index].size = m_particleList[i].size;
			index++;
		}
	}

	// Initialize the vertex buffer pointer to null first.
	verticesPtr = 0;

	// Lock the vertex buffer.
	result = m_vertexBuffer->Map(D3D10_MAP_WRITE_DISCARD, 0, (void**)&verticesPtr);
	if (FAILED(result)){
		return false;
	}

	// Copy the vertex array data into the dynamic vertex buffer.
	memcpy(verticesPtr, (void*)m_vertices, (sizeof(VertexType) * m_vertexCount));

	// Unlock the vertex buffer.
	m_vertexBuffer->Unmap();

	return true;
}


void BaseParticleSystemClass::RenderBuffers(void){
	unsigned int stride;
	unsigned int offset;


	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType);
	offset = 0;
	D3DClass* p_D3D = D3DClass::GetInstance();
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	p_D3D->GetDevice()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	p_D3D->GetDevice()->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer.
	//device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINELIST);
	p_D3D->GetDevice()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);

	return;
}

