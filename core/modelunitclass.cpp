////////////////////////////////////////////////////////////////////////////////
// Filename: modelunitclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "modelunitclass.h"
#include "stdafx.h"

ModelUnitClass::ModelUnitClass(void) {
	ID = -1;
	modeRender = 0;
	modeShader = 0;
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	radiusModel = 1.0f;

}
ModelUnitClass::~ModelUnitClass() {

}

TypeAnimation ModelUnitClass::GetTypeAnimation(string type) {
	if (type == "NONE") {
		return TypeAnimation::NONE;
	}
	if (type == "MOVE_UP") {
		return TypeAnimation::MOVE_UP;
	}
	if (type == "MOVE_DOWN") {
		return TypeAnimation::MOVE_DOWN;
	}
	if (type == "TURN_L") {
		return TypeAnimation::TURN_L;
	}
	if (type == "TURN_R") {
		return TypeAnimation::TURN_R;
	}
	if (type == "WAIT") {
		return TypeAnimation::WAIT;
	}
	if (type == "IDLE") {
		return TypeAnimation::IDLE;
	}
	if (type == "TYPE_0") {
		return TypeAnimation::TYPE_0;
	}
	if (type == "TYPE_1") {
		return TypeAnimation::TYPE_1;
	}
	if (type == "STRAFE_L") {
		return TypeAnimation::STRAFE_L;
	}
	if (type == "STRAFE_R") {
		return TypeAnimation::STRAFE_R;
	}
	if (type == "DEATH") {
		return TypeAnimation::DEATH;
	}

	return TypeAnimation::NONE;
}

TypeBody ModelUnitClass::GetTypeBody(string type) {
	if (type == "NONE") {
		return TypeBody::NONE;
	}
	if (type == "LEGS") {
		return TypeBody::LEGS;
	}
	if (type == "BODY") {
		return TypeBody::BODY;
	}
	if (type == "HEAD") {
		return TypeBody::HEAD;
	}
	if (type == "WEAPON_0") {
		return TypeBody::WEAPON_0;
	}
	if (type == "WEAPON_1") {
		return TypeBody::WEAPON_1;
	}
	if (type == "WEAPON_2") {
		return TypeBody::WEAPON_2;
	}
	if (type == "WEAPON_3") {
		return TypeBody::WEAPON_3;
	}
	return TypeBody::NONE;
}

float ModelUnitClass::GetAnglePoint(D3DXVECTOR3 point) {
	D3DXVECTOR3 direction = GetView();
	D3DXVECTOR3 markDirection = (point - position);
	D3DXVec3Normalize(&markDirection, &markDirection);
	// ���������� ���� ����� ������������ �������� � �����
	double angle = (direction.x * markDirection.x + direction.z * markDirection.z) /
		(sqrt((double)direction.x * direction.x + direction.z * direction.z) *
		(sqrt((double)markDirection.x * markDirection.x + markDirection.z * markDirection.z)));
	if (angle < -1.0f) {
		angle = -1.0f;
	}
	if (angle > 1.0f) {
		angle = 1.0f;
	}
	return (float)(acos(angle));
}

void ModelUnitClass::Update(float dTime, TerrainClass* p_Terrain) {
	ModelManagerClass* p_ModelManager = ModelManagerClass::GetInstance();

	p_Terrain->GetHeight(position, &position.y);


	D3DXVECTOR3 direction;
	D3DXVECTOR3 directionTangent(0.0f, 1.0f, 0.0f);
	D3DXVECTOR3 SupportPoint[4];
	direction = GetView();

	D3DXVec3Cross(&directionTangent, &direction, &directionTangent);
	D3DXVec3Normalize(&directionTangent, &directionTangent);
	// ���������� ������� �������
	SupportPoint[0] = position - 0.90f * direction - 0.90f * directionTangent;
	p_Terrain->GetHeight(SupportPoint[0], &SupportPoint[0].y);

	SupportPoint[1] = position + 0.90f * direction - 0.90f * directionTangent;
	p_Terrain->GetHeight(SupportPoint[1], &SupportPoint[1].y);

	SupportPoint[2] = position - 0.90f * direction + 0.90f * directionTangent;
	p_Terrain->GetHeight(SupportPoint[2], &SupportPoint[2].y);

	SupportPoint[3] = position + 0.90f * direction + 0.90f * directionTangent;
	p_Terrain->GetHeight(SupportPoint[3], &SupportPoint[3].y);

	D3DXVECTOR3 normalUnit[4]; 
	D3DXVECTOR3 supportPoint_20 = (SupportPoint[2] - SupportPoint[0]);
	D3DXVECTOR3 supportPoint_12 = (SupportPoint[1] - SupportPoint[0]);
	D3DXVECTOR3 supportPoint_01 = (SupportPoint[0] - SupportPoint[1]);
	D3DXVECTOR3 supportPoint_31 = (SupportPoint[3] - SupportPoint[1]);
	D3DXVECTOR3 supportPoint_32 = (SupportPoint[3] - SupportPoint[2]);
	D3DXVECTOR3 supportPoint_02 = (SupportPoint[0] - SupportPoint[2]);
	D3DXVECTOR3 supportPoint_13 = (SupportPoint[1] - SupportPoint[3]);
	D3DXVECTOR3 supportPoint_23 = (SupportPoint[2] - SupportPoint[3]);

	D3DXVec3Cross(&normalUnit[0], &supportPoint_20, &supportPoint_12);
	D3DXVec3Cross(&normalUnit[1], &supportPoint_01, &supportPoint_31);
	D3DXVec3Cross(&normalUnit[2], &supportPoint_32, &supportPoint_02);
	D3DXVec3Cross(&normalUnit[3], &supportPoint_13, &supportPoint_23);

	for (unsigned int i = 0; i < 4; i++) {
		D3DXVec3Normalize(&normalUnit[i], &normalUnit[i]);

		//p_Terrain->CreateNormal(i, SupportPoint[i], SupportPoint[i] + normalUnit[i]);
	}

	D3DXVECTOR3 normalAvr = (normalUnit[0] + normalUnit[1]  + normalUnit[2]  + normalUnit[3])/4.0f;
	D3DXVec3Normalize(&normalAvr, &normalAvr);
	D3DXVECTOR3 bCross;
	D3DXVec3Cross(&bCross, &normalAvr, &direction);
	D3DXVECTOR3 directionPlayerCross;
	D3DXVECTOR3 vec = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	D3DXVec3Cross(&directionPlayerCross, &direction, &vec);
	// ������
	rotation.y = -atan2(D3DXVec3Length(&bCross), D3DXVec3Dot(&direction, &normalAvr)) - 3.14159f * 3.0f / 2.0f;
	D3DXVec3Cross(&bCross, &normalAvr, &directionPlayerCross);
	// ������
	rotation.z = -atan2(D3DXVec3Length(&bCross), D3DXVec3Dot(&directionPlayerCross, &normalAvr)) - 3.14159f * 3.0f / 2.0f;



	//p_Terrain->GetNormal(position, &rotation);
	

	for (unsigned int i = 0; i < m_PartModels.size(); ++i) {

		p_ModelManager->Update(m_PartModels[i].ID, dTime, p_Terrain);
	}
}

void ModelUnitClass::MovePosition(float speed) {
	position += speed * GetView();
}

void ModelUnitClass::ShiftPosition(float speed) {

}


void ModelUnitClass::AddRotation(D3DXVECTOR3 r) {
	
	rotation += r * 0.0174532925f;

	if (rotation.x > 2.0f*D3DX_PI) {
		rotation.x = 0.0f;
	}
	if (rotation.x < -2.0f * D3DX_PI) {
		rotation.x = 0.0f;
	}
}

D3DXVECTOR3 ModelUnitClass::GetView(void) {
	// Setup where the camera is looking by default.
	D3DXVECTOR3 lookAt(0.0f, 0.0f, 1.0f);
	
	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	float pitch = 0;
	float yaw = rotation.x;
	float roll = 0;
	D3DXMATRIX rotationMatrix;
	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3Normalize(&lookAt, &lookAt);
	// ������ �������
	m_View = lookAt;
	return m_View;
}

void ModelUnitClass::Render(CameraClass *p_Camera) {
	// ������� ��� ������
	ModelManagerClass* p_ModelManager = ModelManagerClass::GetInstance();

	D3DXMATRIX matrixUnit;
	D3DXMATRIX translateMatrix;
	D3DXMATRIX rotationMatrix;
	D3DXMATRIX scaleMatrix;

	p_Camera->GetWorldMatrix(&matrixUnit);
	// ������� �������� ��������
	D3DXMatrixScaling(&scaleMatrix, scale.x, scale.y, scale.z);
	// ������� �������� ��������
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, rotation.x, rotation.y, rotation.z);
	// ������� ����������� ��������
	D3DXMatrixTranslation(&translateMatrix, position.x, position.y, position.z);

	D3DXMatrixMultiply(&matrixUnit, &matrixUnit, &scaleMatrix);
	D3DXMatrixMultiply(&matrixUnit, &matrixUnit, &rotationMatrix);
	D3DXMatrixMultiply(&matrixUnit, &matrixUnit, &translateMatrix);

	for (unsigned int i = 0; i < m_PartModels.size(); ++i) {
		p_ModelManager->Render(m_PartModels[i].ID, p_Camera, matrixUnit);
	}

}


// ������ ������ ���� �������� ������ �������� � 
// ��� �� ���������� ���� �������� �����������
void ModelUnitClass::UpdateAnimation(double dTime) {

}

void ModelUnitClass::SetTypeAnimation(TypeAnimation type) {
	ModelManagerClass* p_ModelManager = ModelManagerClass::GetInstance();
	for (unsigned int i = 0; i < m_PartModels.size(); i++) {
		p_ModelManager->GetModel(m_PartModels[i].ID)->typeAnimation = type;
	}
}

void ModelUnitClass::SetPartModel(PartModelIDClass item) {
	bool isFind = false;
	// �������� ��� ��������� �������� � ������
	for (unsigned int i = 0; i < m_PartModels.size(); i++) {
		if (m_PartModels[i].Type == item.Type) {
			ModelManagerClass *p_ModelManager = ModelManagerClass::GetInstance();
			p_ModelManager->EraseModel(m_PartModels[i].ID);
			m_PartModels[i].ID = item.ID;
			isFind = true;
		}
	}
	if (isFind == false) {
		m_PartModels.push_back(item);
	}
	CamputeRadiusModel();
}

float ModelUnitClass::CamputeRadiusModel(void) {
	radiusModel = 1.25f;
	return radiusModel;
}

bool ModelUnitClass::CreatePartModel(TypeBody type, string path) {
	// ����� �������� �����
	ifstream fileOpenInfoModel;
	fileOpenInfoModel.open(path);
	// ���������, ��� ���� ������� �������
	if (fileOpenInfoModel.fail()) {
		return false;
	}	
	// ���������� ���� �� �������� ����� 
	// ���� ���� '/' � ����� ������
	string pathMap;
	for (int i = path.length() - 1; i > 0; i--) {
		// �����
		if (path[i] == '/') {
			// �������� ������ ���� �� �������� ����� ������ �� ������ '/'
			pathMap = std::string(path, 0, i + 1);
			break;
		}
	}

	string line;
	TypeBody typeModel = TypeBody::NONE;
	float size = 1.0f;
	D3DXVECTOR3 shift(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 rotation(0.0f, 0.0f, 0.0f);
	string strPathModel;
	string strPathTextureDefault;
	std::vector<KeyBoneClass> keyBones;
	std::vector<KeyAnimationClass> keyAnimations;
	while (getline(fileOpenInfoModel, line)) {
		// �� �����������
		if (line.find("//") != 0) {
			// �������� ������
			if (line.find("START_MODEL") == 0) {
				// �������������� ������
			}
			// ��� ������
			if (line.find("TYPE_MODEL") == 0) {
				int indexS = line.find("<");
				int indexE = line.find(">");
				if (indexS != -1 && indexE != -1 && indexS < indexE) {
					typeModel = GetTypeBody(line.substr(indexS+1, indexE - indexS-1));
				}
			}
			// ������� ������
			if (line.find("SIZE") == 0) {
				int indexS = line.find("<");
				int indexE = line.find(">");
				if (indexS != -1 && indexE != -1 && indexS < indexE) {
					string str = line.substr(indexS + 1, indexE - indexS - 1);
					size = stof(str);
				}
			}
			// ����� ������
			if (line.find("SHIFT") == 0) {
				int indexS = line.find("<");
				int indexE = line.find(">");
				if (indexS != -1 && indexE != -1 && indexS < indexE) {
					// �������� ������ �����
					string str = line.substr(indexS + 1, indexE - indexS - 1);
					int indexX = str.find(";");
					shift.x = stof(str.substr(0, indexX));
					// �������� ������ �����
					str = str.substr(indexX + 1);
					int indexY = str.find(";");
					shift.y = stof(str.substr(0, indexY));
					// �������� ������ �����
					str = str.substr(indexY + 1);
					shift.z = stof(str);
				}
			}
			// ������� ������
			if (line.find("ROTATION") == 0) {
				int indexS = line.find("<");
				int indexE = line.find(">");
				if (indexS != -1 && indexE != -1 && indexS < indexE) {
					// �������� ������ �����
					string str = line.substr(indexS + 1, indexE - indexS - 1);
					int indexX = str.find(";");
					rotation.x = stof(str.substr(0, indexX));
					// �������� ������ �����
					str = str.substr(indexX + 1);
					int indexY = str.find(";");
					rotation.y = stof(str.substr(0, indexY));
					// �������� ������ �����
					str = str.substr(indexY + 1);
					rotation.z = stof(str);
				}
			}
			// ���� �� ������
			if (line.find("PATH_MODEL") == 0) {
				int indexS = line.find("<");
				int indexE = line.find(">");
				if (indexS != -1 && indexE != -1 && indexS < indexE) {
					strPathModel = line.substr(indexS + 1, indexE - indexS - 1);

				}
			}
			// ���� �� �������� �� ���������
			if (line.find("PATH_TEXTURE_DEFAULT") == 0) {
				int indexS = line.find("<");
				int indexE = line.find(">");
				if (indexS != -1 && indexE != -1 && indexS < indexE) {
					strPathTextureDefault = line.substr(indexS + 1, indexE - indexS - 1);
				}
			}

			// ����������� ����� ///
			if (line.find("BONE_KEY") == 0) {
				int indexS = line.find("<");
				int indexE = line.find(">");
				if (indexS != -1 && indexE != -1 && indexS < indexE) {
					KeyBoneClass bone;
					string str = line.substr(indexS + 1, indexE - indexS - 1);
					int index = str.find(";");
					bone.Key = GetTypeBody(str.substr(0, index));
					bone.Name = str.substr(index+1);
					keyBones.push_back(bone);
				}
			}
			// �������� 
			if (line.find("ANIMATION") == 0) {
				int indexS = line.find("<");
				int indexE = line.find(">");
				if (indexS != -1 && indexE != -1 && indexS < indexE) {
					KeyAnimationClass animation;
					string str = line.substr(indexS + 1, indexE - indexS - 1);
					int index = str.find(";");
					animation.Key = GetTypeAnimation(str.substr(0, index));
					animation.path = str.substr(index + 1);
					keyAnimations.push_back(animation);
				}
			}
				
			if (line.find("END_MODEL") == 0) {
				// ��������� ������
				MeshManagerClass* p_MeshManagerClass = MeshManagerClass::GetInstance();
				TextureManagerClass* p_TextureManager = TextureManagerClass::GetInstance();
				ModelManagerClass* p_ModelManager = ModelManagerClass::GetInstance();
				// ��������� �����
				FileInfoModelClass infoMesh = p_MeshManagerClass->LoadFileMesh(pathMap + strPathModel);
				// ��������� ��������
				for (unsigned int i = 0; i < keyAnimations.size(); i++) {
					AnimationClass animation = p_MeshManagerClass->LoadFileAnimation(pathMap + keyAnimations[i].path);
					animation.lastIndexFrame = animation.lastIndexFrame-1;
					animation.widthFrame = 1.0f / animation.maxFrame;
					animation.Type = keyAnimations[i].Key;
					p_MeshManagerClass->AddAnimation(infoMesh.meshID, animation);
				}
				keyAnimations.clear();
				// ������� ������ �� ��������
				ModelClass* model = new ModelClass();
				model->CreateModel(infoMesh);
				model->position = shift;
				model->rotation = rotation;
				model->scale *= size;
				model->typeAnimation = TypeAnimation::IDLE;
				if (strPathTextureDefault != "") {
					model->material.pathDiffuse = pathMap + strPathTextureDefault;
				}
				p_TextureManager->Add(model->material);
				// �������� �������� �����
				for (unsigned int i = 0; i < keyBones.size(); i++) {
					keyBones[i].p_Bone = model->m_Skeleton->GetBone(keyBones[i].Name);
					model->m_KeyBone.push_back(keyBones[i]);
				}
				keyBones.clear();
				PartModelIDClass partModelID;
				partModelID.Type = typeModel;
				partModelID.ID = p_ModelManager->AddModel(model);

				m_PartModels.push_back(partModelID);
			}
		}
	}
	fileOpenInfoModel.close();
	CamputeLinkBone();
	CamputeRadiusModel();
	return true;
}



void ModelUnitClass::CamputeLinkBone(void) {
	ModelManagerClass* p_ModelManager = ModelManagerClass::GetInstance();
	ModelClass* LegsModel = nullptr;
	BoneTreeClass* rootBody = nullptr;
	BoneTreeClass* rootHead = nullptr;
	// ����������
	// ������� ������ ����� � ������� ������� �����
	for (unsigned int i = 0; i < m_PartModels.size(); i++) {
		if (m_PartModels[i].Type == TypeBody::LEGS) {
			LegsModel = p_ModelManager->GetModel(m_PartModels[i].ID);
			for (unsigned int k = 0; k < LegsModel->m_KeyBone.size(); k++) {
				if (LegsModel->m_KeyBone[k].Key == TypeBody::BODY) {
					rootBody = LegsModel->m_KeyBone[k].p_Bone;
				}
				if (LegsModel->m_KeyBone[k].Key == TypeBody::HEAD) {
					rootHead = LegsModel->m_KeyBone[k].p_Bone;
				}
			}
			break;
		}

	}
	// ����������� ��� �����
	for (unsigned int i = 0; i < m_PartModels.size(); i++) {
		if (m_PartModels[i].Type == TypeBody::BODY) {
			ModelClass* model = p_ModelManager->GetModel(m_PartModels[i].ID);
			model->m_Skeleton->p_RootBone = rootBody;
		}
		if (m_PartModels[i].Type == TypeBody::HEAD) {
			ModelClass* model = p_ModelManager->GetModel(m_PartModels[i].ID);
			model->m_Skeleton->p_RootBone = rootHead;
		}
		
	}
}

PartModelIDClass ModelUnitClass::GetPartModel(TypeBody item) {
	PartModelIDClass result;
	result.Type = TypeBody::NONE;
	result.ID = -1;
	// �������� ��� ��������� �������� � ������
	for (unsigned int i = 0; i < m_PartModels.size(); i++) {
		if (m_PartModels[i].Type == item) {
			result = m_PartModels[i];
			return result;
		}
	}
	return result;
}



void ModelUnitClass::Shutdown() {
	m_PartModels.clear();
}
