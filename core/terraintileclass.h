////////////////////////////////////////////////////////////////////////////////
// Filename: TerrainTileClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TERRAINTILECLASS_H_
#define _TERRAINTILECLASS_H_



enum class alter_argument { DEFAULT, POSSITION, MASK1, MASK2, MASK3, MASK4, MASK_NORMAL };
class SpriteDataClass{
public:
	int sizePxImageX;
	int sizePxImageY;
	int sizePxFrameX;
	int sizePxFrameY;
	int sizeLineGrid;
	int countFrameX;
	int countFrameY;
	int frameIndex;
	int frameItemX;
	int frameItemY;
};

class TerrainTileClass{
private:
	struct HeightMapType{ 
		D3DXVECTOR3 position;
		D3DXVECTOR3 normal;
		D3DXVECTOR3 tangent;
		D3DXVECTOR3 binormal;
	};
	struct TempVertexType{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};
public:
	struct VertexType{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	    D3DXVECTOR3 normal;
		D3DXVECTOR3 tangent;
		D3DXVECTOR3 binormal;
		D3DXVECTOR4 mask;
	};
	
public:
	SpriteDataClass m_Sprite;
	bool flagInitialize;
	bool flagModel;
	int m_MethodRender;
	float offsetTileLeft;
	float offsetTileTop;
	float sizeStepGrid;
	LONG countCellColumn;
	LONG countCellRow;
	int countIndexColumn;
	int countIndexRow;
	TerrainTileClass();
	TerrainTileClass(const TerrainTileClass&);
	~TerrainTileClass();
	bool GetIndexVertex(int *index, D3DXVECTOR3 point, int offsetIndexColumn, int offsetIndexRow);
	void SetMethodRender(int);
	int GetMethodRender(void);
	bool SaveTerrainMap(std::ofstream& file);
	bool LoadTerrainMap(std::ifstream& file);
	bool Initialize(ID3D10Device*);
	bool CreatePlane(float step, float offset_X, float offset_Y, int width, int height, SpriteDataClass sprite);
	void Shutdown();
	void Clear();
	void Render(ID3D10Device*);
	void RenderRound(ID3D10Device*);
	void RenderLine(ID3D10Device*);
	int GetIndexCount();
	int GetCountMaterial();
	bool GetIntersects(D3DXVECTOR3, D3DXVECTOR3, D3DXVECTOR3*);
	bool GetHeight(D3DXVECTOR3, float*, D3DXVECTOR3*);
	bool GetHeight(D3DXVECTOR3, float*);
	bool GetNormal(D3DXVECTOR3, D3DXVECTOR3*);
	void AlterPoint(unsigned int, float, alter_argument);
	// �������� ���������� ��������������� �� �����������
	void CreateRound(ID3D10Device*, D3DXVECTOR3, int radius);
	void CreateLine(ID3D10Device*, D3DXVECTOR3, D3DXVECTOR3);
	bool GetVertex(TerrainTileClass::VertexType *vertex, D3DXVECTOR3 point, int offsetIndexColumn, int offsetIndexRow);
	bool SetCamputeNodeMeanNormalTangentBinormal(TerrainTileClass::VertexType[5], unsigned int index);
	int GetMaterial(int);
	bool UpdateBuffer(ID3D10Device*);
private:
	bool InitializeBuffers(ID3D10Device*);
	void ReleaseBuffers();
	void ReleaseModel();
	void RenderBuffers(ID3D10Device*);

private:
	std::vector<int> m_MaterialIndex;
	int m_MaterialIndexCount;
	unsigned long m_VertexCount;
	unsigned long m_IndexCount;
	TerrainTileClass::VertexType* m_Model;
	ID3D10Buffer *m_VertexBuffer;
	ID3D10Buffer *m_IndexBuffer;
};

#endif