////////////////////////////////////////////////////////////////////////////////
// Filename: LabelClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "labelclass.h"
#include "stdafx.h"

LabelClass::LabelClass() {
	m_Text = NULL;
	this->Text = L"Label";
}
// ������� ��������� ������
bool LabelClass::Create(ID3D10Device* device, TextureManagerClass* ,int, int ) {
	// �����
	m_Text = new TextClass;
	RECT posSize;
	posSize.left = (LONG)this->Left;
	posSize.top = (LONG)this->Top;
	posSize.right = (LONG)(this->Left + this->Width);
	posSize.bottom = (LONG)(this->Top + this->Height);
	m_Text->Initialize(device, posSize, DT_LEFT | DT_VCENTER, this->fontSize);
	return true;
}
bool LabelClass::Render(ID3D10Device* , ShaderManagerClass* ,
						 D3DXMATRIX , D3DXMATRIX ,D3DXMATRIX) {

	// ������ ����� 
	m_Text->Render(this->Text);
	return true;
}

void  LabelClass::SetText(std::wstring inText) {
	this->Text = inText;
}

void LabelClass::Shutdown(void) {
	if (m_Text) {
		m_Text->Shutdown();
		delete m_Text;
		m_Text = 0;
	}
}