////////////////////////////////////////////////////////////////////////////////
// Filename: objclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _PLAYERCLASS_H_
#define _PLAYERCLASS_H_


//#define MINF 1.7e-300
//#define INF 1.7e+300
//////////////
// INCLUDES //
//////////////
// ��������


// �������� � ���������
#include <D3DX10math.h>
#include <string>
#include <cmath>
#include "cameraclass.h"
#include "terrainclass.h"
#include "modelmanagerclass.h"
class WeaponClass {
public:
	D3DXVECTOR3 position;
	D3DXVECTOR3 local;
	int effectID;
	float cooldown;
};
class PlayerClass {
public:
	int modelID;
	std::vector<WeaponClass> m_Weapons;
	WeaponClass GetWeapon(int index);
	void AddWeapon(WeaponClass weapon);
	void UpdateWeapon(float dTime);
	D3DXVECTOR3 position;
	D3DXVECTOR3 rotation;
	D3DXVECTOR3 scale;
	D3DXVECTOR3 markMove;
	float DEBUG;
	bool flagMove;
	float m_SpeedMove;
	float m_SpeedRotation;
	float GetAngleMarker(void);
	bool MoveTo(float deltaTime);
	bool Step(float);
	bool Rotation(float deltaTime);
	bool AddRotation(float angle);
	void CamputePitchRoll(void);
	void SetMoveTo(D3DXVECTOR3 marker);
	PlayerClass();
	void Initialize(ModelManagerClass *p_ModelMng, CameraClass* camera, TerrainClass* terrain);
	void Render(CameraClass* camera);
	void Animation();
	//~PlayerClass();
	long int TimeMoveToMark;
	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetRotation();
	D3DXVECTOR3 GetDirection(void);
private:
	ModelManagerClass *p_ModelManager;
	TerrainClass *p_Terrain;
	CameraClass *p_Camera;
};


#endif