///////////////////////////////////////////////////////////////////////////////
// Filename: textclass.cpp
///////////////////////////////////////////////////////////////////////////////
#include "textclass.h"
#include "stdafx.h"

TextClass::TextClass(){
	m_pFont = 0;
	m_screenWidth = 0;
	m_screenHeight = 0;
}

TextClass::TextClass(const TextClass&){
	m_pFont = 0;
	m_screenWidth = 0;
	m_screenHeight = 0;
}


TextClass::~TextClass(){
	
}

// ������� �������� ������, ������� ����� ������� �� �������
bool TextClass::Initialize(ID3D10Device* device, RECT posSize, UINT format, float fontSize){
	m_Format = format;
	m_PositionAndSize = posSize;
	// ������� �����
	D3DX10CreateFontW(device, 2*(INT)fontSize, (UINT)fontSize, 1, 1,
						TRUE, 0, 0, 0, DEFAULT_PITCH | FF_MODERN, L"Arial", &m_pFont); //L"Algerian"
	return true;
}


void TextClass::Shutdown(){
	if (m_pFont != 0) {
		m_pFont->Release();
		m_pFont = 0;
	}
	return;
}

// ������� �����
void TextClass::Render(std::wstring text) {
	//std::wstring textLabel = std::wstring(text.begin(), text.end());

	m_pFont->DrawTextW(NULL, text.c_str(), -1, &m_PositionAndSize,
					   m_Format, D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f));
}
