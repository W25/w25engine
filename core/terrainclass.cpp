////////////////////////////////////////////////////////////////////////////////
// Filename: TerrainClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "terrainclass.h"
#include "stdafx.h"

QuadTreeClass::QuadTreeClass() {
	northWest = 0;
	northEast = 0;
	southWest = 0;
	southEast = 0;
	m_IndexQuad = -1;
}
QuadTreeClass::~QuadTreeClass() {
	if (northWest != 0) {
		delete northWest;
		northWest = 0;
	}
	if (northEast != 0) {
		delete northEast;
		northEast = 0;
	}	
	if (southWest != 0) {
		delete southWest;
		southWest = 0;
	}	
	if (southEast != 0) {
		delete southEast;
		southEast = 0;
	}
}
// ���������� ������� �����
bool QuadTreeClass::GetIndexVisibleTile(FrustumClass* FrustumView, std::vector<int> &result) {
	
	// ���������� ����� ������� �������� � �����
	if (FrustumView->CheckCube(QuadPosition, QuadSize.x)==true) {
		// Quad ����� ����� �������� �����
		if (m_IndexQuad != -1) {
			result.push_back(m_IndexQuad);
		}
		if (northWest != 0) {
			northWest->GetIndexVisibleTile(FrustumView, result);
		}
		if (northEast != 0) {
			northEast->GetIndexVisibleTile(FrustumView, result);
		}
		if (southWest != 0) {
			southWest->GetIndexVisibleTile(FrustumView, result);
		}
		if (southEast != 0) {
			southEast->GetIndexVisibleTile(FrustumView, result);
		}
		return true;
	}
	return false;
}
int QuadTreeClass::GetIndexTilePoint(D3DXVECTOR3) {
	int result = -1;
	return result;
}

// ������� ������
void QuadTreeClass::CreatTree(	std::vector<int> indexTile, D3DXVECTOR3 pos, 
								int width, int height,
								float widthTile, float heightTile) {
	// ��������� ������� ������ �� �������� �����
	std::vector<int> indexNW; // �-�
	std::vector<int> indexNE; // �-�
	std::vector<int> indexSW; // �-�
	std::vector<int> indexSE; // �-�
	QuadPosition = pos;
	QuadSize = D3DXVECTOR3((float)width*widthTile, 0.0f, (float)height*heightTile);
	// ������������ ����� ������
	if (indexTile.size() > 1) {
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				// ���������� �����
				if (j >= (height / 2)) {
					// ���������� ������-�����				
					if (i < (width / 2)) {
						indexNW.push_back(indexTile[j*width + i]);
					// ���������� ������-������
					}else {
						indexNE.push_back(indexTile[j*width + i]);
					}
				// ���������� ��
				} else {
					// ���������� ���-�����
					if (i < (width / 2)) {
						indexSW.push_back(indexTile[j*width + i]);
					// ���������� ���-������
					}else {
						indexSE.push_back(indexTile[j*width + i]);
					}
				}
			}
		}
		// ������-�����
		if (indexNW.size() > 0) {
			northWest = new QuadTreeClass;
			D3DXVECTOR3 posNW;
			posNW = pos + D3DXVECTOR3(	-(float)width*widthTile / 4.0f, 0.0f, 
										(float)height*heightTile / 4.0f);
			northWest->CreatTree(indexNW, posNW, width / 2, height / 2, widthTile, heightTile);
		}
		// ������-������
		if (indexNE.size() > 0) {
			northEast = new QuadTreeClass;
			D3DXVECTOR3 posNE;
			posNE = pos + D3DXVECTOR3(	(float)width*widthTile / 4.0f, 0.0f, 
										(float)height*heightTile / 4.0f);
			northEast->CreatTree(indexNE, posNE, width / 2, height / 2, widthTile, heightTile);
		}
		// ���-�����
		if (indexSW.size() > 0) {
			southWest = new QuadTreeClass;
			D3DXVECTOR3 posSW;
			posSW = pos + D3DXVECTOR3(	-(float)width*widthTile / 4.0f, 0.0f, 
										-(float)height*heightTile / 4.0f);
			southWest->CreatTree(indexSW, posSW, width / 2, height / 2, widthTile, heightTile);
		}
		// ���-������
		if (indexSE.size() > 0) {
			southEast = new QuadTreeClass;
			D3DXVECTOR3 posSE;
			posSE = pos + D3DXVECTOR3(	(float)width*widthTile / 4.0f, 0.0f, 
										-(float)height*heightTile / 4.0f);
			southEast->CreatTree(indexSE, posSE, width / 2, height / 2, widthTile, heightTile);
		}
	} else {
		m_IndexQuad = indexTile[0];
	}

	
}


TerrainClass::TerrainClass() {
	m_Tile = 0;
	m_Round = nullptr;
	m_Line = nullptr;
	for (int i = 0; i < 4; i++) {
		m_Normal[i] = nullptr;
	}
	m_TileCount = 0;
	p_TextureManager = 0;
	p_ShaderManager = 0;
	p_LightManager = 0;
	m_QuadTree = 0;
	m_TerrainWidth = 0;
	m_TerrainHeight = 0;
	delayTime = 0;
	sizeStepGrid = 0;
	listEditTile.clear();
}

bool TerrainClass::Initialize(ID3D10Device* device, ShaderManagerClass* p_ShaderMng,
								TextureManagerClass* p_TextureMng,
								LightManagerClass* p_LightMng, std::string filename) {
	bool result;
	// ����������� ���������
	p_TextureManager = p_TextureMng;
	p_ShaderManager = p_ShaderMng;
	p_LightManager = p_LightMng;
	// ��������� �������� �� �����
	result = LoadTerrainMap(filename);
	// ��������� ������ ��� ������, ������� ���������
	if (result == false) {
		// ������� ���������
		result = CreateTerrainMap(device, 16, 16);
	} else {
		// ��������� �������� ��������� � �������
		for (unsigned int i = 0; i < m_Material.size(); i++) {
			p_TextureManager->Add(m_Material[i]);
		}
	}
	// ��������  ��������/�������� ��������
	if (result == true) {
		for (int i = 0; i < m_TileCount; i++) {
			m_Tile[i].Initialize(device);
		}
	}
	// ������� ��������� ������

	CreateQuadTree();
	return result;
}



// ������ ������ ������ ������� �������� ��������� ������ �� �������
int TerrainClass::GetIndexFrame(int *itemX, int *itemY, 
								const int indexLeft, const int, 
								const int, const int indexBottom){
	(*itemX) = 0;
	(*itemY) = 0;
	// ��������� ����� ��� ������
	std::vector<FrameJointClass> frameGlobal;
	std::vector<int> result;
	// ����� ������� �������
	int frameIndexLeftGreen[] = { 2, 3, 6, 7, 10, 11, 14 , 15 };
	int frameIndexLeftRed[] = { 0, 1, 4, 5, 8, 9, 12, 13 };
	// ����� �������� �������
	int frameIndexRightGreen[] = { 1, 2, 5, 6, 9, 10, 13, 14 };
	int frameIndexRightRed[] = { 0, 3, 4, 7, 8, 11, 12, 15 };
	// ����� ��������� �������
	int frameIndexTopGreen[] = { 4, 5, 6, 7, 8, 9, 10, 11 };
	int frameIndexTopRed[] = { 0, 1, 2, 3, 12, 13, 14, 15 };
	// ����� �������� �������
	int frameIndexBottomGreen[] = { 0, 1, 2, 3, 4, 5, 6, 7 };
	int frameIndexBottomRed[] = { 8, 9, 10, 11, 12, 13, 14, 15 };
	
	// ��� ����� ���� �� � ��������� ������� �� ����
	for (int j = 0; j < 16; j++) {
		FrameJointClass tile;
		for (int i = 0; i < 8; i++) {
			// ���������� ����� ������ �����
			if (frameIndexLeftGreen[i] == j) {
				tile.left = 1;
			}
			if (frameIndexLeftRed[i] == j) {
				tile.left = -1;
			}
			// ���������� ����� ������ ������
			if (frameIndexRightGreen[i] == j) {
				tile.right = 1;
			}
			if (frameIndexRightRed[i] == j) {
				tile.right = -1;
			}
			// ���������� ����� ������ ������
			if (frameIndexTopGreen[i] == j) {
				tile.top = 1;
			}
			if (frameIndexTopRed[i] == j) {
				tile.top = -1;
			}
			// ���������� ����� ������ �����
			if (frameIndexBottomGreen[i] == j) {
				tile.bottom = 1;
			}
			if (frameIndexBottomRed[i] == j) {
				tile.bottom = -1;
			}
		}
		frameGlobal.push_back(tile);
	}

	// ��������� �����
	// ������� ��� ���������� ������
	if (indexLeft >= 0) {
		for (int i = 0; i < 16; i++) {
			// ���������� ������ �� ������ ������
			if (frameGlobal[indexLeft].right == frameGlobal[i].left) {
				if (indexBottom >= 0) {
					// ���������� ������ �� ������� ������
					if (frameGlobal[indexBottom].top == frameGlobal[i].bottom) {
						result.push_back(i);
					}	
				// ����� ����� �����������
				} else {
					result.push_back(i);
				}
			}
		}
	// ����� ����� �����������
	} else {
		for (int i = 0; i < 16; i++) {
			if (indexBottom >= 0) {
				// ���������� ������ �� ������� ������
				if (frameGlobal[indexBottom].top == frameGlobal[i].bottom) {
					result.push_back(i);
				}
			// ����� ����� �����������
			} else {
				result.push_back(i);
			}
		}
	}

	if (result.size() == 0) {
		return 0;
	}
	int indexRnd = rand() % (int)(result.size());
	(*itemX) = (int)(result[indexRnd] % 4);
	(*itemY) = (int)(result[indexRnd] / 4);
	return result[indexRnd];
}
bool TerrainClass::CreateTerrainMap(ID3D10Device* device, int widthTerrain, int heightTerrain) {
	// ������� ������ ��������
	EraseTerrain();
	// ���������� ������ � ���������
	m_TerrainWidth = widthTerrain;
	m_TerrainHeight = heightTerrain;
	m_TileCount = m_TerrainWidth * m_TerrainHeight;
	sizeStepGrid = 1.000f;
	m_TileWidth = 9;
	m_TileHeight = 9;
	// �������� ������ ��� ������ ����
	m_Tile = new TerrainTileClass[m_TileCount];
	if (m_Tile == 0) {
		return false;
	}
	
	// ��������� �� ���� ������
	for (int j = 0; j < m_TerrainHeight; j++) {
		for (int i = 0; i < m_TerrainWidth; i++) {
			// ������� ������� ����
			// ������ �� ��� ������ ��� ����� (��������)
			int index = j * m_TerrainWidth + i;
			SpriteDataClass sprite;
			sprite.sizePxImageX = 465;
			sprite.sizePxImageY = 465;
			sprite.sizePxFrameX = 116;
			sprite.sizePxFrameY = 116;
			sprite.sizeLineGrid = 0;
			sprite.countFrameX = 4;
			sprite.countFrameY = 4;
			sprite.frameItemX = i % 4;
			sprite.frameItemY = j % 4;
			
			int indexFrameLeft = -1;
			int indexFrameBottom = -1;
			if (i != 0) {
				indexFrameLeft = m_Tile[index - 1].m_Sprite.frameIndex;
			}
			if (j != 0) {
				indexFrameBottom = m_Tile[index - m_TerrainWidth].m_Sprite.frameIndex;
			}
			sprite.frameIndex = GetIndexFrame(&sprite.frameItemX, &sprite.frameItemY, indexFrameLeft, -1, -1, indexFrameBottom);

			m_Tile[index].CreatePlane(sizeStepGrid, (float)i*sizeStepGrid *(float)m_TileWidth,
									  ((float)j)*sizeStepGrid *((float)m_TileHeight),
									  m_TileWidth, m_TileHeight, sprite);
		}
	}
	// ������ NTB
	for (int j = 0; j < m_TerrainHeight*(m_TileHeight-1)*sizeStepGrid; j++) {
		for (int i = 0; i < m_TerrainWidth*(m_TileWidth-1)*sizeStepGrid; i++) {
			D3DXVECTOR3 point((float)i*sizeStepGrid, 0.0f, (float)j*sizeStepGrid);
			SetCamputeMeanNTB(point);
		}
	}
	// �������� ��������
	for (int i = 0; i < m_TileCount; i++) {
		m_Tile[i].Initialize(device);
	}
	// ������� ��������� ������
	CreateQuadTree();

	// ��������� ��������
	MaterialClass material;

	material.pathDiffuse = "./Data/terrain/texture/tileStone.png";
	material.pathNormal = "./Data/terrain//texture/tileStone_normal.png";
	m_Material.push_back(material);

	material.pathDiffuse = "./Data/terrain/texture/tileTest.png";
	material.pathNormal = "./Data/terrain/texture/tileNoise_normal.bmp";
	m_Material.push_back(material);

	material.pathDiffuse = "./Data/terrain/texture/hh.bmp";
	material.pathNormal = "./Data/terrain/texture/tileNoise_normal.bmp";	
	m_Material.push_back(material);

	// ��������� �������� ��������� � �������
	for (unsigned int i = 0; i < m_Material.size(); i++) {
		p_TextureManager->Add(m_Material[i]);
	}
	return true;
}
void TerrainClass::CreateQuadTree(void) {
	m_QuadTree = new QuadTreeClass;
	std::vector<int> indexTile;
	for (int j = 0; j < m_TerrainHeight; j++) {
		for (int i = 0; i < m_TerrainWidth; i++) {
			indexTile.push_back(j * m_TerrainWidth + i);
		}
	}
	// ����� ������
	D3DXVECTOR3 pos = D3DXVECTOR3((float)(m_TerrainWidth*sizeStepGrid *m_Tile[0].countCellColumn) / 2.0f, 0,
								  (float)(m_TerrainHeight*sizeStepGrid *m_Tile[0].countCellColumn) / 2.0f);
	m_QuadTree->CreatTree(indexTile, pos,
							m_TerrainWidth, m_TerrainHeight,
							(float)m_Tile[0].countCellColumn * sizeStepGrid,
							(float)m_Tile[0].countCellRow * sizeStepGrid);
}
// ������ ������ ����� ���������
void TerrainClass::fileReadString(std::ifstream& file, std::string& text) {
	unsigned int length;
	// ������ ���������� �������� � ������
	file.read((char*)&length, sizeof(unsigned int));
	char* line = new char[length];
	// ������ ������
	file.read(line, length);
	text = line;
	// ������ ���������� ����� �� �������
	text.resize(length);
	delete[] line;
}
// ������ ������, ����� ���������
void TerrainClass::fileWriteString(std::ofstream& file, std::string text) {
	// �������� ������ ��� ������
	// ������ ���������� �������� � ������
	unsigned int count = text.length();
	// ������ ���������� ��������

	file.write((char*)&count, sizeof(unsigned int));
	// ������ ������
	file.write(text.c_str(), count);
}
// ��������� ���� ��������
bool TerrainClass::SaveTerrainMap(std::string filename){
	// ��������� �� ����� ������ � ����
	std::ofstream fileSaveTerrain;
	fileSaveTerrain.open(filename, std::ios::binary);
	// ���������, ��� ���� ������� �������
	if (fileSaveTerrain.fail()) {
		return false;
	}
	// ���������� ���������� ������� 
	int countTexture = (int)m_Material.size();
	fileSaveTerrain.write((char*)&countTexture, sizeof(int));

	std::string pathTexture;
	// ���������� ���� �� �������� (����) 
	for (int i = 0; i < countTexture; i++) {
		// �������� ��������
		fileWriteString(fileSaveTerrain, m_Material[i].pathDiffuse);
		// ���������� ��������
		fileWriteString(fileSaveTerrain, m_Material[i].pathNormal);
	}
	// ���������� ���������� ������ � ���������
	fileSaveTerrain.write((char*)&m_TerrainWidth, sizeof(LONG));
	fileSaveTerrain.write((char*)&m_TerrainHeight, sizeof(LONG));
	fileSaveTerrain.write((char*)&sizeStepGrid, sizeof(float));
	fileSaveTerrain.write((char*)&m_TileWidth, sizeof(int));
	fileSaveTerrain.write((char*)&m_TileHeight, sizeof(int));

	// ���������� ��� �����
	for (int i = 0; i < m_TileCount; i++) {
		m_Tile[i].SaveTerrainMap(fileSaveTerrain);
	}
	fileSaveTerrain.close();
	return false;
}

// ��������� �������
bool TerrainClass::LoadTerrainMap(std::string filename) {
	// ������� ������ ��������
	EraseTerrain();
	// ��������� �� ����� ���������� �����
	std::ifstream fileOpenTerrain;
	fileOpenTerrain.open(filename, std::ios::binary);
	// ���������, ��� ���� ������� �������
	if (fileOpenTerrain.fail()) {
		return false;
	}
	// ���������� ���� �� �������� ����� 
	// ���� ���� '/' � ����� ������
	for (int i = filename.length() - 1; i>0; i--) {
		// �����
		if (filename[i] == '/') {
			// �������� ������ ���� �� �������� ����� ������ �� ������ '/'
			pathMap = std::string(filename, 0, i + 1);
			break;
		}
	}
	// ��������� ���������� ������� 
	int countTexture;
	fileOpenTerrain.read((char*)&countTexture, sizeof(int));

	std::string pathTexture;
	// ��������� ���� �� �������� (����) 
	for (int i = 0; i < countTexture; i++) {
		MaterialClass material;
		// �������� ��������
		fileReadString(fileOpenTerrain, pathTexture);
		material.pathDiffuse = pathTexture;
		// ���������� ��������
		fileReadString(fileOpenTerrain, pathTexture);
		material.pathNormal = pathTexture;
		// ��������� ��������
		m_Material.push_back(material);
	}

	// ��������� ���������� ������ � ���������
	fileOpenTerrain.read((char*)&m_TerrainWidth, sizeof(LONG));
	fileOpenTerrain.read((char*)&m_TerrainHeight, sizeof(LONG));	
	fileOpenTerrain.read((char*)&sizeStepGrid, sizeof(float));
	fileOpenTerrain.read((char*)&m_TileWidth, sizeof(int));
	fileOpenTerrain.read((char*)&m_TileHeight, sizeof(int));
	m_TileCount = m_TerrainWidth * m_TerrainHeight;
	// �������� ������ ��� ������ ����
	m_Tile = new TerrainTileClass[m_TileCount];
	if (m_Tile == 0) {
		return false;
	}
	// ��������� ��� �����
	for (int i = 0; i < m_TileCount; i++) {
		// ��������� ��������
		m_Tile[i].LoadTerrainMap(fileOpenTerrain);
		// ���������� ������
		m_TileWidth = m_Tile[i].countCellColumn;
		m_TileHeight = m_Tile[i].countCellRow;
	}
	fileOpenTerrain.close();
	return true;
}

void TerrainClass::Render(	ID3D10Device* device, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
							D3DXMATRIX projectionMatrix, FrustumClass* p_Frustum) {
	std::vector<int> visibleTile;
	m_QuadTree->GetIndexVisibleTile(p_Frustum, visibleTile);
	for (unsigned int i = 0; i< visibleTile.size(); i++) {
		int index = visibleTile[i];
		m_Tile[index].Render(device);
		std::vector<MaterialClass> vecMaterial;
		for (int j = 0; j < m_Tile[index].GetCountMaterial(); j++) {
			vecMaterial.push_back(m_Material[m_Tile[index].GetMaterial(j)]);
		}
		LightClass* light = p_LightManager->GetDirectionalLight();
		p_ShaderManager->RenderTerrainShader(	m_Tile[index].GetIndexCount(),
												worldMatrix, viewMatrix, projectionMatrix,
												vecMaterial, light);
	}
	/*if (m_Round != nullptr) {
		MaterialClass material;
		material.pathDiffuse = "./data/tileTest.png";
		m_Round->Render(device);
		p_ShaderManager->RenderTextureShader(	m_Round->m_IndexCount,
													worldMatrix, viewMatrix, projectionMatrix, material);
	}*/

	if (m_Line != nullptr) {
		MaterialClass material;
		material.pathDiffuse = "./data/tileTest.png";
		m_Line->Render(device);
		p_ShaderManager->RenderTextureShader(m_Line->m_IndexCount,
			worldMatrix, viewMatrix, projectionMatrix, material);
	}

	for (int i = 0; i < 4; i++) {
		if (m_Normal[i] != nullptr) {
			MaterialClass material;
			material.pathDiffuse = "./data/tileTest.png";
			m_Normal[i]->Render(device);
			p_ShaderManager->RenderTextureShader(m_Normal[i]->m_IndexCount,
				worldMatrix, viewMatrix, projectionMatrix, material);
		}
	}

}
int TerrainClass::GetMethodRender(void) {
	for (int i = 0; i < m_TileCount; i++) {
		return m_Tile[i].GetMethodRender();
	}
	return -1;
}
void TerrainClass::SetMethodRender(int state) {
	for (int i = 0; i < m_TileCount; i++) {
		m_Tile[i].SetMethodRender(state);
	}
}

bool TerrainClass::GetIndexTile(int *index_1, int *index_2, int *index_3, int *index_4, D3DXVECTOR3 point) {
	int indexTile_X = (int)((ceil(point.x / sizeStepGrid)) / m_TileWidth);
	int indexTile_Z = (int)((ceil(point.z / sizeStepGrid)) / m_TileHeight);
	(*index_1) = -1;
	(*index_2) = -1;
	(*index_3) = -1;
	(*index_4) = -1;
	if (indexTile_X >= 0 && indexTile_X < (m_TerrainWidth) &&
		indexTile_Z >= 0 && indexTile_Z < (m_TerrainHeight)) {
		(*index_1) = (indexTile_Z * m_TerrainWidth) + indexTile_X;
		// ���������� ������ �����
		if (indexTile_X*m_TileWidth*sizeStepGrid == (int)point.x && (indexTile_X - 1) >= 0) {
			(*index_2) = (indexTile_Z * m_TerrainWidth) + indexTile_X - 1;
		}
		// ���������� ������ �����
		if ((indexTile_Z)*m_TileHeight*sizeStepGrid  == (int)point.z && (indexTile_Z - 1) >= 0) {
			(*index_3) = ((indexTile_Z - 1) * m_TerrainWidth) + indexTile_X;
		}
		// ���������� ������ �� ���������
		if ((indexTile_Z)*m_TileHeight*sizeStepGrid == (int)point.z && (indexTile_Z - 1) >= 0 &&
			(indexTile_X*m_TileWidth*sizeStepGrid == (int)point.x && (indexTile_X - 1) >= 0)) {
			(*index_4) = ((indexTile_Z - 1) * m_TerrainWidth) + indexTile_X - 1;
		}
		return true;
	}
	return false;
}

bool TerrainClass::GetIndexTile(int *index, D3DXVECTOR3 point) {
	int indexTile_X = (int)((ceil(point.x / sizeStepGrid) ) / m_TileWidth);
	int indexTile_Z = (int)((ceil(point.z / sizeStepGrid) ) / m_TileHeight);
	(*index) = 0;
	if (indexTile_X >= 0 && indexTile_X < (m_TerrainWidth) &&
		indexTile_Z >= 0 && indexTile_Z < (m_TerrainHeight)) {
		(*index) = (indexTile_Z * m_TerrainWidth) + indexTile_X;
		return true;
	}
	return false;
}

bool TerrainClass::GetHeight(D3DXVECTOR3 point, float* height) {
	// ���������� ������ ����� 
	int indexTile;
	if (GetIndexTile(&indexTile, point) == true) {
		if (m_Tile[indexTile].GetHeight(point, &(*height)) == true) {
			return true;
		}
	}
	return false;
}

bool TerrainClass::GetNormal(D3DXVECTOR3 point, D3DXVECTOR3 *normal) {
	// ���������� ������ �����
	int indexTile;
	if (GetIndexTile(&indexTile, point) == true) {
		if (m_Tile[indexTile].GetNormal(point, &(*normal)) == true) {
			return true;
		}
	}
	return false;
}
bool TerrainClass::GetHeight(D3DXVECTOR3 point, float *height, D3DXVECTOR3 *normal) {
	// ���������� ������ ����� 
	int indexTile;
	if (GetIndexTile(&indexTile, point) == true) {
		if (m_Tile[indexTile].GetHeight(point, &(*height), &(*normal)) == true) {
			return true;
		}
	}
	return false;

}
// ���������� ������� �� ����� �����
bool TerrainClass::GetVertex(TerrainTileClass::VertexType *vertex, D3DXVECTOR3 point , int offsetIndexColumn, int offsetIndexRow) {
	// ���������� ������ ����� 
	int indexTile;
	point.x += (float)(offsetIndexColumn)*sizeStepGrid;
	point.z += (float)(offsetIndexRow)*sizeStepGrid;
	if (GetIndexTile(&indexTile, point) == true) {
		if (m_Tile[indexTile].GetVertex(&(*vertex), point, 0, 0) == true) {
			return true;
		}
	}
	return false;
}
bool TerrainClass::SetCamputeMeanNTB(D3DXVECTOR3 point) {

	int indexTile1;
	int indexTile2;
	int indexTile3;
	int indexTile4;
	int indexVertex;
	// ������ ����
	//		1  
	//	4	0	2 
	//		3
	TerrainTileClass::VertexType vertex[5];
	if (GetVertex(&vertex[0], point, 0, 0) == false) {
		return false;
	}
	if (GetVertex(&vertex[1], point, 0, 1) == false) {
		return false;
	}
	if (GetVertex(&vertex[2], point, 1, 0) == false) {
		return false;
	}
	if (GetVertex(&vertex[3], point, 0, -1) == false){
		return false;
	}
	if (GetVertex(&vertex[4], point, -1, 0) == false){
		return false;
	}
	GetIndexTile(&indexTile1, &indexTile2, &indexTile3, &indexTile4, point);
	if (indexTile1 >= 0) {
		if (m_Tile[indexTile1].GetIndexVertex(&indexVertex, point, 0, 0) == true) {
			m_Tile[indexTile1].SetCamputeNodeMeanNormalTangentBinormal(vertex, indexVertex);
			listEditTile.push_back(indexTile1);
		}
	}
	if (indexTile2 >= 0) {
		if (m_Tile[indexTile2].GetIndexVertex(&indexVertex, point, 0, 0) == true) {
			m_Tile[indexTile2].SetCamputeNodeMeanNormalTangentBinormal(vertex, indexVertex);
			listEditTile.push_back(indexTile2);
		}
	}
	if (indexTile3 >= 0) {
		if (m_Tile[indexTile3].GetIndexVertex(&indexVertex, point, 0, 0) == true) {
			m_Tile[indexTile3].SetCamputeNodeMeanNormalTangentBinormal(vertex, indexVertex);
			listEditTile.push_back(indexTile3);
		}
	}
	if (indexTile4 >= 0) {
		if (m_Tile[indexTile4].GetIndexVertex(&indexVertex, point, 0, 0) == true) {
			m_Tile[indexTile4].SetCamputeNodeMeanNormalTangentBinormal(vertex, indexVertex);
			listEditTile.push_back(indexTile4);
		}
	}
	return true;
	
}

// ���������� ����� ������� ����������� �����
bool TerrainClass::AlterPoint(D3DXVECTOR3 point, float value, alter_argument argument) {

	// ���������� ������ ����� 
	int indexTile1;
	int indexTile2;
	int indexTile3;
	int indexTile4;
	// ���������� ������ �������
	int indexVertex;
	GetIndexTile(&indexTile1, &indexTile2, &indexTile3, &indexTile4, point);
	if (indexTile1 >= 0) {
		if (m_Tile[indexTile1].GetIndexVertex(&indexVertex, point, 0, 0) == true) {
			m_Tile[indexTile1].AlterPoint(indexVertex, value, argument);
			listEditTile.push_back(indexTile1);
		}
	}
	if (indexTile2 >= 0) {
		if (m_Tile[indexTile2].GetIndexVertex(&indexVertex, point, 0, 0) == true) {
			m_Tile[indexTile2].AlterPoint(indexVertex, value, argument);
			listEditTile.push_back(indexTile2);
		}
	}
	if (indexTile3 >= 0) {
		if (m_Tile[indexTile3].GetIndexVertex(&indexVertex, point, 0, 0) == true) {
			m_Tile[indexTile3].AlterPoint(indexVertex, value, argument);
			listEditTile.push_back(indexTile3);
		}
	}
	if (indexTile4 >= 0) {
		if (m_Tile[indexTile4].GetIndexVertex(&indexVertex, point, 0, 0) == true) {
			m_Tile[indexTile4].AlterPoint(indexVertex, value, argument);
			listEditTile.push_back(indexTile4);
		}
	}
	return true;
}
// �������� ������ ����� ������� � �������
void TerrainClass::AlterArea(ID3D10Device* device, D3DXVECTOR3 position, 
							 float value, float radius, alter_argument argument) {
	// ���������� ������ �����
	D3DXVECTOR3 point;
	listEditTile.clear();
	position = D3DXVECTOR3((float)(int(position.x)), (float)(int(position.y)), (float)(int(position.z)));
	// ��������� �� ��������
	for (int j = 0; j < 2 * radius / sizeStepGrid + 1; j++) {
		for (int i = 0; i < 2 * radius / sizeStepGrid + 1; i++) {
			point = position + D3DXVECTOR3(((float)i*sizeStepGrid - radius), 0.0f, ((float)j*sizeStepGrid - radius));
			// ����������
			float selectRadius = (float)sqrt(pow(j*sizeStepGrid - radius, 2) + pow(i*sizeStepGrid - radius, 2));
			if ((int)selectRadius <= radius) {
				// �������� �������
				if (selectRadius / radius > 1.0f) {
					selectRadius = radius;
				}
				float edtValue = value;
				if (selectRadius > 2 * sizeStepGrid) {
					edtValue = value * (1.0f - selectRadius / radius);
				}
				AlterPoint(point, edtValue, argument);
			}
		}
	}
	// �������� �������� ���������� ������
	for (int j = 0; j < 2 * (radius / sizeStepGrid + 1) + 1; j++) {
		for (int i = 0; i < 2 * (radius / sizeStepGrid + 1) + 1; i++) {
			point = position + D3DXVECTOR3((float)(i*sizeStepGrid - radius), 0.0f, (float)(j*sizeStepGrid - radius));
			SetCamputeMeanNTB(point);
		}
	}
	// ������� ������������� ��������
	listEditTile.unique();
	// ��������� �� ���� ����������� ������
	for (std::list<int>::iterator it = listEditTile.begin(); it != listEditTile.end(); ++it) {
		// ������� ����� ��������� ����� (��������� ����� ���� �� ��������)
		m_Tile[(*it)].UpdateBuffer(device);
	}

}

bool TerrainClass::CreateLine(ID3D10Device* device, D3DXVECTOR3 start, D3DXVECTOR3 end) {
	if (m_Line != nullptr) {
		m_Line->Shutdown();
		delete m_Line;
		m_Line = nullptr;
	}
	m_Line = new MeshLineClass;
	m_Line->Initialize(device, 600);
	m_Line->Update(start, end);
	return true;
}


bool TerrainClass::CreateNormal(int index, D3DXVECTOR3 start, D3DXVECTOR3 end) {
	ID3D10Device* p_device = D3DClass::GetInstance()->GetDevice();
	if (m_Normal[index] != nullptr) {
		m_Normal[index]->Shutdown();
		delete m_Normal[index];
		m_Normal[index] = nullptr;
	}
	m_Normal[index] = new MeshLineClass;
	m_Normal[index]->Initialize(p_device, 600);
	m_Normal[index]->Update(start, end);
	return true;
}


bool TerrainClass::CreateRound(ID3D10Device* device, D3DXVECTOR3 position, float radius) {
	
	if (m_Round != nullptr) {
		m_Round->Shutdown();
		delete m_Round;
		m_Round = 0;
	}
	m_Round = new TerrainRoundClass();
	std::vector<D3DXVECTOR3> vertex;
	// ���������� ���������� �� ��������
	vertex.resize(120);
	float width = 0.125f;
	float step = 360.0f / (float)vertex.size();
	for (unsigned int i = 0; i < vertex.size(); i += 2) {
		vertex[i].x = position.x + (float)radius*cos(step*(float)i*0.0174532925f);
		vertex[i].z = position.z + (float)radius*sin(step*(float)i*0.0174532925f);
		vertex[i].y = 0;
		// ���������� ������ � �����
		if (GetHeight(vertex[i], &vertex[i].y) == true) {
			vertex[i].y += 0.1250f;

			vertex[i + 1].x = position.x + (float)(radius + width) * cos(step * (float)i * 0.0174532925f);
			vertex[i + 1].z = position.z + (float)(radius + width) * sin(step * (float)i * 0.0174532925f);
			vertex[i + 1].y = vertex[i].y;
		}

	}
	m_Round->Initialize(device, vertex);
	return true;
}
bool TerrainClass::GetIntersects(D3DXVECTOR3 position, D3DXVECTOR3 view, D3DXVECTOR3* point) {
	// ��� �������� ������ ���������
	D3DXVECTOR3 rayStep = view*0.5f;
	D3DXVECTOR3 rayStartPosition = position;
	// ����� ��� ��������� ������
	D3DXVECTOR3 startPosition(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 endPosition(0.0f, 0.0f, 0.0f);
	float height = 0.0f;
	bool flag = true;
	int iteration = 0;
	// ���� ���� �� ������ ��� �������� ��� �� ���������� :)
	while ((position.y + view.y) > height && iteration < 1024) {
		startPosition = view;
		view = view + rayStep;
		float h;
		flag = GetHeight(position + view, &h);
		// ���������, ��� ������� ���������� ������
		if (flag == true) {
			height = h;
		}
		iteration++;
	}
	// ���� ��� ������������ � ����������
	if (flag == true) {
		startPosition = startPosition + position;
		endPosition = view + position;
		// �������� �����. ���� ������ ����� ����������� 
		for (int i = 0; i < 32; i++) {
			// ������ ��������� ������
			D3DXVECTOR3 middlePoint = (startPosition + endPosition) * 0.5f;

			if (middlePoint.y < height)
				endPosition = middlePoint;
			else
				startPosition = middlePoint;
		}

		(*point) = (startPosition + endPosition) * 0.5f;
		return true;
	}
	return false;
}
void TerrainClass::Shutdown(void){
	EraseTerrain();
}
void TerrainClass::EraseTerrain(void) {
	// ����������� ��������� �������
	if (m_Tile != 0) {
		for (int i = 0; i < m_TileCount; i++) {
			m_Tile[i].Shutdown();
		}
		delete []m_Tile;
		m_Tile = nullptr;
	}		
	if (m_QuadTree) {
		delete m_QuadTree;
		m_QuadTree = nullptr;
	}
	if (p_TextureManager != 0) {
		for (unsigned int i = 0; i < m_Material.size(); i++) {
			p_TextureManager->Erase(m_Material[i].pathDiffuse);
			p_TextureManager->Erase(m_Material[i].pathNormal);
		}
	}
	m_Material.clear();

	if(m_Line != nullptr) {
		m_Line->Shutdown();
		delete m_Line;
		m_Line = nullptr;
	}

	if (m_Round != nullptr) {
		m_Round->Shutdown();
		delete m_Round;
		m_Round = nullptr;
	}
	for (int i = 0; i < 4; i++) {
		if (m_Normal[i] != nullptr) {
			m_Normal[i]->Shutdown();
			delete m_Normal[i];
			m_Normal[i] = nullptr;
		}
	}
}
