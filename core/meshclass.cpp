////////////////////////////////////////////////////////////////////////////////
// Filename: meshclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "meshclass.h"
#include "stdafx.h"

VertexMeshClass::VertexMeshClass() {
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	texture = D3DXVECTOR2(0.0f, 0.0f);
	normal = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	tangent = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	binormal = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < 4; i++) {
		BoneID[i] = 0;
		BoneWidth[i] = 0.0f;
	}
}

MeshClass::MeshClass(){
	countOfVertex = 0;		// ���������� ������
	countOfIndex = 0;		// ���������� ��������
	radius = 1.0f;
	center = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	min = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	max = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	m_IndexBuffer = nullptr;
	m_VertexBuffer = nullptr;
	m_Skeleton = nullptr;
	m_ShaderRender = 0;
	countLink = 0;
}
MeshClass::~MeshClass(){
	m_Vertex.clear();  
	m_Index.clear();
}
void MeshClass::Shutdown() {
	m_Vertex.clear();
	m_Index.clear();
	ReleaseBuffers();
	countOfVertex = 0;
	countOfIndex = 0;
}
bool MeshClass::AddVertex(VertexMeshClass vertex) {
	m_Vertex.push_back(vertex);
	countOfVertex = m_Vertex.size();
	return true;
}

bool MeshClass::AddIndex(unsigned int index) {
	m_Index.push_back(index);
	countOfIndex = m_Index.size();
	return true;
}
// ����������� ������� � �������
void MeshClass::ConnectSkeletonToMesh(std::vector<BoneMechClass> bones) {
	
	// ��������� �� ���� ������ �������
	for (unsigned int i = 0; i < bones.size(); i++) {
		// ������ �� ��� ������ � ������� ������� �����
		for (unsigned int j = 0; j < bones[i].vertexIDs.size(); j++) {
			if (bones[i].vertexIDs[j] >= 0 && bones[i].vertexIDs[j] < countOfVertex) {
				int index = bones[i].vertexIDs[j];
				for (int k = 0; k < 4; k++) {					
					if (m_Vertex[index].BoneWidth[k] == 0.0f) {
						m_Vertex[index].BoneID[k] = bones[i].BoneID;
						m_Vertex[index].BoneWidth[k] = bones[i].Weights[k];
						break;
					}
				}
			}
		}
	}
	// ��������� ��� ������
	for (unsigned int i = 0; i < m_Vertex.size(); i++) {
		float sum = m_Vertex[i].BoneWidth[0] + m_Vertex[i].BoneWidth[1] +
					m_Vertex[i].BoneWidth[2] + m_Vertex[i].BoneWidth[3];
		if (sum != 0.0f) {
			m_Vertex[i].BoneWidth[0] = m_Vertex[i].BoneWidth[0] / sum;
			m_Vertex[i].BoneWidth[1] = m_Vertex[i].BoneWidth[1] / sum;
			m_Vertex[i].BoneWidth[2] = m_Vertex[i].BoneWidth[2] / sum;
			m_Vertex[i].BoneWidth[3] = m_Vertex[i].BoneWidth[3] / sum;
		} else {
			m_Vertex[i].BoneWidth[0] = 0.0f;
			m_Vertex[i].BoneWidth[1] = 0.0f;
			m_Vertex[i].BoneWidth[2] = 0.0f;
			m_Vertex[i].BoneWidth[3] = 0.0f;
		}
	}
}

SkeletonClass* MeshClass::GetSkeleton(void) {
	return m_Skeleton;
}

void MeshClass::SetSkeleton(SkeletonClass* skeleton) {
	m_Skeleton = skeleton;
}

int MeshClass::GetCountVertex() {
	return countOfVertex;
}

int MeshClass::GetCountIndex() {
	return countOfIndex;
}

bool MeshClass::InitializeBuffers(ID3D10Device* device) {
	unsigned long* indices;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	// ���������� ������ �����
	ReleaseBuffers();

	// ������� ������ ��������
	indices = new unsigned long[countOfIndex];
	if (indices == 0) {
		return false;
	}
	for (int i = 0; i < countOfIndex; i++) {
		indices[i] = m_Index[i];
	}
	// Set up the description of the index buffer.
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * countOfIndex;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if (FAILED(result)) {
		return false;
	}
	delete[] indices;
	indices = 0;

	VertexMeshClass* vertex = new VertexMeshClass[countOfVertex];
	if (vertex == 0) {
		return false;
	}
	for (int i = 0; i < countOfVertex; i++) {
		vertex[i] = m_Vertex[i];
	}
	// Set up the description of the vertex buffer.
	vertexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexMeshClass) * countOfVertex;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	// ��������� CPU ������ � �����
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertex;

	// Now finally create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if (FAILED(result)) {
		return false;
	}
	delete[] vertex;
	vertex = 0;
	return true;
}

void MeshClass::RenderBuffers(ID3D10Device* device, int methodRender) {
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexMeshClass);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	device->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	// ������ ������ ������
	switch (methodRender) {
		// �����������
	case 0:
		device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		break;
		// ����� �����
	case 1:
		device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINESTRIP);
		break;
		// ����� ����
	case 2:
		device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);
		break;
		//
	case 3:
		device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
		break;
		// �����
	case 4:
		device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
		break;
	}

	return;
}

void MeshClass::ReleaseBuffers() {
	// ����������� ��������� �����.
	if (m_IndexBuffer != nullptr) {
		m_IndexBuffer->Release();
		m_IndexBuffer = 0;
	}
	// ����������� ��������� �����
	if (m_VertexBuffer != nullptr) {
		m_VertexBuffer->Release();
		m_VertexBuffer = 0;
	}
	return;
}