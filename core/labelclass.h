////////////////////////////////////////////////////////////////////////////////
// Filename: labelclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _LABELCLASS_H_
#define _LABELCLASS_H_

class LabelClass: public WidgetClass {
public:
	LabelClass();
	bool Create(ID3D10Device*, TextureManagerClass*, int, int);
	bool Render(ID3D10Device*, ShaderManagerClass*,
		D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	void Shutdown(void);
	void SetText(std::wstring);
private:
	TextClass* m_Text;
};

#endif