////////////////////////////////////////////////////////////////////////////////
// Filename: timerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TIMERCLASS_H_
#define _TIMERCLASS_H_

////////////////////////////////////////////////////////////////////////////////
// Class name: TimerClass
////////////////////////////////////////////////////////////////////////////////
class TimerClass{
// ������� �������� ������
public:
	static TimerClass* GetInstance() {
		static TimerClass m_Instance;
		return &m_Instance;
	}
public:
	bool Initialize();
	bool Frame();
	signed __int64 GetTiks();
	void Shutdown();
	float GetFrameTime();
private:
	signed __int64 m_frequency;
	signed __int64 m_ticksPerMs;
	signed __int64 m_startTime;
	float m_frameTime;
private:
	// ������������ � �������� ������������ ���������� ��������
	TimerClass();
	~TimerClass(){};
	TimerClass(const TimerClass&);
	TimerClass& operator=(TimerClass&) {};
};

#endif
