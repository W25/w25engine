////////////////////////////////////////////////////////////////////////////////
// Filename: inputclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _INPUTCLASS_H_
#define _INPUTCLASS_H_

////////////////////////////////////////////////////////////////////////////////
// Class name: InputClass
////////////////////////////////////////////////////////////////////////////////
class InputClass{
// ������� �������� ������
public:
	static  InputClass* GetInstance() {
		static  InputClass m_Instance;
		return &m_Instance;
	}
public:
	bool Initialize(HINSTANCE, HWND, int, int);
	void Shutdown();
	bool Frame();

	bool IsKeyDown(int key);
	bool IsKeyPress(int key);
	bool IsKeyUp(int key);

	void GetMouseLocation(int&, int&);
	void GetMouseMove(int&, int&);
	void GetMouseZ(int&);
	void GetMouseMoveZ(int&);
	bool IsMouseDown(int key);
	bool IsMousePress(int key);
	bool IsMouseUp(int key);
private:
	bool ReadKeyboard();
	bool ReadMouse();
	void ProcessInput();

private:
	IDirectInput8* m_directInput;
	IDirectInputDevice8* m_keyboard;
	IDirectInputDevice8* m_mouse;
	unsigned char m_keyboardState[256];
	int m_KeyState[256];

	DIMOUSESTATE m_mouseState;
	int m_KeyMouseState[256];

	int m_screenWidth, m_screenHeight;
	int m_mouseX, m_mouseY, m_mouseZ;
	int m_dmouseX, m_dmouseY, m_dmouseZ;
private:
	// ������������ � �������� ������������ ���������� ��������
	InputClass();
	~InputClass() {};
	InputClass(const InputClass&) {};
	InputClass& operator=(InputClass&) {};

};

#endif