////////////////////////////////////////////////////////////////////////////////
// Filename: frustumclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _FRUSTUMCLASS_H_
#define _FRUSTUMCLASS_H_


class FrustumClass{
public:
	FrustumClass();
	FrustumClass(const FrustumClass&);
	~FrustumClass();
	void ConstructFrustum(float screenDepth, D3DXMATRIX projectionMatrix, D3DXMATRIX viewMatrix);
	bool CheckPoint(float, float, float);
	bool CheckCube(D3DXVECTOR3, float);
	bool CheckSphere(float, float, float, float);
	bool CheckSphere(D3DXVECTOR3, float);
	bool CheckRectangle(float, float, float, float, float, float);

private:
	D3DXPLANE m_planes[6];
};

#endif