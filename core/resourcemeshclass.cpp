////////////////////////////////////////////////////////////////////////////////
// Filename: resourcemeshclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "resourcemeshclass.h"
#include "stdafx.h"

ResourceMeshClass::ResourceMeshClass() {
	m_Skeleton = nullptr;
	m_Mesh = nullptr;
	countLink = 0;
	scale = 0.0f;
}
ResourceMeshClass::~ResourceMeshClass() {

}

std::vector<D3DXMATRIX> ResourceMeshClass::GetBoneMatrix(void) {
	return m_Skeleton->GetMatrixBones();
}
bool ResourceMeshClass::RenderBuffers(ID3D10Device* device, int pickRender) {
	m_Mesh->RenderBuffers(device, pickRender);
	return true;
}
void ResourceMeshClass::Shutdown() {
	// ������� �����
	if (m_Mesh != nullptr) {
		m_Mesh->Shutdown();
		delete m_Mesh;
		m_Mesh = nullptr;
	}

	// ������� ��� ������ ������
	if (m_Skeleton != nullptr) {
		m_Skeleton->Shutdown();
		// �������� �������������� �����
		delete m_Skeleton;
		m_Skeleton = nullptr;
	}

	countLink = 0;
}