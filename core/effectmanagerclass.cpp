////////////////////////////////////////////////////////////////////////////////
// Filename: effectmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "effectmanagerclass.h"
#include "stdafx.h"

EffectManagerClass::EffectManagerClass() {

	incrementID = 0;
}

bool EffectManagerClass::Initialize(void) {

	incrementID = 0;
	return true;
}

void EffectManagerClass::SetDirectionToID(int id, D3DXVECTOR3 dir) {
	std::map<int, EffectClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_Effect.find(id);
	if (iter != m_Effect.end()) {
		iter->second.SetDirection(dir);
	}
}

void EffectManagerClass::SetPositionToID(int id, D3DXVECTOR3 pos) {
	std::map<int, EffectClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_Effect.find(id);
	if (iter != m_Effect.end()) {
		iter->second.SetPosition(pos);
	}
}

void EffectManagerClass::SetEmitting(int id, bool flag) {
	std::map<int, EffectClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_Effect.find(id);
	if (iter != m_Effect.end()) {
		iter->second.SetEmitting(flag);
	}
}

bool  EffectManagerClass::GetPosition(int id, D3DXVECTOR3& resultPos) {
	std::map<int, EffectClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_Effect.find(id);
	if (iter != m_Effect.end()) {
		resultPos = iter->second.GetPosition();
		return true;
	}
	return false;
}

bool  EffectManagerClass::GetDirection(int id, D3DXVECTOR3& resultDir) {
	std::map<int, EffectClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_Effect.find(id);
	if (iter != m_Effect.end()) {
		resultDir = iter->second.GetDirection();
		return true;
	}
	return false;

}

void EffectManagerClass::Update(CameraClass *p_Camera, float dTime) {
	std::map<int, EffectClass>::iterator iter;
	for (iter = m_Effect.begin(); iter != m_Effect.end(); ++iter) {
		if (iter->second.GetFlagDeath() == false) {
			iter->second.Update(p_Camera, dTime);
		}
	}
}

void EffectManagerClass::Refresh(int ID) {
	std::map<int, EffectClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_Effect.find(ID);
	// ����� ����
	if (iter != m_Effect.end()) {
		iter->second.Refresh();
	}
}

void EffectManagerClass::Render(CameraClass *p_Camera) {
	
	D3DXMATRIX  worldMatrix, viewMatrix, projectionMatrix;
	p_Camera->GetWorldMatrix(&worldMatrix);
	p_Camera->GetViewMatrix(&viewMatrix);
	p_Camera->GetProjectionMatrix(&projectionMatrix);
	std::map<int, EffectClass>::iterator iter;
	for (iter = m_Effect.begin(); iter != m_Effect.end(); ++iter) {
		iter->second.Render(p_Camera);
	}
}
int EffectManagerClass::AddEffect(EffectClass effect) {
	incrementID++;
	effect.Initialize();
	m_Effect.insert(std::pair<int, EffectClass>(incrementID, effect));
	return incrementID;
}
void EffectManagerClass::Shutdown(void) {
	std::map<int, EffectClass>::iterator iter;
	for (iter = m_Effect.begin(); iter != m_Effect.end(); ++iter) {
		iter->second.Shutdown();
	}
	m_Effect.clear();
}