////////////////////////////////////////////////////////////////////////////////
// Filename: guimanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _GUICLASS_H_
#define _GUICLASS_H_

class WidgetClass;
class CursorClass;

class GuiManagerClass{
public:
	GuiManagerClass(void);
	void Add(ID3D10Device* device, WidgetClass*);
	void Shutdown(void);
	bool Update(InputClass*, int, int);
	void Render(ID3D10Device*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	bool SetText(std::string NameWidget, std::string text);
	bool SetText(std::string NamePanel, std::string NameWidget, std::string text);
	bool SetVisible(std::string, bool);
	unsigned int GetCount(void);
	bool Initialize(ID3D10Device*, ShaderManagerClass*, TextureManagerClass*, int, int);
private:
	ShaderManagerClass* p_ShaderManager;
	TextureManagerClass* p_TextureManager;
	int screenWidth;
	int screenHeight;
private:
	std::vector<WidgetClass*> m_Widget;
	CursorClass* m_Cursor;
};

#endif
