////////////////////////////////////////////////////////////////////////////////
// Filename: d3dclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _D3DCLASS_H_
#define _D3DCLASS_H_

struct ID3D10Device;
struct ID3D10DepthStencilView;
struct ID3D10Texture2D;
struct ID3D10DepthStencilState;
struct ID3D10RenderTargetView;
struct ID3D10RasterizerState;
struct ID3D10BlendState;
struct IDXGISwapChain;

////////////////////////////////////////////////////////////////////////////////
// Class name: D3DClass
////////////////////////////////////////////////////////////////////////////////
class D3DClass{
public:
	static  D3DClass* GetInstance() {
		static  D3DClass m_Instance;
		return &m_Instance;
	}
public:
	bool Initialize(int, int, bool, HWND, bool);
	void Shutdown();
	
	void BeginScene(float, float, float, float);
	void EndScene();

	ID3D10Device* GetDevice();
	ID3D10DepthStencilView* GetDepthStencilView();
	void SetupPixelFog(DWORD Color, DWORD Mode);
	void SetBackBufferRenderTarget();

	HWND GetHWND();
	void GetVideoCardInfo(char*, int&);

	void TurnZBufferOn();
	void TurnZBufferOff();
	void EnableWriteZBuffer();
	void DisableWriteZBuffer();
	void GetScreenSize(int&, int&);
	void EnableAlphaBlending();
	void DisableAlphaBlending();
	ID3D10Texture2D* m_depthStencilBuffer;
private:
	bool InitializeStencilStateDisabledZBuffer();
	bool InitializeStencilStateDisabledWriteZBuffer();
	HWND p_hwnd;
	int m_screenWidth, m_screenHeight;
	bool m_vsync_enabled;
	int m_videoCardMemory;
	char m_videoCardDescription[128];
	IDXGISwapChain* m_swapChain;
	ID3D10Device* m_device;
	ID3D10RenderTargetView* m_renderTargetView;
	ID3D10DepthStencilState* m_depthStencilState;
	ID3D10DepthStencilState* m_depthDisabledZBufferStencilState;
	ID3D10DepthStencilState* m_depthDisabledWriteZBufferStencilState;
	ID3D10DepthStencilView* m_depthStencilView;
	ID3D10RasterizerState* m_rasterState;

	ID3D10BlendState* m_alphaEnableBlendingState;
	ID3D10BlendState* m_alphaDisableBlendingState;
private:
	// Конструкторы и оператор присваивания недоступны клиентам
	D3DClass();
	~D3DClass() {};
	D3DClass(const D3DClass&) {};
	D3DClass& operator=(D3DClass&) {};

};

#endif