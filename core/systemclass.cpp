////////////////////////////////////////////////////////////////////////////////
// Filename: systemclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "systemclass.h"
#include "stdafx.h"

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

SystemClass::SystemClass(){
	timeDelayUpdateFrame = 0;
	timeDelayRender = 0;
	m_hWnd = nullptr;
	m_D3D = nullptr;
	m_Timer = nullptr;
	m_Input = nullptr;
	m_TextureManager = nullptr;
	m_MeshManager = nullptr;
	m_ModelManager = nullptr;
	m_LightManager = nullptr;
	m_ShaderManager = nullptr;
	m_MsgManager = nullptr;
	m_StateManager = nullptr;

	WinCursorX = 0;
	WinCursorY = 0;
	prevScreenWidth = 0;
	prevScreenHeight = 0;
	m_applicationName = 0;
	
	m_hInstance = 0;
}

bool SystemClass::Initialize(HINSTANCE hInstance, int screenWidth, int screenHeight,
							 bool isFullScreen, bool isVSYNC){
	bool result;
	// The instance of this application.
	m_hInstance = hInstance;
	m_ScreenWidth = screenWidth;
	m_ScreenHeight = screenHeight;
	m_IsFullScreen = isFullScreen;
	m_IsVSYNC = isVSYNC;
	// �������������� ���� API.
	InitializeWindows(screenWidth, screenHeight);

	//========================================================
	// ������� ������ �������.
	//========================================================
	m_Timer = TimerClass::GetInstance();
	result = m_Timer->Initialize();
	if(result == false){
		MessageBox(m_hWnd, "Could not initialize the timer object.", "Error", MB_OK);
		return false;
	}
	//=====================================================
	// ������� ������ �����.
	//=====================================================
	m_Input = InputClass::GetInstance();
	result = m_Input->Initialize(hInstance, m_hWnd, screenWidth, screenHeight);
	if(result == false){
		MessageBox(m_hWnd, "�� ������� ���������������� ������ �����.", "Error", MB_OK);
		return false;
	}

	//=====================================================
	// ������� ����������� ������.
	//=====================================================
	m_D3D = D3DClass::GetInstance();
	result = m_D3D->Initialize(screenWidth, screenHeight, isVSYNC, m_hWnd, isFullScreen);
	if(result == false){
		MessageBox(m_hWnd, "�� ������� ���������������� Direct3D.", "Error", MB_OK);
		return false;
	}

	//=====================================================
	// ������� �������� �������
	//=====================================================
	m_TextureManager = TextureManagerClass::GetInstance();
	result = m_TextureManager->Initialize(m_D3D->GetDevice());
	if(result == false){
		MessageBox(m_hWnd, "Could not initialize the texture manager object.", "Error", MB_OK);
		return false;
	}
	
	//=====================================================
	// ������� �������� �����
	//=====================================================
	m_MeshManager = MeshManagerClass::GetInstance();
	if(!m_MeshManager){
		MessageBox(m_hWnd, "Could not initialize the model manager object.", "Error", MB_OK);
		return false;
	}
	m_MeshManager->Initialize(m_hWnd, m_D3D->GetDevice(), m_TextureManager);
	
	//=====================================================
	// ������� �������� �����
	//=====================================================
	m_LightManager = LightManagerClass::GetInstance();
	if (!m_LightManager) {
		MessageBox(m_hWnd, "Could not initialize the light manager object.", "Error", MB_OK);
		return false;
	}
	
	//=====================================================
	// �������������� ������ �������� �������.
	//=====================================================
	m_ShaderManager = ShaderManagerClass::GetInstance();
	result = m_ShaderManager->Initialize(m_D3D->GetDevice(), m_hWnd, m_TextureManager, m_MeshManager, m_LightManager);
	if (result == false) {
		MessageBox(m_hWnd, "Could not initialize the shader manager object.", "Error", MB_OK);
		return false;
	}

	//=====================================================
	// �������������� ������ �������� �������.
	//=====================================================
	m_ModelManager = ModelManagerClass::GetInstance();
	result = m_ModelManager->Initialize(m_D3D->GetDevice(), m_MeshManager, m_ShaderManager, m_LightManager);
	if (result == false) {
		MessageBox(m_hWnd, "Could not initialize the model manager object.", "Error", MB_OK);
		return false;
	}

	//=====================================================
	// �������������� ������ �������� ��������.
	//=====================================================
	m_EffectManager = EffectManagerClass::GetInstance();
	result = m_EffectManager->Initialize();
	if (result == false) {
		MessageBox(m_hWnd, "Could not initialize the effect manager object.", "Error", MB_OK);
		return false;
	}

	//=====================================================
	// ������� �������� ���������
	//=====================================================
	m_MsgManager = MsgManagerClass::GetInstance();
	
	//=====================================================
	// ������� �������� �������� ���������
	//=====================================================
	m_StateManager = StateManagerClass::GetInstance();
	m_StateManager->Initialize(&m_hWnd, m_D3D, m_MsgManager, m_MeshManager, m_ModelManager,
								m_TextureManager, m_LightManager, m_ShaderManager);
	if (result == false) {
		MessageBox(m_hWnd, "Could not initialize the first state.", "Error", MB_OK);
		return false;
	}
	// �������� ��������� � ����
	//result = m_StateManager->PushState(CamputeState::GetInstance());
	result = m_StateManager->PushState(AboutState::GetInstance());
	if (result == false){
		MessageBox(m_hWnd, "Could not initialize the first state.", "Error", MB_OK);
		return false;
	}
	////////////////////////////////////////////////////////////////////////////////
	return true;
}


void SystemClass::Shutdown(){
	// ����������� ������ ��������� ���������
	m_MsgManager->Shutdown();
	// ����������� ������ ��������� ���������
	m_StateManager->Shutdown();
	// ����������� ������ �������� ��������
	m_ShaderManager->Shutdown();
	// ����������� ������ ��������� �����
	m_LightManager->Shutdown();
	// ����������� ������ ��������� �����
	m_MeshManager->Shutdown();
	// ����������� ������ ��������� �������
	m_ModelManager->Shutdown();
	// ����������� ������ ��������� �������
	m_TextureManager->Shutdown();
	// ����������� ����������� ������
	m_D3D->Shutdown();
	// ����������� ������ �������
	m_Timer->Shutdown();
	// ����������� ������ �����
	m_Input->Shutdown();
	// ����������� ����
	ShutdownWindows();
	return;
}

// ���� ���������
// ����������� �������� �������, ��������� 100 ��� � ���� �� �����������
// ��������� ����� �������� � ��������� �����
MSG SystemClass::Run(){
	MSG msg;
	bool isExit = false;
	bool result = false;
	// ������������� ��������� ���������
	ZeroMemory(&msg, sizeof(MSG));
	// ��������� ���� �� ����� ��������� ���������� ������ ���������
	while (isExit == false) {
		// Handle the windows messages.
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		// ������ ������ ������ �� ����������, ��������� ����������
		if (msg.message == WM_QUIT) {
			isExit = true;
		} else {
			// ��������� ����������
			result = Frame();
			// ��� ������ ��������� ����������
			if (result == false) {
				isExit = true;
			}
		}
		// ������ ������ ESC ����� �� ����������
		if (m_Input->IsKeyPress(DIK_ESCAPE) == true) {
			isExit = true;
		}
		if (m_Input->IsKeyPress(DIK_SYSRQ) == true) {
			PrintScreen(m_hWnd);
		}

	}
	return msg;
}


// ��������� ����������
bool SystemClass::Frame(){
	bool result;
	// ��������� �������
	result = m_Timer->Frame();
	if (!result){
		return false;
	}
	/*
	// ��������
	PackMsg sendMsg;
	if (m_Input->IsKeyPress(DIK_F1) == true) {
		sendMsg.aimStateId = TypeState::DEBUG;
		if (m_StateManager->CheckStateName(L"DEBUG") == false) {
			sendMsg.type = TypeMSG::ADD;
		}
		else {
			sendMsg.type = TypeMSG::ERASE;
		}
		m_MsgManager->AddMsg(sendMsg);
	}
	if (m_Input->IsKeyPress(DIK_F2) == true) {
		sendMsg.aimStateId = TypeState::MENU;
		if (m_StateManager->CheckStateName(L"MENU") == false) {
			sendMsg.type = TypeMSG::ADD;
		}
		else {
			sendMsg.type = TypeMSG::ERASE;
		}
		m_MsgManager->AddMsg(sendMsg);
	}
	if (m_Input->IsKeyPress(DIK_F3) == true) {
		sendMsg.aimStateId = TypeState::PLAY;
		if (m_StateManager->CheckStateName(L"PLAY") == false) {
			sendMsg.type = TypeMSG::ADD;
		}
		else {
			sendMsg.type = TypeMSG::ERASE;
		}
		m_MsgManager->AddMsg(sendMsg);
	}*/
	/////////////////////////////////
	//////////////////////////


	// ��������� ��������� ����� (�������������� ����� ������)

	// ��������� �������
	timeDelayUpdateFrame += m_Timer->GetFrameTime();
	// ���������� ���������
	if (timeDelayUpdateFrame > 1.0f/120.0f) {
		result = m_Input->Frame();
		if (result == false) {
			return false;
		}
		result = m_StateManager->Frame(m_Input, timeDelayUpdateFrame);
		timeDelayUpdateFrame = 0;
		if (!result) {
			return false;
		}
	}
	// ��������� �������� ���������
	// 120 ����� � �������
	timeDelayRender += m_Timer->GetFrameTime();
	if (timeDelayRender> 1.0f / 120.0f){
		result = m_StateManager->Render();
		timeDelayRender = 0;
		if(!result){
			return false;
		}
	}
	////
	// ������������ ������� �� ������������ ���������
	/*PackMsg msgState;
	
	msgState = m_MsgManager->GetBackMsg();
	switch (msgState.type){
		//////////////////////////////////////////////
		case TypeMSG::NONE:
			break;
		//////////////////////////////////////////////
		case TypeMSG::ADD:
			// ����� �������� � ���� �������, ����� ������ �� ������, �������� ��� ������ ����
			switch(msgState.aimStateId){
				case TypeState::NONE:
					break;
				case TypeState::ABOUT:
					result = m_StateManager->PushState(AboutState::GetInstance());
					if (!result) {return false;}
					break;
				case TypeState::DEBUG:
					result = m_StateManager->PushState(DebugState::GetInstance());
					if (!result) {return false;}
					break;
				case TypeState::MENU:
					result = m_StateManager->PushState(MenuState::GetInstance());
					if (!result) {return false;}
					break;
				case TypeState::PLAY:
					result = m_StateManager->PushState(PlayState::GetInstance());
					if (!result) {return false;}
					break;
			}
			break;
		//////////////////////////////////////////////
		case TypeMSG::ERASE:
			// �� ����� ������� �������
			switch (msgState.aimStateId){
				case TypeState::NONE:
					result = m_StateManager->EraseState(L"NONE");
					if (!result) {return false;}
					break;
				case TypeState::ABOUT:
					result = m_StateManager->EraseState(L"ABOUT");
					if (!result) {return false;}
					break;
				case TypeState::DEBUG:
					result = m_StateManager->EraseState(L"DEBUG");
					if (!result) {return false;}
					break;
				case TypeState::MENU:
					result = m_StateManager->EraseState(L"MENU");
					if (!result) {return false;}
					break;
				case TypeState::PLAY:
					result = m_StateManager->EraseState(L"PLAY");
					if (!result) {return false;}
					break;
			}
			break;
		//////////////////////////////////////////////
		case TypeMSG::PAUSE:
			break;
		//////////////////////////////////////////////
		case TypeMSG::RESUME:
			break;
		case TypeMSG::EXIT:
			return false;
			break;
	}*/
	
	return true;
}


// ������������ ���� ����������
void SystemClass::InitializeWindows(int& screenWidth, int& screenHeight){
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;

	// Give the application a name.
	m_applicationName = "W25";

	// ��������� ���� Windows
	wc.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc   = WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = m_hInstance;
	wc.hIcon		 = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wc.hIconSm       = wc.hIcon;
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = m_applicationName;
	wc.cbSize        = sizeof(WNDCLASSEX);
	
	// ������������ ���� ���������� 
	RegisterClassEx(&wc);

	// Determine the resolution of the clients desktop screen.
	screenWidth  = GetSystemMetrics(SM_CXSCREEN); // 1536
	screenHeight = GetSystemMetrics(SM_CYSCREEN); // 864

	prevScreenWidth = screenWidth;
	prevScreenHeight = screenHeight;

	if(m_IsFullScreen == true){
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize       = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth  = (unsigned long)screenWidth;
		dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
		dmScreenSettings.dmBitsPerPel = 32;			
		dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = 0;
		posY = 0;
	}else{
		// Place the window in the middle of the screen.
		posX = 0;
		posY = 0;
	}

	// Create the window with the screen settings and get the handle to it.
	m_hWnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName, 
						    WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
						    posX, posY, screenWidth, screenHeight, NULL, NULL, m_hInstance, NULL);

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_hWnd, SW_SHOW);
	SetForegroundWindow(m_hWnd);
	SetFocus(m_hWnd);

	// �������� ������ �����
	ShowCursor(false);
	return;
}
void SystemClass::PrintScreen(HWND hwnd) {
	// ��������� ����� ������
	if (OpenClipboard(hwnd)) {
		// ������� �����
		EmptyClipboard();
	}
	int nScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int nScreenHeight = GetSystemMetrics(SM_CYSCREEN);
	//int nScreenWidth = 1920;
	//int nScreenHeight = 1080;
	HWND hDesktopWnd = GetDesktopWindow();
	HDC hDesktopDC = GetDC(hDesktopWnd);
	HDC hCaptureDC = CreateCompatibleDC(hDesktopDC);
	HBITMAP hCaptureBitmap = CreateCompatibleBitmap(hDesktopDC,
		nScreenWidth, nScreenHeight);
	SelectObject(hCaptureDC, hCaptureBitmap);
	BitBlt(hCaptureDC, 0, 0, nScreenWidth, nScreenHeight,
		hDesktopDC, 0, 0, SRCCOPY | CAPTUREBLT);
	// ������ � ����� ������
	SetClipboardData(CF_BITMAP, hCaptureBitmap);
	ReleaseDC(hDesktopWnd, hDesktopDC);
	DeleteDC(hCaptureDC);
	DeleteObject(hCaptureBitmap);
	// ��������� ����� ������
	CloseClipboard();
}

void SystemClass::SaveCapturedBitmap(char* szFileName, HBITMAP hBitmap) {
	/*HDC    hdc = NULL;
	FILE * fp = NULL;
	LPVOID pBuf = NULL;
	BITMAPINFO bmpInfo;
	BITMAPFILEHEADER bmpFileHeader;

	hdc = GetDC(NULL);
	ZeroMemory(&bmpInfo, sizeof(BITMAPINFO));
	bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	GetDIBits(hdc, hBitmap, 0, 0, NULL, &bmpInfo, DIB_RGB_COLORS);
	if (bmpInfo.bmiHeader.biSizeImage <= 0) {
		bmpInfo.bmiHeader.biSizeImage = bmpInfo.bmiHeader.biWidth *
			abs(bmpInfo.bmiHeader.biHeight) *
			(bmpInfo.bmiHeader.biBitCount + 7) / 8;
	}

	if ((pBuf = malloc(bmpInfo.bmiHeader.biSizeImage)) == NULL) {

		MessageBox(NULL, "Unable to Allocate Bitmap Memory", "Error", MB_OK | MB_ICONERROR);
		return;
	}

	bmpInfo.bmiHeader.biCompression = BI_RGB;
	//GetDIBits(hdc, hBitmap, 0, bmpInfo.bmiHeader.biHeight, pBuf, DIB_RGB_COLORS);

	GetDIBits(hdc, hBitmap, 0, bmpInfo.bmiHeader.biHeight, pBuf, &bmpInfo, DIB_RGB_COLORS);

	if (fopen_s(&fp, szFileName, "wb")) {
		MessageBox(NULL, "Unable to Create Bitmap File", "Error", MB_OK | MB_ICONERROR);
		return;
	}
	if (fp == 0) {
		MessageBox(NULL, "Unable to Create Bitmap File", "Error", MB_OK | MB_ICONERROR);
		return;
	}

	bmpFileHeader.bfReserved1 = bmpFileHeader.bfReserved2 = 0;
	bmpFileHeader.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + bmpInfo.bmiHeader.biSizeImage;
	bmpFileHeader.bfType = 'MB';
	bmpFileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	fwrite(&bmpFileHeader, sizeof(BITMAPFILEHEADER), 1, fp);
	fwrite(&bmpInfo.bmiHeader, sizeof(BITMAPINFOHEADER), 1, fp);
	fwrite(pBuf, bmpInfo.bmiHeader.biSizeImage, 1, fp);

	if (hdc != NULL) ReleaseDC(NULL, hdc);
	if (pBuf != NULL) free(pBuf);
	if (fp != NULL) fclose(fp);*/
}


void SystemClass::ShutdownWindows(){
	// Show the mouse cursor.
	ShowCursor(true);
	// Fix the display settings if leaving full screen mode.
	if(m_IsFullScreen == true){
		ChangeDisplaySettings(NULL, 0);
	}

	// Remove the window.
	DestroyWindow(m_hWnd);
	m_hWnd = NULL;

	// Remove the application instance.
	UnregisterClass(m_applicationName, m_hInstance);
	m_hInstance = NULL;

	return;
}


//
//  �������: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ����������:  ������������ ��������� � ������� ����.
//
//  WM_COMMAND � ���������� ���� ����������
//  WM_PAINT � ���������� ������� ����
//  WM_DESTROY � ��������� ��������� � ������ � ���������
//
//
extern HINSTANCE hInst;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {

	switch (message) {
		case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);
			// ��������� ����� � ����:
			switch (wmId) {
			case IDM_ABOUT:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
				break;
			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;
			default:	
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
		break;
		case WM_CREATE:
			break;
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);
			// TODO: �������� ���� ����� ��� ����������, ������������ HDC...
			EndPaint(hWnd, &ps);
		}
		break;
		case WM_DESTROY:
		{
			PostQuitMessage(0);
		}
		break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// ���������� ��������� ��� ���� "� ���������".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);
	switch (message) {
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) {
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
