#ifndef _STATECLASS_PLAY_H_
#define _STATECLASS_PLAY_H_

#include "stateclass.h"
////////////////////////////////////////////////////////////////////////////////
// ��������� ����
////////////////////////////////////////////////////////////////////////////////
class StateClass;
class PlayState;

class PlayState: public StateClass { 
public:
	//���������� ��������� �� ������ ��� ���������� ��� � �������� ��������� 
	static PlayState* GetInstance() { return &m_PlayState; }
	// �����������
	PlayState(WCHAR*);
	// ��������� � ���������� ���������
	bool Render(); 
	// ������ � ��������
	bool RenderModelInTexture(int ID);
	bool RenderInTexture();
	bool Update( InputClass*, float);
	// �������� ��� ����� � �����(�������� ������)
	bool InitializeState(); 
	// �������� �� ������ �� ������(�������� �������� ��������)
	bool Exit(); 
	// ������������ ���� ��������
	bool Shutdown();
private:	
	bool RenderPrimary(); 
	bool UpdatePrimary(InputClass*, float);
	static PlayState m_PlayState;
	TerrainClass *m_Terrain;
	FrustumClass *m_Frustum;
	MeshLineClass *m_MeshLine;
	ModelManagerClass *m_ModelManager;
	EffectManagerClass *m_EffectManager;
	ShotManagerClass *m_ShotManager;
	PlayerClass *m_Player;
	PlayerClass *m_MasPlayer;
};

#endif