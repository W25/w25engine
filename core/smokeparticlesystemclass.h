////////////////////////////////////////////////////////////////////////////////
// Filename: particlesystemclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _SMOKEPARTICLESYSTEMCLASS_H_
#define _SMOKEPARTICLESYSTEMCLASS_H_



///////////////////////////////////////////////////////////////////////////////
// Class name: ParticleSystemClass
///////////////////////////////////////////////////////////////////////////////


class SmokeParticleSystemClass : public BaseParticleSystemClass {

public:
	SmokeParticleSystemClass();
	void SetPosition(D3DXVECTOR3 pos);
	void SetDirection(D3DXVECTOR3 dirertion);
	D3DXVECTOR3 GetPosition(void);
	D3DXVECTOR3 GetDirection(void);
protected:
	bool InitializeParticleSystem();
	void EmitParticles(CameraClass *p_Camera,float dTime);
	void UpdateParticles(CameraClass *p_Camera,float dTime);

};

#endif