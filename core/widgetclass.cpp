////////////////////////////////////////////////////////////////////////////////
// Filename: widgetclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "widgetclass.h"
#include "stdafx.h"

WidgetClass::WidgetClass() {
	this->Left = 0;
	this->Top = 0;
	this->Width = 400;
	this->Height = 25;
	fontSize = 8;
	pxTextureWidth = 0;
	pxTextureHeight = 0;
	pxTileOffsetX = 0;
	pxTileOffsetY = 0;
	pxTileWidth = 0;
	pxTileHeight = 0;
	this->Visible = true;
}
WidgetClass::~WidgetClass() {

}
// ���� ��������� �� GUI
bool WidgetClass::GetReside(int MouseX, int MouseY) {
	if (this->Left <= MouseX && (this->Left + this->Width) >= MouseX &&
		this->Top <= MouseY && (this->Top + this->Height) >= MouseY) {
		return true;
	}
	return false;
}
bool WidgetClass::Create(ID3D10Device* , TextureManagerClass*,
						 int, int ) {
	return true;
}
void WidgetClass::Shutdown(void) {

}

bool WidgetClass::Update(InputClass*, int mouseX, int mouseY) {
	return (this->GetReside(mouseX, mouseY));
}
bool WidgetClass::Render(ID3D10Device*, ShaderManagerClass*,
						 D3DXMATRIX, D3DXMATRIX,D3DXMATRIX) {
	return true;
}
std::wstring WidgetClass::GetText(void) {
	return Text;
}
bool  WidgetClass::SetVisible(bool flag) {
	Visible = flag;
	return true;
}
void WidgetClass::SetText(std::wstring inText) {
	Text = inText;
}
bool WidgetClass::SetText(std::string name, std::wstring inText) {
	return true;
}
BoxClass::BoxClass(void){
	m_VertexBuffer = 0;
	m_IndexBuffer = 0;
	m_VertexCount = 0;
	m_IndexCount = 0;
	m_PreviousPosX = 0.0f;
	m_PreviousPosY = 0.0f;
}
BoxClass::~BoxClass() {
	this->ShutdownBuffers();
}
bool BoxClass::InitializeBuffers(ID3D10Device* device) {
	VertexMeshClass* vertices;
	unsigned long* indices;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;

	// Set the number of vertices in the vertex array.
	m_VertexCount = 6;

	// Set the number of indices in the index array.
	m_IndexCount = m_VertexCount;

	// Create the vertex array.
	vertices = new VertexMeshClass[m_VertexCount];
	if (!vertices) {
		return false;
	}
	// �������������� ������� �� ������ 0 0
	// Calculate the screen coordinates of the left side of the bitmap.
	float left = Left - ((float)m_screenWidth / 2.0f);
	// Calculate the screen coordinates of the right side of the bitmap.
	float right = left + Width;
	// Calculate the screen coordinates of the top of the bitmap.
	float top = ((float)m_screenHeight / 2.0f) - Top;
	// Calculate the screen coordinates of the bottom of the bitmap.
	float bottom = top - Height;
	// ���������� �������� � ������������� ���������
	D3DXVECTOR2 posTextureLeftTop(0.0f, 0.0f);
	D3DXVECTOR2 posTextureLeftBottom(0.0f, 1.0f);
	D3DXVECTOR2 posTextureRightTop(1.0f, 0.0f);
	D3DXVECTOR2 posTextureRightBottom(1.0f, 1.0f);

	if (pxTextureWidth != 0) {
		posTextureLeftTop.x = ((float)pxTileOffsetX / (float)pxTextureWidth);
		posTextureLeftBottom.x = posTextureLeftTop.x;
		posTextureRightTop.x = posTextureLeftTop.x + ((float)(pxTileWidth + 1) / (float)pxTextureWidth);
		posTextureRightBottom.x = posTextureRightTop.x;
	}
	if (pxTextureHeight != 0) {
		posTextureLeftTop.y = ((float)pxTileOffsetY / (float)pxTextureHeight);
		posTextureRightTop.y = posTextureLeftTop.y;

		posTextureLeftBottom.y = posTextureLeftTop.y + ((float)(pxTileHeight + 1) / (float)pxTextureHeight);
		posTextureRightBottom.y = posTextureLeftBottom.y;
	}
	// Load the vertex array with data.
	// First triangle.
	vertices[0].position = D3DXVECTOR3(left, top, 0.0f);  // Left Top
	vertices[0].texture = posTextureLeftTop;

	vertices[1].position = D3DXVECTOR3(right, bottom, 0.0f);  // Right Bottom
	vertices[1].texture = posTextureRightBottom;

	vertices[2].position = D3DXVECTOR3(left, bottom, 0.0f);  // Left Bottom 
	vertices[2].texture = posTextureLeftBottom;

	// Second triangle.
	vertices[3].position = D3DXVECTOR3(left, top, 0.0f);  // Left Top
	vertices[3].texture = posTextureLeftTop;

	vertices[4].position = D3DXVECTOR3(right, top, 0.0f);  // Right Top
	vertices[4].texture = posTextureRightTop;

	vertices[5].position = D3DXVECTOR3(right, bottom, 0.0f);  // Right Bottom
	vertices[5].texture = posTextureRightBottom;
	for (unsigned int i = 0; i < 6; i++) {
		m_Mesh.push_back(vertices[i]);
	}

	// Set up the description of the dynamic vertex buffer.
	vertexBufferDesc.Usage = D3D10_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(VertexMeshClass) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;
	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;



	// Now finally create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if (FAILED(result)) {
		return false;
	}

	// Create the index array.
	indices = new unsigned long[m_IndexCount];
	if (!indices) {
		return false;
	}
	// Load the index array with data.
	for (int i = 0; i<m_IndexCount; i++) {
		indices[i] = i;
	}
	// Set up the description of the index buffer.
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if (FAILED(result)) {
		return false;
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;

	return true;
}


void BoxClass::ShutdownBuffers() {
	// Release the index buffer.
	if (m_IndexBuffer) {
		m_IndexBuffer->Release();
		m_IndexBuffer = 0;
	}
	m_IndexCount = 0;
	// Release the vertex buffer.
	if (m_VertexBuffer) {
		m_VertexBuffer->Release();
		m_VertexBuffer = 0;
	}
	m_VertexCount = 0;
	return;
}




void BoxClass::RenderBuffers(ID3D10Device* device) {
	unsigned int stride;
	unsigned int offset;


	// Set vertex buffer stride and offset.
	stride = sizeof(VertexMeshClass);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	device->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	//device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINESTRIP);
	return;
}

