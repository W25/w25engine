////////////////////////////////////////////////////////////////////////////////
// Filename: d3dclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "d3dclass.h"
#include "stdafx.h"


D3DClass::D3DClass(){
	m_device = 0;
	m_swapChain = 0;
	m_renderTargetView = 0;
	m_depthStencilBuffer = 0;
	m_depthStencilState = 0;
	m_depthDisabledZBufferStencilState = 0;
	m_depthDisabledWriteZBufferStencilState = 0;
	m_depthStencilView = 0;
	m_videoCardMemory = 0;
	m_rasterState = 0;
	m_alphaEnableBlendingState = 0;
	m_alphaDisableBlendingState = 0;
	m_screenWidth = 0;
	m_screenHeight = 0;
	p_hwnd = 0;
}


bool D3DClass::Initialize(int screenWidth, int screenHeight, bool vsync, HWND hwnd, bool fullscreen){
	HRESULT result;
	IDXGIFactory* factory;
	IDXGIAdapter* adapter;
	IDXGIOutput* adapterOutput;
	unsigned int numModes = 0;
	unsigned int numerator = 0;
	unsigned int denominator = 0;
	size_t stringLength = 0;
	DXGI_MODE_DESC* displayModeList;
	DXGI_ADAPTER_DESC adapterDesc;
	int error;
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ID3D10Texture2D* backBufferPtr;
	D3D10_TEXTURE2D_DESC depthBufferDesc;
	D3D10_DEPTH_STENCIL_DESC depthStencilDesc;
	D3D10_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	
	D3D10_RASTERIZER_DESC rasterDesc;
	// ���������� ���������� ������
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	// Store the vsync setting.
	m_vsync_enabled = vsync;
	p_hwnd = hwnd;
	// Create a DirectX graphics interface factory.
	result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
	if(FAILED(result)){
		return false;
	}

	// Use the factory to create an adapter for the primary graphics interface (video card).
	result = factory->EnumAdapters(0, &adapter);
	if(FAILED(result)){
		return false;
	}

	// Enumerate the primary adapter output (monitor).
	result = adapter->EnumOutputs(0, &adapterOutput);
	if(FAILED(result)){
		return false;
	}

	// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
	result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, 
												&numModes, NULL);
	if(FAILED(result)){
		return false;
	}
	if (numModes == 0) {
		return false;
	}
	// Create a list to hold all the possible display modes for this monitor/video card combination.
	displayModeList = new DXGI_MODE_DESC[numModes];
	unsigned int countModes = numModes;
	if(!displayModeList){
		return false;
	}

	// Now fill the display mode list structures.
	result = adapterOutput->GetDisplayModeList(	DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, 
												&numModes, displayModeList);
	if(FAILED(result)){
		return false;
	}

	// ������ �������� ��� ������ ����������� � ������� ��, ������� ������������� ������ � ������ ������.
	// ����� ������� ����������, ������� ��������� � ����������� ������� ���������� ��� ����� ��������.
	for(unsigned int i = 0; i < countModes; ++i){
		if(displayModeList[i].Width == (unsigned int)screenWidth){
			if(displayModeList[i].Height == (unsigned int)screenHeight){
				numerator = displayModeList[i].RefreshRate.Numerator;
				denominator = displayModeList[i].RefreshRate.Denominator;
			}
		}
	}

	// ���������� �������� ����������.
	result = adapter->GetDesc(&adapterDesc);
	if(FAILED(result)){
		return false;
	}

	// ���������� ���������� ������ ���������� � ����������.
	m_videoCardMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

	// ������������ ��� ���������� � ������ �������� � ��������� ���.

	error = wcstombs_s(&stringLength, m_videoCardDescription, 128, adapterDesc.Description, (size_t)128);

	if(error != 0){
		return false;
	}
	// Release the display mode list.
	delete []displayModeList;
	displayModeList = 0;
	
	// Release the adapter output.
	adapterOutput->Release();
	adapterOutput = 0;

	// Release the adapter.
	adapter->Release();
	adapter = 0;

	// Release the factory.
	factory->Release();
	factory = 0;

	// Initialize the swap chain description.
    ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

	// Set to a single back buffer.
    swapChainDesc.BufferCount = 1;

	// Set the width and height of the back buffer.
    swapChainDesc.BufferDesc.Width = screenWidth;
    swapChainDesc.BufferDesc.Height = screenHeight;

	// Set regular 32-bit surface for the back buffer.
    swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	// Set the refresh rate of the back buffer.
	if(m_vsync_enabled){
	    swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
	}else{
	    swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	}

	// Set the usage of the back buffer.
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

	// Set the handle for the window to render to.
    swapChainDesc.OutputWindow = hwnd;
	// Turn multisampling off.
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.SampleDesc.Quality = 0;

	// Set to full screen or windowed mode.
	if(fullscreen){
		swapChainDesc.Windowed = false;
	}else{
		swapChainDesc.Windowed = true;
	}

	// Set the scan line ordering and scaling to unspecified.
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	// Discard the back buffer contents after presenting.
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	// Don't set the advanced flags.
	swapChainDesc.Flags = 0;

	// Create the swap chain and the Direct3D device.
	result = D3D10CreateDeviceAndSwapChain(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0, D3D10_SDK_VERSION, 
										   &swapChainDesc, &m_swapChain, &m_device);
	if(FAILED(result)){
		return false;
	}

	// Get the pointer to the back buffer.
	result = m_swapChain->GetBuffer(0, __uuidof(ID3D10Texture2D), (LPVOID*)&backBufferPtr);
	if(FAILED(result)){
		return false;
	}

	// Create the render target view with the back buffer pointer.
	result = m_device->CreateRenderTargetView(backBufferPtr, NULL, &m_renderTargetView);
	if(FAILED(result))
	{
		return false;
	}

	// Release pointer to the back buffer as we no longer need it.
	backBufferPtr->Release();
	backBufferPtr = 0;

	// Initialize the description of the depth buffer.
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

	// Set up the description of the depth buffer.
	depthBufferDesc.Width = screenWidth;
	depthBufferDesc.Height = screenHeight;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	depthBufferDesc.BindFlags =  D3D10_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	// Create the texture for the depth buffer using the filled out description.
	result = m_device->CreateTexture2D(&depthBufferDesc, NULL, &m_depthStencilBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Initialize the description of the stencil state.
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	// Set up the description of the stencil state.
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D10_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D10_COMPARISON_LESS;

	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing.
	depthStencilDesc.FrontFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D10_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D10_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing.
	depthStencilDesc.BackFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D10_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D10_COMPARISON_ALWAYS;

	// Create the depth stencil state.
	result = m_device->CreateDepthStencilState(&depthStencilDesc, &m_depthStencilState);
	if(FAILED(result)){
		return false;
	}

	// Set the depth stencil state on the D3D device.
	m_device->OMSetDepthStencilState(m_depthStencilState, 1);

	// Initialize the depth stencil view.
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

	// Set up the depth stencil view description.
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D10_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	// Create the depth stencil view.
	result = m_device->CreateDepthStencilView(m_depthStencilBuffer, &depthStencilViewDesc, &m_depthStencilView);
	if(FAILED(result)){
		return false;
	}
	//m_device->SetR
	// Bind the render target view and depth stencil buffer to the output render pipeline.
	m_device->OMSetRenderTargets(1, &m_renderTargetView, m_depthStencilView);

	// Setup the raster description which will determine how and what polygons will be drawn.
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D10_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D10_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state from the description we just filled out.
	result = m_device->CreateRasterizerState(&rasterDesc, &m_rasterState);
	if(FAILED(result)){
		return false;
	}

	// Now set the rasterizer state.
	m_device->RSSetState(m_rasterState);

	D3D10_VIEWPORT viewport;
	// Setup the viewport for rendering.
    viewport.Width = screenWidth;
    viewport.Height = screenHeight;
    viewport.MinDepth = 0.0f;
    viewport.MaxDepth = 1.00f;
    viewport.TopLeftX = 0;
    viewport.TopLeftY = 0;

	// Create the viewport.
    m_device->RSSetViewports(1, &viewport);
	
	// �������������� ��������� ��� �������� Z ������
	InitializeStencilStateDisabledZBuffer();
	// �������������� ��������� ��� ������ � Z �����
	InitializeStencilStateDisabledWriteZBuffer();

	D3D10_BLEND_DESC blendStateDescription;
	///////////////////////////////
	// Clear the blend state description.
	ZeroMemory(&blendStateDescription, sizeof(D3D10_BLEND_DESC));

	// Create an alpha enabled blend state description.
	blendStateDescription.AlphaToCoverageEnable = FALSE;
	blendStateDescription.RenderTargetWriteMask[0] = 0x0f;
	blendStateDescription.BlendEnable[0] = TRUE;
	blendStateDescription.BlendOp = D3D10_BLEND_OP_ADD;
	blendStateDescription.BlendOpAlpha = D3D10_BLEND_OP_ADD;
	blendStateDescription.SrcBlend = D3D10_BLEND_ONE;
	blendStateDescription.DestBlend = D3D10_BLEND_ONE;
	blendStateDescription.SrcBlendAlpha = D3D10_BLEND_ONE;
	blendStateDescription.DestBlendAlpha = D3D10_BLEND_ZERO;

	// Create the blend state using the description.
	result = m_device->CreateBlendState(&blendStateDescription, &m_alphaEnableBlendingState);
	if (FAILED(result)){
		return false;
	}

	// Modify the description to create an alpha disabled blend state description.
	blendStateDescription.BlendEnable[0] = FALSE;

	// Create the blend state using the description.
	result = m_device->CreateBlendState(&blendStateDescription, &m_alphaDisableBlendingState);
	if (FAILED(result)){
		return false;
	}

    return true;
}
// ��������� � ���������� Z �������
bool D3DClass::InitializeStencilStateDisabledZBuffer() {
	HRESULT result;
	D3D10_DEPTH_STENCIL_DESC depthDisabledZBufferStencilDesc;
	// Clear the second depth stencil state before setting the parameters.
	ZeroMemory(&depthDisabledZBufferStencilDesc, sizeof(depthDisabledZBufferStencilDesc));

	// Now create a second depth stencil state which turns off the Z buffer for 2D rendering.  The only difference is 
	// that DepthEnable is set to false, all other parameters are the same as the other depth stencil state.
	depthDisabledZBufferStencilDesc.DepthEnable = false;
	depthDisabledZBufferStencilDesc.DepthWriteMask = D3D10_DEPTH_WRITE_MASK_ALL;
	depthDisabledZBufferStencilDesc.DepthFunc = D3D10_COMPARISON_LESS;
	depthDisabledZBufferStencilDesc.StencilEnable = true;
	depthDisabledZBufferStencilDesc.StencilReadMask = 0xFF;
	depthDisabledZBufferStencilDesc.StencilWriteMask = 0xFF;
	depthDisabledZBufferStencilDesc.FrontFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	depthDisabledZBufferStencilDesc.FrontFace.StencilDepthFailOp = D3D10_STENCIL_OP_INCR;
	depthDisabledZBufferStencilDesc.FrontFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	depthDisabledZBufferStencilDesc.FrontFace.StencilFunc = D3D10_COMPARISON_ALWAYS;
	depthDisabledZBufferStencilDesc.BackFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	depthDisabledZBufferStencilDesc.BackFace.StencilDepthFailOp = D3D10_STENCIL_OP_DECR;
	depthDisabledZBufferStencilDesc.BackFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	depthDisabledZBufferStencilDesc.BackFace.StencilFunc = D3D10_COMPARISON_ALWAYS;

	// Create the state using the device.
	result = m_device->CreateDepthStencilState(&depthDisabledZBufferStencilDesc,
											   &m_depthDisabledZBufferStencilState);
	if (FAILED(result)) {
		return false;
	}
	return true;
}
// ��������� � ���������� ������� � Z �����
bool D3DClass::InitializeStencilStateDisabledWriteZBuffer() {
	HRESULT result;
	D3D10_DEPTH_STENCIL_DESC depthDisabledWriteZBufferStencilDesc;
	// Initialize the description of the stencil state.
	ZeroMemory(&depthDisabledWriteZBufferStencilDesc, sizeof(depthDisabledWriteZBufferStencilDesc));

	// Set up the description of the stencil state.
	depthDisabledWriteZBufferStencilDesc.DepthEnable = true;
	// ������ � Z �����
	depthDisabledWriteZBufferStencilDesc.DepthWriteMask = D3D10_DEPTH_WRITE_MASK_ZERO;
	depthDisabledWriteZBufferStencilDesc.DepthFunc = D3D10_COMPARISON_LESS;

	depthDisabledWriteZBufferStencilDesc.StencilEnable = true;
	depthDisabledWriteZBufferStencilDesc.StencilReadMask = 0xFF;
	depthDisabledWriteZBufferStencilDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing.
	depthDisabledWriteZBufferStencilDesc.FrontFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	depthDisabledWriteZBufferStencilDesc.FrontFace.StencilDepthFailOp = D3D10_STENCIL_OP_INCR;
	depthDisabledWriteZBufferStencilDesc.FrontFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	depthDisabledWriteZBufferStencilDesc.FrontFace.StencilFunc = D3D10_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing.
	depthDisabledWriteZBufferStencilDesc.BackFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	depthDisabledWriteZBufferStencilDesc.BackFace.StencilDepthFailOp = D3D10_STENCIL_OP_DECR;
	depthDisabledWriteZBufferStencilDesc.BackFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	depthDisabledWriteZBufferStencilDesc.BackFace.StencilFunc = D3D10_COMPARISON_ALWAYS;

	// Create the depth stencil state.
	result = m_device->CreateDepthStencilState(&depthDisabledWriteZBufferStencilDesc,
											   &m_depthDisabledWriteZBufferStencilState);
	if (FAILED(result)) {
		return false;
	}
	//========================================================
	return true;
}
// 
void D3DClass::GetScreenSize(int& screenWidth, int& screenHeight){
	screenWidth = m_screenWidth;
	screenHeight = m_screenHeight;
}
HWND D3DClass::GetHWND() {
	return p_hwnd;
}

void D3DClass::EnableWriteZBuffer() {
	m_device->OMSetDepthStencilState(m_depthStencilState, 1);
}

void D3DClass::DisableWriteZBuffer() {
	m_device->OMSetDepthStencilState(m_depthDisabledWriteZBufferStencilState, 1);

}
void D3DClass::EnableAlphaBlending(){
	float blendFactor[4];
	// Setup the blend factor.
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;
	// Turn on the alpha blending.
	m_device->OMSetBlendState(m_alphaEnableBlendingState, blendFactor, 0xffffffff);
	return;
}

void D3DClass::DisableAlphaBlending(){
	float blendFactor[4];
	// Setup the blend factor.
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;
	// Turn off the alpha blending.
	m_device->OMSetBlendState(m_alphaDisableBlendingState, blendFactor, 0xffffffff);
	return;
}

void D3DClass::Shutdown(){
	// Before shutting down set to windowed mode or when you release the swap chain it will throw an exception.
	if(m_swapChain){
		m_swapChain->SetFullscreenState(false, NULL);
	}

	if(m_rasterState){
		m_rasterState->Release();
		m_rasterState = 0;
	}

	if(m_depthStencilView){
		m_depthStencilView->Release();
		m_depthStencilView = 0;
	}

	if(m_depthStencilState){
		m_depthStencilState->Release();
		m_depthStencilState = 0;
	}

	if(m_depthStencilBuffer){
		m_depthStencilBuffer->Release();
		m_depthStencilBuffer = 0;
	}

	if(m_renderTargetView){
		m_renderTargetView->Release();
		m_renderTargetView = 0;
	}

	if(m_swapChain){
		m_swapChain->Release();
		m_swapChain = 0;
	}

	if(m_device){
		m_device->Release();
		m_device = 0;
	}

	if(m_depthDisabledZBufferStencilState){
		m_depthDisabledZBufferStencilState->Release();
		m_depthDisabledZBufferStencilState = 0;
	}

	if (m_depthDisabledWriteZBufferStencilState) {
		m_depthDisabledWriteZBufferStencilState->Release();
		m_depthDisabledWriteZBufferStencilState = 0;
	}

	if (m_alphaEnableBlendingState){
		m_alphaEnableBlendingState->Release();
		m_alphaEnableBlendingState = 0;
	}

	if (m_alphaDisableBlendingState){
		m_alphaDisableBlendingState->Release();
		m_alphaDisableBlendingState = 0;
	}

	return;
}


void D3DClass::BeginScene(float red, float green, float blue, float alpha){
	float color[4];
	// Setup the color to clear the buffer to.
	color[0] = red;
	color[1] = green;
	color[2] = blue;
	color[3] = alpha;
	// Clear the back buffer.
	m_device->ClearRenderTargetView(m_renderTargetView, color);
	// Clear the depth buffer.
	m_device->ClearDepthStencilView(m_depthStencilView, D3D10_CLEAR_DEPTH, 1.0f, 0);
	return;
}

void D3DClass::SetupPixelFog(DWORD Color, DWORD Mode) {
	//  ��� ��������� ������
	float Start = 3.0f;	// ���������� �� ������  �� ������� 
						// ������ ��������� �����
	float End = 7.0f;	// ���������� �� ������  �� ������� 
						// ����� ����� ��������� �������� �������

	float Density = 0.26f;	// ��������� (������  ��� exp'�������) ������

	// �������� ������������� ������

}

void D3DClass::EndScene(){
	// ������� ������ ����� �� �����, ��������� ��������� ��������.
	if(m_vsync_enabled == true){
		// Lock to screen refresh rate.
		m_swapChain->Present(1, 0);
	}else{
		// Present as fast as possible.
		m_swapChain->Present(0, 0);
	}
	return;
}


ID3D10Device* D3DClass::GetDevice(){
	return m_device;
}

ID3D10DepthStencilView* D3DClass::GetDepthStencilView(){
	return m_depthStencilView;
}

void D3DClass::SetBackBufferRenderTarget(){
	// Bind the render target view and depth stencil buffer to the output render pipeline.
	m_device->OMSetRenderTargets(1, &m_renderTargetView, m_depthStencilView);
	return;
}

void D3DClass::GetVideoCardInfo(char* cardName, int& memory){
	strcpy_s(cardName, 128, m_videoCardDescription);
	memory = m_videoCardMemory;
	return;
}

void D3DClass::TurnZBufferOn(){
	m_device->OMSetDepthStencilState(m_depthStencilState, 1);
	return;
}


void D3DClass::TurnZBufferOff(){
	m_device->OMSetDepthStencilState(m_depthDisabledZBufferStencilState, 1);
	return;
}