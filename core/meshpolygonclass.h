#ifndef _MESHPOLYGONCLASS_H_
#define _MESHPOLYGONCLASS_H_

class VertexMeshClass;

class MeshPolygonClass{
public:
	MeshPolygonClass();
	virtual ~MeshPolygonClass();
	void Shutdown();
	void Initialize(ID3D10Device* device, std::vector<D3DXVECTOR3>);
	void Render(ID3D10Device*);
	LONG m_VertexCount;
	LONG m_IndexCount;
	bool flagCreate;
	void RenderBuffers(ID3D10Device*);
	virtual bool InitializeBuffers(ID3D10Device*, std::vector<VertexMeshClass>);
	void ReleaseBuffers();
	ID3D10Buffer *m_VertexBuffer;
	ID3D10Buffer *m_IndexBuffer;
};

class MeshLineClass : public MeshPolygonClass {
public:
	MeshLineClass();
	void Initialize(ID3D10Device* device, int countVertex);
	bool InitializeBuffers(ID3D10Device*, std::vector<VertexMeshClass>);
	void Update(D3DXVECTOR3 start, D3DXVECTOR3 end);
	void UpdateBuffers();
	std::vector<VertexMeshClass> m_Vertex;
};

class MeshFlatClass : public MeshPolygonClass {
public :
	MeshFlatClass();
	void Initialize(ID3D10Device* device);
	void Update(D3DXVECTOR3 pos, D3DXVECTOR3 normal, float width, float height);
	void UpdateBuffers();
	std::vector<VertexMeshClass> m_Vertex;
};

class MeshBoxClass : public MeshPolygonClass {
public:
	D3DXMATRIX m_GlobalMatrix;
	D3DXMATRIX m_LocalMatrix;
	std::string Name;
	MeshBoxClass();
	bool InitializeBuffers(ID3D10Device*, D3DXMATRIX local, D3DXMATRIX global, float height);
private:
	bool CreateBox(float width, std::vector<VertexMeshClass>& vertex, std::vector<int>& indices);

};

class TerrainRoundClass : public MeshPolygonClass {
public:
	TerrainRoundClass();
private:
	bool InitializeBuffers(ID3D10Device*, std::vector<VertexMeshClass>);
};

#endif

