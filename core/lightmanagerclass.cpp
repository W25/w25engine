////////////////////////////////////////////////////////////////////////////////
// Filename: lightmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "lightmanagerclass.h"
#include "stdafx.h"

LightManagerClass::LightManagerClass() {
	m_DirectionalLight = nullptr;
}


void LightManagerClass::Shutdown(void) {
	for (unsigned int i = 0; i < m_Light.size(); i++) {
		m_Light[i]->Shutdown();
		delete m_Light[i];
		m_Light[i] = nullptr;
	}
	m_Light.clear();
	if (m_DirectionalLight != nullptr) {
		m_DirectionalLight->Shutdown();
		delete m_DirectionalLight;
		m_DirectionalLight = nullptr;
	}

}
LightClass* LightManagerClass::GetLight(void) {
	return m_DirectionalLight;
}

LightClass* LightManagerClass::GetLight(int ID) {

	for (unsigned int i = 0; i<m_Light.size(); i++) {
		if (m_Light[i]->ID == ID) {
			return m_Light[i];
		}
	}
	return nullptr;
}
void LightManagerClass::SetDirectionalLight(LightClass* light) {
	m_DirectionalLight = light;
}
LightClass* LightManagerClass::GetDirectionalLight(void) {
	return m_DirectionalLight;
}

void LightManagerClass::AddLight(LightClass *light) {
	light->ID = m_Light.size();
	m_Light.push_back(light);

}