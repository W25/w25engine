////////////////////////////////////////////////////////////////////////////////
// Filename: mechpartclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _BONECLASS_H_
#define _BONECLASS_H_

#include <vector3.h>
#include <matrix4x4.h>

struct aiNode;
class BoneMechClass;
struct D3DXMATRIX;

enum class TypeAnimation { NONE, MOVE_UP, MOVE_DOWN, TURN_L, TURN_R, WAIT, IDLE, TYPE_0, TYPE_1, STRAFE_L, STRAFE_R, DEATH };

struct QuatKeyStruct {
	D3DXQUATERNION value;
	double time;
};

struct Vec3KeyStruct {
	D3DXVECTOR3 value;
	double time;
};

class AnimationChannelClass {
public:
	std::string Name;
	std::vector<Vec3KeyStruct> m_PositionKeys;
	std::vector<QuatKeyStruct> m_RotationKeys;
	std::vector<Vec3KeyStruct> m_ScalingKeys;
};

class AnimationClass {
public:
	AnimationClass();
	void Shutdown();
	void CamputeInterpolatedRotation(D3DXQUATERNION* outQuaternion, double AnimationTime, int indexChannel);
	void CamputeInterpolatedScaling(D3DXVECTOR3* outScaling, double AnimationTime, int indexChannel);
	void CamputeInterpolatedPosition(D3DXVECTOR3* outScaling, double AnimationTime, int indexChannel);
	unsigned int FindIndexRotation(double AnimationTime, int indexNodeAnim);
	unsigned int FindIndexPosition(double AnimationTime, int indexNodeAnim);
	unsigned int FindIndexScaling(double AnimationTime, int indexNodeAnim);
	TypeAnimation Type;
	unsigned int reloadIndexFrame;
	unsigned int lastIndexFrame;
	unsigned int firstIndexFrame;
	unsigned int indexFrame;
	double maxFrame;
	double widthFrame;
	double pauseFrame;
	std::vector<AnimationChannelClass> m_AnimationChannel;
};

class BoneMechClass {
public:
	BoneMechClass();
	std::string BoneName;
	D3DXMATRIX BoneOffset;
	int BoneID;
	BoneMechClass* m_ParentBone;
	std::vector<BoneMechClass*> m_ChildBone;
	std::vector<int> vertexIDs;
	std::vector<float> Weights;
};

class BoneTreeClass {
public:
	BoneTreeClass();
	int BoneID;
	std::string BoneName;
	D3DXMATRIX LocalTransform;
	D3DXMATRIX Offset;
	D3DXMATRIX GlobalTransform;
	D3DXMATRIX OriginalLocalTransform;
	bool flagDoneAnimation;
	BoneTreeClass* m_ParentBone;
	std::vector<BoneTreeClass*> m_ChildBone;
	bool GetMatrixBones(std::map<int, D3DXMATRIX> &outBoneMatrix);
	std::vector<D3DXMATRIX> GetMatrixBones();
	void GetSkeleton(std::map<std::string, BoneTreeClass*> &vecBones);
	void Shutdown();
	bool CamputeGlobalMatrix(void);
	bool FindBoneByName(std::string name, BoneTreeClass*& outBone);
	void LoadTreeBone(aiNode* p_Node, BoneTreeClass* parent);
	void CopyTreeBone(BoneTreeClass* source, BoneTreeClass* parent);
	void SetBonesID(std::vector<BoneMechClass> bones);
	D3DXMATRIX ConvertMatrix(const aiMatrix4x4* matrix);
	D3DXVECTOR3 ConvertVector3(const aiVector3D* vec);	
};


class VertexBoneDataClass {
public:
	std::vector<int> IDs;
	std::vector<float> Weights;
};

// ��������� ������ ������ �����
// �������� ����� �������
// �������� �� ��������
class SkeletonClass {
public:
	SkeletonClass();
	BoneTreeClass *m_BoneTree;
	BoneTreeClass *p_RootBone;
	std::map<std::string, BoneTreeClass*> m_Bones;
	std::map<TypeAnimation, AnimationClass> m_Animation;
	// ���������� ������ ��������
	unsigned int indexFrame;
	double fractionFrame;
	//
	signed __int64 dubugCountTiks;

	BoneTreeClass* GetBone(string name);
	void CopySkeleton(SkeletonClass* skeleton);
	void CreateSkeleton(BoneTreeClass* boneTree);
	void UpdateAnimation(TypeAnimation animation, double dTime);
	void RefreshAnimation(TypeAnimation animation);
	void CamputeAnimation(AnimationClass *animation, double frame);
	std::vector<D3DXMATRIX> GetMatrixBones(void);
	void Shutdown(void);
};




#endif