////////////////////////////////////////////////////////////////////////////////
// Filename: cursorclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _CURSORCLASS_H_
#define _CURSORCLASS_H_

class BoxClass;
class TextureManagerClass;
class ShaderManagerClass;
class MaterialClass;

class CursorClass: public BoxClass {
public:
	CursorClass();
	bool Update(InputClass*, int, int );
	bool Create(ID3D10Device*, TextureManagerClass*, int, int);
	bool Render(ID3D10Device*, ShaderManagerClass*, 
				D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	void Shutdown(void);
	MaterialClass m_Material;
	enum type_state_cursor {ACTIVE, NORMAL, PRESSURE};
	int itemAnimation;
	float timeAnimation;
};

#endif