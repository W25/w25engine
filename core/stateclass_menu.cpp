////////////////////////////////////////////////////////////////////////////////
// Filename: stateclass_menu.cpp
////////////////////////////////////////////////////////////////////////////////
#include "stateclass_menu.h"
#include "stdafx.h"

////////////////////////////////////////////////////////////////////////////////
// ����
////////////////////////////////////////////////////////////////////////////////

// ���������� ��������� (������������)
bool MenuState::InitializeState(void) {
	
	bool result;

	// ������� ������ FPS.
	/*m_Fps = new FpsClass;
	if (!m_Fps) {
		return false;
	}
	// �������������� ������ FPS.
	result = m_Fps->Initialize();
	if (!result) {
		MessageBox(*p_HWND, "Could not initialize the FPS object.", "Error", MB_OK);
		return false;
	}*/

	// ������������� ��������� ��������� ������
	m_Camera->SetPosition(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_Camera->SetRotation(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_Camera->Render();
	
	return true;
}
void MenuState::OnClickButtonStart(int, int, int){
	PackMsg sendMsg;
	sendMsg.aimStateId = TypeState::PLAY;
	sendMsg.type = TypeMSG::ADD;
	p_MsgManager->AddMsg(sendMsg);

	sendMsg.aimStateId = TypeState::MENU;
	sendMsg.type = TypeMSG::ERASE;
	p_MsgManager->AddMsg(sendMsg);
	return;
}
void MenuState::OnClickButtonEnd(int, int, int){
	PackMsg sendMsg;
	sendMsg.aimStateId = TypeState::NONE;
	sendMsg.type = TypeMSG::EXIT;
	p_MsgManager->AddMsg(sendMsg);

	return;
}

void MenuState::OnClickButtonDebug(int, int, int) {
	PackMsg sendMsg;
	sendMsg.aimStateId = TypeState::DEBUG;
	sendMsg.type = TypeMSG::ADD;
	p_MsgManager->AddMsg(sendMsg);

	sendMsg.aimStateId = TypeState::MENU;
	sendMsg.type = TypeMSG::ERASE;
	p_MsgManager->AddMsg(sendMsg);
	return;
}
// �������� ��������� (������������ ������)
bool MenuState::Exit(){
	Shutdown();
	return true;
}
bool MenuState::Render()
{
	return RenderPrimary();
}
// ���������
bool MenuState::RenderPrimary(){
	// ������� 
	D3DXMATRIX  orthoMatrix, worldMatrix, viewMatrix,  guiMatrix;
	// ��������� ��������� ������
	m_Camera->Render();
	///////////////////////////////////////////////////////////////////////////////////
	// GUI 2D �������
	///////////////////////////////////////////////////////////////////////////////////	
	p_D3D->TurnZBufferOff();
	// ���������� ������� ������// ���� �� �������� � �������� �� ���� ���� ��
	int screenWidth;
	int screenHeight;
	p_D3D->GetScreenSize(screenWidth, screenHeight);
	// ��� ����� ����������� � 2� ���������, �� ����� ���� ��� ��������� ����� ������������� �����
	m_Camera->GetWorldMatrix(&worldMatrix);
	m_Camera->GetOrthoMatrix(&orthoMatrix);
	m_Camera->GetMatrixGUI(&guiMatrix);
	// ������ GUI
	//m_GuiManager->Render(p_D3D->GetDevice(), worldMatrix, guiMatrix, orthoMatrix);
	p_D3D->TurnZBufferOn();
	///////////////////////////////////////////////////////////////////////////////////	
	return true;
}
bool MenuState::Update(InputClass* p_Input, float time){
	return UpdatePrimary(p_Input, time);
}
// ���������� �����
bool MenuState::UpdatePrimary(InputClass* p_Input, float time){	
	//bool result;
	// StateClass
	runTime += time;
	// ��������� ���������� � FPS
	//m_Fps->Frame();
	/////////////
	int dMouseX;
	int dMouseY;
	p_Input->GetMouseLocation(MouseX,MouseY);
	p_Input->GetMouseMove(dMouseX, dMouseY);
	// 2.13 3.8 �� ��������� ������ 768/360 1366/360 
	//m_GuiManager->Update(p_Input, MouseX, MouseY);
	/*
	// ��������� ������ �� �� �������� ��������� ��� ������� ����� ������ ����
	if (p_Input->IsMousePress(MOUSE_LEFT) == true){
		for (unsigned int i=0; i<m_GuiManager->GetCount(); i++){
			m_GuiManager->Update(0, MouseX, MouseY);
		}
	}
	
	std::string text;
	text = "���:" + std::to_string(m_Fps->GetFps());
	m_GuiManager->SetText("Label1", text);
	*/
	return true;	
}
// ������������� ��������
bool MenuState::Shutdown(){
	// ����������� ������ �����������
	// ����������� ������ FPS
	/*if (m_Fps) {
		m_Fps->Shutdown();
		delete m_Fps;
		m_Fps = 0;
	}

	// Release the frustum object.
	if(m_Frustum){
		delete m_Frustum;
		m_Frustum = 0;
	}*/

	return true;
}