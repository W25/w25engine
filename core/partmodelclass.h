////////////////////////////////////////////////////////////////////////////////
// Filename: partmodelclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _PARTMODELCLASS_H_
#define _PARTMODELCLASS_H_

enum class TypeAnimation;
enum class TypeBody;

class MaterialClass;
class SkeletonClass;
class BoneTreeClass;

struct StructPartModelID {
	int id;
	int type;
};

class ModelClass {
public:
	ModelClass();	
	SkeletonClass *m_Skeleton;
	std::vector<int> MeshID;
	MaterialClass material;
	BoneTreeClass *p_HeadBone;
	D3DXVECTOR3 position;
	D3DXVECTOR3 rotation;
	D3DXVECTOR3 scale;
	BoneTreeClass* GetBoneRoot(TypeBody item);
	void Shutdown(void);
	void SkeletonClone(SkeletonClass *skeleton);
	void PlayAnimation(TypeAnimation animation, double dTime);
	void RefreshAnimation(TypeAnimation animation);
	void AddMeshID(int ID);
	std::vector<D3DXMATRIX> GetMatrixBones(void);
};



#endif