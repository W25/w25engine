////////////////////////////////////////////////////////////////////////////////
// Filename: TerrainClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TERRAINCLASS_H_
#define _TERRAINCLASS_H_

#include "terraintileclass.h"

class MaterialClass;
class ShaderManagerClass;
class TextureManagerClass;
class LightManagerClass;
class FrustumClass;

// ��������� ��������� �� ������
class QuadTreeClass{
public:
	QuadTreeClass();
	~QuadTreeClass();
	// ���������� ������� �����
	bool GetIndexVisibleTile(FrustumClass* FrustumView, std::vector<int>&);
	int GetIndexTilePoint(D3DXVECTOR3 position);

	// ������� ������ �� �������� ������ ��� ����� � ����������� �� pos
	// � ��������� width x height � ������� widthTile x heightTile
	void CreatTree(	std::vector<int> indexTile, D3DXVECTOR3 pos, 
					int width, int height,
					float widthTile, float heightTile);
private:
	// ������ �����
	D3DXVECTOR3 QuadPosition;
	D3DXVECTOR3 QuadSize;
	int m_IndexQuad;
	// �������
	QuadTreeClass* northWest;
	QuadTreeClass* northEast;
	QuadTreeClass* southWest;
	QuadTreeClass* southEast;
};

class TerrainClass {
public:
	class FrameJointClass {
	public:
		int left;
		int right;
		int top;
		int bottom;
	};
	long int delayTime;
	float sizeStepGrid;
	bool Initialize(ID3D10Device* device, ShaderManagerClass* p_ShaderMng,
					TextureManagerClass* p_TextureMng,
					LightManagerClass* p_LightMng, std::string filename);
	bool SaveTerrainMap(std::string );
	bool LoadTerrainMap(std::string );
	bool CreateTerrainMap(	ID3D10Device*, int width, int heigth);
	bool CreateRound(ID3D10Device* device, D3DXVECTOR3 position, float radius);
	bool CreateLine(ID3D10Device* device, D3DXVECTOR3 start, D3DXVECTOR3 end);
	bool CreateNormal(int index, D3DXVECTOR3 start, D3DXVECTOR3 end);
	void Render(ID3D10Device*, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, FrustumClass*);
	void SetMethodRender(int);
	int GetMethodRender(void);
	void EraseTerrain(void);
	void Shutdown(void);
	void AlterArea(ID3D10Device* device, D3DXVECTOR3 position,
				   float value, float radius, alter_argument argument);
	bool AlterPoint(D3DXVECTOR3 point, float, alter_argument argument);
	bool SetCamputeMeanNTB(D3DXVECTOR3 point);
	bool GetHeight(D3DXVECTOR3, float*);
	bool GetHeight(D3DXVECTOR3, float*, D3DXVECTOR3*);
	bool GetNormal(D3DXVECTOR3 point, D3DXVECTOR3 *normal);
	bool GetIntersects(D3DXVECTOR3 position, D3DXVECTOR3 view, D3DXVECTOR3* point);

	bool GetIndexTile(int *index_1, int *index_2, int *index_3, int *index_4, D3DXVECTOR3 point);
	bool GetIndexTile(int *index, D3DXVECTOR3 point);
	TerrainClass();
private:
	void CreateQuadTree(void);

	int GetIndexFrame(int* itemX, int* itemY,
					 const int index_NW, const int index_NE,
					 const int index_SW, const int index_SE);
	std::list<int> listEditTile;
	bool GetVertex(TerrainTileClass::VertexType *vertex, D3DXVECTOR3 point, int offsetIndexColumn, int offsetIndexRow);
	void fileReadString(std::ifstream& file, std::string& text);
	void fileWriteString(std::ofstream& file, std::string text);
	std::string pathMap;
	TerrainRoundClass* m_Round;
	MeshLineClass* m_Line;
	MeshLineClass* m_Normal[4];
	TerrainTileClass* m_Tile;
	// ������ ��������� � �������
	int m_TerrainWidth;
	int m_TerrainHeight;
	int m_TileWidth;
	int m_TileHeight;
	int m_TileCount;
	// Quad ������ ������
	QuadTreeClass* m_QuadTree;
	std::vector<MaterialClass> m_Material;
	ShaderManagerClass* p_ShaderManager;
	TextureManagerClass* p_TextureManager;
	LightManagerClass* p_LightManager;

};


#endif