////////////////////////////////////////////////////////////////////////////////
// Filename: shadermanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _SHADERMANAGERCLASS_H_
#define _SHADERMANAGERCLASS_H_

class TextureShaderClass;
class LightShaderClass;
class BumpMapShaderClass;
class TerrainShaderClass;
class FontShaderClass;
class MultiTextureShaderClass;
class ParticleShaderClass;
class TextureManagerClass;
class MeshManagerClass;
class LightManagerClass;
class LightClass;


class ShaderManagerClass{
// ������� �������� ������
public:
	static ShaderManagerClass* GetInstance() {
		static ShaderManagerClass m_Instance;
		return &m_Instance;
	}
public:
	bool Initialize(ID3D10Device*, HWND, TextureManagerClass* p_TextureManager,
					MeshManagerClass* p_MeshManager, LightManagerClass* p_LightManager);
	void Shutdown();
	void RenderTextureShader(int countIndex, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, MaterialClass);
	void RenderLightShader(int countIndex, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXVECTOR3, MaterialClass, std::vector<D3DXMATRIX>);
	void RenderBumpMapShader(int countIndex, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, MaterialClass);
	void RenderMultiTextureShader(int countIndex, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D10ShaderResourceView*,
								 ID3D10ShaderResourceView*);
	void RenderSkeleton(int countIndex, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);

	void RenderFontShader(int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D10ShaderResourceView*, D3DXVECTOR4);
	void RenderParticleShader(int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, MaterialClass material, D3DXVECTOR3);
	void RenderTerrainShader(int indexCount, D3DXMATRIX worldMatrix,
								D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
								std::vector<MaterialClass> vecMaterial, LightClass* light);
private:
	TextureShaderClass* m_TextureShader;
	LightShaderClass* m_LightShader;
	BumpMapShaderClass* m_BumpMapShader;
	TerrainShaderClass* m_TerrainShader;
	FontShaderClass* m_FontShader;
	MultiTextureShaderClass* m_MultiTextureShader;
	ParticleShaderClass* m_ParticleShader;
	// ��������� �� ��������� ��������
	TextureManagerClass* p_TextureManager;
	MeshManagerClass* p_MeshManager;
	LightManagerClass* p_LightManager;
	ID3D10Device* p_Device;
private:
	// ������������ � �������� ������������ ���������� ��������
	ShaderManagerClass();
	~ShaderManagerClass() {};
	ShaderManagerClass(const ShaderManagerClass&) {};
	ShaderManagerClass& operator=(ShaderManagerClass&) {};
};

#endif