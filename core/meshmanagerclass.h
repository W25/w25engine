////////////////////////////////////////////////////////////////////////////////
// Filename: meshmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MESHMANAGERCLASS_H_
#define _MESHMANAGERCLASS_H_

class MeshClass;
class BoneMechClass;
class BoneTreeClass;
class AnimationClass;
class ResourceMeshClass;
class ModelClass;
class SkeletonClass;

struct aiMesh;

class FileInfoModelClass {
public:
	bool IsCorrected = false;
	int meshID;
};

class MeshManagerClass{
// ������� �������� ������
public:
	static MeshManagerClass* GetInstance() {
		static MeshManagerClass m_Instance;
		return &m_Instance;
	}
public:
	void Shutdown(void);
	bool Initialize(HWND hwnd, ID3D10Device* device, TextureManagerClass* p_TextureMng);
	FileInfoModelClass LoadFileMesh(std::string);
	AnimationClass LoadFileAnimation(std::string path);
	int GetID(std::string path, unsigned int indexMesh);
	MeshClass* GetMesh(int ID);
	SkeletonClass* GetSkeleton(int ID);
	std::vector<BoneMechClass> LoadBones(const aiMesh* pMesh);
	bool Erase(std::string);
	bool IsLoaded(std::string);
	bool RenderBuffers(int ID, int methodRender);
	ResourceMeshClass* GetResourceMech(int ID);
	void SetScaleMesh(int ID, float value);
	int GetIndexCount(int ID);
	int GetVertexCount(int ID);
	unsigned int GetCountLink(int ID);
	void AddAnimation(int ID, AnimationClass animation);
	int GetCoutMesh(std::string);
	MaterialClass GetMaterial(int ID);
	float GetRadiusMesh(int ID);
	D3DXVECTOR3 GetCenterMesh(int ID);
private:
	int incrementMeshID;
	FileInfoModelClass AssimpLoadFileMesh(std::string);
	D3DXMATRIX ConvertMatrix(const aiMatrix4x4* matrix);
	MeshClass* ProcessMesh(const aiMesh *mesh);
	HWND p_hwnd;
	ID3D10Device* p_Device;
	// ���������� � ���������� ����� � �� ID
	std::map<std::string, FileInfoModelClass> m_FileInfoModel;
	// ����� c ������� � ���������
	std::map<int, ResourceMeshClass*> m_ResourseMesh;
	TextureManagerClass* p_TextureManager;
private:
	// ������������ � �������� ������������ ���������� ��������
	MeshManagerClass();
	~MeshManagerClass() {};
	MeshManagerClass(const MeshManagerClass&) {};
	MeshManagerClass& operator=(MeshManagerClass&) {};
};

#endif

