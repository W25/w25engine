////////////////////////////////////////////////////////////////////////////////
// Filename: msgmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "msgmanagerclass.h"
#include "stdafx.h"

// ������������ ���������
PackMsg::PackMsg(){
	this->aimStateId =  TypeState::NONE; 
	this->stateId =  TypeState::NONE;
	this->type = TypeMSG::NONE;
	this->time = 0.0f;
}

// ��������� ���������
bool MsgManagerClass::AddMsg(PackMsg msg){
	m_Msg.push_back(msg);
	return true;
}
// ������� ���� ��������� �� ������
bool MsgManagerClass::EraseMsg(int item){
	if (item < (int)m_Msg.size()){
		m_Msg.erase(m_Msg.begin() + item);
		return true;
	}
	return false;
}
// ������� ��� ���������
void MsgManagerClass::Shutdown(){
	m_Msg.clear();
}
// ���������� ���������� ���������
int MsgManagerClass::GetCountMsg(){

	return m_Msg.size();
}
// ���������� ��������� �� ������
PackMsg MsgManagerClass::GetMsg(int item){

	return m_Msg[item];
}
// ���������� ������ ��������� � ������� ���
PackMsg MsgManagerClass::GetBackMsg(){
	PackMsg result;
	
	// ������� ��������� �� �������
	if (this->GetCountMsg()>0){
		result = m_Msg[0];
		this->EraseMsg(0);
	}
	return result;
}