////////////////////////////////////////////////////////////////////////////////
// Filename: meshclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MESHCLASS_H_
#define _MESHCLASS_H_

class SkeletonClass;

class VertexMeshClass{
public:
	VertexMeshClass();
	D3DXVECTOR3 position;
	D3DXVECTOR2 texture;
	D3DXVECTOR3 normal;
	D3DXVECTOR3 tangent;
	D3DXVECTOR3 binormal;
	unsigned int BoneID[4];
	float BoneWidth[4];
};

// ����������� ����� ������
class MeshClass {
public:
	MeshClass();
	virtual ~MeshClass();
	int countLink;
	int m_ShaderRender;
	void Shutdown();
	bool AddVertex(VertexMeshClass vertex);
	bool AddIndex(unsigned int index);
	int GetCountVertex();			// ���������� ���������� ������
	int GetCountIndex();			// ���������� ���������� ��������
	void ConnectSkeletonToMesh(std::vector<BoneMechClass> boneInfo);
	SkeletonClass* GetSkeleton(void);
	void SetSkeleton(SkeletonClass* skeleton);
    MaterialClass material;					
	float radius;						// ������ ������
	std::vector<VertexMeshClass> m_Vertex;   // ������� ������
	std::vector<unsigned int> m_Index;		// ������� ������
	int countOfVertex;					// ���������� ������
	int countOfIndex;					// ���������� ��������
	void ReleaseBuffers(void); 
	virtual bool InitializeBuffers(ID3D10Device* device);
	void RenderBuffers(ID3D10Device* device, int methodRender);

	D3DXVECTOR3 center;
private:
	SkeletonClass *m_Skeleton;
	D3DXVECTOR3 position;
	D3DXVECTOR3 rotation;
	D3DXVECTOR3 scale;

	D3DXVECTOR3 max;				// ������������ �������
	D3DXVECTOR3 min;				// ����������� �������
	ID3D10Buffer *m_VertexBuffer;
	ID3D10Buffer *m_IndexBuffer;
};


#endif