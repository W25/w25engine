////////////////////////////////////////////////////////////////////////////////
// Filename: widget.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _WIDGETCLASS_H_
#define _WIDGETCLASS_H_

class WidgetClass {
public:
	WidgetClass();
	virtual ~WidgetClass();
	float Width;
	float Height;
	float Top;
	float Left;
	unsigned int pxTextureWidth;
	unsigned int pxTextureHeight;
	unsigned int pxTileOffsetX;
	unsigned int pxTileOffsetY;
	unsigned int pxTileWidth;
	unsigned int pxTileHeight;
	int Cursor; // ��� ������� ��� ���������
	std::string Name;
	std::wstring Text;
	float fontSize;
	bool Enabled;
	bool Visible;
	int m_screenWidth;
	int m_screenHeight;
	// ������ �� �������
	bool GetReside(int, int);
	virtual bool Update(InputClass*, int, int);
	virtual bool Render(ID3D10Device*, ShaderManagerClass*,
						D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	virtual bool Create(ID3D10Device*, TextureManagerClass*, int, int);	
	virtual void Shutdown(void);
	virtual void SetText(std::wstring);
	virtual bool SetText(std::string, std::wstring);
	virtual bool SetVisible(bool);
	std::wstring GetText(void);
};

// ������� � ����������
class BoxClass : public WidgetClass {
public:
	BoxClass(void);
	virtual ~BoxClass();
	void RenderBuffers(ID3D10Device*);
	bool InitializeBuffers(ID3D10Device*);
	void ShutdownBuffers();
	ID3D10Buffer *m_VertexBuffer, *m_IndexBuffer;
	std::vector<VertexMeshClass> m_Mesh;
	int m_VertexCount;
	int m_IndexCount;
	float m_PreviousPosX;
	float m_PreviousPosY;

};
#endif
