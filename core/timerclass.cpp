///////////////////////////////////////////////////////////////////////////////
// Filename: timerclass.cpp
///////////////////////////////////////////////////////////////////////////////
#include "timerclass.h"
#include "stdafx.h"

TimerClass::TimerClass(){
	m_frequency = 0;
	m_ticksPerMs = 0;
	m_startTime = 0;
	m_frameTime = 0;
}

TimerClass::TimerClass(const TimerClass&){
	m_frequency = 0;
	m_ticksPerMs = 0;
	m_startTime = 0;
	m_frameTime = 0;
}

bool TimerClass::Initialize(){
	// Check to see if this system supports high performance timers.
	QueryPerformanceFrequency((LARGE_INTEGER*)&m_frequency);
	if(m_frequency == 0){
		return false;
	}

	// Find out how many times the frequency counter ticks every millisecond./////-/
	m_ticksPerMs = (signed __int64)(m_frequency);

	QueryPerformanceCounter((LARGE_INTEGER*)&m_startTime);

	return true;
}

signed __int64 TimerClass::GetTiks() {
	signed __int64 result = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&result);
	return result;
}

bool TimerClass::Frame(){

	signed __int64 currentTime = 0;
	signed __int64 timeDifference;

	QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);

	timeDifference = (currentTime - m_startTime);

	m_frameTime = ((float)timeDifference / (float)m_ticksPerMs);

	m_startTime = currentTime;

	return true;
}

void TimerClass::Shutdown(){
	return;
}

float TimerClass::GetFrameTime(){
	return m_frameTime;
}
