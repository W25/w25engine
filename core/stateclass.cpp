////////////////////////////////////////////////////////////////////////////////
// Filename: stateclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "stateclass.h"
#include "stdafx.h"

// �����������
StateClass::StateClass(){
	runTime = 0.0f;
	countRenderVertex = 0;
	mode.itemRender = 0;
	mode.itemFrame = 0;
	mode.pause = false;
	m_GuiManager = nullptr;
	m_Camera = nullptr;
	m_RenderTexture = nullptr;
	p_HWND = nullptr;
	p_D3D = nullptr;
	
	p_ShaderManager = nullptr;
	p_MsgManager = nullptr;
	p_MeshManager = nullptr;
	p_TextureManager = nullptr;

	p_ModelManager = nullptr;

}
// �������� � ������� ������ ��� �� ������ ��� ��������� �� ����������������
StateClass::~StateClass(){
}

bool StateClass::Initialize(HWND* hwnd, D3DClass* d3d, MsgManagerClass* p_MsgMng, 
							MeshManagerClass* p_MeshMng, ModelManagerClass* p_ModelMng, TextureManagerClass* p_TextureMng,
							LightManagerClass* p_LightMng, ShaderManagerClass* p_ShaderMng){
	
	//========================================================
	// ����������� ��������� ��� �������� �����
	//========================================================
	p_HWND = hwnd;
	p_D3D = D3DClass::GetInstance();
	//p_MsgManager = MsgManagerClass::GetInstance();
	p_MeshManager = MeshManagerClass::GetInstance();
	p_TextureManager = TextureManagerClass::GetInstance();
	p_LightManager = LightManagerClass::GetInstance();
	p_ShaderManager = ShaderManagerClass::GetInstance();
	p_ModelManager = ModelManagerClass::GetInstance();

	p_D3D->GetScreenSize(m_ScreenWidth, m_ScrenHeight);

	m_Fps.Initialize();
	m_Cpu.Initialize();
	//========================================================
	// ������� ������ ������.
	//========================================================
	m_Camera = new CameraClass;
	m_Camera->Initialize(m_ScreenWidth, m_ScrenHeight, 250.f, 0.1f);	

	m_RenderTexture = new RenderTextureClass;
	m_RenderTexture->Initialize(p_D3D->GetDevice(), m_ScreenWidth, m_ScrenHeight);
	// ������������� ��������� ��������� ������
	m_Camera->SetPosition(D3DXVECTOR3(100.0f, 10.0f, 100.0f));
	m_Camera->SetRotation(D3DXVECTOR3(0.0f, 45.0f, 0.0f));
	m_Camera->Render();
	//========================================================
	// ������� �������� ����������
	//========================================================
	m_GuiManager = new GuiManagerClass;
	int screenWidth, screenHeight;
	// ������ ������� ����.
	p_D3D->GetScreenSize(screenWidth, screenHeight);
	m_GuiManager->Initialize(p_D3D->GetDevice(), p_ShaderManager,
							 p_TextureManager, screenWidth, screenHeight);
	InitializeComponentGUI(screenWidth, screenHeight);

	return InitializeState();
}

bool StateClass::InitializeState(void) {
	return true;
}

WCHAR* StateClass::GetNameState(){
	return nameState;
}

float StateClass::GetRunTime(){
	return runTime;
}
bool StateClass::Exit(){
	Shutdown();
	return true;
}
bool StateClass::Render(){
	RenderState();
	// ������� 
	D3DXMATRIX  orthoMatrix, worldMatrix, guiMatrix;
	// ��������� ��������� ������
	m_Camera->Render();
	///////////////////////////////////////////////////////////////////////////////////
	// GUI 2D �������
	///////////////////////////////////////////////////////////////////////////////////	
	p_D3D->TurnZBufferOff();
	// ��� ����� ����������� � 2� ���������, �� ����� ���� ��� ��������� ����� ������������� �����
	m_Camera->GetWorldMatrix(&worldMatrix);
	m_Camera->GetOrthoMatrix(&orthoMatrix);
	m_Camera->GetMatrixGUI(&guiMatrix);
	// ������ GUI
	m_GuiManager->Render(p_D3D->GetDevice(), worldMatrix, guiMatrix, orthoMatrix);
	p_D3D->TurnZBufferOn();
	///////////////////////////////////////////////////////////////////////////////////	
	return true;
}
bool StateClass::Update(InputClass* p_Input, float time){
	int MouseX;
	int MouseY;
	runTime += time;
	p_Input->GetMouseLocation(MouseX, MouseY);
	m_GuiManager->Update(p_Input, MouseX, MouseY);
	m_Cpu.Frame();
	m_Fps.Frame();
	UpdateState(p_Input, time);
	return true;
}
bool StateClass::RenderState() {
	return true;
}

bool StateClass::UpdateState(InputClass*, float) {
	return true;
}

bool StateClass::InitializeComponentGUI(int, int) {
	return true;
}
bool StateClass::Pause(){
	mode.pause = true;
	return true;
}

bool StateClass::Resume(){
	mode.pause = false;
	return true;
}
bool StateClass::ShutdownState(void) {
	return true;
}
bool StateClass::Shutdown(){
	ShutdownState();
	p_ShaderManager = nullptr;
	p_MsgManager = nullptr;
	p_MeshManager = nullptr;
	p_TextureManager = nullptr;
	p_HWND = nullptr;
	p_D3D = nullptr;
	p_ModelManager = nullptr;
	// ������� GUI ������� ���������
	if (m_GuiManager != nullptr) {
		m_GuiManager->Shutdown();
		delete m_GuiManager;
		m_GuiManager = nullptr;
	}
	// ������� ������ ��� ������� � ��������
	if (m_RenderTexture != nullptr) {
		m_RenderTexture->Shutdown();
		delete m_RenderTexture;
		m_RenderTexture = 0;
	}
	// ������� ������
	if (m_Camera != nullptr) {
		m_Camera->Shutdown();
		delete m_Camera;
		m_Camera = 0;
	}	
	
	this->nameState = 0;
	this->runTime = 0;

	return true;
}

ModeStruct StateClass::GetMode(){
	return mode;
}

bool StateClass::RenderTexture(){
	// Set the render target to be the render to texture.
	m_RenderTexture->SetRenderTarget(p_D3D->GetDevice(), p_D3D->GetDepthStencilView());
	//Clear the render to texture background to blue so we can differentiate it 
	// from the rest of the normal scene.
	// Clear the render to texture.
	m_RenderTexture->ClearRenderTarget(p_D3D->GetDevice(), p_D3D->GetDepthStencilView(),
									   0.5f, 0.5f, 0.5f, 1.0f);
	// Render the scene now and it will draw to the render to texture instead of the back buffer.
	RenderState();
	// Reset the render target back to the original back buffer and not the render to texture anymore.
	p_D3D->SetBackBufferRenderTarget();

	return true;
}



