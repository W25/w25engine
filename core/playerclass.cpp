////////////////////////////////////////////////////////////////////////////////
// Filename: playerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "playerclass.h"


PlayerClass::PlayerClass() {
	modelID = -1;
	m_SpeedMove = 2.0f;
	m_SpeedRotation = 180.0f*0.01745329f;
	p_Camera = 0;
	TimeMoveToMark = 0;
	p_Terrain = 0;
	flagMove = false;
	this->rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	this->position = D3DXVECTOR3(10.0f, 0.0f, 10.0f);
	this->scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	this->markMove = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
}
void PlayerClass::Initialize(ModelManagerClass *p_ModelMng, CameraClass* camera, TerrainClass* terrain) {
	p_ModelManager = p_ModelMng;
	p_Camera = camera;
	p_Terrain = terrain;
}
void PlayerClass::Animation() {


}
// ���������� ������
WeaponClass PlayerClass::GetWeapon(int index) {
	if ((unsigned int)index < m_Weapons.size()) {
		return m_Weapons[index];
	}
	return WeaponClass();
	
}
void PlayerClass::UpdateWeapon(float dTime) {
	D3DXVECTOR3 pos;
	for (unsigned int i = 0; i < m_Weapons.size(); i++) {
		pos = m_Weapons[i].local;
		D3DXMATRIX rotationMatrix;
		// Create the rotation matrix from the yaw, pitch, and roll values.<^*
		D3DXMatrixRotationYawPitchRoll(&rotationMatrix, this->rotation.x, this->rotation.y, this->rotation.z);
		// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
		D3DXVec3TransformCoord(&pos, &pos, &rotationMatrix);
		m_Weapons[i].position = pos + this->position;
	}
}
void PlayerClass::AddWeapon(WeaponClass weapon) {
	m_Weapons.push_back(weapon);
}
// ��������� � �������
bool PlayerClass::MoveTo(float dTime) {
	if (flagMove == true) {

		D3DXVECTOR3 vec = (position - markMove);
		float length = D3DXVec3Length(&vec);
		if ( length < 0.5f*m_SpeedMove) {
			flagMove = false;
			TimeMoveToMark = timeGetTime() - TimeMoveToMark;
		}else {
			Rotation(dTime);
			Step(dTime);
			CamputePitchRoll();
		}
		ModelUnitClass *model = p_ModelManager->GetModel(modelID);
		model->rotation = rotation;
		model->position = position;
		UpdateWeapon(dTime);
	}
	return true;
}

// ��� � ������� �������
bool  PlayerClass::Step(float deltaTime) {
	//rotation
	int countIteration = 100;
	float step = m_SpeedMove * deltaTime / ((float)countIteration);
	// ���������� ����������� �������� � ���������
	D3DXVECTOR3 stepDirection = GetDirection()*step;
	D3DXVECTOR3 pos = position;
	D3DXVECTOR3 prevPosPlayer = position;
	// ����� ����������
	float length = 0.0f;
	// ��������� ��� �� ��������� ����������
	for (int i = 0; i < countIteration; i++) {
		if (length < m_SpeedMove*deltaTime) {
			// ����������� �� ����� ���
			pos = pos + stepDirection;
			float height;
			p_Terrain->GetHeight(pos, &height);
			// ���������� ���������� ���������
			length += sqrt(step*step + pow((height - position.y), 2.0f));
			position = D3DXVECTOR3(pos.x, height, pos.z);
		}
	}
	// ����������� ������
	//p_Camera->AddPosition(position - prevPosPlayer);
	return true;
}
bool PlayerClass::Rotation(float deltaTime) {
	// ������������ � ������� �������
	D3DXVECTOR3 direction = GetDirection();
	D3DXVECTOR3 markDirection = (markMove-position) - direction;
	D3DXVec3Normalize(&markDirection, &markDirection);
	// ���������� ���� ����� ������������ �������� � �����
	// atan2(axbz - azbx, axbx + azbz)
	float angle = atan2(direction.x*markDirection.z - direction.z*markDirection.x,
						direction.x*markDirection.x - direction.z*markDirection.z);
	
	// ������ ������� ������ ������
	if (fabs(GetAngleMarker()) >= (deltaTime*m_SpeedRotation)){
		if (angle > 0.0f) {
			rotation.x -= deltaTime*m_SpeedRotation;
		}else {
			rotation.x += deltaTime*m_SpeedRotation;
		}
	// ������� ������ ������ 0 0 1
	} else {
		// ���������� ���� ����� ����������� ���������� ������ � ������������
		rotation.x = atan2(markDirection.x, markDirection.z);
	}
	
	//rotation.x = atan2(markDirection.x, markDirection.z);
	DEBUG = angle* 57.295779513f;
	return true;
}
// ����������� ������ �� ���������
void PlayerClass::CamputePitchRoll(void) {
	D3DXVECTOR3 directionPlayer;
	D3DXVECTOR3 directionTangent(0.0f, 1.0f, 0.0f);
	D3DXVECTOR3 normalPlayer[6];
	directionPlayer = GetDirection();

	D3DXVec3Cross(&directionTangent, &directionPlayer, &directionTangent);
	D3DXVec3Normalize(&directionTangent, &directionTangent);
	// ���������� ������� �������
	p_Terrain->GetNormal(position - 0.1f*directionPlayer - 0.1f*directionTangent, &normalPlayer[0]);
	p_Terrain->GetNormal(position - 0.1f*directionTangent, &normalPlayer[1]);
	p_Terrain->GetNormal(position + 0.1f*directionPlayer - 0.1f*directionTangent, &normalPlayer[2]);
	p_Terrain->GetNormal(position - 0.1f*directionPlayer + 0.1f*directionTangent, &normalPlayer[3]);
	p_Terrain->GetNormal(position + 0.1f*directionTangent, &normalPlayer[4]);
	p_Terrain->GetNormal(position + 0.1f*directionPlayer + 0.1f*directionTangent, &normalPlayer[5]);

	p_Terrain->GetNormal(position, &normalPlayer[5]);
	D3DXVECTOR3 normalAvr = (normalPlayer[0] + normalPlayer[1] + normalPlayer[2] +
							 normalPlayer[3] + normalPlayer[4] + normalPlayer[5]) / 6.0f;
	D3DXVec3Normalize(&normalAvr, &normalAvr);
	D3DXVECTOR3 bCross;
	D3DXVec3Cross(&bCross, &normalAvr, &directionPlayer);
	D3DXVECTOR3 directionPlayerCross;
	D3DXVECTOR3 vec = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	D3DXVec3Cross(&directionPlayerCross, &directionPlayer, &vec);
	// ������
	rotation.y = -atan2(D3DXVec3Length(&bCross), D3DXVec3Dot(&directionPlayer, &normalAvr)) - 3.14159f*3.0f / 2.0f;
	D3DXVec3Cross(&bCross, &normalAvr, &directionPlayerCross);
	// ������
	rotation.z = -atan2(D3DXVec3Length(&bCross), D3DXVec3Dot(&directionPlayerCross, &normalAvr)) - 3.14159f*3.0f / 2.0f;
}
bool PlayerClass::AddRotation(float angle) {
	rotation.x += angle*0.01745329f;
	ModelUnitClass *model = p_ModelManager->GetModel(modelID);
	model->position = position;
	return true;
}
void PlayerClass::SetMoveTo(D3DXVECTOR3 mark) {
	D3DXVECTOR3 vec = (position - mark);
	float length = D3DXVec3Length(&vec);
	if ( length > 0.0f) {
		markMove = mark;
		flagMove = true;
		TimeMoveToMark = timeGetTime();
	}
}
// ����������� ����� ������
D3DXVECTOR3 PlayerClass::GetDirection(void) {
	D3DXMATRIX rotationMatrix;
	D3DXVECTOR3 direction(0.0f, 0.0f, 1.0f);
	D3DXVec3Normalize(&direction, &direction);
	// Create the rotation matrix from the yaw, pitch, and roll values.<^*
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, this->rotation.x, 0.0f, 0.0f);
	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&direction, &direction, &rotationMatrix);
	D3DXVec3Normalize(&direction, &direction);
	return direction;
}

D3DXVECTOR3 PlayerClass::GetPosition() {
	return position;
}

D3DXVECTOR3 PlayerClass::GetRotation() {
	return rotation;
}
float PlayerClass::GetAngleMarker(void) {
	// ������������ � ������� �������
	D3DXVECTOR3 direction = GetDirection();
	D3DXVECTOR3 markDirection = markMove - position;
	D3DXVec3Normalize(&markDirection, &markDirection);
	// ���������� ���� �������� 
	// atan2(axbz - azbx, axbx + azbz)
	double angle =	(direction.x*markDirection.x + direction.z*markDirection.z) / 
					(sqrt((double)direction.x*direction.x + direction.z*direction.z)*
					(sqrt((double)markDirection.x*markDirection.x + markDirection.z*markDirection.z)));
	if (angle < -1.0f) {
		angle = -1.0f;
	}
	if (angle > 1.0f) { 
		angle = 1.0f;
	}
	return (float)(acos(angle));

}

void PlayerClass::Render(CameraClass* camera){
	p_ModelManager->Render(modelID, camera);
}
