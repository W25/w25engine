////////////////////////////////////////////////////////////////////////////////
// Filename: lightclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "lightclass.h"
#include "stdafx.h"

LightClass::LightClass(){
	m_specularPower = 0;
	m_ambientColor = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f);
	m_diffuseColor = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f);
	m_specularColor = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f);
	m_direction = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_possition = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rotationX = 0.0f;
	m_rotationY = 0.0f;
	m_rotationZ = 0.0f;
}
// ������� ����������� �����
void LightClass::AddRotationDirection(float x, float y, float z){
	m_rotationX	+= x;
	m_rotationY	+= y;
	m_rotationZ	+= z;
	UpdateDirection();
}
void LightClass::UpdateDirection(void) {
	D3DXVECTOR3 up, position, lookAt;
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

	// Setup where the camera is looking by default.
	m_direction.x = 0.0f;
	m_direction.y = 0.0f;
	m_direction.z = 1.0f;

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = m_rotationX * 0.0174532925f;
	yaw = m_rotationY * 0.0174532925f;
	roll = m_rotationZ * 0.0174532925f;

	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&m_direction, &m_direction, &rotationMatrix);
	D3DXVec3Normalize(&m_direction, &m_direction);
}

LightClass::~LightClass(){
}


void LightClass::SetAmbientColor(float red, float green, float blue, float alpha){
	m_ambientColor = D3DXVECTOR4(red, green, blue, alpha);
	return;
}


void LightClass::SetDiffuseColor(float red, float green, float blue, float alpha){
	m_diffuseColor = D3DXVECTOR4(red, green, blue, alpha);
	return;
}


void LightClass::SetDirection(float x, float y, float z){
	m_rotationX = x;
	m_rotationY = y;
	m_rotationZ = z;
	UpdateDirection();
	return;
}


void LightClass::SetSpecularColor(float red, float green, float blue, float alpha){
	m_specularColor = D3DXVECTOR4(red, green, blue, alpha);
	return;
}


void LightClass::SetSpecularPower(float power){
	m_specularPower = power;
	return;
}

void LightClass::SetPossition(float x, float y, float z) {
	m_possition = D3DXVECTOR3(x, y, z);
	return;
}

D3DXVECTOR4 LightClass::GetAmbientColor(){
	return m_ambientColor;
}


D3DXVECTOR4 LightClass::GetDiffuseColor(){
	return m_diffuseColor;
}


D3DXVECTOR3 LightClass::GetDirection(){
	return m_direction;
}


D3DXVECTOR4 LightClass::GetSpecularColor(){
	return m_specularColor;
}


float LightClass::GetSpecularPower(){
	return m_specularPower;
}

D3DXVECTOR3 LightClass::GetPossition() {
	return m_possition;
}