///////////////////////////////////////////////////////////
// Filename: statemanagerclass.h
///////////////////////////////////////////////////////////
#ifndef _STATEMANAGERCLASS_H_
#define _STATEMANAGERCLASS_H_

class StateClass;

class StateManagerClass{
// ������� �������� ������
public:
	static  StateManagerClass* GetInstance() {
		static  StateManagerClass m_Instance;
		return &m_Instance;
	}
public:
	bool Initialize(HWND*, D3DClass*, MsgManagerClass*, MeshManagerClass*, ModelManagerClass*,
		TextureManagerClass*, LightManagerClass*, ShaderManagerClass*);
	// ���������
	bool Render();
	// ���������� �����
	bool Frame(InputClass*, float);
	// ��������� ��������� � ������� ���������
	bool Inspector();
	// C���� ���������
	bool ChangeState(StateClass*);
	// ���������� ���������
	bool PushState(StateClass*);
	// ������� � ����������� ���������
	bool PopState();
	bool EraseState(WCHAR*);
	bool CheckStateName(WCHAR*);
	// ������� ��� ��������� �� �����
	void Shutdown();
private:
	HWND* p_HWND;
	D3DClass* p_D3D;
	//MsgManagerClass* p_MsgManager;
	MeshManagerClass* p_MeshManager;
	TextureManagerClass* p_TextureManager;
	LightManagerClass* p_LightManager;
	ShaderManagerClass* p_ShaderManager;
	ModelManagerClass* p_ModelManager;
	std::vector<StateClass*> m_States;

private:
	// ������������ � �������� ������������ ���������� ��������
	StateManagerClass() {};
	~StateManagerClass() {};
	StateManagerClass(const StateManagerClass&) {};
	StateManagerClass& operator=(StateManagerClass&) {};
};

#endif