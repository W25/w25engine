////////////////////////////////////////////////////////////////////////////////
// Filename: modelmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MODELMANAGERCLASS_H_
#define _MODELMANAGERCLASS_H_

class ModelClass;
struct ID3D10Device;
class MeshManagerClass;
class ShaderManagerClass;
class LightManagerClass;
class ModelClass;
enum class TypeAnimation;



class ModelManagerClass{
// ������� �������� ������
public:
	static ModelManagerClass* GetInstance() {
		static ModelManagerClass m_Instance;
		return &m_Instance;
	}
public:
	bool Initialize(ID3D10Device*, MeshManagerClass*, ShaderManagerClass*, LightManagerClass*);
	//void RefreshAnimation();
	void Update(int ID, double dTime, TerrainClass* p_Terrain);
	void Render(int ID, CameraClass* p_Camera, D3DXMATRIX matrixUnit);
	int GetCountModel(void);
	int AddModel(ModelClass* model);
	void EraseModel(int ID);
	void Shutdown();
	ModelClass* GetModel(int ID);
private:
	int incrementID;
	std::map<int, ModelClass*> m_Models;
	ID3D10Device* p_Device;
	MeshManagerClass *p_MeshManager;
	ShaderManagerClass *p_ShaderManager;
	LightManagerClass *p_LightManager;
private:
	// ������������ � �������� ������������ ���������� ��������
	ModelManagerClass() {};
	~ModelManagerClass() {};
	ModelManagerClass(const ModelManagerClass&) {};
	ModelManagerClass& operator=(ModelManagerClass&) {};
};

#endif

