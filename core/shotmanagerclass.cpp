////////////////////////////////////////////////////////////////////////////////
// Filename: shotmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "shotmanagerclass.h"
#include "stdafx.h"

ShotManagerClass::ShotManagerClass() {
	p_Terrain = nullptr;
}						
void ShotManagerClass::Initialize(TerrainClass* terrain) {

	p_Terrain = terrain;
}
bool ShotManagerClass::AddShot(ShotBaseClass shot) {
	m_Shot.push_back(shot);
	return true;
}
bool ShotManagerClass::Update(float deltaTime) {
	std::vector<unsigned int> deathIndexShot;
	// ��������� ��������� �������
	for (unsigned int i = 0; i < m_Shot.size(); i++) {
		// ����������� ������ ������ ����, ��� �� �� ������� � �������� (��� ��� ������ ����������� �������?)
		float height_t1;
		float height_t2;
		D3DXVECTOR3 offset;
		float lengthTarget;
		p_Terrain->GetHeight(m_Shot[i].position, &height_t1);
		p_Terrain->GetHeight(m_Shot[i].position + m_Shot[i].direction*m_Shot[i].speed*deltaTime, &height_t2);
		D3DXVECTOR3 vecTarget = m_Shot[i].targetPos - m_Shot[i].position;
		lengthTarget = D3DXVec3Length(&vecTarget);
		float offsetTerrain = lengthTarget*(height_t2 - height_t1) / (m_Shot[i].speed*deltaTime);

		offset.x = 0.0f;
		offset.y = m_Shot[i].targetPos.y - m_Shot[i].position.y - offsetTerrain;
		offset.z = 0.0f;
		
		m_Shot[i].Update(deltaTime);
		p_Terrain->GetHeight(m_Shot[i].position, &height_t1);
		m_Shot[i].position.y = height_t1 + 1.0f;
		m_Shot[i].direction =  m_Shot[i].targetPos - m_Shot[i].position;
		D3DXVec3Normalize(&m_Shot[i].direction, &m_Shot[i].direction);
		EffectManagerClass* p_EffectManager = EffectManagerClass::GetInstance();
		if (m_Shot[i].effectID != -1) {
			p_EffectManager->SetPositionToID(m_Shot[i].effectID, m_Shot[i].position);
			p_EffectManager->SetDirectionToID(m_Shot[i].effectID, m_Shot[i].direction);
		}
		vecTarget = m_Shot[i].targetPos - m_Shot[i].position;
		lengthTarget = D3DXVec3Length(&vecTarget);
		if (lengthTarget < 1.1f) {
			// ����������� ����
			m_Shot[i].flagActive = false;
			// ������� ������������ ������
			if (m_Shot[i].effectID != -1) {
				//p_EffectManager->SetEmitting(m_Shot[i].effectID, false);
			}
			m_Shot[i].effectID = -1;
		}
		if (m_Shot[i].flagActive == false) {
			deathIndexShot.push_back(i);
		}
	}
	// ������� ���������� ������� �� ���������� � �������
	unsigned int count = deathIndexShot.size();
	for (unsigned int i = 0; i < count; i++) {
		unsigned int index = deathIndexShot[count - i - 1];
		if (m_Shot[index].flagActive == false) {
			m_Shot[index].Shutdown();
			m_Shot.erase(m_Shot.begin() + index, m_Shot.begin() + index + 1);
		}
	}
	return true;
}
int ShotManagerClass::GetEffectToIndex(int index) {
	if ((unsigned int)index < m_Shot.size() && m_Shot.size() > 0) {
		return m_Shot[index].effectID;
	}
	return 0;
}
ShotBaseClass ShotManagerClass::GetShotToIndex(int index) {
	if ((unsigned int)index < m_Shot.size() && m_Shot.size() > 0) {
		return m_Shot[index];
	}
	return ShotBaseClass();
}
void  ShotManagerClass::SetShotToIndex(ShotBaseClass shot, int index) {
	if ((unsigned int)index < m_Shot.size() && m_Shot.size() > 0) {
		m_Shot[index] = shot;
	}
}
void ShotManagerClass::Shutdown() {
	p_Terrain = nullptr;
	m_Shot.clear();
}
int ShotManagerClass::GetCountShot() {
	return m_Shot.size();
}