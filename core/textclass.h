////////////////////////////////////////////////////////////////////////////////
// Filename: textclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TEXTCLASS_H_
#define _TEXTCLASS_H_

class TextClass{
public:
	TextClass();
	TextClass(const TextClass&);
	~TextClass();
	bool Initialize(ID3D10Device*, RECT, UINT, float);
	void Shutdown();
	void Render(std::wstring);
private:
	LPD3DX10FONT m_pFont;
	UINT m_Format;
	RECT m_PositionAndSize;
	int m_screenWidth, m_screenHeight;
};

#endif