#include "meshpolygonclass.h"
#include "stdafx.h"

MeshPolygonClass::MeshPolygonClass() {
	m_VertexBuffer = 0;
	m_IndexBuffer = 0;
	m_IndexCount = 0;
	m_VertexCount = 0;
	flagCreate = false;
}
void MeshPolygonClass::Initialize(ID3D10Device* device, std::vector<D3DXVECTOR3> vertex) {
	std::vector <VertexMeshClass> m_Model;
	this->Shutdown();
	m_Model.resize(vertex.size());
	for (unsigned int i = 0; i < vertex.size(); i++) {
		m_Model[i].position = vertex[i];
	}
	this->InitializeBuffers(device, m_Model);
	flagCreate = true;
}

void MeshPolygonClass::ReleaseBuffers() {
	// ����������� ��������� �����.
	if (m_IndexBuffer) {
		m_IndexBuffer->Release();
		m_IndexBuffer = 0;
	}
	// ����������� ��������� �����
	if (m_VertexBuffer) {
		m_VertexBuffer->Release();
		m_VertexBuffer = 0;
	}
	return;
}

MeshPolygonClass::~MeshPolygonClass() {
	this->Shutdown();
}
void MeshPolygonClass::Shutdown() {
	this->ReleaseBuffers();
	flagCreate = false;
	m_IndexCount = 0;
}
void MeshPolygonClass::Render(ID3D10Device* device) {
	if (flagCreate == true) {
		this->RenderBuffers(device);
	}
}
void MeshPolygonClass::RenderBuffers(ID3D10Device* device) {
	unsigned int stride;
	unsigned int offset;
	// Set vertex buffer stride and offset.
	stride = sizeof(VertexMeshClass);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	device->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	// ��������
	device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	// �����
	//device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINESTRIP);
	// �����
	//device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);

	return;
}
bool MeshPolygonClass::InitializeBuffers(ID3D10Device* device,
	std::vector <VertexMeshClass> m_Model) {
	unsigned long* indices;
	VertexMeshClass *vertex;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	m_VertexCount = m_Model.size();
	// ������������� ���������� ��������� ����� �� ��� � ���������� ������
	m_IndexCount = 6;

	// ������� ������ ��������
	indices = new unsigned long[m_IndexCount];

	// �����������
	indices[0] = 0; 
	indices[1] = 1;
	indices[2] = 2;
	indices[3] = 2;
	indices[4] = 3;
	indices[5] = 0;

	vertex = new VertexMeshClass[m_VertexCount];
	for (int i = 0; i < m_VertexCount; i++) {
		vertex[i] = m_Model[i];
	}

	// Set up the description of the vertex buffer.
	vertexBufferDesc.Usage = D3D10_USAGE_DYNAMIC; //D3D10_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexMeshClass) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	// ��������� CPU ������ � �����
	vertexBufferDesc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertex;

	// Now finally create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if (FAILED(result)) {
		return false;
	}

	// Set up the description of the index buffer.
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if (FAILED(result)) {
		return false;
	}

	delete[] indices;
	indices = 0;
	delete[] vertex;
	vertex = 0;
	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
TerrainRoundClass::TerrainRoundClass(void) {
	m_VertexBuffer = 0;
	m_IndexBuffer = 0;
	m_IndexCount = 0;
	flagCreate = true;
}
bool TerrainRoundClass::InitializeBuffers(ID3D10Device* device,
	std::vector <VertexMeshClass> m_Model) {
	unsigned long* indices;
	VertexMeshClass *vertex;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	m_VertexCount = m_Model.size();
	// ������������� ���������� ��������� ����� �� ��� � ���������� ������
	m_IndexCount = m_VertexCount * 3;

	// ������� ������ ��������
	indices = new unsigned long[m_IndexCount];
	if (!indices) {
		return false;
	}
	// �������� ����������
	int numberIndex = 0;
	for (int index = 0; index + 6<m_IndexCount; index += 6) {

		indices[index] = numberIndex + 1;
		indices[index + 1] = numberIndex;
		indices[index + 2] = numberIndex + 2;
		indices[index + 3] = numberIndex + 2;
		indices[index + 4] = numberIndex + 3;
		indices[index + 5] = numberIndex + 1;
		numberIndex += 2;
		//1 0 2 2 3 1
	}
	// ���������� �������
	indices[m_IndexCount - 6] = numberIndex + 1;
	indices[m_IndexCount - 6 + 1] = numberIndex;
	indices[m_IndexCount - 6 + 2] = 0;
	indices[m_IndexCount - 6 + 3] = 0;
	indices[m_IndexCount - 6 + 4] = 1;
	indices[m_IndexCount - 6 + 5] = numberIndex + 1;

	vertex = new VertexMeshClass[m_VertexCount];
	for (int i = 0; i < m_VertexCount; i++) {
		vertex[i] = m_Model[i];
	}

	// Set up the description of the vertex buffer.
	vertexBufferDesc.Usage = D3D10_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(VertexMeshClass) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;
	
	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertex;

	// Now finally create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if (FAILED(result)) {
		return false;
	}

	// Set up the description of the index buffer.
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if (FAILED(result)) {
		return false;
	}

	delete[] indices;
	indices = 0;
	delete[] vertex;
	vertex = 0;
	return true;
}


///////////////////////////////////////////////////////////
// ����� ���������� ����� ��� �����
MeshLineClass::MeshLineClass(void) {

}

void MeshLineClass::Initialize(ID3D10Device* device, int countVertex) {
	VertexMeshClass vertex;
	m_Vertex.clear();
	for (int i = 0; i < countVertex; i++) {
		m_Vertex.push_back(vertex);
	}
	m_VertexCount = countVertex;
	InitializeBuffers(device, m_Vertex);
}


bool MeshLineClass::InitializeBuffers(ID3D10Device* device,
	std::vector <VertexMeshClass> m_Model) {
	unsigned long* indices;
	VertexMeshClass* vertex;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	m_VertexCount = m_Model.size();
	// ������������� ���������� ��������� ����� �� ��� � ���������� ������
	m_IndexCount = m_VertexCount * 3;

	// ������� ������ ��������
	indices = new unsigned long[m_IndexCount];
	if (!indices) {
		return false;
	}
	// �������� ����������
	int numberIndex = 0;
	for (int index = 0; index + 6 < m_IndexCount; index += 6) {

		indices[index] = numberIndex + 1;
		indices[index + 1] = numberIndex;
		indices[index + 2] = numberIndex + 2;
		indices[index + 3] = numberIndex + 2;
		indices[index + 4] = numberIndex + 3;
		indices[index + 5] = numberIndex + 1;
		numberIndex += 2;
		//1 0 2 2 3 1
	}
	// ���������� �������
	indices[m_IndexCount - 6] = numberIndex + 1;
	indices[m_IndexCount - 6 + 1] = numberIndex;
	indices[m_IndexCount - 6 + 2] = 0;
	indices[m_IndexCount - 6 + 3] = 0;
	indices[m_IndexCount - 6 + 4] = 1;
	indices[m_IndexCount - 6 + 5] = numberIndex + 1;

	vertex = new VertexMeshClass[m_VertexCount];
	for (int i = 0; i < m_VertexCount; i++) {
		vertex[i] = m_Model[i];
	}

	// Set up the description of the vertex buffer.
	vertexBufferDesc.Usage = D3D10_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(VertexMeshClass) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertex;

	// Now finally create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if (FAILED(result)) {
		return false;
	}

	// Set up the description of the index buffer.
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if (FAILED(result)) {
		return false;
	}

	delete[] indices;
	indices = 0;
	delete[] vertex;
	vertex = 0;
	return true;
}

void MeshLineClass::Update(D3DXVECTOR3 start, D3DXVECTOR3 end) {

	float width = 0.1f;
	D3DXVECTOR3 direction = end - start;
	D3DXVECTOR3 step = direction  / (float)m_VertexCount;
	for (LONG i = 0; i < m_VertexCount - 1; i += 2) {
		m_Vertex[i].position = start + step*(float)i;
		m_Vertex[i + 1].position = start + step * (float)i;
		m_Vertex[i + 1].position.x += width;
		m_Vertex[i + 1].position.y += width;
		m_Vertex[i + 1].position.z += width;
	}
	UpdateBuffers();
}
void MeshLineClass::UpdateBuffers() {
	// ����������� �����
	HRESULT hresult;
	// Update the vertex buffer with the new solution
	VertexMeshClass *v = 0;
	hresult = m_VertexBuffer->Map(D3D10_MAP_WRITE_DISCARD, 0, (void**)&v);
	if (hresult == S_OK) {
		for (LONG i = 0; i < m_VertexCount; i++) {
			v[i] = m_Vertex[i];
		}
	}
	m_VertexBuffer->Unmap();
	flagCreate = true;
}




///////////////////////////////////////////////////////////
MeshFlatClass::MeshFlatClass() {
}

void MeshFlatClass::Initialize(ID3D10Device* device) {
	VertexMeshClass vertex;
	m_Vertex.clear();
	for (int i = 0; i < 4; i++) {
		m_Vertex.push_back(vertex);
	}
	m_VertexCount = 4;
	InitializeBuffers(device, m_Vertex);
}

void MeshFlatClass::Update(D3DXVECTOR3 pos, D3DXVECTOR3 normal, float width, float height) {

	m_Vertex[0].position = pos;
	m_Vertex[1].position = pos;
	m_Vertex[2].position = pos;
	m_Vertex[3].position = pos;

	m_Vertex[0].position.z -= width;
	m_Vertex[0].position.y -= height;
	m_Vertex[0].texture.x = 0.0f;
	m_Vertex[0].texture.y = 0.0f;

	m_Vertex[1].position.z += width;
	m_Vertex[1].position.y -= height;
	m_Vertex[1].texture.x = 1.0f;
	m_Vertex[1].texture.y = 0.0f;

	m_Vertex[2].position.z += width;
	m_Vertex[2].position.y += height;
	m_Vertex[2].texture.x = 1.0f;
	m_Vertex[2].texture.y = 1.0f;

	m_Vertex[3].position.z -= width;
	m_Vertex[3].position.y += height;
	m_Vertex[3].texture.x = 0.0f;
	m_Vertex[3].texture.y = 1.0f;

	UpdateBuffers();
}

void MeshFlatClass::UpdateBuffers() {
	// ����������� �����
	HRESULT hresult;
	// Update the vertex buffer with the new solution
	VertexMeshClass *v = 0;
	hresult = m_VertexBuffer->Map(D3D10_MAP_WRITE_DISCARD, 0, (void**)&v);
	if (hresult == S_OK) {
		for (LONG i = 0; i < m_VertexCount; i++) {
			v[i] = m_Vertex[i];
		}
	}
	m_VertexBuffer->Unmap();
	flagCreate = true;
}


///////////////////////////////////////////////////////////
MeshBoxClass::MeshBoxClass() {

}

bool MeshBoxClass::CreateBox(float height, std::vector<VertexMeshClass>& ver, std::vector<int>& ind) {
	VertexMeshClass vertex;
	int index = 0;
	float width = 0.01f;
	//height = 0.10f;
	vertex.position = D3DXVECTOR3(-width, height, -width);
	vertex.texture = D3DXVECTOR2(0.0f, 0.0f);
	ver.push_back(vertex);
	vertex.position = D3DXVECTOR3(-width, height, width);
	vertex.texture = D3DXVECTOR2(0.0f, 1.0f);
	ver.push_back(vertex);
	vertex.position = D3DXVECTOR3(width, height, width);
	vertex.texture = D3DXVECTOR2(0.0f, 0.0f);
	ver.push_back(vertex);
	vertex.position = D3DXVECTOR3(width, height, -width);
	vertex.texture = D3DXVECTOR2(0.0f, 1.0f);
	ver.push_back(vertex);
	vertex.position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	vertex.texture = D3DXVECTOR2(1.0f, 1.0f);
	ver.push_back(vertex);
	
	index = 0;
	ind.push_back(index);
	index = 1;
	ind.push_back(index);
	index = 2;
	ind.push_back(index);
	index = 2;
	ind.push_back(index);
	index = 3;
	ind.push_back(index);
	index = 0;
	ind.push_back(index);

	index = 0;
	ind.push_back(index);
	index = 4;
	ind.push_back(index);
	index = 1;
	ind.push_back(index);

	index = 1;
	ind.push_back(index);
	index = 4;
	ind.push_back(index);
	index = 2;
	ind.push_back(index);

	index = 2;
	ind.push_back(index);
	index = 4;
	ind.push_back(index);
	index = 3;
	ind.push_back(index);

	index = 3;
	ind.push_back(index);
	index = 4;
	ind.push_back(index);
	index = 0;
	ind.push_back(index);
	return true;
}

bool MeshBoxClass::InitializeBuffers(ID3D10Device* device, D3DXMATRIX local, D3DXMATRIX global, float height) {
	unsigned long* indices;
	VertexMeshClass *vertex;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;

	std::vector<VertexMeshClass> vertexModel;
	std::vector<int> indexModel;

	m_LocalMatrix = local;
	m_GlobalMatrix = global;
	CreateBox(height, vertexModel, indexModel);
	m_VertexCount = vertexModel.size();

	vertex = new VertexMeshClass[m_VertexCount];
	for (int i = 0; i < m_VertexCount; i++) {
		vertex[i] = vertexModel[i];
	}

	// Set up the description of the vertex buffer.
	vertexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexMeshClass) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertex;

	// Now finally create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if (FAILED(result)) {
		return false;
	}
	// ������������� ���������� ��������
	m_IndexCount = indexModel.size();
	// ��������� �������
	indices = new unsigned long[m_IndexCount];
	for (int i = 0; i < m_IndexCount; i++) {
		indices[i] = indexModel[i];
	}
	// Set up the description of the index buffer.
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if (FAILED(result)) {
		return false;
	}

	delete[] indices;
	indices = 0;
	delete[] vertex;
	vertex = 0;
	flagCreate = true;
	return true;
}