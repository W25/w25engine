////////////////////////////////////////////////////////////////////////////////
// Filename: lightmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _LIGHTMANAGERCLASS_H_
#define _LIGHTMANAGERCLASS_H_

class LightClass;

class LightManagerClass{
// ������� �������� ������
public:
	static LightManagerClass* GetInstance() {
		static LightManagerClass m_Instance;
		return &m_Instance;
	}
public:
	void Shutdown(void);
	LightClass* GetLight();
	LightClass* GetLight(int ID);
	LightClass* GetDirectionalLight(void);
	void SetDirectionalLight(LightClass* light);
	void AddLight(LightClass* light);
private:
	std::vector<LightClass*> m_Light;
	LightClass* m_DirectionalLight = nullptr;
private:
	// ������������ � �������� ������������ ���������� ��������
	LightManagerClass();
	~LightManagerClass(){};
	LightManagerClass(const LightManagerClass&){};
	LightManagerClass& operator=(LightManagerClass&) {};
};

#endif
