////////////////////////////////////////////////////////////////////////////////
// Filename: effectclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _EFFECTCLASS_H_
#define _EFFECTCLASS_H_

class BaseParticleSystemClass;

class EffectClass{
public:
	EffectClass();
	bool Initialize(void);
	void SetDirection(D3DXVECTOR3 dir);
	void SetPosition(D3DXVECTOR3 pos);
	void SetTimeLife(float valueTimeLife, bool immortal);
	void SetSpeed(float);
	void SetEmitting(bool);
	float GetSpeed();
	float GetTimeLife();
	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetDirection();
	bool GetFlagDeath();
	int GetID();
	void SetID(int id);
	void Update(CameraClass *p_Camera, float dTime);
	void Refresh(void);
	void Render(CameraClass *p_Camera);
	void AddParticleSystem(BaseParticleSystemClass* particleSystem);
	void Shutdown(void);
private:
	std::vector<BaseParticleSystemClass*> m_ParticleSystem;
	D3DXVECTOR3 position;
	D3DXVECTOR3 direction;
	D3DXVECTOR3 scale;
	float time;
	float timeLife;
	float speed;
	bool flagDeath;
	bool flagImmortality;
	int ID;
};

#endif