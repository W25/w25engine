////////////////////////////////////////////////////////////////////////////////
// Filename: texturemanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TEXTUREMANAGERCLASS_H_
#define _TEXTUREMANAGERCLASS_H_

class MaterialClass;
class TextureClass;
class TexturePack;

class TextureManagerClass{
// ������� �������� ������
public:
	static TextureManagerClass* GetInstance() {
		static TextureManagerClass m_Instance;
		return &m_Instance;
	}
public:
	bool Initialize(ID3D10Device*);
	void Shutdown(void);
	std::string Add(std::string);
	void Add(TexturePack inPackTexture);
	void Add(MaterialClass material);
	void Erase(std::string);
	void Erase(MaterialClass material);
	unsigned int GetCountPack(void);
	unsigned int GetCountLink(std::string);
	std::string GetPathTexture(unsigned int);
	ID3D10ShaderResourceView* GetTextureByItem(unsigned int);
	ID3D10ShaderResourceView* GetTexture(std::string);
private:
	ID3D10Device *p_Device;
	// ������ �� �����,���� � �����
	std::map<std::string,TexturePack> m_Pack;
private:
	// ������������ � �������� ������������ ���������� ��������
	TextureManagerClass() {};
	~TextureManagerClass() {};
	TextureManagerClass(const TextureManagerClass&) {};
	TextureManagerClass& operator=(TextureManagerClass&) {};
};


#endif