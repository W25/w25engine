////////////////////////////////////////////////////////////////////////////////
// Filename: stateclass_play.cpp
////////////////////////////////////////////////////////////////////////////////
#include "stateclass_play.h"

#include "scu.cpp"

////////////////////////////////////////////////////////////////////////////////
// ��������� ����
////////////////////////////////////////////////////////////////////////////////
// �����������
// ���������� ����������� � ���������� ���������� � ������
PlayState::PlayState(WCHAR* name):StateClass(name){
	m_Terrain = nullptr;
	m_Frustum = nullptr;
	m_MeshLine = nullptr;
	m_ModelManager = nullptr;
	m_EffectManager = nullptr;
	m_ShotManager = nullptr;
}

// ���������� ��������� (������������)
bool PlayState::InitializeState(void){
	m_Terrain = new TerrainClass();
	m_Terrain->Initialize(p_D3D->GetDevice(), p_ShaderManager, p_TextureManager, p_LightManager, "Map");
	m_Frustum = new FrustumClass();
	// ������������� ��������� ��������� ������
	m_Camera->SetPosition(D3DXVECTOR3(0.0f, 5.0f, 0.0f));
	m_Camera->SetRotation(D3DXVECTOR3(35.0f, 0.0f, 0.0f));
	m_Camera->Render();
	// ������� ������ �����
	LightClass light;

	// ������������ ������� �����
	light.SetAmbientColor(0.1250f, 0.1250f, 0.1250f, 1.00f);
	//light.SetAmbientColor(1.0f, 1.0f, 1.0f, 1.0f);
	light.SetDiffuseColor(1.0f, 1.0f, 1.0f, 1.0f);
	light.SetDirection(45.0f, 0.0f, 0.0f);
	light.SetSpecularColor(1.0f, 1.0f, 1.0f, 1.0f);
	light.SetSpecularPower(0.7f);
	p_LightManager->SetDirectionalLight(light);

	m_MeshLine = new MeshLineClass();
	m_MeshLine->Initialize(p_D3D->GetDevice(), 1000);

	m_ModelManager = new ModelManagerClass();
	if (m_ModelManager == nullptr) {
		return false;
	}
	m_ModelManager->Initialize(p_D3D->GetDevice(), p_MeshManager, p_ShaderManager, p_LightManager);



	m_Player = new PlayerClass();
	m_Player->Initialize(m_ModelManager, m_Camera, m_Terrain);


	ModelUnitClass model;
	PartModelClass partModel_1[2];
	FileInfoMeshClass infoMesh;
	
	
	infoMesh = p_MeshManager->LoadFileMesh("./data/PL/Legs_Transformer_Hvy.fbx");
	partModel_1[0].AddMesh(infoMesh.IDs[0]); // ������
	partModel_1[0].material.pathDiffuse = "./data/PL/Texture.png";
	partModel_1[0].m_BoneTree = p_MeshManager->GetSkeleton(infoMesh.IDs[0]);

	// �������� �����
	std::vector<AnimationClass>  animations;
	animations = p_MeshManager->LoadFileAnimation("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Walk.fbx");
	animations[0].Type = TypeAnimation::MOVE_UP;
	animations[0].maxFrame = 26.0f;
	animations[0].lastIndexFrame = 24;
	animations[0].widthFrame = 1.0f / 26.0;
	partModel_1[0].m_BoneTree->AddAnimation(animations[0]);

	// ����� �� �����
	animations = p_MeshManager->LoadFileAnimation("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Idle.fbx");
	animations[0].Type = TypeAnimation::IDLY;
	animations[0].maxFrame = 50.0f;
	animations[0].lastIndexFrame = 49;
	animations[0].widthFrame = 1.0f / 50.0;
	partModel_1[0].m_BoneTree->AddAnimation(animations[0]);
	// ������� �����
	animations = p_MeshManager->LoadFileAnimation("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Turn_L.fbx");
	animations[0].Type = TypeAnimation::TURN_L;
	animations[0].maxFrame = 30.0f;
	animations[0].lastIndexFrame = 29;
	animations[0].widthFrame = 1.0f / 50.0;
	partModel_1[0].m_BoneTree->AddAnimation(animations[0]);
	// ������� ������
	animations = p_MeshManager->LoadFileAnimation("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Turn_R.fbx");
	animations[0].Type = TypeAnimation::TURN_R;
	animations[0].maxFrame = 30.0f;
	animations[0].lastIndexFrame = 29;
	animations[0].widthFrame = 1.0f / 30.0;
	partModel_1[0].m_BoneTree->AddAnimation(animations[0]);

	animations = p_MeshManager->LoadFileAnimation("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Transform_to_Roller.fbx");
	animations[0].Type = TypeAnimation::TYPE_0;
	animations[0].maxFrame = 50.0f;
	animations[0].reloadIndexFrame = 48;
	animations[0].lastIndexFrame = 48;
	animations[0].firstIndexFrame = 0;
	animations[0].widthFrame = 1.0f / 50.0;
	partModel_1[0].m_BoneTree->AddAnimation(animations[0]);
	
	animations = p_MeshManager->LoadFileAnimation("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Transform_to_Spider.fbx");
	animations[0].Type = TypeAnimation::TYPE_1;
	animations[0].maxFrame = 50.0f;
	animations[0].reloadIndexFrame = 48;
	animations[0].lastIndexFrame = 48;
	animations[0].firstIndexFrame = 0;
	animations[0].widthFrame = 1.0f / 50.0;
	partModel_1[0].m_BoneTree->AddAnimation(animations[0]);
	
	animations = p_MeshManager->LoadFileAnimation("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Strafe_L.fbx");
	animations[0].Type = TypeAnimation::STRAFE_L;
	partModel_1[0].m_BoneTree->AddAnimation(animations[0]);

	animations = p_MeshManager->LoadFileAnimation("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Strafe_R.fbx");
	animations[0].Type = TypeAnimation::STRAFE_R;
	partModel_1[0].m_BoneTree->AddAnimation(animations[0]);

	p_TextureManager->Add(partModel_1[0].material);
	model.m_PartModels.push_back(partModel_1[0]);

	infoMesh = p_MeshManager->LoadFileMesh("./data/PL/Cockpits_Spiders.fbx");
	partModel_1[1].AddMesh(infoMesh.IDs[0]); // ������
	partModel_1[1].m_BoneTree = p_MeshManager->GetSkeleton(infoMesh.IDs[0]);
	partModel_1[1].material.pathDiffuse = "./data/PL/Texture.png";
	partModel_1[1].position.y = 0.0f;
	model.m_PartModels.push_back(partModel_1[1]);

	model.position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	model.scale *= 0.005f;
	// ���������� ID
	m_Player->modelID = m_ModelManager->AddModel(model);

	m_MasPlayer = new PlayerClass[10];
	for (int i = 0; i < 10; i++) {
		m_MasPlayer[i].Initialize(m_ModelManager, m_Camera, m_Terrain);
		model.position.x = (float)i*4;
		m_MasPlayer[i].modelID = m_ModelManager->AddModel(model);
	}

	PartModelClass partModel_2[1];
	model.m_PartModels.clear();
	infoMesh = p_MeshManager->LoadFileMesh("./data/smooth_icosphere.obj");
	partModel_2[0].AddMesh(infoMesh.IDs[0]); // ������
	partModel_2[0].material.pathDiffuse = "./data/RED.bmp";
	partModel_2[0].m_BoneTree = p_MeshManager->GetSkeleton(infoMesh.IDs[0]);
	p_TextureManager->Add(partModel_2[0].material);
	model.m_PartModels.push_back(partModel_2[0]);
	model.scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	m_ModelManager->AddModel(model);

	m_EffectManager = new EffectManagerClass();
	m_EffectManager->Initialize(p_D3D, p_TextureManager, p_ShaderManager);
	
	//WeaponClass weapon1;
	//WeaponClass weapon2;
	//// ������� ������
	//EffectClass effect1;
	//BaseParticleSystemClass *particle1 = new SmokeParticleSystemClass();
	//particle1->SetEmitting(true);
	//effect1.AddParticleSystem(particle1);
	//effect1.SetTimeLife(1, true);
	//weapon1.effectID = m_EffectManager->AddEffect(effect1);
	//weapon1.position = D3DXVECTOR3(-0.5f, 1.0f, 0.0f);
	//weapon1.local = D3DXVECTOR3(-0.5f, 1.0f, 0.0f);
	//
	//EffectClass effect2;
	//BaseParticleSystemClass *particle2 = new BaseParticleSystemClass();
	//particle2->SetEmitting(true);
	//effect2.AddParticleSystem(particle2);
	//effect2.SetTimeLife(1, true);
	//weapon2.effectID = m_EffectManager->AddEffect(effect2);
	//weapon2.position = D3DXVECTOR3(0.5f, 1.0f, 0.0f);
	//weapon2.local = D3DXVECTOR3(0.5f, 1.0f, 0.0f);

	//m_Player->AddWeapon(weapon1);
	//m_Player->AddWeapon(weapon2);

	m_ShotManager = new ShotManagerClass();
	m_ShotManager->Initialize(NULL, m_EffectManager, m_Terrain);

	return true;
}

// �������� ��������� (������������ ������)
bool PlayState::Exit(){
	Shutdown();
	return true;
}

bool PlayState::Update(InputClass* p_Input, float time){
	return UpdatePrimary(p_Input, time);
}
// ���������� �����
bool PlayState::UpdatePrimary(InputClass* p_Input, float dTime){
	// StateClass
	runTime += dTime;

	int MouseX = 0;
	int MouseY = 0;
	int dMouseX;
	int dMouseY;
	p_Input->GetMouseLocation(MouseX, MouseY);
	p_Input->GetMouseMove(dMouseX, dMouseY);
	m_GuiManager->Update(p_Input, MouseX, MouseY);

	float speed = 25.0f;


	if (p_Input->IsMouseDown(MOUSE_RIGHT) == true) {
		m_Camera->AddRotation(D3DXVECTOR3((float)dMouseY / 2.13f, (float)dMouseX / 3.8f, 0.0f));
	}
	if (p_Input->IsKeyDown(DIK_W) == true) {
		m_Camera->MovePosition(speed*dTime);
	}
	if (p_Input->IsKeyDown(DIK_A) == true) {
		m_Camera->ShiftPosition(-speed * dTime, 0.0f);
	}
	if (p_Input->IsKeyDown(DIK_D) == true) {
		m_Camera->ShiftPosition(speed*dTime, 0.0f);
	}
	if (p_Input->IsKeyDown(DIK_S) == true) {
		m_Camera->MovePosition(-speed * dTime);
	}
	if (p_Input->IsKeyDown(DIK_Q) == true) {
		m_Player->AddRotation(dTime * 10.0f);
	}
	if (p_Input->IsKeyDown(DIK_E) == true) {
		m_Player->AddRotation(-dTime * 10.0f);
	}
	if (p_Input->IsKeyDown(DIK_Z) == true) {
		D3DXVECTOR3 mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 end(0.0f, 0.0f, 0.0f);
		start = m_Camera->GetPosition();
		m_Terrain->GetIntersects(start, mouseDir, &end);
		m_Terrain->AlterArea(p_D3D->GetDevice(), end, -1.0f, 5, alter_argument::POSSITION);
	}
	if (p_Input->IsKeyDown(DIK_X) == true) {
		D3DXVECTOR3 mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 end(0.0f, 0.0f, 0.0f);
		start = m_Camera->GetPosition();
		m_Terrain->GetIntersects(start, mouseDir, &end);
		m_Terrain->AlterArea(p_D3D->GetDevice(), end, 1.0f, 5, alter_argument::POSSITION);
	}
	if (p_Input->IsMouseDown(MOUSE_LEFT) == true) {
		// ��������� ���
		D3DXVECTOR3 mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		ModelUnitClass *model = m_ModelManager->GetModel(0);
		D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 end(0.0f, 0.0f, 0.0f);
		start = m_Camera->GetPosition();
		m_Terrain->GetIntersects(start, mouseDir, &end);
		
		D3DXMATRIX rotationMatrix;
		D3DXVECTOR3 direction(0.0f, 1.0f, 0.0f);
		//// Create the rotation matrix from the yaw, pitch, and roll values.<^*
		D3DXMatrixRotationYawPitchRoll(&rotationMatrix, model->m_PartModels[1].rotation.y, 0.0f, 0.0f);
		//// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
		D3DXVec3TransformCoord(&direction, &direction, &rotationMatrix);
		D3DXVec3Normalize(&direction, &direction);
		D3DXVECTOR3 markDirection = direction - (model->position - end);
		D3DXVec3Normalize(&markDirection, &markDirection);
		model->m_PartModels[1].rotation.x = atan2(markDirection.x, markDirection.z) - model->rotation.x;

		m_Player->SetMoveTo(end);
		// ������
		start = model->position;
		m_MeshLine->Update(start, end);
		model = m_ModelManager->GetModel(1);
		model->position = end;
		ShotBaseClass shot = m_ShotManager->GetShotToIndex(0);
		shot.targetPos = end;
		m_ShotManager->SetShotToIndex(shot, 0);
		shot = m_ShotManager->GetShotToIndex(1);
		shot.targetPos = end;
		m_ShotManager->SetShotToIndex(shot, 1);
	}

	if (p_Input->IsKeyPress(DIK_1) == true) {
		WeaponClass weapon;
		weapon = m_Player->GetWeapon(0);
		m_EffectManager->Refresh(weapon.effectID);
		m_EffectManager->SetEmitting(weapon.effectID, true);
		ShotBaseClass shot;
		shot.effectID = weapon.effectID;
		shot.speed = 15.0f;
		shot.targetPos = m_ModelManager->GetModel(1)->position;
		shot.position = weapon.position;
		shot.timeLife = 10.0f;
		m_ShotManager->AddShot(shot);
	}
	if (p_Input->IsKeyPress(DIK_2) == true) {
		WeaponClass weapon;
		weapon = m_Player->GetWeapon(1);
		m_EffectManager->Refresh(weapon.effectID);
		m_EffectManager->SetEmitting(weapon.effectID, true);
		ShotBaseClass shot;
		shot.effectID = weapon.effectID;
		shot.speed = 15.0f;
		shot.targetPos = m_ModelManager->GetModel(1)->position;
		shot.position = weapon.position;
		shot.timeLife = 10.0f;
		m_ShotManager->AddShot(shot);
	}
	static int itemDebug = 0;
	if (p_Input->IsKeyPress(DIK_3) == true) {
		itemDebug--;
		switch (itemDebug) {
			case 0:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::IDLY);
				break;
			case 1:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::MOVE_UP);
				break;
			case 2:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TURN_L);
				break;
			case 3:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TURN_R);
				break;
			case 4:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TYPE_0);
				break;
			case 5:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TYPE_1);
				break;
			case 6:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::STRAFE_L);
				break;
			case 7:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::STRAFE_R);
				break;
		}
	}
	if (p_Input->IsKeyPress(DIK_4) == true) {
		itemDebug++;
		switch (itemDebug) {
			case 0:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::IDLY);
				break;
			case 1:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::MOVE_UP);
				break;
			case 2:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TURN_L);
				break;
			case 3:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TURN_R);
				break;
			case 4:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TYPE_0);
				break;
			case 5:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TYPE_1);
				break;
			case 6:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::STRAFE_L);
				break;
			case 7:
				m_ModelManager->RefreshAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::STRAFE_R);
				break;
		}
	}
	static float timeCtrlFrame;
	timeCtrlFrame = 0.0f;
	if (p_Input->IsKeyDown(DIK_SUBTRACT) == true) {
		timeCtrlFrame = -0.01f;
	}
	if (p_Input->IsKeyDown(DIK_ADD) == true) {
		timeCtrlFrame = 0.01f;
	}
	
	switch (itemDebug) {
		case 0:
			m_ModelManager->PlayAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::IDLY, dTime);
			break;
		case 1:
			m_ModelManager->PlayAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::MOVE_UP, timeCtrlFrame);
			break;
		case 2:
			m_ModelManager->PlayAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TURN_L, dTime);
			break;
		case 3:
			m_ModelManager->PlayAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TURN_R, dTime);
			break;
		case 4:
			m_ModelManager->PlayAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TYPE_0, dTime);
			break;
		case 5:
			m_ModelManager->PlayAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::TYPE_1, dTime);
			break;
		case 6:
			m_ModelManager->PlayAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::STRAFE_L, dTime);
			break;
		case 7:
			m_ModelManager->PlayAnimation(m_Player->modelID, TypeBody::LEGS, TypeAnimation::STRAFE_R, dTime);
			break;
	}
	
	m_MeshLine->Update(m_Player->GetPosition(), m_Player->markMove);
	m_Player->MoveTo(dTime);
	//for (int i = 0; i < 10; i++){
	//	m_MasPlayer[i].MoveTo(dTime);
	//}
	m_ShotManager->Update(dTime);
	m_EffectManager->Update(m_Camera, dTime);
	return true;
}
bool PlayState::Render() {
	bool result;
	result = RenderPrimary();
	return result;
}
// ���������
bool PlayState::RenderPrimary() {
	D3DXMATRIX orthoMatrix, worldMatrix, viewMatrix, projectionMatrix, translateMatrix, guiMatrix;
	// ��������� ��������� ������
	m_Camera->Render();

	m_Camera->GetWorldMatrix(&worldMatrix);
	m_Camera->GetOrthoMatrix(&orthoMatrix);
	m_Camera->GetProjectionMatrix(&projectionMatrix);
	m_Camera->GetViewMatrix(&viewMatrix);
	m_Camera->GetMatrixGUI(&guiMatrix);

	m_Frustum->ConstructFrustum(100.0f, projectionMatrix, viewMatrix);
	m_Terrain->Render(p_D3D->GetDevice(), worldMatrix, viewMatrix, projectionMatrix, m_Frustum);

	m_MeshLine->Render(p_D3D->GetDevice());
	p_ShaderManager->RenderTextureShader(m_MeshLine->m_IndexCount, worldMatrix,
		viewMatrix, projectionMatrix, MaterialClass());
	
	//m_ModelManager->Render(1, m_Camera);
	m_Player->Render(m_Camera);


	for (int i = 0; i < 10; i++) {
		m_MasPlayer[i].Render(m_Camera);
	}


	p_D3D->DisableWriteZBuffer();
		m_EffectManager->Render(m_Camera);
	p_D3D->EnableWriteZBuffer();
	
	///////////////////////////////////////////////////////////////////////////////////
	// GUI 2D �������
	///////////////////////////////////////////////////////////////////////////////////	
	p_D3D->TurnZBufferOff();
	// ��� ����� ����������� � 2� ���������, �� ����� ���� ��� ��������� ����� ������������� �����
	m_GuiManager->Render(p_D3D->GetDevice(), worldMatrix, guiMatrix, orthoMatrix);
	p_D3D->TurnZBufferOn();
	
	return true;
}
bool PlayState::RenderInTexture() {
	return true;
}
bool PlayState::RenderModelInTexture(int) {

	return true;
}
// ������������� ��������
bool PlayState::Shutdown(){
	// StateClass
	runTime = 0;
	if (m_Terrain != nullptr) {
		m_Terrain->Shutdown();
		delete m_Terrain;
		m_Terrain = nullptr;
	}
	if (m_Frustum != nullptr) {
		delete m_Frustum;
		m_Frustum = nullptr;
	}
	if (m_MeshLine != nullptr) {
		delete m_MeshLine;
		m_MeshLine = nullptr;
	}
	if (m_ModelManager != nullptr) {
		m_ModelManager->Shutdown();
		delete m_ModelManager;
		m_ModelManager = nullptr;
	}
	if (m_EffectManager != nullptr) {
		m_EffectManager->Shutdown();
		delete m_EffectManager;
		m_EffectManager = nullptr;
	}
	if (m_ShotManager != nullptr) {
		m_ShotManager->Shutdown();
		delete m_ShotManager;
		m_ShotManager = nullptr;
	}
	return true;
}
