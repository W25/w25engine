////////////////////////////////////////////////////////////////////////////////
// Filename: statemanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "statemanagerclass.h"
#include "stdafx.h"



bool StateManagerClass::Initialize(HWND* hwnd, D3DClass* d3d, MsgManagerClass* p_MsgMng,
									MeshManagerClass* p_MeshMng, ModelManagerClass* p_ModelManager, TextureManagerClass* p_TextureMng,
									LightManagerClass* p_LightMng, ShaderManagerClass* p_ShaderMng) {
	p_HWND = hwnd;
	p_D3D = D3DClass::GetInstance();
	//p_MsgManager = MsgManagerClass::GetInstance();
	p_MeshManager = MeshManagerClass::GetInstance();
	p_TextureManager = TextureManagerClass::GetInstance();
	p_LightManager = LightManagerClass::GetInstance();
	p_ShaderManager = ShaderManagerClass::GetInstance();
	p_ModelManager = ModelManagerClass::GetInstance();
	return true;

}
// ���������� 
// ��������� ���� ���������
bool StateManagerClass::Render(){
	bool result=false;
	//�������� ������ �������� ���������
	p_D3D->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);
	for (unsigned int i=0; i<m_States.size(); i++){
		if (!m_States[i]->GetMode().pause){	
			result = m_States[i]->Render();
			// ��������� ������ ���������� ����� ������ ������ ��������� � ����������
			if (result == false) {
				p_D3D->EndScene();
				return result;
			}
		}

	}
	// �������� ��� �����.
	p_D3D->EndScene();
	return true;
}
// ����������, ������������� ������ ������ ��������� �������� ������� ���������
// ��������� ���� ���������
bool StateManagerClass::Frame(InputClass* p_Input, float time){
	bool result = false;
	//�������� ���������� ������ �������� ���������

	for (unsigned int i=0; i<m_States.size(); i++){
		if (!m_States[i]->GetMode().pause){	
			result = m_States[i]->Update(p_Input, time);	
			// ��������� ������ ���������� ����� ������ ������ ��������� � ����������
			if (!result) {
				return result;
			}
		}

	}
		
	return result;	
}
bool StateManagerClass::Inspector(){
	return true;
}
// ������� ��� ���������, ���������� ���������� ��� ������
void StateManagerClass::Shutdown()
{
	// �������� ����� �� ��������� � ������� �� �� �������
	while ( !m_States.empty() ) {
		m_States.back()->Exit();
		m_States.pop_back(); 
	}
	m_States.clear();
}
bool StateManagerClass::ChangeState(StateClass* state){
	// ����� �� �������� ���������
	if ( !m_States.empty() ) {
		m_States.back()->Exit();
		m_States.pop_back(); 
	}
	// ������������� ������ ������
	m_States.push_back(state);
	return false;
}

// ���������� ������ ���������
bool StateManagerClass::PushState(StateClass* state){
	// ������������� ������ ������
	bool result = false;
	unsigned int number;
	// ���� ��������� � �����
	for (unsigned int i=0; i<m_States.size(); i++) {
		if (m_States[i]->GetNameState() == state->GetNameState()) {
			result = true;
			number = i;
			m_States[i]->Resume();
			break;
		}
	}
	// ���� ��������� ��� �� ���������� ��������� ��� � ����
	if (result==false){
		m_States.push_back(state);
		return m_States.back()->Initialize(p_HWND, p_D3D, nullptr, p_MeshManager, p_ModelManager,
											p_TextureManager, p_LightManager, p_ShaderManager);
	} 
	return true;
}
// ������� � �����
bool StateManagerClass::PopState(void){
	// ����� �� �������� ������
	if ( m_States.size()>0 ) {
		m_States.back()->Exit();
		m_States.pop_back();
	}
	// ������������ ����������
	/*if ( !mStates.empty() ) 
	{
		return mStates.back()->Resume();
	}*/
	return true;
}
// �������� ��������� �� �����
bool StateManagerClass::EraseState(WCHAR* name){
	for (unsigned int i=0; i<m_States.size(); i++){
		if (*m_States[i]->GetNameState() == *name){
			m_States[i]->Exit();
			m_States.erase(m_States.begin() + i);
		}
	}
	return true;
}
bool StateManagerClass::CheckStateName(WCHAR* name){
	for (unsigned int i=0; i<m_States.size(); i++){
		if (*m_States[i]->GetNameState() == *name){
			return true;
		}
	}
	return false;
}