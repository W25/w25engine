////////////////////////////////////////////////////////////////////////////////
// Filename: shotclass.cpp
////////////////////////////////////////////////////////////////////////////////

#include "shotbaseclass.h"
#include "stdafx.h"

ShotBaseClass::ShotBaseClass() {
	timeLife = 0.0f;
	speed = 1.0f;
	flagActive = true;
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	direction = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	targetPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	lengthPath = 0.0f;
	damage = 0.0f;
	miss = 0.0f;
	lengthTarget = 0.0f;
	effectID = -1;
	targetID = -1;
	sourceID = -1;
	typeShotID = -1;
}
void ShotBaseClass::Shutdown() {

}
void ShotBaseClass::Update(float deltaTime) {
	timeLife += deltaTime;
	direction = targetPos - position;
	D3DXVec3Normalize(&direction, &direction);

	position += direction*speed*deltaTime;
	D3DXVECTOR3 vecTarget = position - targetPos;
	lengthTarget = D3DXVec3Length(&vecTarget);

}
ShotBaseClass::~ShotBaseClass() {}
