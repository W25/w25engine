////////////////////////////////////////////////////////////////////////////////
// Filename: shotmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _SHOTMANAGERCLASS_H_
#define _SHOTMANAGERCLASS_H_


//////////////
// INCLUDES //
//////////////
#include <vector>
#include "shotbaseclass.h"
#include "terrainclass.h"

#include "effectmanagerclass.h"
////////////////////////////////////////////////////////////////////////////////
// Class name: shoymanagerclass
////////////////////////////////////////////////////////////////////////////////


class ShotManagerClass{
public:
	ShotManagerClass(void);
	bool AddShot(ShotBaseClass);
	bool Update(float);
	void Initialize(TerrainClass*);
	int GetEffectToIndex(int index);
	ShotBaseClass GetShotToIndex(int index);
	void  SetShotToIndex(ShotBaseClass shot, int index);
	void Shutdown();
	int GetCountShot();
private:
	TerrainClass* p_Terrain;
	std::vector<ShotBaseClass> m_Shot;
};
#endif