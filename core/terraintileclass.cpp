////////////////////////////////////////////////////////////////////////////////
// Filename: TerrainTileClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "terraintileclass.h"
#include "stdafx.h"


////////////////////////////////////////////////////////////////////////////////////////////
TerrainTileClass::TerrainTileClass(){
	m_MaterialIndex.clear();
	m_MaterialIndexCount = 0;
	m_VertexBuffer = 0;
	m_IndexBuffer = 0;
	m_Model = 0;
	m_MethodRender = 0;
	countCellColumn = 0;
	countCellRow = 0;
	m_VertexCount = 0;
	m_IndexCount = 0;
	sizeStepGrid = 0.250f;
	offsetTileLeft = 0.0f;
	offsetTileTop = 0.0f;
	sizeStepGrid = 0.0f;
	countIndexColumn = 0;
	countIndexRow = 0;
	flagInitialize = false;
	flagModel = false;
}
int TerrainTileClass::GetCountMaterial() {
	return (int)m_MaterialIndex.size();
}
TerrainTileClass::TerrainTileClass(const TerrainTileClass&){
}

TerrainTileClass::~TerrainTileClass(){
}

// ���������� ������ ��������� �� ����������� ������
int TerrainTileClass::GetMaterial(int index) {
	return m_MaterialIndex[index];
}

// ����������� ����� �����
bool TerrainTileClass::Initialize(ID3D10Device* device) {
	bool result = flagModel;
	// ������� ������
	if (result == true) {
		ReleaseBuffers();
		InitializeBuffers(device);
		flagInitialize = true;
		return true;
	}

	return false;
}
void TerrainTileClass::Clear() {
	flagInitialize = false;
	flagModel = false;
	m_MethodRender = 0;
	m_MaterialIndex.clear();
	m_MaterialIndexCount = 0;
	m_IndexCount = 0;
	m_VertexCount = 0;
	// ����������� �����
	ReleaseBuffers();
	// ����������� ������
	ReleaseModel();
}
void TerrainTileClass::Shutdown(){
	Clear();
	return;
}


void TerrainTileClass::Render(ID3D10Device* device){
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(device);
	return;
}
void TerrainTileClass::RenderRound(ID3D10Device* ) {
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	//m_Round.Render(device);
	return;
}
void TerrainTileClass::RenderLine(ID3D10Device* ) {
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	//m_Line.Render(device);
	return;
}


int TerrainTileClass::GetIndexCount(){
	return m_IndexCount;
}

// ������� ������ �����������
bool TerrainTileClass::CreatePlane(float step, float offset_X, float offset_Y,
									int countColumn, int countRow, SpriteDataClass sprite) {
	// ���������� ������ ������
	Clear();
	m_Sprite = sprite;
	sizeStepGrid = step;
	offsetTileLeft = offset_X;
	offsetTileTop = offset_Y;
	countCellColumn = countColumn;
	countCellRow = countRow;
	countIndexColumn = countCellColumn;
	countIndexRow = countCellRow;
	m_VertexCount = countIndexColumn * countIndexRow;
	D3DXVECTOR2 shiftPosTexture;
	shiftPosTexture.x = (float)(sprite.frameItemX * sprite.sizePxFrameX + 
						(sprite.frameItemX * 2 + 1) * sprite.sizeLineGrid) / (float)sprite.sizePxImageX;

	shiftPosTexture.y = (float)(sprite.frameItemY * sprite.sizePxFrameY +
						(sprite.frameItemY * 2 + 1) * sprite.sizeLineGrid) / (float)sprite.sizePxImageY;
	float microStepTextureX = ((float)sprite.sizePxFrameX / (float)sprite.sizePxImageX) / (float)(countIndexRow - 1);
	float microStepTextureY = ((float)sprite.sizePxFrameY / (float)sprite.sizePxImageY) / (float)(countIndexColumn - 1 );
	// ��������� ���������
	// �������� ������
	m_Model = new TerrainTileClass::VertexType[m_VertexCount];
	// ���������� ������ ������
	for (int j = 0; j < countIndexRow; j++) {
		for (int i = 0; i < countIndexColumn; i++) {
			int index = j * countIndexColumn + i;
			m_Model[index].position.x = (float)i*sizeStepGrid + offset_X;
			m_Model[index].position.y = 0.0f;
			m_Model[index].position.z = (float)j*sizeStepGrid + offset_Y;
			m_Model[index].normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
			m_Model[index].tangent = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
			m_Model[index].binormal = D3DXVECTOR3(0.0f, 0.0f, -1.0f);
			m_Model[index].texture = shiftPosTexture +
									 D3DXVECTOR2(microStepTextureX * i, 
										         microStepTextureY *(countIndexRow - j - 1));
			m_Model[index].mask = D3DXVECTOR4(0.0f, 1.0f, 0.0f, 0.0f);
		}
	}
	// ��������� ����� ���������
	m_MaterialIndex.push_back(0);
	m_MaterialIndex.push_back(1);
	m_MaterialIndex.push_back(2);
	m_MaterialIndexCount = 3;
	flagModel = true;
	return true;
}
// ��������� �������� � ����
bool TerrainTileClass::SaveTerrainMap(std::ofstream& fileSaveTerrain) {

	// ���������� ���������� ����������
	fileSaveTerrain.write((char*)&m_MaterialIndexCount, sizeof(int));
	// ���������� ������� ������� ���������
	for (int i = 0; i < m_MaterialIndexCount; i++) {
		fileSaveTerrain.write((char*)&m_MaterialIndex[i], sizeof(int));
	}
	// ���������� ������ �����
	fileSaveTerrain.write((char*)&countCellColumn, sizeof(LONG));
	fileSaveTerrain.write((char*)&countCellRow, sizeof(LONG));
	fileSaveTerrain.write((char*)&sizeStepGrid, sizeof(float));
	fileSaveTerrain.write((char*)&offsetTileLeft, sizeof(float));
	fileSaveTerrain.write((char*)&offsetTileTop, sizeof(float));
	// ���������� ��� ������� �����
	for (unsigned int i = 0; i < m_VertexCount; i++) {
		// ����������
		fileSaveTerrain.write((char*)&m_Model[i].position, sizeof(D3DXVECTOR3));
		// ������� ��������� ��������
		fileSaveTerrain.write((char*)&m_Model[i].normal, sizeof(D3DXVECTOR3));
		fileSaveTerrain.write((char*)&m_Model[i].tangent, sizeof(D3DXVECTOR3));
		fileSaveTerrain.write((char*)&m_Model[i].binormal, sizeof(D3DXVECTOR3));
		// ��� ���������� ��������
		fileSaveTerrain.write((char*)&m_Model[i].texture, sizeof(D3DXVECTOR2));
		// ������� ��������
		fileSaveTerrain.write((char*)&m_Model[i].mask, sizeof(D3DXVECTOR4));
	}

	return true;
}
// ��������� �������� �� ������� ������
bool TerrainTileClass::LoadTerrainMap(std::ifstream& fileOpenTerrain){
	// ������� ������ ���� (����� ��������� ��������� �����)
	Shutdown();
	// ��������� ���������� ����������
	fileOpenTerrain.read((char*)&m_MaterialIndexCount, sizeof(int));
	// ��������� �� ������� ���������
	for (int i = 0; i < m_MaterialIndexCount; i++) {
		int indexMaterial;
		// ��������� ������ ����������
		fileOpenTerrain.read((char*)&indexMaterial, sizeof(int));
		// ��������� ����� ���������� �����
		m_MaterialIndex.push_back(indexMaterial);
	}
	// ��������� ������ �����
	fileOpenTerrain.read((char*)&countCellColumn, sizeof(LONG));
	fileOpenTerrain.read((char*)&countCellRow, sizeof(LONG));
	fileOpenTerrain.read((char*)&sizeStepGrid, sizeof(float));
	fileOpenTerrain.read((char*)&offsetTileLeft, sizeof(float));
	fileOpenTerrain.read((char*)&offsetTileTop, sizeof(float));
	countIndexColumn = countCellColumn + 1;
	countIndexRow = countCellRow + 1;
	m_VertexCount = countIndexColumn*countIndexRow;
	// �������� ������ ��� ������ �����
	m_Model = new TerrainTileClass::VertexType[m_VertexCount];
	if (m_Model == 0) {
		return false;
	}
	// ��������� ��� ������� �����
	for (unsigned int i = 0; i < m_VertexCount; i++) {
		// ����������
		fileOpenTerrain.read((char*)&m_Model[i].position, sizeof(D3DXVECTOR3));
		// ������� ��������� ��������
		fileOpenTerrain.read((char*)&m_Model[i].normal, sizeof(D3DXVECTOR3));
		fileOpenTerrain.read((char*)&m_Model[i].tangent, sizeof(D3DXVECTOR3));
		fileOpenTerrain.read((char*)&m_Model[i].binormal, sizeof(D3DXVECTOR3));
		// ��� ���������� ��������
		fileOpenTerrain.read((char*)&m_Model[i].texture, sizeof(D3DXVECTOR2));
		// ������� ��������
		fileOpenTerrain.read((char*)&m_Model[i].mask, sizeof(D3DXVECTOR4));
	}
	flagModel = true;
	return true;
}
// ���������� ������ ������� �� ��������� �� �������
bool TerrainTileClass::GetIndexVertex(int *index, D3DXVECTOR3 point, int offsetIndexColumn, int offsetIndexRow) {
	(*index) = 0;
	int indexColumn = (int)((point.x - offsetTileLeft) / sizeStepGrid) + offsetIndexColumn;
	int indexRow = (int)((point.z - offsetTileTop) / sizeStepGrid) + offsetIndexRow;
	// ����� ��������� �� �����
	if (indexColumn >= 0 && indexColumn < countIndexColumn &&
		indexRow >= 0 && indexRow < countIndexRow) {
		(*index) = indexRow * countIndexColumn + indexColumn;
		return true;
	}
	return false;
}

// ���������� ������ 
bool TerrainTileClass::GetHeight(D3DXVECTOR3 point, float *height) {
	(*height) = 0.0f;
	
	int index1;				
	int index2;				
	int index3;				
	int index4;				
	if (GetIndexVertex(&index1, point, 0, 0) == false ||
		GetIndexVertex(&index2, point, 1, 0) == false ||
		GetIndexVertex(&index3, point, 0, 1) == false ||
		GetIndexVertex(&index4, point, 1, 1) == false) {
		return false;
	}

	// ���������� �� ����� ������� ����� ����� (������� ������������ � ���������)
	float a = (m_Model[index1].position.x - point.x) *
		(m_Model[index2].position.z - m_Model[index1].position.z) -
		(m_Model[index2].position.x - m_Model[index1].position.x) *
		(m_Model[index1].position.z - point.z);
	float b = (m_Model[index2].position.x - point.x) *
		(m_Model[index4].position.z - m_Model[index2].position.z) -
		(m_Model[index4].position.x - m_Model[index2].position.x) *
		(m_Model[index2].position.z - point.z);
	float c = (m_Model[index4].position.x - point.x) *
		(m_Model[index1].position.z - m_Model[index4].position.z) -
		(m_Model[index1].position.x - m_Model[index4].position.x) *
		(m_Model[index4].position.z - point.z);

	D3DXVECTOR3 v1, v2, v3;
	// ���������� � ������ �������� ����������� �� �����
	if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0)) {
		v1 = m_Model[index1].position;
		v2 = m_Model[index2].position;
		v3 = m_Model[index4].position;
	}else {
		v1 = m_Model[index4].position;
		v2 = m_Model[index1].position;
		v3 = m_Model[index3].position;
	}	
	// ����������� ��������� y �� ���������
	float k1 = (v2.z - v1.z)*(v3.x - v1.x) - (v2.x - v1.x)*(v3.z - v1.z);
	float k2 = (point.x - v1.x)*((v2.z - v1.z)*(v3.y - v1.y) - (v2.y - v1.y)*(v3.z - v1.z));
	float k3 = (point.z - v1.z)*((v2.y - v1.y)*(v3.x - v1.x) - (v2.x - v1.x)*(v3.y - v1.y));
	(*height) = (k2 + k3) / k1 + v1.y;

	return true;
}
			
// ���������� ������ � ���������� ������� � ���� �����
bool TerrainTileClass::GetHeight(D3DXVECTOR3 point, float *height, D3DXVECTOR3 *normal) {
	(*height) = 0.0f;
	int index1;
	int index2;
	int index3;
	int index4;
	if (GetIndexVertex(&index1, point, 0, 0) == false ||
		GetIndexVertex(&index2, point, 1, 0) == false ||
		GetIndexVertex(&index3, point, 0, 1) == false ||
		GetIndexVertex(&index4, point, 1, 1) == false) {
		return false;
	}
	// ���������� �� ����� ������� ����� ����� (������� ������������ � ���������)
	float a = (m_Model[index1].position.x - point.x) *
		(m_Model[index2].position.z - m_Model[index1].position.z) -
		(m_Model[index2].position.x - m_Model[index1].position.x) *
		(m_Model[index1].position.z - point.z);
	float b = (m_Model[index2].position.x - point.x) *
		(m_Model[index4].position.z - m_Model[index2].position.z) -
		(m_Model[index4].position.x - m_Model[index2].position.x) *
		(m_Model[index2].position.z - point.z);
	float c = (m_Model[index4].position.x - point.x) *
		(m_Model[index1].position.z - m_Model[index4].position.z) -
		(m_Model[index1].position.x - m_Model[index4].position.x) *
		(m_Model[index4].position.z - point.z);
	
	D3DXVECTOR3 v1, v2, v3;
	// ���������� � ������ �������� ����������� �� �����
	if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0)) {
		v1 = m_Model[index1].position;
		v2 = m_Model[index2].position;
		v3 = m_Model[index4].position;
		D3DXVECTOR3 vec1 = (v1 - v2);
		D3DXVECTOR3 vec2 = (v3 - v2);
		D3DXVec3Cross(&(*normal), &vec1, &vec2);
	}
	else {
		v1 = m_Model[index4].position;
		v2 = m_Model[index3].position;
		v3 = m_Model[index1].position;
		D3DXVECTOR3 vec1 = (v2 - v1);
		D3DXVECTOR3 vec2 = (v2 - v3);
		D3DXVec3Cross(&(*normal), &vec1, &vec2);
	}
	
	// ����������� ��������� y �� ���������
	float k1 = (v2.z - v1.z)*(v3.x - v1.x) - (v2.x - v1.x)*(v3.z - v1.z);
	float k2 = (point.x - v1.x)*((v2.z - v1.z)*(v3.y - v1.y) - (v2.y - v1.y)*(v3.z - v1.z));
	float k3 = (point.z - v1.z)*((v2.y - v1.y)*(v3.x - v1.x) - (v2.x - v1.x)*(v3.y - v1.y));
	(*height) = (k2 + k3) / k1 + v1.y;
	D3DXVec3Normalize(&(*normal), &(*normal));

	return true;
}
// ���������� ������� � �����
bool TerrainTileClass::GetNormal(D3DXVECTOR3 point, D3DXVECTOR3 *normal) {

	int index1;
	int index2;
	int index3;
	int index4;
	if (GetIndexVertex(&index1, point, 0, 0) == false ||
		GetIndexVertex(&index2, point, 1, 0) == false ||
		GetIndexVertex(&index3, point, 0, 1) == false ||
		GetIndexVertex(&index4, point, 1, 1) == false) {
		return false;
	}
	// ���������� �� ����� ������� ����� ����� (������� ������������ � ���������)
	float a = (m_Model[index1].position.x - point.x) *
		(m_Model[index2].position.z - m_Model[index1].position.z) -
		(m_Model[index2].position.x - m_Model[index1].position.x) *
		(m_Model[index1].position.z - point.z);
	float b = (m_Model[index2].position.x - point.x) *
		(m_Model[index4].position.z - m_Model[index2].position.z) -
		(m_Model[index4].position.x - m_Model[index2].position.x) *
		(m_Model[index2].position.z - point.z);
	float c = (m_Model[index4].position.x - point.x) *
		(m_Model[index1].position.z - m_Model[index4].position.z) -
		(m_Model[index1].position.x - m_Model[index4].position.x) *
		(m_Model[index4].position.z - point.z);

	D3DXVECTOR3 v1, v2, v3;
	// ���������� � ������ �������� ����������� �� �����
	if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0)) {
		v1 = m_Model[index1].position;
		v2 = m_Model[index2].position;
		v3 = m_Model[index4].position;
		D3DXVECTOR3 vec1 = (v1 - v2);
		D3DXVECTOR3 vec2 = (v3 - v2);
		D3DXVec3Cross(&(*normal), &vec1, &vec2);
	}
	else {
		v1 = m_Model[index4].position;
		v2 = m_Model[index3].position;
		v3 = m_Model[index1].position;
		D3DXVECTOR3 vec1 = (v2 - v1);
		D3DXVECTOR3 vec2 = (v2 - v3);
		D3DXVec3Cross(&(*normal), &vec1, &vec2);
	}

	// ����������� ��������� y �� ���������
	//float k1 = (v2.z - v1.z)*(v3.x - v1.x) - (v2.x - v1.x)*(v3.z - v1.z);
	//float k2 = (point.x - v1.x)*((v2.z - v1.z)*(v3.y - v1.y) - (v2.y - v1.y)*(v3.z - v1.z));
	//float k3 = (point.z - v1.z)*((v2.y - v1.y)*(v3.x - v1.x) - (v2.x - v1.x)*(v3.y - v1.y));
	D3DXVec3Normalize(&(*normal), &(*normal));
	return true;
}

// �������� ���� ��������� ������ �� ���������
void TerrainTileClass::AlterPoint(unsigned int index, float value, alter_argument argument){
	// ����������� ������, �� ������� �� �������
	if ((index >= 0) && (index < m_VertexCount)) {
		// ���������� ���������� ��������
		switch (argument) {
			case alter_argument::POSSITION:
				m_Model[index].position.y += value;
				break;
			case alter_argument::DEFAULT:
				break;
			case alter_argument::MASK1:
				m_Model[index].mask.x += value;
				m_Model[index].mask.y -= value;
				m_Model[index].mask.z -= value;
				m_Model[index].mask.w -= value;
				if (m_Model[index].mask.x > 1.0f) {
					m_Model[index].mask.x = 1.0;
				}
				if (m_Model[index].mask.y < 0.0f) {
					m_Model[index].mask.y = 0.0;
				}
				if (m_Model[index].mask.z < 0.0f) {
					m_Model[index].mask.z = 0.0;
				}
				if (m_Model[index].mask.w < 0.0f) {
					m_Model[index].mask.w = 0.0;
				}
				D3DXVec4Normalize(&m_Model[index].mask, &m_Model[index].mask);
				break;
			case alter_argument::MASK2:
				m_Model[index].mask.y += value;
				m_Model[index].mask.x -= value;
				m_Model[index].mask.z -= value;
				m_Model[index].mask.w -= value;
				if (m_Model[index].mask.y > 1.0f) {
					m_Model[index].mask.y = 1.0;
				}
				if (m_Model[index].mask.x < 0.0f) {
					m_Model[index].mask.x = 0.0f;
				}
				if (m_Model[index].mask.z < 0.0f) {
					m_Model[index].mask.z = 0.0f;
				}
				if (m_Model[index].mask.w < 0.0f) {
					m_Model[index].mask.w = 0.0f;
				}
				D3DXVec4Normalize(&m_Model[index].mask, &m_Model[index].mask);
				break;
			case alter_argument::MASK3:
				m_Model[index].mask.z += value;
				m_Model[index].mask.y -= value;
				m_Model[index].mask.x -= value;
				m_Model[index].mask.w -= value;
				if (m_Model[index].mask.z > 1.0f) {
					m_Model[index].mask.z = 1.0f;
				}
				if (m_Model[index].mask.y < 0.0f) {
					m_Model[index].mask.y = 0.0f;
				}
				if (m_Model[index].mask.x < 0.0f) {
					m_Model[index].mask.x = 0.0f;
				}
				if (m_Model[index].mask.w < 0.0f) {
					m_Model[index].mask.w = 0.0f;
				}
				D3DXVec4Normalize(&m_Model[index].mask, &m_Model[index].mask);
				break;
			case alter_argument::MASK4:
				m_Model[index].mask.w += value;
				m_Model[index].mask.y -= value;
				m_Model[index].mask.z -= value;
				m_Model[index].mask.x -= value;
				if (m_Model[index].mask.w > 1.0f) {
					m_Model[index].mask.w = 1.0f;
				}
				if (m_Model[index].mask.y < 0.0f) {
					m_Model[index].mask.y = 0.0f;
				}
				if (m_Model[index].mask.z < 0.0f) {
					m_Model[index].mask.z = 0.0f;
				}
				if (m_Model[index].mask.x < 0.0f) {
					m_Model[index].mask.x = 0.0f;
				}
				D3DXVec4Normalize(&m_Model[index].mask, &m_Model[index].mask);
				break;		
			case alter_argument::MASK_NORMAL:
				D3DXVECTOR3 normal(0.0f, 1.0f, 0.0f);
				float angle = fabs(D3DXVec3Dot(&m_Model[index].normal, &normal));
				if (angle < 0.86f) {
					m_Model[index].mask.y += value;
					D3DXVec4Normalize(&m_Model[index].mask, &m_Model[index].mask);
				}
				break;
		}		
	}
}
// 
bool TerrainTileClass::GetVertex(TerrainTileClass::VertexType *vertex, D3DXVECTOR3 point,
									int offsetIndexColumn, int offsetIndexRow) {
	int index;
	if (GetIndexVertex(&index, point, offsetIndexColumn, offsetIndexRow) == true) {
		(*vertex) = m_Model[index];
		return true;
	}
	return false;
}
// ��������� � ������������� ������� �������� � ���� ������� ��������� ���������
 bool TerrainTileClass::SetCamputeNodeMeanNormalTangentBinormal(TerrainTileClass::VertexType vertex[5], 
																unsigned int index) {
	// ��������������� �������
	D3DXVECTOR3 v1, v2, v3;
	// �������
	D3DXVECTOR3 normal[4];
	// ���������
	D3DXVECTOR3 binormal[4];
	// ��������
	D3DXVECTOR3 tangent[4];
	// �������� �������� � ����
	if (index < 0  || index > m_VertexCount) {
		return false;
	}
	//		1	
	//	4	0	2 
	//		3
	for (int i = 0; i < 4; i++) {
		switch (i){
			//-------------------------------------------------------------------------
			// ������� ������� ������ ����
			//-------------------------------------------------------------------------
			case 0:
				v1 = vertex[4].position;
				v2 = vertex[0].position;
				v3 = vertex[1].position;
				break;
			//-------------------------------------------------------------------------
			// ������� ������� ����� ����
			//-------------------------------------------------------------------------
			case 1:
				v1 = vertex[1].position;
				v2 = vertex[0].position;
				v3 = vertex[2].position;
				break;
			//-------------------------------------------------------------------------
			// ������� ������ ����� ����
			//-------------------------------------------------------------------------
			case 2:
				v1 = vertex[3].position;
				v2 = vertex[0].position;
				v3 = vertex[4].position;
				break;
			//-------------------------------------------------------------------------
			// ������� ������ ������ ����
			//-------------------------------------------------------------------------
			case 3:
				v1 = vertex[2].position;
				v2 = vertex[0].position;
				v3 = vertex[3].position;
				break;
		}
		// ������� �������
		D3DXVECTOR3 vec1 = (v1 - v2);
		D3DXVECTOR3 vec2 = (v3 - v2);
		D3DXVec3Cross(&normal[i], &vec1, &vec2);
		D3DXVec3Normalize(&normal[i], &normal[i]);
		// ������� ���������
		vec1 = D3DXVECTOR3(-1.0f, 0.0f, 0.0f);
		D3DXVec3Cross(&binormal[i], &vec1, &normal[i] );
		D3DXVec3Normalize(&binormal[i], &binormal[i]);
		// ������� ��������
		D3DXVec3Cross(&tangent[i],  &binormal[i], &normal[i]);
		D3DXVec3Normalize(&tangent[i], &tangent[i]);
	}
	TerrainTileClass::VertexType result;
	result.normal = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	result.binormal = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	result.tangent = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// ��������� ��������
	for (int i = 0; i < 4; i++) {
		result.normal = result.normal + normal[i];
		result.binormal = result.binormal + binormal[i];
		result.tangent = result.tangent + tangent[i];
	}
	m_Model[index].normal = result.normal / 4.0f;
	D3DXVec3Normalize(&m_Model[index].normal, &m_Model[index].normal);
	m_Model[index].binormal = result.binormal / 4.0f;
	D3DXVec3Normalize(&m_Model[index].binormal, &m_Model[index].binormal);
	m_Model[index].tangent = result.tangent / 4.0f;
	D3DXVec3Normalize(&m_Model[index].tangent, &m_Model[index].tangent);

	return true;
}


// ���������� ����� �����������
bool TerrainTileClass::GetIntersects(D3DXVECTOR3, D3DXVECTOR3, D3DXVECTOR3* ) {
	//int indexVertex = 0;
	//bool result = GetIndexVertex(&indexVertex, position, 0, 0);
	return false;
}

bool TerrainTileClass::InitializeBuffers(ID3D10Device* device){
	unsigned long* indices;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
    D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	// ���������� ������ �����
	ReleaseBuffers();
	if (countCellColumn <= 0 || countCellRow <= 0) {
		return false;
	}
	// ������������� ���������� ��������
	m_IndexCount = countCellColumn*countCellRow * 6;

	// ������� ������ ��������
	try {
		indices = new unsigned long[m_IndexCount];
	}
	catch (std::bad_alloc) {
		return false;
	}


	unsigned long index = 0;

	// Load the vertex and index arrays with the terrain data.






	// Set up the description of the index buffer.
    indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * (m_IndexCount)*6;
    indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
	// Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if(FAILED(result)){
		return false;
	}

	delete [] indices;
	indices = 0;

	// Set up the description of the vertex buffer.
	vertexBufferDesc.Usage = D3D10_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(TerrainTileClass::VertexType) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	// ��������� CPU ������ � �����
	vertexBufferDesc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;
	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = m_Model;

	// Now finally create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if (FAILED(result)) {
		return false;
	}
	return true;
}

// ����������� ��������
bool TerrainTileClass::UpdateBuffer(ID3D10Device*) {

	bool result = false;
	HRESULT hresult;
	// Update the vertex buffer with the new solution
	TerrainTileClass::VertexType *v = 0;
	hresult = m_VertexBuffer->Map(D3D10_MAP_WRITE_DISCARD, 0, (void**)&v);
	if (result == S_OK) {
		for (unsigned int i = 0; i < m_VertexCount; i++) {
			v[i] = m_Model[i];
		}
		result = true;
	}
	m_VertexBuffer->Unmap();
	return result;
}

void TerrainTileClass::ReleaseModel(){
	// ����������� ������
	if(m_Model){
		delete [] m_Model;
		m_Model = 0;
	}
	countIndexColumn = 0;
	countIndexRow = 0;
	countCellColumn = 0;
	countCellRow = 0;
	return;
}

void TerrainTileClass::ReleaseBuffers(){
	// ����������� ��������� �����.
	if(m_IndexBuffer){
		m_IndexBuffer->Release();
		m_IndexBuffer = 0;
	}
	// ����������� ��������� �����
	if(m_VertexBuffer){
		m_VertexBuffer->Release();
		m_VertexBuffer = 0;
	}
	return;
}
// ������� ��������
void TerrainTileClass::CreateLine(ID3D10Device*, D3DXVECTOR3 PosStart, D3DXVECTOR3 PosStop) {
	// ���������� ���������� �� ��������
	std::vector<D3DXVECTOR3> vertex;
	vertex.resize(1200);
	PosStart.y = 0;
	PosStop.y = 0; 
	float stepX = (PosStop.x - PosStart.x)/(float)(vertex.size()-2);
	float stepZ = (PosStop.z - PosStart.z)/ (float)(vertex.size()-2);
	D3DXVECTOR3 width;
	D3DXVECTOR3 vec1 = (PosStop - PosStart);
	D3DXVECTOR3 vec2(0.0f, 1.0f, 0.0f);
	D3DXVec3Cross(&width, &vec1, &vec2);
	D3DXVec3Normalize(&width, &width);
	/*if (PosStart.z > PosStop.z) {
		PosStart.z = PosStop.z;
	}*/
	for (unsigned int i = 0; i < vertex.size(); i += 2) {
		vertex[i].x = PosStart.x + stepX*(float)i;
		vertex[i].z = PosStart.z + stepZ*(float)i;
		vertex[i].y = 0;
		// �������� ������ ��� ���������
		this->GetHeight(vertex[i], &vertex[i].y);
		vertex[i].y += 0.750f;

		vertex[i + 1].x = PosStart.x - width.x + (stepX)*(float)(i);
		vertex[i + 1].z = PosStart.z - width.z + (stepZ)*(float)(i);
		vertex[i + 1].y = vertex[i].y;

	}


	//m_Line.Initialize(device, vertex);
}
// ������� ���������� ��������������� �� ��������
void TerrainTileClass::CreateRound(ID3D10Device*, D3DXVECTOR3 position, int radius) {

	// ���������� ���������� �� ��������
	std::vector<D3DXVECTOR3> vertex;
	vertex.resize(120);
	float width = 0.2f;
	float step = 360.0f / (float)vertex.size();
	for (unsigned int i = 0; i < vertex.size(); i+=2) {
		vertex[i].x = position.x + (float)radius*cos(step*(float)i*0.0174532925f);
		vertex[i].z = position.z + (float)radius*sin(step*(float)i*0.0174532925f);
		vertex[i].y = 0;
		// ������
		this->GetHeight(vertex[i], &vertex[i].y);
		vertex[i].y += 0.25f;

		vertex[i+1].x = position.x + (float)(radius+ width)*cos(step*(float)i*0.0174532925f);
		vertex[i+1].z = position.z + (float)(radius+ width)*sin(step*(float)i*0.0174532925f);
		vertex[i+1].y = vertex[i].y;

	}
	//m_Round.Initialize(device, vertex);

}

void TerrainTileClass::SetMethodRender(int state){
	m_MethodRender = state;
}

int TerrainTileClass::GetMethodRender(void) {
	return m_MethodRender;
}

void TerrainTileClass::RenderBuffers(ID3D10Device* device){
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
    stride = sizeof(TerrainTileClass::VertexType);
	offset = 0;
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
    device->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	
    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	// ������ ������ ������

	switch (m_MethodRender) {
		// �����������
		case 0:
			device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_UNDEFINED);
			break;
		// �����
		case 1:
			device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
			break;
		// �����
		case 2:
			device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);
			break;
		case 3:
			device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINESTRIP);
			break;
		case 4:
			device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			break;
		case 5:
			device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST_ADJ);
			break;
		case 6:
			device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ);
			break;
		case 7:
			device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST_ADJ);
			break;
		case 8:
			device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP_ADJ);
			break;
		case 9:
			device->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_1_CONTROL_POINT_PATCHLIST);
			break;

	}


	return;
}