////////////////////////////////////////////////////////////////////////////////
// Filename: textureclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TEXTURECLASS_H_
#define _TEXTURECLASS_H_

class TextureClass{
public:
	TextureClass();
	TextureClass(const TextureClass&);
	~TextureClass();
	bool Initialize(ID3D10Device*, std::string);
	void Shutdown();
	ID3D10ShaderResourceView* GetTexture();
	void SetTexture(ID3D10ShaderResourceView*);
private:
	ID3D10ShaderResourceView* m_Texture;
};

class TexturePack {
public:
	TextureClass * m_Texture;
	int countLink;
	std::string m_Path;
	TexturePack(void);
	~TexturePack(void){};
	bool Shutdown(void);
};

#endif