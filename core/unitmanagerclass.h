////////////////////////////////////////////////////////////////////////////////
// Filename: unitmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _UNITMANAGERCLASS_H_
#define _UNITMANAGERCLASS_H_


class ModelManagerClass;
class CameraClass;
class UnitClass;
class ModelUnitClass;
struct D3DXVECTOR3;


class UnitClass {
public:
	ModelUnitClass unitModel;
	void SetMovePoint(D3DXVECTOR3 point);
	void UpdateMove(float dTime);
	float HitPoint;
	int TypeEffectAttack; // ���������
	int targetID;
	D3DXVECTOR3 markPoint;
	bool isMove;
	bool isRotation;
	float SpeedMove;
	float SpeedRotation;
	void Shutdown(void);
	float debug;
	int ID;
};
////////////////////////////////////////////////////////////////////////////////
// Class name: UnitManagerClass
////////////////////////////////////////////////////////////////////////////////

class UnitManagerClass{
public:
	UnitManagerClass();
	bool Initialize(void);
	void Render(CameraClass* p_Camera);
	void Update(float dTime, TerrainClass* p_Terrain);
	void AddUnit(UnitClass* unit);
	UnitClass* GetUnitSelection(D3DXVECTOR3 pos, D3DXVECTOR3 dir);
	void Shutdown();
	UnitClass* GetUnit(int ID);
	std::map<int, UnitClass*> m_Units;
private:
	int unitID = 0;

};

#endif

