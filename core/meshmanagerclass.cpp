////////////////////////////////////////////////////////////////////////////////
// Filename: meshmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "meshmanagerclass.h"
#include "stdafx.h"

MeshManagerClass::MeshManagerClass(void){
	p_Device = nullptr;
	p_hwnd = nullptr;
	p_TextureManager = nullptr;
	incrementMeshID = 0;
}

// ��������������� �������� �������
bool MeshManagerClass::Initialize(HWND hwnd, ID3D10Device* device, TextureManagerClass* p_TextureMng){
	p_Device = device;
	p_hwnd = hwnd;
	p_TextureManager = p_TextureMng;
	incrementMeshID = 0;
	return true;
}

FileInfoModelClass MeshManagerClass::LoadFileMesh(std::string path){
	FileInfoModelClass result;
	std::map<std::string, FileInfoModelClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_FileInfoModel.find(path);
	// ���� ����������
	if (iter == m_FileInfoModel.end()) {
		// ��������� ��� ����� ������� ��������� � �����
		result = AssimpLoadFileMesh(path);
	}
	else {
		result = iter->second;
	}
	return result;
}

int MeshManagerClass::GetID(std::string path, unsigned int indexMesh) {
	std::map<std::string, FileInfoModelClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_FileInfoModel.find(path);
	// �� ����� ����
	if (iter != m_FileInfoModel.end()) {
		return iter->second.meshID;
	}
	return -1;
}

MeshClass* MeshManagerClass::GetMesh(int ID) {
	std::map<int, ResourceMeshClass*>::iterator iter;
	// ���� ���� � ����������
	iter = m_ResourseMesh.find(ID);
	if (iter == m_ResourseMesh.end()) {
		return nullptr;
	}
	return iter->second->m_Mesh;
}

SkeletonClass* MeshManagerClass::GetSkeleton(int ID) {
	std::map<int, ResourceMeshClass*>::iterator iter;
	// ���� ���� � ����������
	iter = m_ResourseMesh.find(ID);
	if (iter == m_ResourseMesh.end()) {
		return nullptr;
	}
	return iter->second->m_Skeleton;
}

bool MeshManagerClass::IsLoaded(std::string key) {
	std::map<std::string, FileInfoModelClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_FileInfoModel.find(key);
	// �� ����� ����
	if (iter != m_FileInfoModel.end()) {
		return true;
	}
	return false;
}

MeshClass* MeshManagerClass::ProcessMesh(const aiMesh *mesh){
	MeshClass* result = new MeshClass;
	aiVector3D normal;
	aiVector3D tangent;
	aiVector3D binormal;
	for (unsigned int i = 0; i < mesh->mNumVertices; i++){
		aiVector3D pos = mesh->mVertices[i];
		aiVector3D uv(0.0f, 0.0f, 0.0f);
		if (mesh->mTextureCoords[0] != nullptr) {
			uv = mesh->mTextureCoords[0][i];
		}
		if (mesh->mNormals != nullptr) {
			normal = mesh->HasNormals() ? mesh->mNormals[i] : aiVector3D(1.0f, 1.0f, 1.0f);
		}
		if (mesh->mTangents != nullptr) {
			tangent = mesh->mTangents[i];
		}
		if (mesh->mBitangents != nullptr) {
			binormal = mesh->mBitangents[i];
		}
		VertexMeshClass vertex;
		vertex.position.x = pos.x;
		vertex.position.y = pos.y;
		vertex.position.z = pos.z;
		vertex.texture.x = uv.x;
		vertex.texture.y = 1.0f - uv.y;
		vertex.normal.x = normal.x;
		vertex.normal.y = normal.y;
		vertex.normal.z = normal.z;
		vertex.tangent.x = tangent.x;
		vertex.tangent.y = tangent.y;
		vertex.tangent.z = tangent.z;
		vertex.binormal.x = binormal.x;
		vertex.binormal.y = binormal.y;
		vertex.binormal.z = binormal.z;
		result->AddVertex(vertex);
	}
	int iMeshFaces = mesh->mNumFaces;
	for (int i = 0; i < iMeshFaces; i++) {
		const aiFace &face = mesh->mFaces[i];
		for (int k = 0; k < 3; k++) {
			result->AddIndex(face.mIndices[k]);
		}
	}
	// ��������� ���������
	if (mesh->mMaterialIndex >= 0){
	}
	
	return result;
}

D3DXMATRIX MeshManagerClass::ConvertMatrix(const aiMatrix4x4* matrix) {
	D3DXMATRIX result;
	result._11 = matrix->a1;
	result._12 = matrix->a2;
	result._13 = matrix->a3;
	result._14 = matrix->a4;

	result._21 = matrix->b1;
	result._22 = matrix->b2;
	result._23 = matrix->b3;
	result._24 = matrix->b4;

	result._31 = matrix->c1;
	result._32 = matrix->c2;
	result._33 = matrix->c3;
	result._34 = matrix->c4;

	result._41 = matrix->d1;
	result._42 = matrix->d2;
	result._43 = matrix->d3;
	result._44 = matrix->d4;
	return result;
}
// �������� ������ �����
std::vector<BoneMechClass> MeshManagerClass::LoadBones(const aiMesh* pMesh) {
	//m_BoneInfo
	std::map<std::string, int> m_BoneMapping;
	std::vector<BoneMechClass> m_BoneInfo;
	int m_NumBones = 0;
	for (unsigned int i = 0; i < pMesh->mNumBones; i++) {
		int BoneIndex = 0;
		std::string BoneName(pMesh->mBones[i]->mName.data);
		// ������� ���������� �����
		if (m_BoneMapping.find(BoneName) == m_BoneMapping.end()) {
			BoneIndex = m_NumBones;
			m_NumBones++;
			BoneMechClass bi;
			m_BoneInfo.push_back(bi);
		} else {
			BoneIndex = m_BoneMapping[BoneName];
		}
		m_BoneMapping[BoneName] = BoneIndex;
		m_BoneInfo[BoneIndex].BoneName = BoneName;
		m_BoneInfo[BoneIndex].BoneID = BoneIndex;
		m_BoneInfo[BoneIndex].BoneOffset = ConvertMatrix(&pMesh->mBones[i]->mOffsetMatrix.Transpose());
		// ����� ������ � ������
		VertexBoneDataClass vecBuf;
		for (unsigned int j = 0; j < pMesh->mBones[i]->mNumWeights; j++) {
			unsigned int VertexID = pMesh->mBones[i]->mWeights[j].mVertexId;
			float Weight = pMesh->mBones[i]->mWeights[j].mWeight;
			m_BoneInfo[BoneIndex].vertexIDs.push_back(VertexID);
			m_BoneInfo[BoneIndex].Weights.push_back(Weight);
		}
	}
	return m_BoneInfo;
}

AnimationClass MeshManagerClass::LoadFileAnimation(std::string path) {
	AnimationClass result;
	Assimp::Importer importer;
	// ��������� ����
	const aiScene* scene = importer.ReadFile(path.c_str(),
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);
	// ������ �������� �����
	if (scene == 0) {
		MessageBox(p_hwnd, "Couldn't load model ", "Error Importing Asset", MB_ICONERROR);
		return result;
	}

	//std::vector<AnimationClass>  mAnimations;

	// ��������� ��������
	AnimationClass mAnimation;
	for (unsigned int j = 0; j < scene->mAnimations[0]->mNumChannels; j++) {
		AnimationChannelClass mChannel;
		//mAnimation.firstIndexFrame =
		aiNodeAnim *mChannels = scene->mAnimations[0]->mChannels[j];
		mChannel.Name = mChannels->mNodeName.data;
		QuatKeyStruct rotationKey;
		Vec3KeyStruct positonKey;
		Vec3KeyStruct scalingKey;
		// ���������� ���������� ������
		if ((mChannels->mNumPositionKeys >= mChannels->mNumRotationKeys) && (mChannels->mNumPositionKeys >= mChannels->mNumScalingKeys)) {
			if (mChannels->mNumPositionKeys > mAnimation.lastIndexFrame + 1) {
				mAnimation.lastIndexFrame = mChannels->mNumPositionKeys - 1;
			}
		}
		if ((mChannels->mNumRotationKeys >= mChannels->mNumPositionKeys) && (mChannels->mNumRotationKeys >= mChannels->mNumScalingKeys)) {
			if (mChannels->mNumRotationKeys > mAnimation.lastIndexFrame + 1) {
				mAnimation.lastIndexFrame = mChannels->mNumRotationKeys - 1;
			}
		}
		if ((mChannels->mNumScalingKeys >= mChannels->mNumRotationKeys) && (mChannels->mNumScalingKeys >= mChannels->mNumPositionKeys )) {
			if (mChannels->mNumScalingKeys > mAnimation.lastIndexFrame + 1) {
				mAnimation.lastIndexFrame = mChannels->mNumScalingKeys - 1;
			}				
		}
		mAnimation.widthFrame = 1.0f / scene->mAnimations[0]->mTicksPerSecond;
		mAnimation.maxFrame = mAnimation.lastIndexFrame;
		for (unsigned int k = 0; k < mChannels->mNumRotationKeys; k++) {
			rotationKey.time = mChannels->mRotationKeys[k].mTime;
			rotationKey.value.x = mChannels->mRotationKeys[k].mValue.x;
			rotationKey.value.y = mChannels->mRotationKeys[k].mValue.y;
			rotationKey.value.z = mChannels->mRotationKeys[k].mValue.z;
			rotationKey.value.w = mChannels->mRotationKeys[k].mValue.w;
			mChannel.m_RotationKeys.push_back(rotationKey);
		}
		for (unsigned int k = 0; k < mChannels->mNumPositionKeys; k++) {
			positonKey.time = mChannels->mPositionKeys[k].mTime;
			positonKey.value.x = mChannels->mPositionKeys[k].mValue.x;
			positonKey.value.y = mChannels->mPositionKeys[k].mValue.y;
			positonKey.value.z = mChannels->mPositionKeys[k].mValue.z;
			mChannel.m_PositionKeys.push_back(positonKey);
		}
		for (unsigned int k = 0; k < mChannels->mNumScalingKeys; k++) {
			scalingKey.time = mChannels->mScalingKeys[k].mTime;
			scalingKey.value.x = mChannels->mScalingKeys[k].mValue.x;
			scalingKey.value.y = mChannels->mScalingKeys[k].mValue.y;
			scalingKey.value.z = mChannels->mScalingKeys[k].mValue.z;
			mChannel.m_ScalingKeys.push_back(scalingKey);
		}
		if (mChannel.Name != "ROOT") {
			mAnimation.m_AnimationChannel.push_back(mChannel);
		}
	}
	result = mAnimation;
	return result;
}

FileInfoModelClass MeshManagerClass::AssimpLoadFileMesh(std::string path) {
	FileInfoModelClass result;
	
	Assimp::Importer importer;
	// ��������� ����
	const aiScene* scene = importer.ReadFile(path.c_str(),
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);
	// ������ �������� �����
	if (scene == 0) {
		MessageBox(p_hwnd, "Couldn't load model ", "Error Importing Asset", MB_ICONERROR);
		return result;
	}

	std::vector<unsigned int> iMaterialIndices;
	std::vector<unsigned int> index;
	std::vector<VertexMeshClass> vertex;

	ResourceMeshClass *ResourceMesh = new ResourceMeshClass();
	std::vector<BoneMechClass> bones;
	std::vector<AnimationClass>  mAnimations;
	
	// ��������� ��������
	for (unsigned int i = 0; i < scene->mNumAnimations; i++) {
		AnimationClass mAnimation;
		for (unsigned int j = 0; j < scene->mAnimations[i]->mNumChannels; j++) {
			AnimationChannelClass mChannel;
			aiNodeAnim *mChannels = scene->mAnimations[i]->mChannels[j];
			mChannel.Name = mChannels->mNodeName.data;
			QuatKeyStruct rotationKey;
			Vec3KeyStruct positonKey;
			Vec3KeyStruct scalingKey;
			for (unsigned int k = 0; k < mChannels->mNumRotationKeys; k++) {
				rotationKey.time = mChannels->mRotationKeys[k].mTime;
				rotationKey.value.x = mChannels->mRotationKeys[k].mValue.x;
				rotationKey.value.y = mChannels->mRotationKeys[k].mValue.y;
				rotationKey.value.z = mChannels->mRotationKeys[k].mValue.z;
				rotationKey.value.w = mChannels->mRotationKeys[k].mValue.w;
				mChannel.m_RotationKeys.push_back(rotationKey);
			}
			for (unsigned int k = 0; k < mChannels->mNumPositionKeys; k++) {
				positonKey.time = mChannels->mPositionKeys[k].mTime;
				positonKey.value.x = mChannels->mPositionKeys[k].mValue.x;
				positonKey.value.y = mChannels->mPositionKeys[k].mValue.y;
				positonKey.value.z = mChannels->mPositionKeys[k].mValue.z;
				mChannel.m_PositionKeys.push_back(positonKey);
			}
			for (unsigned int k = 0; k < mChannels->mNumScalingKeys; k++) {
				scalingKey.time = mChannels->mScalingKeys[k].mTime;
				scalingKey.value.x = mChannels->mScalingKeys[k].mValue.x;
				scalingKey.value.y = mChannels->mScalingKeys[k].mValue.y;
				scalingKey.value.z = mChannels->mScalingKeys[k].mValue.z;
				mChannel.m_ScalingKeys.push_back(scalingKey);
			}
			mAnimation.m_AnimationChannel.push_back(mChannel);
		}
		mAnimations.push_back(mAnimation);
	}
	// ��������� ��������
	for (unsigned int i = 0; i < mAnimations.size(); i++) {
		//ResourceMesh->m_Animation.push_back(mAnimations[i]);
	}
	aiNode* Node = scene->mRootNode;
	// ����� ��������� � ������
	
	// ��������� ��� ����, � ��� ����� 
	for (unsigned int i = 0; i < 1; i++) {
		bones = LoadBones(scene->mMeshes[i]);
		// ���������������� �����
		ResourceMesh->m_Mesh = ProcessMesh(scene->mMeshes[i]);
		// ��������� ����� � �������
		ResourceMesh->m_Mesh->ConnectSkeletonToMesh(bones);
		// ������������� �������
		ResourceMesh->m_Mesh->InitializeBuffers(p_Device);
		// ������������ ����� � ������ ������
		BoneTreeClass* m_BoneTree = new BoneTreeClass();
		m_BoneTree->LoadTreeBone(Node, nullptr);
		// ����������� ������� � ���������� �����������
		m_BoneTree->CamputeGlobalMatrix();
		m_BoneTree->SetBonesID(bones);
		// ������� ������ �� ������
		ResourceMesh->m_Skeleton = new SkeletonClass();
		ResourceMesh->m_Skeleton->CreateSkeleton(m_BoneTree);
		// ������� ��������������� ������
		m_BoneTree->Shutdown();
		delete m_BoneTree;
		m_BoneTree = nullptr;
		// ��������� ������ � �������
		incrementMeshID++;
		m_ResourseMesh.insert(std::pair<int, ResourceMeshClass*>(incrementMeshID, ResourceMesh));
		result.meshID = incrementMeshID;
	}
	if (scene->mNumMeshes == 0) {
		incrementMeshID++;
		// ��������� ������ � �������
		m_ResourseMesh.insert(std::pair<int, ResourceMeshClass*>(incrementMeshID, ResourceMesh));
		result.meshID  = incrementMeshID;
	}
	result.IsCorrected = true;
	m_FileInfoModel.insert(std::pair<std::string, FileInfoModelClass>(path, result));
	
	return result;

}
// ������� ������ �� �������
bool MeshManagerClass::Erase(std::string key){
	
	return false;
}
int MeshManagerClass::GetCoutMesh(std::string key){
	std::map<std::string, FileInfoModelClass>::iterator iter;
	iter = m_FileInfoModel.find(key);
	if (iter != m_FileInfoModel.end()) {
		return  1;
		
	}
	return -1;
}

void MeshManagerClass::Shutdown(){
	std::map<int, ResourceMeshClass*>::iterator iter;
	for (iter = m_ResourseMesh.begin(); iter != m_ResourseMesh.end(); ++iter) {
		if (iter->second != nullptr) {
			iter->second->Shutdown();
			delete iter->second;
			iter->second = nullptr;
		}

	}
	m_ResourseMesh.clear();
}


// ���� ������ � ����� ������ ���� ������ ������� ���������� �������
bool MeshManagerClass::RenderBuffers(int ID, int pickRender){
	std::map<int, ResourceMeshClass*>::iterator iter;
	// ���� ���� � ����������
	iter = m_ResourseMesh.find(ID);
	// ����� ����
	if (iter != m_ResourseMesh.end()) {
		iter->second->RenderBuffers(p_Device, pickRender);
		return true;
	}
	return false;
}

ResourceMeshClass*  MeshManagerClass::GetResourceMech(int ID) {
	std::map<int, ResourceMeshClass*>::iterator iter;
	iter = m_ResourseMesh.find(ID);
	// ����� ����
	if (iter != m_ResourseMesh.end()) {
		return iter->second;
	}
	return nullptr;
}

void MeshManagerClass::SetScaleMesh(int ID, float value) {
	std::map<int, ResourceMeshClass*>::iterator iter;
	iter = m_ResourseMesh.find(ID);
	// ����� ����
	if (iter != m_ResourseMesh.end()) {
		iter->second->scale = value;
	}

}
// ���������� ���������� ��������
int MeshManagerClass::GetIndexCount(int ID){
	std::map<int, ResourceMeshClass*>::iterator iter;
	iter = m_ResourseMesh.find(ID);
	// ����� ����
	if (iter != m_ResourseMesh.end()) {
		return iter->second->m_Mesh->GetCountIndex();
	}
	return 0;
}
// ���������� ���������� ������
int MeshManagerClass::GetVertexCount(int ID){
	std::map<int, ResourceMeshClass*>::iterator iter;
	iter = m_ResourseMesh.find(ID);
	// ����� ����
	if (iter != m_ResourseMesh.end()) {
		return iter->second->m_Mesh->countOfVertex;
	}
	return 0;
}

// ���������� �������� � ������ ������
unsigned int MeshManagerClass::GetCountLink(int ID){
	std::map<int, ResourceMeshClass*>::iterator iter;
	iter = m_ResourseMesh.find(ID);
	// ����� ����
	if (iter != m_ResourseMesh.end()) {
		return iter->second->countLink;
	}
	return 0;
}

void MeshManagerClass::AddAnimation(int ID, AnimationClass animation) {
	std::map<int, ResourceMeshClass*>::iterator iter;
	iter = m_ResourseMesh.find(ID);
	// ����� ����
	if (iter != m_ResourseMesh.end()) {
		iter->second->m_Skeleton->m_Animation.insert(std::pair<TypeAnimation, 
													 AnimationClass>(animation.Type, animation));
	}
}


MaterialClass MeshManagerClass::GetMaterial(int ID){
	MaterialClass result;
	std::map<int, ResourceMeshClass*>::iterator iter;
	iter = m_ResourseMesh.find(ID);
	// ����� ����
	if (iter != m_ResourseMesh.end()) {
		return iter->second->m_Mesh->material;
	}
	return result;

}

float MeshManagerClass::GetRadiusMesh(int ID){
	std::map<int, ResourceMeshClass*>::iterator iter;
	iter = m_ResourseMesh.find(ID);
	// ����� ����
	if (iter != m_ResourseMesh.end()) {
		return iter->second->m_Mesh->radius;
	}
	return 0.0f;
}
D3DXVECTOR3 MeshManagerClass::GetCenterMesh(int ID){
	std::map<int, ResourceMeshClass*>::iterator iter;
	iter = m_ResourseMesh.find(ID);
	// ����� ����
	if (iter != m_ResourseMesh.end()) {
		return iter->second->m_Mesh->center;
	}
	return D3DXVECTOR3(0.0f, 0.0f, 0.0f);
}