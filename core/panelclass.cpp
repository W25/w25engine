///////////////////////////////////////////////////////////////////////////////
// Filename: panelclass.cpp
///////////////////////////////////////////////////////////////////////////////
#include "panelclass.h"
#include "stdafx.h"

// ������� ������ �������
bool PanelClass::Create(ID3D10Device* device, TextureManagerClass* p_TextureManager,
						int screenWidth, int screenHeight) {
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	p_TextureManager->Add(m_Material.pathDiffuse);
	this->InitializeBuffers(device);
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		m_Widget[i]->Left += this->Left;
		m_Widget[i]->Top += this->Top;
		m_Widget[i]->Create(device, p_TextureManager, screenWidth, screenHeight);
	}
	
	return true;
}
// ��������� ������� � ������
void PanelClass::Add(ID3D10Device*, WidgetClass* widget) {
	this->m_Widget.push_back(widget);
}
// map �� ����� NAME ������� �������� �� �����
bool PanelClass::SetText(std::string NameWidget, std::wstring inText) {
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		if (m_Widget[i]->Name == NameWidget) {
			m_Widget[i]->SetText(inText);
			return true;
		}
	}
	return false;
}
bool PanelClass::Render(ID3D10Device* device, ShaderManagerClass* p_ShaderManager,
						D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
						D3DXMATRIX projectionMatrix) {

	RenderBuffers(device);
	p_ShaderManager->RenderTextureShader(this->m_IndexCount,
										 worldMatrix, viewMatrix, projectionMatrix, m_Material);
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		m_Widget[i]->Render(device, p_ShaderManager, worldMatrix, viewMatrix, projectionMatrix);
	}
	return true;
}

bool PanelClass::Update(InputClass* p_Input, int mouseX, int mouseY) {
	bool result = false;
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		// ���� ���� �� ���� ������� ��� ������
		result |= m_Widget[i]->Update(p_Input, mouseX, mouseY);
	}
	// �������� ���� ������
	result |= this->GetReside(mouseX, mouseY);
	return result;
}

void PanelClass::Shutdown(void) {
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		this->m_Widget[i]->Shutdown();
		delete m_Widget[i];
		m_Widget[i] = 0;
	}
	m_Widget.clear();
}

PanelClass::PanelClass(){
	m_Material.pathDiffuse = "./data/metal.dds";
}



