////////////////////////////////////////////////////////////////////////////////
// Filename: msgmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MSGMANAGERCLASS_H_
#define _MSGMANAGERCLASS_H_

enum class TypeState { NONE, DEBUG, ABOUT, MENU, PLAY };
enum class TypeMSG { NONE, ADD, ERASE, PAUSE, RESUME, EXIT };

class PackMsg{
public:
	PackMsg();
	TypeState stateId; // ��� ��������
	TypeMSG type; // � ��� ��������
	TypeState aimStateId;   // ��� ���� ��������
	float time; // ����� ��������
};

class MsgManagerClass{
// ������� �������� ������
public:
	static MsgManagerClass* GetInstance() {
		static MsgManagerClass m_Instance;
		return &m_Instance;
	}
public:
	bool AddMsg(PackMsg);
	bool EraseMsg(int);
	void Shutdown();
	int GetCountMsg();
	PackMsg GetMsg(int);
	PackMsg GetBackMsg();
private:
	std::vector<PackMsg> m_Msg;
private:
	// ������������ � �������� ������������ ���������� ��������
	MsgManagerClass() {};
	~MsgManagerClass() {};
	MsgManagerClass(const MsgManagerClass&) {};
	MsgManagerClass& operator=(MsgManagerClass&) {};
};
#endif
