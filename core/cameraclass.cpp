////////////////////////////////////////////////////////////////////////////////
// Filename: cameraclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "cameraclass.h"
#include "stdafx.h"

CameraClass::CameraClass(){
	m_Length = 1.0f;
	flagFreez = false;
	flagUpdate = true;
}

CameraClass::CameraClass(const CameraClass&){
	m_Length = 1.0f;
}


CameraClass::~CameraClass(){
}

void CameraClass::Initialize(int screenWidth, int screenHeight,	float screenDepth, float screenNear) {
	m_ScreenWidth = (float)screenWidth;
	m_ScreenHeight = (float)screenHeight;
	m_ScreenNear = screenNear;
	m_ScreenDepth = screenDepth;
	// Setup the projection matrix.
	// ���� ������ 
	m_FieldOfView = (float)D3DX_PI / 4.0f;
	m_ScreenAspect = (float)screenWidth / (float)screenHeight;
	// Create the projection matrix for 3D rendering.
	D3DXMatrixPerspectiveFovLH(&m_ProjectionMatrix, m_FieldOfView, m_ScreenAspect, screenNear, screenDepth);
	// Initialize the world matrix to the identity matrix.
	D3DXMatrixIdentity(&m_WorldMatrix);
	// Create an orthographic projection matrix for 2D rendering.
	D3DXMatrixOrthoLH(&m_OrthoMatrix, (float)screenWidth, (float)screenHeight, screenNear, screenDepth);
	D3DXVECTOR3 up, position, lookAt;
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;
	// Setup the vector that points upwards.
	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;
	// Setup the position of the camera in the world.
	position.x = 0;
	position.y = 0;
	position.z = -1;
	// Setup where the camera is looking by default.
	lookAt.x = 0.0f;
	lookAt.y = 0.0f;
	lookAt.z = 1.0f;
	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = 0;
	yaw = 0;
	roll = 0;
	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
	// Translate the rotated camera position to the location of the viewer.
	lookAt = position + lookAt;
	// Finally create the view matrix from the three updated vectors.
	D3DXMatrixLookAtLH(&m_MatrixGUI, &position, &lookAt, &up);
	return;
}


// ��������� ����������� �������
void CameraClass::SetRotation(D3DXVECTOR3 rotation){
	m_Rotation.x = rotation.x; // ����� ������	
	m_Rotation.y = rotation.y; 
	m_Rotation.z = rotation.z; 
	return;
}
// ������� ������ ������ �����
void CameraClass::AddRotation(D3DXVECTOR3 controlPosition, D3DXVECTOR3 rotation) {
	if (flagFreez == false) {
		m_Rotation += rotation;
		if (m_Rotation.x > 90.0f) {
			m_Rotation.x = 90.0f;
		}
		if (m_Rotation.x < -90.0f) {
			m_Rotation.x = -90.0f;
		}
		D3DXVECTOR3 direction = m_Position - controlPosition;
		D3DXVECTOR3 look(0.0f, 0.0f, 1.0f);
		float length = D3DXVec3Length(&direction);
		D3DXVec3Normalize(&direction, &direction);
		D3DXMATRIX rotationMatrix;
		// ���������� ��������� ������
		// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
		float pitch = m_Rotation.x * 0.0174532925f;
		float yaw = m_Rotation.y * 0.0174532925f;
		float roll = m_Rotation.z * 0.0174532925f;
		// Create the rotation matrix from the yaw, pitch, and roll values.
		D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
		D3DXVec3TransformCoord(&look, &look, &rotationMatrix);
		D3DXVec3Normalize(&look, &look);
		m_Position = controlPosition - look*length;
	}


}
// ��������� ��������� �������
void CameraClass::AddRotation(D3DXVECTOR3 rotation){
	if (flagFreez == false) {
		m_Rotation.x += rotation.x;
		m_Rotation.y += rotation.y;
		m_Rotation.z += rotation.z;
		if (m_Rotation.x > 90.0f) {
			m_Rotation.x = 90.0f;
		}
		if (m_Rotation.x < -90.0f) {
			m_Rotation.x = -90.0f;
		}
	}
	return;
}

// ��������� �������������� ������
void CameraClass::SetPosition(D3DXVECTOR3 position){
	m_Position.x = position.x;
	m_Position.y = position.y;
	m_Position.z = position.z;
	return;
}


// ��������� �������������� ������
void CameraClass::AddPosition(D3DXVECTOR3 position){
	if (flagFreez == false) {
		m_Position.x += position.x;
		m_Position.y += position.y;
		m_Position.z += position.z;
	}
	return;
}
void  CameraClass::AddLength(float length) {
	if (flagFreez == false) {
		m_Length += length;
	}
}

void  CameraClass::SetLength(D3DXVECTOR3 controlPosition, float length) {
	m_Length = length;
	D3DXVECTOR3 vec = controlPosition - m_Position;
	float l = D3DXVec3Length(&vec);
	if ( l > m_Length) {
		m_Position = m_Position + D3DXVECTOR3(m_ViewMatrix._13, m_ViewMatrix._23, m_ViewMatrix._33)*m_Length;
	}
}

void CameraClass::Clone(CameraClass* camera) {
	m_FieldOfView = camera->m_FieldOfView;
	m_ScreenAspect = camera->m_ScreenAspect;
	m_ScreenWidth = camera->m_ScreenWidth;
	m_ScreenHeight = camera->m_ScreenHeight;
	m_ScreenNear = camera->m_ScreenNear;
	m_ScreenDepth = camera->m_ScreenDepth;
	m_Length = camera->m_Length;
	m_Position = camera->m_Position;
	m_Rotation = camera->m_Rotation;
	m_View = camera->m_View;
	m_ViewMatrix = camera->m_ViewMatrix;
	m_ProjectionMatrix = camera->m_ProjectionMatrix;
	m_WorldMatrix = camera->m_WorldMatrix;
	m_MatrixGUI = camera->m_MatrixGUI;
	m_OrthoMatrix = camera->m_OrthoMatrix;
	flagFreez = camera->flagFreez;
}

void CameraClass::MovePosition(float speed){
	if (flagFreez == false) {
		// ��������� � ������� �������
		m_Position += speed*GetView();
	}
	return;
}

void CameraClass::ShiftPosition(float x, float y){
	if (flagFreez == false) {
		m_Position.x += x*(float)sin(m_Rotation.y * 0.0174532925f + 1.5707963267f); //
		m_Position.y -= y*(float)sin(m_Rotation.x * 0.0174532925f + 1.5707963267f); //
		m_Position.z += x*(float)cos(m_Rotation.y * 0.0174532925f + 1.5707963267f) +
			y*(float)cos(m_Rotation.x * 0.0174532925f + 1.5707963267f);//
	}
	return;
}

D3DXVECTOR3 CameraClass::GetPosition(){
	return m_Position;
}
/////////////////
// �������� ������ ������� �������
D3DXVECTOR3 CameraClass::GetCursorView(float MouseX, float MouseY, D3DXVECTOR3 position){

	D3DXMATRIX pMatrixProj;

	// ��������  �� -1 �� 1
	float NMouseX =  (2.0f * MouseX / ((float)m_ScreenWidth) - 1.0f)/ m_ProjectionMatrix(0, 0);
	float NMouseY = (1.0f - 2.0f * MouseY / ((float)m_ScreenHeight) )/ m_ProjectionMatrix(1, 1);

	D3DXMATRIX pMatrixView, pMatrixWorld;
	pMatrixView = m_ViewMatrix;
	pMatrixWorld = m_WorldMatrix;

	D3DXMATRIX pMatrixViewInverse, pMatrixWorldInverse;
	D3DXMatrixInverse(&pMatrixViewInverse, 0, &pMatrixView);
	D3DXMatrixInverse(&pMatrixWorldInverse, 0, &pMatrixWorld);

	D3DXVECTOR3 pVectorPosition = position;
	D3DXVECTOR3 pVectorDirection = D3DXVECTOR3(NMouseX, NMouseY, 1.0f);

	D3DXVec3TransformNormal(&pVectorDirection, &pVectorDirection, &pMatrixViewInverse);
	D3DXVec3TransformNormal(&pVectorDirection, &pVectorDirection, &pMatrixWorldInverse);

	D3DXVec3Normalize(&pVectorDirection, &pVectorDirection);

	//pVectorDirection - ����������� �������
	//pVectorPosition - ����� �������(��� ��� ��, ��� ��������� �����)
	return pVectorDirection;
}
D3DXVECTOR3 CameraClass::GetView(){
	return m_View;
}

D3DXVECTOR3 CameraClass::GetRotation(){
	return m_Rotation;
}

void CameraClass::Render(){

	D3DXVECTOR3 up, position, lookAt;
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

	// Setup the vector that points upwards.
	up.x = 0.0f; 
	up.y = 1.0f;
	up.z = 0.0f;

	// Setup the position of the camera in the world.
	position.x = m_Position.x;
	position.y = m_Position.y;
	position.z = m_Position.z;

	// Setup where the camera is looking by default.
	lookAt.x = 0.0f;
	lookAt.y = 0.0f;
	lookAt.z = 1.0f;

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = m_Rotation.x * 0.0174532925f;
	yaw   = m_Rotation.y * 0.0174532925f;
	roll  = m_Rotation.z * 0.0174532925f;

	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3Normalize(&lookAt, &lookAt);
	// ������ �������
	m_View = lookAt;
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);

	// Translate the rotated camera position to the location of the viewer.
	lookAt = position + lookAt;

	// Finally create the view matrix from the three updated vectors.
	D3DXMatrixLookAtLH(&m_ViewMatrix, &position, &lookAt, &up);

	return;
}


void CameraClass::GetViewMatrix(D3DXMATRIX* viewMatrix){
	*viewMatrix = m_ViewMatrix;
	return;
}

void CameraClass::GetMatrixGUI(D3DXMATRIX* MatrixGUI){
	*MatrixGUI = m_MatrixGUI;
	return;
}

void CameraClass::GetProjectionMatrix(D3DXMATRIX* projectionMatrix) {
	*projectionMatrix = m_ProjectionMatrix;
	return;
}

void CameraClass::GetWorldMatrix(D3DXMATRIX* worldMatrix) {
	*worldMatrix = m_WorldMatrix;
	return;
}

void CameraClass::GetOrthoMatrix(D3DXMATRIX* orthoMatrix) {
	*orthoMatrix = m_OrthoMatrix;
	return;
}


void  CameraClass::GetMatrixModel(D3DXMATRIX* MatrixGUI) {

	D3DXVECTOR3 up, position, lookAt;
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;


	// Setup the vector that points upwards.
	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;

	// Setup the position of the camera in the world.
	position.x = 0.0f;
	position.y = 10.0f;
	position.z = 0.0f;

	// Setup where the camera is looking by default.
	lookAt.x = 0.0f;
	lookAt.y = 0.0f;
	lookAt.z = 1.0f;

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = 90 * 0.0174532925f;
	yaw = 0 * 0.0174532925f; // <>
	roll = 0 * 0.0174532925f;

	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);

	// Translate the rotated camera position to the location of the viewer.
	lookAt = position + lookAt;

	// Finally create the view matrix from the three updated vectors.
	D3DXMatrixLookAtLH(MatrixGUI, &position, &lookAt, &up);

	return;
}