#ifndef _STATECLASS_MENU_H_
#define _STATECLASS_MENU_H_

////////////////////////////////////////////////////////////////////////////////
// ��������� ������ ����
////////////////////////////////////////////////////////////////////////////////
class StateClass;
class MenuState;
class FpsClass;
class InputClass;

class MenuState: public StateClass { 

public:
	//���������� ��������� �� ������ ��� ���������� ��� � �������� ��������� 
	static MenuState* GetInstance() { return &m_MenuState; }
	// ��������� �� �����
	static MenuState m_MenuState;
	// ��������� � ���������� ���������
	bool Render(); 
	bool Update(InputClass*, float);
	// �������� ��� ����� � �����(�������� ������)
	bool InitializeState();
	// �������� �� ������ �� ������(�������� �������� ��������)
	bool Exit(); 
	// ������������ ���� ��������
	bool Shutdown();

private:
	// ������� ��� ������� �� ������
	void OnClickButtonStart(int, int, int);
	void OnClickButtonEnd(int, int, int);
	void OnClickButtonDebug(int, int, int);
	bool RenderPrimary(); 
	bool UpdatePrimary(InputClass*, float);


	// ������� ��������� ������
	//FrustumClass* m_Frustum;
	// ������ FPS
	//FpsClass* m_Fps;
	// ���������� �������
	int MouseX;
	int MouseY;

};

#endif