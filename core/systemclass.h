////////////////////////////////////////////////////////////////////////////////
// Filename: systemclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _SYSTEMCLASS_H_
#define _SYSTEMCLASS_H_

#include "GameW25.h"

class D3DClass;
class TimerClass;
class InputClass;
class EffectManagerClass;
class LightManagerClass;
class TextureManagerClass;
class MeshManagerClass;
class ModelManagerClass;
class ShaderManagerClass;
class StateManagerClass;
class MsgManagerClass;
enum class TypeMSG;
class PackMsg;
///////////////////////////////////////////////////////////
// Class name: SystemClass
///////////////////////////////////////////////////////////
class SystemClass{
// ������� �������� ������
public:
	static SystemClass* GetInstance() {
		static SystemClass m_Instance;
		return &m_Instance;
}
public:
	bool Initialize(HINSTANCE hInstance, int screenWidth, int screenHeight, bool isFullScreen, bool isVSYNC);
	void Shutdown();
	MSG Run();
	bool Frame();
	void InitializeWindows(int&, int&);
	void ShutdownWindows();
	void PrintScreen(HWND hwnd);
	void SaveCapturedBitmap(char* szFileName, HBITMAP hBitmap);
private:
	// �������� ���������
	float timeDelayRender;
	float timeDelayUpdateFrame;
	int WinCursorX;
	int WinCursorY;
	int prevScreenWidth;
	int prevScreenHeight;
	int ScreenWidth;
	int m_ScreenWidth = 1920;
	int m_ScreenHeight = 1080;
	bool m_IsFullScreen = true;
	bool m_IsVSYNC = true;
	LPCSTR m_applicationName;
	HINSTANCE m_hInstance;
	HWND m_hWnd;
	// ������ �������
	D3DClass* m_D3D;
	// ������ �������
	TimerClass* m_Timer;	
	// ������ �����
	InputClass* m_Input;
	// �������� �������
	TextureManagerClass* m_TextureManager;
	// �������� ���������
	MsgManagerClass* m_MsgManager;
	// �������� ���������
	StateManagerClass* m_StateManager;
	// �������� �����
	MeshManagerClass* m_MeshManager;
	// �������� ��������� �������
	ModelManagerClass* m_ModelManager;
	// �������� ��������
	ShaderManagerClass* m_ShaderManager;
	// �������� �����
	LightManagerClass* m_LightManager;
	// �������� ��������
	EffectManagerClass* m_EffectManager;
private:
	// ������������ � �������� ������������ ���������� ��������
	SystemClass();
	~SystemClass(){};
	SystemClass(const SystemClass&){};
	SystemClass& operator=(SystemClass&){};
};

#endif
