////////////////////////////////////////////////////////////////////////////////
// Filename: textureclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "textureclass.h"
#include "stdafx.h"

TexturePack::TexturePack(void) {
	m_Texture = nullptr;
	countLink = 0;
	m_Path = "NULL";
}
bool TexturePack::Shutdown(void) {
	if (m_Texture) {
		m_Texture->Shutdown();
		delete m_Texture;
		m_Texture = nullptr;
	}

	countLink = 0;
	return true;
}


TextureClass::TextureClass(){
	m_Texture = nullptr;
}


TextureClass::TextureClass(const TextureClass&){
	m_Texture = nullptr;
}


TextureClass::~TextureClass(){
}



bool TextureClass::Initialize(ID3D10Device* device, std::string path){
	HRESULT result;
	// Load the texture in.
	// ���������� ���� std::string � LPCWSTR
	result = D3DX10CreateShaderResourceViewFromFile(device, path.c_str(), NULL, NULL, &m_Texture, NULL);
	if(FAILED(result)){
		return false;
	}

	return true;
}

ID3D10ShaderResourceView* TextureClass::GetTexture(){
	return m_Texture;
}

void TextureClass::SetTexture(ID3D10ShaderResourceView* texture) {
	m_Texture = texture;
}

void TextureClass::Shutdown() {
	// Release the texture resource.
	if (m_Texture) {
		m_Texture->Release();
		m_Texture = nullptr;
	}

	return;
}