////////////////////////////////////////////////////////////////////////////////
// Filename: modelunitclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MODELUNITCLASS_H_
#define _MODELUNITCLASS_H_

enum class TypeBody { NONE, LEGS, BODY, HEAD, WEAPON_0, WEAPON_1, WEAPON_2, WEAPON_3 };
struct D3DXVECTOR3;

class PartModelIDClass{
public:
	int ID;
	TypeBody Type;
};

class ModelUnitClass {
public:
	ModelUnitClass();
	~ModelUnitClass();
	void UpdateAnimation(double dTime);
	//void RefreshAnimation();
	void SetPartModel(PartModelIDClass item);
	void SetTypeAnimation(TypeAnimation type);
	PartModelIDClass GetPartModel(TypeBody item);
	bool CreatePartModel(TypeBody, string path);
	float GetAnglePoint(D3DXVECTOR3 point);
	void Render(CameraClass *p_Camera);
	void Update(float time, TerrainClass* p_Terrain);
	void Shutdown();
	void MovePosition(float speed);
	void ShiftPosition(float speed);
	void AddRotation(D3DXVECTOR3 r);
	void CamputeLinkBone(void);
	float CamputeRadiusModel(void);
	TypeBody GetTypeBody(string type);
	TypeAnimation GetTypeAnimation(string type);
	int ID;
	int FirstTypeAnimation;
	int SecondTypeAnimation;
	float radiusModel;
	D3DXVECTOR3 GetView(void);
	D3DXVECTOR3 position;
	D3DXVECTOR3 rotation;
	D3DXVECTOR3 scale;
	D3DXVECTOR3 m_View;
	std::vector<PartModelIDClass> m_PartModels;
	int modeRender;
	int modeShader;
};



#endif