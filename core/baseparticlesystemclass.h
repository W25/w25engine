////////////////////////////////////////////////////////////////////////////////
// Filename: particlesystemclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _BASEPARTICLESYSTEMCLASS_H_
#define _BASEPARTICLESYSTEMCLASS_H_


///////////////////////////////////////////////////////////////////////////////
// Class name: ParticleSystemClass
///////////////////////////////////////////////////////////////////////////////
class EmitterClass {
public :
	EmitterClass();
	D3DXVECTOR3 position;
	D3DXVECTOR3 prevPosition;
	D3DXVECTOR3 direction;
	D3DXVECTOR3 particleDeviation;
	float particleVelocity;
	float particleTimeLife;
	float particleVelocityVariation;
	int limitParticles;
	int currentParticleCount;
	float particleSize;
	float particlesPerSecond;
	float accumulatedTime;
	bool flagEmitting;
	bool flagInterpolation;

};

class BaseParticleSystemClass {
protected:
	struct ParticleType{
		D3DXVECTOR3 position;
		D3DXVECTOR3 speed;
		double distance;
		float size;
		D3DXVECTOR4 color;
		bool active;
		float energy;
	};
	struct VertexType {
		D3DXVECTOR3 position;
		D3DXVECTOR4 color;
		float size;
	};
public:
	BaseParticleSystemClass();
	virtual ~BaseParticleSystemClass();
	bool Initialize(void);
	void Refresh(void);
	virtual void Shutdown();
	virtual bool Update(CameraClass *p_Camera, float);
	void Render(void);
	int GetIndexCount();
	bool GetFlagDeath();
	MaterialClass GetMaterial();
	virtual void SetPosition(D3DXVECTOR3 pos);
	virtual void SetDirection(D3DXVECTOR3 dirertion);
	virtual D3DXVECTOR3 GetPosition(void);
	virtual D3DXVECTOR3 GetDirection(void);
	void SetEmitting(bool);
protected:
	static int CompareParticle(const ParticleType*, const ParticleType*);
	virtual bool InitializeParticleSystem();
	void ShutdownParticleSystem();
	bool InitializeBuffers(void);
	void ShutdownBuffers();
	virtual void EmitParticles(CameraClass *p_Camera, float dTime);
	virtual void UpdateParticles(CameraClass *p_Camera, float dTime);
	virtual void KillParticles();
	bool UpdateBuffers(CameraClass *p_Camera);
	void RenderBuffers(void);
	void CamputeParticleSort(void);
protected:
	bool flagDeath;
	D3DXVECTOR3 position;
	MaterialClass m_Material;
	EmitterClass m_Emitter;
	ParticleType* m_particleList;
	int m_vertexCount, m_indexCount;
	VertexType* m_vertices;
	ID3D10Buffer *m_vertexBuffer, *m_indexBuffer;
};

#endif