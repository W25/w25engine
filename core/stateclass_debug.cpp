////////////////////////////////////////////////////////////////////////////////
// Filename: stateclass_debug.cpp
////////////////////////////////////////////////////////////////////////////////
#include "stateclass_debug.h"
#include "scu.cpp"


////////////////////////////////////////////////////////////////////////////////
// ��������� ������ ���������� ��� �������
////////////////////////////////////////////////////////////////////////////////
// �����������
DebugState::DebugState(WCHAR* name):StateClass(name){
	m_Cpu = 0;	
	m_Fps = 0;
	nameState = name;
	m_ModelManager = nullptr;
	m_MeshLine = nullptr;
}

// ���������� ��������� (������������)
bool DebugState::InitializeState(void) {

	bool result;
	// ������� ������ FPS.
	m_Fps = new FpsClass;
	if (!m_Fps) {
		return false;
	}
	// �������������� ������ FPS.
	result = m_Fps->Initialize();
	if (!result) {
		MessageBox(*p_HWND, "Could not initialize the FPS object.", "Error", MB_OK);
		return false;
	}

	LabelClass* Label_1 = new LabelClass;
	Label_1->Name = "Label1";
	Label_1->Left = 10;
	Label_1->Top = 100;
	Label_1->Text = L"Text in Label";
	m_GuiManager->Add(p_D3D->GetDevice(), Label_1);

	LabelClass* Label_2 = new LabelClass;
	Label_2->Name = "Label2";
	Label_2->Left = 10;
	Label_2->Top = 150;
	Label_2->Text = L"Text in Label";
	m_GuiManager->Add(p_D3D->GetDevice(), Label_2);
	
	// 
	//ButtonSkillClass* Button[10];
	//// ����������� ������� � ��������
	//for (unsigned int i = 0; i < 10; i++) {
	//	Button[i] = new ButtonSkillClass;
	//	Button[i]->Name = "Button1";
	//	Button[i]->Height = 100;
	//	Button[i]->Width = 100;
	//	Button[i]->Top = 100;
	//	Button[i]->Left = 100+(float)i*100;
	//	Button[i]->Text = L"";
	//	Button[i]->sprite.pathDiffuse = "./data/GUI/UI_SkillTree.png";
	//	m_GuiManager->Add(p_D3D->GetDevice(), Button[i]);
	//}

	// ������������� ��������� ��������� ������
	m_Camera->SetPosition(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_Camera->SetRotation(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_Camera->Render();


	// ������� ������ �����
	LightClass light;

	// ������������ ������� �����
	light.SetAmbientColor(0.1250f, 0.1250f, 0.1250f, 1.00f);
	//light.SetAmbientColor(1.0f, 1.0f, 1.0f, 1.0f);
	light.SetDiffuseColor(1.0f, 1.0f, 1.0f, 1.0f);
	light.SetDirection(45.0f, 0.0f, 0.0f);
	light.SetSpecularColor(1.0f, 1.0f, 1.0f, 1.0f);
	light.SetSpecularPower(0.7f);
	p_LightManager->SetDirectionalLight(light);


	m_ModelManager = new ModelManagerClass();
	if (m_ModelManager == nullptr) {
		return false;
	}
	m_ModelManager->Initialize(p_D3D->GetDevice(), p_MeshManager, p_ShaderManager, p_LightManager);


	ModelUnitClass model;
	PartModelClass partModel[2];
	FileInfoMeshClass infoMesh;
	
	infoMesh = p_MeshManager->LoadFileMesh("./data/PL/Legs_Transformer_Hvy.fbx");
	partModel[0].AddMesh(infoMesh.IDs[0]); // ������
	partModel[0].material.pathDiffuse = "./data/PL/Texture.png";
	partModel[0].m_BoneTree = p_MeshManager->GetSkeleton(infoMesh.IDs[0]);
	p_TextureManager->Add(partModel[0].material);
	model.m_PartModels.push_back(partModel[0]);

	infoMesh = p_MeshManager->LoadFileMesh("./data/PL/Cockpits_Spiders.fbx");
	partModel[1].AddMesh(infoMesh.IDs[0]); // ������
	partModel[1].m_BoneTree = p_MeshManager->GetSkeleton(infoMesh.IDs[0]);
	partModel[1].material.pathDiffuse = "./data/PL/Texture.png";
	partModel[1].position.y = 0.0f;
	model.m_PartModels.push_back(partModel[1]);

	model.scale *= 0.001f;
	m_ModelManager->AddModel(model);

	
	m_MeshLine = new MeshLineClass();
	m_MeshLine->Initialize(p_D3D->GetDevice(), 1000);
	D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 end(-5.0f, 5.0f, 5.0f);
	m_MeshLine->Update(start, end);
	//infoMesh = p_MeshManager->LoadFileMesh("./data/wolf/spider.obj");
	//for (int i = 0; i < infoMesh.IDs.size(); i++) {
	//	partModel.AddMesh(infoMesh.IDs[i]);
	//}
	//partModel.material.pathDiffuse = "./data/wolf/textures/SpiderTex.jpg";
	//p_TextureManager->Add(partModel.material);
	//partModel.m_BoneTree = p_MeshManager->GetSkeleton(infoMesh.IDs[0]);
	//model.m_PartModels.push_back(partModel);
	//model.scale *= 0.01f;
	//m_ModelManager->AddModel(model);

	//p_MeshManager->LoadFileMesh("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Walk_Back.fbx");
	//p_MeshManager->LoadFileMesh("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Turn_45deg_L.fbx");
	//p_MeshManager->LoadFileMesh("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Turn_45deg_R.fbx");
	//p_MeshManager->LoadFileMesh("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_DeActivate.fbx");
	//p_MeshManager->LoadFileMesh("./data/PL/Animations/Legs_Spider_Hvy@Legs_Spider_Hvy_Death.fbx");
	//p_MeshManager->LoadFileMesh("./data/Testwuson.X");


	return true;
}


// �������� ��������� (������������ ������)
bool DebugState::Exit(){
	Shutdown();
	return true;
}
bool DebugState::Render(){
	return RenderPrimary();
}
// ���������
bool DebugState::RenderPrimary(){
	// ������� 
	D3DXMATRIX orthoMatrix, worldMatrix, viewMatrix, projectionMatrix, translateMatrix, guiMatrix;
	// ��������� ��������� ������
	m_Camera->Render();

	m_Camera->GetWorldMatrix(&worldMatrix);
	m_Camera->GetOrthoMatrix(&orthoMatrix);
	m_Camera->GetProjectionMatrix(&projectionMatrix);
	m_Camera->GetViewMatrix(&viewMatrix);
	m_Camera->GetMatrixGUI(&guiMatrix);

	/*m_MeshLine->Render(p_D3D->GetDevice());
	p_ShaderManager->RenderTextureShader(m_MeshLine->m_IndexCount, worldMatrix, viewMatrix, projectionMatrix, MaterialClass());

	m_ModelManager->Render(0, m_Camera);*/
	
	
	///////////////////////////////////////////////////////////////////////////////////
	// GUI 2D �������
	///////////////////////////////////////////////////////////////////////////////////	
	p_D3D->TurnZBufferOff();
	// ��� ����� ����������� � 2� ���������, �� ����� ���� ��� ��������� ����� ������������� �����
	m_GuiManager->Render(p_D3D->GetDevice(), worldMatrix, guiMatrix, orthoMatrix);
	p_D3D->TurnZBufferOn();
	///////////////////////////////////////////////////////////////////////////////////
	return true;
}
bool DebugState::Update(InputClass* p_Input, float time){
	return UpdatePrimary(p_Input, time);
}
// ���������� �����
int numberFrame = 0;
float factorFrame = 0;
int backNumberFrame = 0;
bool DebugState::UpdatePrimary(InputClass* p_Input, float time){
	// StateClass
	runTime += time;
	// ��������� ���������� � FPS
	m_Fps->Frame();
	/////////////
	int MouseX = 0;
	int MouseY = 0;
	int dMouseX;
	int dMouseY;
	p_Input->GetMouseLocation(MouseX, MouseY);
	p_Input->GetMouseMove(dMouseX, dMouseY);
	m_GuiManager->Update(p_Input, MouseX, MouseY);
	
	std::string text = "F:" + std::to_string(numberFrame) + " " + std::to_string(factorFrame) + " N:" + std::to_string(backNumberFrame);
	m_GuiManager->SetText("Label1", text);
	text = std::to_string(m_Fps->GetFps());
	m_GuiManager->SetText("Label2", text);
	return true;
}
// ������������� ��������
bool DebugState::Shutdown(){
	// ����������� ������ CPU
	if(m_Cpu){
		m_Cpu->Shutdown();
		delete m_Cpu;
		m_Cpu = 0;
	}
	// ����������� ������ FPS
	if(m_Fps){
		m_Fps->Shutdown();
		delete m_Fps;
		m_Fps = 0;
	}
	// StateClass
	if (m_ModelManager != nullptr) {
		m_ModelManager->Shutdown();
		delete m_ModelManager;
		m_ModelManager = nullptr;
	}
	
	if (m_MeshLine != nullptr) {
		m_MeshLine->Shutdown();
		delete m_MeshLine;
		m_MeshLine = nullptr;
	}
	////////////////////////////////////////////
	return true;
}