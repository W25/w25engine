////////////////////////////////////////////////////////////////////////////////
// Filename: materialclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MATERIALCLASS_H_
#define _MATERIALCLASS_H_

// ����� �������
class MaterialClass{
public:
	MaterialClass();
	std::string  nameMaterial;		// �������� ���������
	std::string  pathAmbient;		// ���� ������� �������� �������
	std::string  pathDiffuse;		// ���� ������������ �������� �����
	std::string  pathNormal;		// ���� �� ����� �������� ��������
	std::string  pathSpecular;		// ���� ������ �������
	D3DXVECTOR3 Ka;					// Ambient-�������� ��������� 
	D3DXVECTOR3 Kd;					// Diffuse-�������� ��������� 
	D3DXVECTOR3 Ks;					// Specular-�������� ���������
	float d;						// ������������ 0..1 
	float Ns;						// ���� ��������
};


#endif
