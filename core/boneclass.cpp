////////////////////////////////////////////////////////////////////////////////
// Filename: boneclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "boneclass.h"
#include "stdafx.h"



BoneMechClass::BoneMechClass() {
	BoneName = "";
	BoneID = -1;
	D3DXMatrixIdentity(&BoneOffset);
}


BoneTreeClass::BoneTreeClass() {
	BoneName = "";
	BoneID = -1;
	m_ParentBone = NULL;
	D3DXMatrixIdentity(&GlobalTransform);
	D3DXMatrixIdentity(&LocalTransform);
	D3DXMatrixIdentity(&Offset);
	flagDoneAnimation = false;
}

D3DXMATRIX BoneTreeClass::ConvertMatrix(const aiMatrix4x4* matrix) {
	D3DXMATRIX result;
	result._11 = matrix->a1;
	result._12 = matrix->b1;
	result._13 = matrix->c1;
	result._14 = matrix->d1;

	result._21 = matrix->a2;
	result._22 = matrix->b2;
	result._23 = matrix->c2;
	result._24 = matrix->d2;

	result._31 = matrix->a3;
	result._32 = matrix->b3;
	result._33 = matrix->c3;
	result._34 = matrix->d3;

	result._41 = matrix->a4;
	result._42 = matrix->b4;
	result._43 = matrix->c4;
	result._44 = matrix->d4;

	return result;
}

// Epic Score
// Gears of War (No Vocals)
// Exit Strategy
// Jack Trammell


// ���� � ������ ����� �� �����
bool BoneTreeClass::FindBoneByName(std::string name, BoneTreeClass*& outBone) {
	if (name == BoneName) {
		outBone = this;
		return true;
	}
	for (unsigned int i = 0; i < m_ChildBone.size(); i++) {
		if (m_ChildBone[i]->FindBoneByName(name, outBone) == true) {
			return true;
		}
	}
	return false;
}

void BoneTreeClass::SetBonesID(std::vector<BoneMechClass> bones) {
	BoneTreeClass *bone;
	for (unsigned int i = 0; i < bones.size(); i++) {
		if (FindBoneByName(bones[i].BoneName, bone) == true){
			bone->BoneID = bones[i].BoneID;
			bone->Offset = bones[i].BoneOffset;
		}
	}
}

D3DXVECTOR3 BoneTreeClass::ConvertVector3(const aiVector3D* vec) {
	return D3DXVECTOR3(vec->x, vec->y, vec->z);
}
void BoneTreeClass::LoadTreeBone(aiNode* p_Node, BoneTreeClass* parent) {

	BoneName = p_Node->mName.C_Str();
	LocalTransform = ConvertMatrix(&p_Node->mTransformation); 
	m_ParentBone = parent;
	// ��������� ����� 
	for (unsigned int i = 0; i < p_Node->mNumChildren; i++) {
		BoneTreeClass *m_BoneTree = new BoneTreeClass();
		m_BoneTree->LoadTreeBone(p_Node->mChildren[i], this);
		m_ChildBone.push_back(m_BoneTree);
	}
}

void BoneTreeClass::CopyTreeBone(BoneTreeClass* source, BoneTreeClass* parent) {
	BoneID = source->BoneID;
	BoneName = source->BoneName;
	LocalTransform = source->LocalTransform;
	Offset = source->Offset;
	GlobalTransform = source->GlobalTransform;
	OriginalLocalTransform = source->OriginalLocalTransform;
	m_ParentBone = parent;
	for (unsigned int i = 0; i < source->m_ChildBone.size(); i++) {
		BoneTreeClass *m_BoneTree = new BoneTreeClass();
		m_BoneTree->CopyTreeBone(source->m_ChildBone[i], this);
		m_ChildBone.push_back(m_BoneTree);
	}
}

bool BoneTreeClass::GetMatrixBones(std::map<int,D3DXMATRIX> &outBoneMatrix) {
	if (this == nullptr) {
		return false;
	}
	if (BoneID != -1) {
		D3DXMATRIX matrix;
		D3DXMatrixMultiply(&matrix, &Offset, &GlobalTransform);
		outBoneMatrix.insert(std::pair<int, D3DXMATRIX>(BoneID, matrix));
	}
	for (unsigned int i = 0; i < m_ChildBone.size(); i++) {
		m_ChildBone[i]->GetMatrixBones(outBoneMatrix);
	}
	return true;
}

std::vector<D3DXMATRIX> BoneTreeClass::GetMatrixBones(){
	std::vector<D3DXMATRIX> result;
	std::map<int, D3DXMATRIX> outBoneMatrix;
	GetMatrixBones(outBoneMatrix);
	std::map<int, D3DXMATRIX>::iterator iter;
	for (iter = outBoneMatrix.begin(); iter != outBoneMatrix.end(); ++iter) {
			result.push_back(iter->second);
	}
	return result;
}

// ���������� ��� ����� ����������� � map
void BoneTreeClass::GetSkeleton(std::map<std::string, BoneTreeClass*> &skeleton){
	skeleton.insert(std::pair<std::string, BoneTreeClass*>(this->BoneName, this));
	for (unsigned int i = 0; i < m_ChildBone.size(); i++) {
		m_ChildBone[i]->GetSkeleton(skeleton);
	}
}

void BoneTreeClass::Shutdown() {
	for (unsigned int i = 0; i < m_ChildBone.size(); i++) {
		m_ChildBone[i]->Shutdown();
		delete m_ChildBone[i];
		m_ChildBone[i] = nullptr;
	}
	m_ChildBone.clear();
}

bool BoneTreeClass::CamputeGlobalMatrix() {
	GlobalTransform = LocalTransform;
	OriginalLocalTransform = LocalTransform;
	if (m_ParentBone != nullptr) {
		D3DXMatrixMultiply(&GlobalTransform, &GlobalTransform, &m_ParentBone->GlobalTransform);
	}
	for (unsigned int i = 0; i < m_ChildBone.size(); i++) {
		m_ChildBone[i]->CamputeGlobalMatrix();
	}
	return true;
}


AnimationClass::AnimationClass() {
	Type = TypeAnimation::NONE;
	indexFrame = 0;
	maxFrame = 1.0;
	widthFrame = 1.0;
	pauseFrame = 0.0;
	firstIndexFrame = 0;	
	lastIndexFrame = 0;
	reloadIndexFrame = 0;
}

void AnimationClass::Shutdown() {
	for (unsigned int i = 0; i < m_AnimationChannel.size(); i++) {
		m_AnimationChannel[i].m_PositionKeys.clear();
		m_AnimationChannel[i].m_RotationKeys.clear();
		m_AnimationChannel[i].m_ScalingKeys.clear();
		m_AnimationChannel[i].Name.clear();
	}
	m_AnimationChannel.clear();
}

void AnimationClass::CamputeInterpolatedPosition(D3DXVECTOR3* outPosition, double AnimationTime, int indexChannel) {
	AnimationChannelClass *pNodeAnim = &m_AnimationChannel[indexChannel];
	// ������������� �� ���� ���������
	if (pNodeAnim->m_PositionKeys.size() == 1) {
		(*outPosition) = pNodeAnim->m_PositionKeys[0].value;
		return;
	}
	unsigned int PositionIndex = FindIndexPosition(AnimationTime, indexChannel);
	unsigned int NextPositionIndex;
	double factor;
	if (PositionIndex == lastIndexFrame) {
		NextPositionIndex = reloadIndexFrame;
		factor = (AnimationTime - pNodeAnim->m_PositionKeys[PositionIndex].time);
	} else {
		NextPositionIndex = (PositionIndex + 1);
		if (NextPositionIndex > lastIndexFrame) {
			NextPositionIndex = reloadIndexFrame;
		}
		factor = (AnimationTime - pNodeAnim->m_PositionKeys[PositionIndex].time);
	}

	if (factor < 0.0) {
		factor = 0.0;
	}
	if (factor > 1.0) {
		factor = 1.0;
	}
	const D3DXVECTOR3& StartPosition = pNodeAnim->m_PositionKeys[PositionIndex].value;
	const D3DXVECTOR3& EndPosition = pNodeAnim->m_PositionKeys[NextPositionIndex].value;
	D3DXVec3Lerp(outPosition, &StartPosition, &EndPosition, (float)factor);
}
void AnimationClass::CamputeInterpolatedScaling(D3DXVECTOR3* outScaling, double AnimationTime, int indexChannel) {
	AnimationChannelClass *pNodeAnim = &m_AnimationChannel[indexChannel];
	// ������������� �� ���� ���������
	if (pNodeAnim->m_ScalingKeys.size() == 1) {
		(*outScaling) = pNodeAnim->m_ScalingKeys[0].value;
		return;
	}
	unsigned int ScalingIndex = FindIndexScaling(AnimationTime, indexChannel);
	unsigned int NextScalingIndex;
	double factor;
	if (ScalingIndex == lastIndexFrame) {
		NextScalingIndex = reloadIndexFrame;
		factor = (AnimationTime - pNodeAnim->m_ScalingKeys[ScalingIndex].time);
	} else {
		NextScalingIndex = (ScalingIndex + 1);
		if (NextScalingIndex > lastIndexFrame) {
			NextScalingIndex = reloadIndexFrame;
		}
		factor = (AnimationTime - pNodeAnim->m_ScalingKeys[ScalingIndex].time);
	}

	if (factor < 0.0) {
		factor = 0.0;
	}
	if (factor > 1.0) {
		factor = 1.0;
	}
	const D3DXVECTOR3& StartScaling = pNodeAnim->m_ScalingKeys[ScalingIndex].value;
	const D3DXVECTOR3& EndScaling = pNodeAnim->m_ScalingKeys[NextScalingIndex].value;
	D3DXVec3Lerp(outScaling, &StartScaling, &EndScaling, (float)factor);
}

void AnimationClass::CamputeInterpolatedRotation(D3DXQUATERNION* outQuaternion, double AnimationTime, int indexChannel) {
	AnimationChannelClass *pNodeAnim = &m_AnimationChannel[indexChannel];
	// ������������� �� ���� ���������
	if (pNodeAnim->m_RotationKeys.size() == 1) {
		(*outQuaternion) = pNodeAnim->m_RotationKeys[0].value;
		return;
	}
	unsigned int RotationIndex = FindIndexRotation(AnimationTime, indexChannel);
	unsigned int NextRotationIndex;
	double factor;
	if (RotationIndex == lastIndexFrame) {
		NextRotationIndex = reloadIndexFrame;
		factor = (AnimationTime - pNodeAnim->m_RotationKeys[RotationIndex].time);
	} else {
		NextRotationIndex = (RotationIndex + 1);
		if (NextRotationIndex > lastIndexFrame) {
			NextRotationIndex = reloadIndexFrame;
		}
		factor = (AnimationTime - pNodeAnim->m_RotationKeys[RotationIndex].time);;
	}

	if (factor < 0.0) {
		factor = 0.0;
	}
	if (factor > 1.0) {
		factor = 1.0;
	}
	const D3DXQUATERNION& StartRotationQ = pNodeAnim->m_RotationKeys[RotationIndex].value;
	const D3DXQUATERNION& EndRotationQ = pNodeAnim->m_RotationKeys[NextRotationIndex].value;

	D3DXQuaternionSlerp(outQuaternion, &StartRotationQ, &EndRotationQ, (float)factor);
	D3DXQuaternionNormalize(outQuaternion, outQuaternion);
}

unsigned int AnimationClass::FindIndexRotation(double AnimationTime, int indexNodeAnim) {
	if (AnimationTime < 0) {
		AnimationTime = 0; 
	}
	for (unsigned int i = 0; i < m_AnimationChannel[indexNodeAnim].m_RotationKeys.size(); i++) {
		if (AnimationTime < m_AnimationChannel[indexNodeAnim].m_RotationKeys[i].time) {
			return i - 1;
		}
	}
	return 0;
}
unsigned int AnimationClass::FindIndexPosition(double AnimationTime, int indexNodeAnim) {
	if (AnimationTime < 0) {
		AnimationTime = 0;
	}
	for (unsigned int i = 0; i < m_AnimationChannel[indexNodeAnim].m_PositionKeys.size(); i++) {
		if (AnimationTime < m_AnimationChannel[indexNodeAnim].m_PositionKeys[i].time) {
			return i - 1;
		}
	}
	return 0;
}
unsigned int AnimationClass::FindIndexScaling(double AnimationTime, int indexNodeAnim) {
	if (AnimationTime < 0) {
		AnimationTime = 0;
	}
	for (unsigned int i = 0; i < m_AnimationChannel[indexNodeAnim].m_ScalingKeys.size(); i++) {
		if (AnimationTime < m_AnimationChannel[indexNodeAnim].m_ScalingKeys[i].time) {
			return i - 1;
		}
	}
	return 0;
}


SkeletonClass::SkeletonClass() {
	m_BoneTree = nullptr;
	p_RootBone = nullptr;
	indexFrame = 0;
	fractionFrame = 0.0;
}

BoneTreeClass* SkeletonClass::GetBone(string name) {
	std::map<std::string, BoneTreeClass*>::iterator iter;
	// ���� ���� � ����������
	iter = m_Bones.find(name);
	if (iter == m_Bones.end()) {
		return nullptr;
	}
	return iter->second;
}

// ������� ������ ������������� ��������
void SkeletonClass::CreateSkeleton(BoneTreeClass* sourceBoneTree) {
	// ������� ����� ����� �������
	p_RootBone = nullptr;
	m_BoneTree = new BoneTreeClass();
	m_BoneTree->CopyTreeBone(sourceBoneTree, p_RootBone);
	m_BoneTree->GetSkeleton(m_Bones);
}

void SkeletonClass::CopySkeleton(SkeletonClass* skeleton) {
	Shutdown();
	CreateSkeleton(skeleton->m_BoneTree);
}

std::vector<D3DXMATRIX> SkeletonClass::GetMatrixBones(void) {
	std::vector<D3DXMATRIX> result;
	if (m_BoneTree != nullptr) {
		result = m_BoneTree->GetMatrixBones();
	}
	return result;
}

void SkeletonClass::RefreshAnimation(TypeAnimation animation) {
	std::map<TypeAnimation, AnimationClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_Animation.find(animation);
	if (iter == m_Animation.end()) {
		return;
	}
	iter->second.indexFrame = 0;
}

void SkeletonClass::UpdateAnimation(TypeAnimation animation, double dTime) {
	std::map<TypeAnimation, AnimationClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_Animation.find(animation);
	if (iter == m_Animation.end()) {
		return;
	}
	iter->second.pauseFrame += dTime;
	int countFrame = (int)(iter->second.pauseFrame / iter->second.widthFrame);

	if (countFrame != 0) {
		iter->second.indexFrame += countFrame;
		iter->second.pauseFrame -= countFrame * iter->second.widthFrame;
		// FIFO
		if (iter->second.indexFrame > iter->second.lastIndexFrame) {
			iter->second.indexFrame = iter->second.reloadIndexFrame;
		}
		if (iter->second.indexFrame < iter->second.firstIndexFrame) {
			iter->second.indexFrame = iter->second.lastIndexFrame;
		}
	}
	// �������� � �������� �������
	if (iter->second.pauseFrame < 0.0) {
		iter->second.pauseFrame = iter->second.widthFrame - abs(iter->second.pauseFrame);
		iter->second.indexFrame -= 1;
	}
	// FIFO
	if (iter->second.indexFrame > iter->second.lastIndexFrame) {
		iter->second.indexFrame = iter->second.reloadIndexFrame;
	}
	if (iter->second.indexFrame < iter->second.firstIndexFrame) {
		iter->second.indexFrame = iter->second.lastIndexFrame;
	}
	// ����� ����� � ����� ����������
	double frame = (double)iter->second.indexFrame + iter->second.pauseFrame / iter->second.widthFrame;
	CamputeAnimation(&iter->second, frame);
}

void SkeletonClass::CamputeAnimation(AnimationClass *animation, double frame){
	TimerClass* p_Time = TimerClass::GetInstance();
	// �������� ����� ����������
	signed __int64 tiksStart = p_Time->GetTiks();


	std::map<std::string, BoneTreeClass*>::iterator iter;
	for (unsigned int i = 0; i < animation->m_AnimationChannel.size(); i++) {
		iter = m_Bones.find(animation->m_AnimationChannel[i].Name);
			if ( iter != m_Bones.end()) {
				D3DXVECTOR3 position;
				D3DXVECTOR3 scale;
				D3DXQUATERNION rotation;
				animation->CamputeInterpolatedPosition(&position, frame, i);
				animation->CamputeInterpolatedScaling(&scale, frame, i);
				animation->CamputeInterpolatedRotation(&rotation, frame, i);
				D3DXMATRIX translateMatrix;
				D3DXMATRIX rotationMatrix;
				D3DXMATRIX scaleMatrix;
				D3DXMATRIX transformMatrix;
				// ������� ��������
				D3DXMatrixScaling(&scaleMatrix, scale.x, scale.y, scale.z);
				// ������� ��������
				D3DXMatrixRotationQuaternion(&rotationMatrix, &rotation);
				// ������� �����������
				D3DXMatrixTranslation(&translateMatrix, position.x, position.y, position.z);
				// �������� ������� �������� � ����������
				D3DXMatrixMultiply(&transformMatrix, &scaleMatrix, &rotationMatrix);
				// �������� ������ �������� � (�������� � ��������)
				D3DXMatrixMultiply(&transformMatrix, &transformMatrix, &translateMatrix);
				iter->second->LocalTransform = transformMatrix;
				int PositionIndex = animation->FindIndexPosition(frame, i);
				int RotationIndex = animation->FindIndexRotation(frame, i);
				int ScalingIndex = animation->FindIndexScaling(frame, i);
				if (PositionIndex + 1 != (int)animation->m_AnimationChannel[i].m_PositionKeys.size() ||
					RotationIndex + 1 != (int)animation->m_AnimationChannel[i].m_RotationKeys.size() ||
					ScalingIndex + 1 != (int)animation->m_AnimationChannel[i].m_ScalingKeys.size()) {
				}
			}
		}
	// ������������� ���� ������ ������
	m_BoneTree->CamputeGlobalMatrix();

	signed __int64 tiksEnd = p_Time->GetTiks();
	dubugCountTiks = tiksEnd - tiksStart;
}

void SkeletonClass::Shutdown(void) {
	p_RootBone = nullptr;

	if (m_BoneTree != nullptr) {
		m_BoneTree->Shutdown();
		delete m_BoneTree;
		m_BoneTree = nullptr;
	}
	m_Bones.clear();
	std::map<TypeAnimation, AnimationClass>::iterator iter;
	for (iter = m_Animation.begin(); iter != m_Animation.end(); ++iter) {
		iter->second.Shutdown();
	}
	m_Animation.clear();
}


