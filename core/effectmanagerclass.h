////////////////////////////////////////////////////////////////////////////////
// Filename: effectmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _EFFECTMANAGERCLASS_H_
#define _EFFECTMANAGERCLASS_H_
#include <d3dx10math.h>
#include <vector>
#include "effectclass.h"

class EffectManagerClass {
// ������� �������� ������
public:
	static EffectManagerClass* GetInstance() {
		static EffectManagerClass m_Instance;
		return &m_Instance;
	}
public:
	bool Initialize(void);
	void SetDirectionToID(int ID, D3DXVECTOR3 dir);
	void SetPositionToID(int ID, D3DXVECTOR3 pos);
	void SetEmitting(int ID, bool flag);
	bool GetPosition(int ID, D3DXVECTOR3& resultPos);
	bool GetDirection(int ID, D3DXVECTOR3& resultDir);
	void Update(CameraClass* p_Camera, float dTime);
	void Render(CameraClass* p_Camera);
	void Refresh(int ID);
	int AddEffect(EffectClass effect);
	void Shutdown(void);
	std::map<int, EffectClass> m_Effect;
private:
	int incrementID;

private:
	// ������������ � �������� ������������ ���������� ��������
	EffectManagerClass();
	~EffectManagerClass() {};
	EffectManagerClass(const EffectManagerClass&) {};
	EffectManagerClass& operator=(EffectManagerClass&) {};
	
};

#endif