////////////////////////////////////////////////////////////////////////////////
// Filename: buttonclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _BUTTONCLASS_H_
#define _BUTTONCLASS_H_

class ShaderManagerClass;
class TextureManagerClass;
class InputClass;
class MaterialClass;
class TextClass;

enum class type_state_control { NORMAL, HOVER, ACTIVE };


class ButtonClass: public BoxClass{
public:
	type_state_control m_State;
	// �������, ��������� ��� ��������� � ������ �� ����������
	ButtonClass();
	virtual ~ButtonClass();
	virtual void UpdateState(type_state_control state);
	bool Update(InputClass*, int, int );
	bool Create(ID3D10Device*, TextureManagerClass*, int, int);
	bool Render(ID3D10Device*, ShaderManagerClass*,
				D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	void Shutdown(void);
	void SetText(std::wstring);
	// ������� �������
	std::function<void(int,int,int)> eventOnClick;
	void onClick(int, int, int);
	MaterialClass sprite;
	bool UpdateBuffer();
	TextClass* m_Text;

};


class ButtonSkillClass : public ButtonClass {
public:
	ButtonSkillClass();
	virtual ~ButtonSkillClass();
	void UpdateState(type_state_control state);
	bool Update(InputClass*, int, int);
private:


};

#endif
