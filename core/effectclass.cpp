
////////////////////////////////////////////////////////////////////////////////
// Filename: effectclass.h
////////////////////////////////////////////////////////////////////////////////
#include "effectclass.h"
#include "stdafx.h"

EffectClass::EffectClass(void) {
	time = 0.0f;
	timeLife = 1.0f;
	flagImmortality = false;
	ID = -1;
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	direction = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	speed = 0.0f;
	flagDeath = false;
}
bool EffectClass::Initialize(void) {
	bool result = true;

	for (unsigned int i = 0; i < m_ParticleSystem.size(); i++) {
		if (m_ParticleSystem[i]->Initialize() == false) {
			result = false;
		}
	}
	return result;
}
void EffectClass::SetDirection(D3DXVECTOR3 dir) {
	//direction = dir;
	for (unsigned int i = 0; i < m_ParticleSystem.size(); i++) {
		m_ParticleSystem[i]->SetDirection(dir);
	}
}
void EffectClass::SetPosition(D3DXVECTOR3 pos) {
	position = pos;
	for (unsigned int i = 0; i < m_ParticleSystem.size(); i++) {
		m_ParticleSystem[i]->SetPosition(pos);
	}
}
void EffectClass::SetTimeLife(float valueTimeLife, bool immortal) {
	timeLife = valueTimeLife;
	flagImmortality = immortal;
}
void EffectClass::SetSpeed(float value) {
	speed = value;
}
float EffectClass::GetSpeed() {
	return speed;
}
float EffectClass::GetTimeLife() {
	return timeLife;
}
D3DXVECTOR3 EffectClass::GetPosition() {
	return position;
}
D3DXVECTOR3 EffectClass::GetDirection() {
	return direction;
}
bool EffectClass::GetFlagDeath() {
	return flagDeath;
}
int EffectClass::GetID() {
	return ID;
}
void EffectClass::SetID(int id) {
	ID = id;
}
void EffectClass::Render(CameraClass *p_Camera) {
	D3DXMATRIX translateMatrix;
	D3DXMATRIX rotationMatrix;
	D3DXMATRIX scaleMatrix;	
	for (unsigned int i = 0; i < m_ParticleSystem.size(); i++) {
		D3DXVECTOR3 positionParticle = m_ParticleSystem[i]->GetPosition();
		D3DXVECTOR3 translation = position - positionParticle;
		D3DXMATRIX worldMatrix;
		D3DXMATRIX viewMatrix;
		D3DXMATRIX projectionMatrix;
		D3DXVECTOR3 cameraPos;
		p_Camera->GetWorldMatrix(&worldMatrix);
		p_Camera->GetViewMatrix(&viewMatrix);
		p_Camera->GetProjectionMatrix(&projectionMatrix);
		cameraPos = p_Camera->GetPosition();
		// ������� ��������
		D3DXMatrixScaling(&scaleMatrix, scale.x, scale.y, scale.z);
		// ������� ��������
		D3DXMatrixRotationYawPitchRoll(&rotationMatrix, 0, 0,0);
		// ������� �����������
		D3DXMatrixTranslation(&translateMatrix, translation.x, translation.y, translation.z);
		// �������� ������� �������� � ����������
		D3DXMatrixMultiply(&worldMatrix, &scaleMatrix, &translateMatrix);
		// �������� ������ �������� � (�������� � ��������)
		D3DXMatrixMultiply(&worldMatrix, &rotationMatrix, &worldMatrix);
		m_ParticleSystem[i]->Render();
		ShaderManagerClass* p_ShaderManager = ShaderManagerClass::GetInstance();
		p_ShaderManager->RenderParticleShader(m_ParticleSystem[i]->GetIndexCount(),
											  worldMatrix, viewMatrix, projectionMatrix, 
											  m_ParticleSystem[i]->GetMaterial(), cameraPos);
	}
}
void EffectClass::SetEmitting(bool flag) {
	flagImmortality = flag;
	// ����� ������� ������
	for (unsigned int i = 0; i < m_ParticleSystem.size(); i++) {
		m_ParticleSystem[i]->SetEmitting(flag);
	}
}

void EffectClass::Update(CameraClass *p_Camera, float dTime) {
	if (flagImmortality == false) {
		time += dTime;
	}
	// ����� ������� ������� ������
	if (time >= timeLife) {
		for (unsigned int i = 0; i < m_ParticleSystem.size(); i++) {
			m_ParticleSystem[i]->SetEmitting(false);
		}
	}
	// ����� ���� ����� ���� �� ���� ������� ������
	bool flag = true;
	for (unsigned int i = 0; i < m_ParticleSystem.size(); i++) {
		if (m_ParticleSystem[i]->GetFlagDeath() == false) {
			flag = false;
		}
	}
	flagDeath = flag;
	for (unsigned int i = 0; i < m_ParticleSystem.size(); i++) {
		m_ParticleSystem[i]->Update(p_Camera, dTime);
	}

}
void EffectClass::Refresh(void) {
	time = 0.0f;
	flagDeath = false;
	for (unsigned int i = 0; i < m_ParticleSystem.size(); i++) {
		m_ParticleSystem[i]->Refresh();
	}
}

void EffectClass::AddParticleSystem(BaseParticleSystemClass* particleSystem) {
	m_ParticleSystem.push_back(particleSystem);
}
void EffectClass::Shutdown(void) {
	for (unsigned int i = 0; i < m_ParticleSystem.size(); i++) {
		m_ParticleSystem[i]->Shutdown();
		delete m_ParticleSystem[i];
		m_ParticleSystem[i] = 0;
	}
	m_ParticleSystem.clear();
	time = 0.0f;
}

