////////////////////////////////////////////////////////////////////////////////
// Filename: linkedlist.cpp
////////////////////////////////////////////////////////////////////////////////
#include "materialclass.h"
#include "stdafx.h"

MaterialClass::MaterialClass() {
	nameMaterial = "NULL";
	pathAmbient = "NULL";
	pathDiffuse = "NULL";
	pathNormal = "NULL";
	pathSpecular = "NULL";
	Ka = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	Kd = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	Ks = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	d = 0.0f;
	Ns = 0.0f;
}