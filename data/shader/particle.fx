////////////////////////////////////////////////////////////////////////////////
// Filename: particle.fx
////////////////////////////////////////////////////////////////////////////////


//=========================================================
// GLOBALS 
//=========================================================
matrix worldMatrix;
matrix viewMatrix;
matrix projectionMatrix;
float3 cameraPosition;
Texture2D shaderTexture;

//=========================================================
// SAMPLE STATES //
//=========================================================
SamplerState SampleType
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Wrap;
    AddressV = Wrap;
};
//=========================================================
// BLENDING STATES 
//=========================================================
BlendState AlphaBlendingState 
{
	BlendEnable[0] = TRUE;
	DestBlend = INV_SRC_ALPHA;
};

//=========================================================
// TYPEDEFS 
//=========================================================

// ��������� ������
struct VertexInputType
{
	float4 position : POSITION;
	float4 color : COLOR;
	float size : WIDTH;
};
// �������������� ������
struct GeometryInputType
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float size : WIDTH;
};
// ���������� ������
struct PixelInputType
{
    float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
    float4 color : COLOR;
};

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
GeometryInputType ParticleVertexShader(VertexInputType input) {
	GeometryInputType output;

	// Change the position vector to be 4 units for proper matrix calculations.
	input.position.w = 1.0f;
	output.position = input.position;
	float4 worldPosition = mul(input.position, worldMatrix);
	float4 viewPosition = mul(worldPosition, viewMatrix);
	output.position = viewPosition;
	output.color = input.color;
	output.size = input.size;
	return output;
}
////////////////////////////////////////////////////////////////////////////////
// Geometry Shader
////////////////////////////////////////////////////////////////////////////////
/*PixelInputType ParticleGeometryShader(GeometryInputType input){
PixelInputType output;
float4 camera = float4(cameraPosition.x, cameraPosition.y, cameraPosition.z, 1.0f);
// Calculate the position of the vertex against the world, view, and projection matrices.
output.position = input.position;
// Store the texture coordinates for the pixel shader.
output.tex = input.tex;
// Store the particle color for the pixel shader.
output.color = input.color;
return  output;
}*/
// ������� ��������� �������� � ����������� �������� ��� � Projection Space
PixelInputType _offsetNprojected(GeometryInputType data, float2 offset, float2 uv)
{
	PixelInputType result;
	data.position.xy += offset;
	data.position = mul(data.position, projectionMatrix);

	result.position = data.position;
	result.tex = uv;
	result.color = data.color;

	return result;
}

[maxvertexcount(4)] // ��������� ������ GS � 4 ��������, ������� �������� TriangleStrip
void ParticleGeometryShader(point GeometryInputType  input[1], inout TriangleStream<PixelInputType> stream)
{
	GeometryInputType pointOut = input[0];

	float size = input[0].size; // ������ ��������� �������� 
							 // �������� ��������
	stream.Append(_offsetNprojected(pointOut, float2(-1, -1) * size, float2(0, 0)));
	stream.Append(_offsetNprojected(pointOut, float2(-1, 1) * size, float2(0, 1)));
	stream.Append(_offsetNprojected(pointOut, float2(1, -1) * size, float2(1, 0)));
	stream.Append(_offsetNprojected(pointOut, float2(1, 1) * size, float2(1, 1)));

	stream.RestartStrip();
}


////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 ParticlePixelShader(PixelInputType input) : SV_Target
{
	float4 finalColor;
	float4 textureColor;

	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
	textureColor = shaderTexture.Sample(SampleType, input.tex);
	// Combine the texture color and the particle color to get the final color result.	
	finalColor.a = textureColor.a*input.color.a;
	finalColor.rgb = finalColor.a*input.color.rgb;
	
	return finalColor;
}


////////////////////////////////////////////////////////////////////////////////
// Technique
////////////////////////////////////////////////////////////////////////////////
technique10 ParticleTechnique {
	pass pass0
	{
		SetBlendState(AlphaBlendingState, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetVertexShader(CompileShader(vs_4_0, ParticleVertexShader()));
		SetGeometryShader(CompileShader(gs_4_0, ParticleGeometryShader()));
		SetPixelShader(CompileShader(ps_4_0, ParticlePixelShader()));
		

	}
}