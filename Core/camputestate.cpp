////////////////////////////////////////////////////////////////////////////////
// Filename: Camputestate.cpp
////////////////////////////////////////////////////////////////////////////////
#include "camputestate.h"
#include "stdafx.h"

CamputeState::CamputeState() {

}

bool CamputeState::ShutdownState() {

	return true;
}

void CamputeState::ClickButtonDebug(int itemClick, int X, int Y) {
	std::string text;
	text = std::to_string(runTime);
	m_GuiManager->SetText("Label1", text);
}



bool CamputeState::UpdateState(InputClass* p_Input, float dTime) {
	int dMouseX;
	int dMouseY;
	int MouseX;
	int MouseY;

	float speed = 5.0f;
	p_Input->GetMouseMove(dMouseX, dMouseY);
	p_Input->GetMouseLocation(MouseX, MouseY);
	if (p_Input->IsMouseDown(MOUSE_RIGHT) == true) {
		m_Camera->AddRotation(D3DXVECTOR3((float)dMouseY / 2.13f, (float)dMouseX / 3.8f, 0.0f));

	}
	if (p_Input->IsKeyDown(DIK_W) == true) {
		m_Camera->MovePosition(speed*dTime);
	}
	if (p_Input->IsKeyDown(DIK_A) == true) {
		m_Camera->ShiftPosition(-speed * dTime, 0.0f);
	}
	if (p_Input->IsKeyDown(DIK_D) == true) {
		m_Camera->ShiftPosition(speed*dTime, 0.0f);
	}
	if (p_Input->IsKeyDown(DIK_S) == true) {
		m_Camera->MovePosition(-speed * dTime);
	}
	if (p_Input->IsKeyDown(DIK_Q) == true) {
		m_Camera->AddRotation(D3DXVECTOR3(0.1f, 0.0f, 0.0f));
	}
	if (p_Input->IsKeyDown(DIK_E) == true) {
		m_Camera->AddRotation(D3DXVECTOR3(-0.1f, 0.0f, 0.0f));
	}
	if (p_Input->IsKeyDown(DIK_NUMPAD7) == true) {
		m_Camera->AddRotation(D3DXVECTOR3(0.0f, 0.5f, 0.0f));
	}
	if (p_Input->IsKeyDown(DIK_NUMPAD9) == true) {
		m_Camera->AddRotation(D3DXVECTOR3(0.0f, -0.5f, 0.0f));
	}
	if (p_Input->IsKeyDown(DIK_NUMPAD1) == true) {
		m_Camera->AddRotation(D3DXVECTOR3(0.0f, 0.0f, 0.2f));
	}
	if (p_Input->IsKeyDown(DIK_NUMPAD3) == true) {
		m_Camera->AddRotation(D3DXVECTOR3(0.0f, 0.0f, -0.2f));
	}
	if (p_Input->IsKeyPress(DIK_NUMPAD5) == true) {
		RenderTexture();
	}

	return true;
}

bool CamputeState::RenderState() {
	D3DXMATRIX orthoMatrix, worldMatrix, viewMatrix, projectionMatrix, translateMatrix, guiMatrix;
	// ��������� ��������� ������
	m_Camera->Render();

	m_Camera->GetWorldMatrix(&worldMatrix);
	m_Camera->GetOrthoMatrix(&orthoMatrix);
	m_Camera->GetProjectionMatrix(&projectionMatrix);
	m_Camera->GetViewMatrix(&viewMatrix);
	m_Camera->GetMatrixGUI(&guiMatrix);

	static MaterialClass material;
	material.pathDiffuse = "./Data/round_hole.png";
	for (int i = 0; i < 2; i++) {
		m_Flat[i].Render(p_D3D->GetDevice());
		p_ShaderManager->RenderTextureShader(m_Flat[i].m_IndexCount, worldMatrix, viewMatrix, projectionMatrix, material);
	}
	

	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			m_Line[i * 10 + j].Render(p_D3D->GetDevice());
			p_ShaderManager->RenderTextureShader(m_Line[i * 10 + j].m_IndexCount, worldMatrix, viewMatrix, projectionMatrix, material);
		}
	}

	return true;
}

bool CamputeState::InitializeState(void) {


	// ������� ������ �����
	LightClass* light = new LightClass();

	// ������������ ������� �����
	//light->SetAmbientColor(0.1250f, 0.1250f, 0.1250f, 1.00f);
	light->SetAmbientColor(1.0f, 1.0f, 1.0f, 1.0f);
	light->SetDiffuseColor(1.0f, 1.0f, 1.0f, 1.0f);
	light->SetDirection(45.0f, 0.0f, 0.0f);
	light->SetSpecularColor(1.0f, 1.0f, 1.0f, 1.0f);
	light->SetSpecularPower(1.0f);
	p_LightManager->SetDirectionalLight(light);

	D3DXVECTOR3 posFlat(5.0f, 0.0f, 0.0f);
	D3DXVECTOR3 normalFlat(1.0f, 0.0f, 0.0f);
	float widthFlat = 1.0f;
	float heightFlat = 1.0f;

	for (int i = 0; i < 2; i++) {
		m_Flat[i].Initialize(p_D3D->GetDevice());
		m_Flat[i].Update(posFlat + posFlat*(float)(1-i), normalFlat, widthFlat, heightFlat);
	}
	MaterialClass material;
	material.pathDiffuse = "./Data/round_hole.png";
	p_TextureManager->Add(material);
	// ������������� ��������� ��������� ������
	m_Camera->SetPosition(D3DXVECTOR3(0.0f, 3.0f, 0.0f));
	m_Camera->SetRotation(D3DXVECTOR3(0.0f, 45.0f, 0.0f));
	m_Camera->Render();
	
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			m_Line[i*10+j].Initialize(p_D3D->GetDevice(), 64);
			m_Line[i * 10 + j].Update(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3((float)i, 10.0f, (float)j));
		}
	}

	return true;
}

bool CamputeState::InitializeComponentGUI(int screenWidth, int screenHeight) {
	LabelClass* Label_1 = new LabelClass;
	Label_1->Name = "Label1";
	Label_1->Left = 10;
	Label_1->Top = 50;
	Label_1->Text = L"Text in Label";
	m_GuiManager->Add(p_D3D->GetDevice(), Label_1);

	using std::placeholders::_1;
	using std::placeholders::_2;
	using std::placeholders::_3;
	// ������ Debug
	ButtonClass* ButtonDebug = new ButtonClass;
	// ����������� ������� � ��������
	ButtonDebug->Name = "Button1";
	ButtonDebug->eventOnClick = std::bind(&CamputeState::ClickButtonDebug, CamputeState::GetInstance(), _1, _2, _3);
	ButtonDebug->Height = 50;
	ButtonDebug->Width = 200;
	ButtonDebug->Top = 160;
	ButtonDebug->Left = 25;
	ButtonDebug->Text = L"Debug";
	m_GuiManager->Add(p_D3D->GetDevice(), ButtonDebug);

	ImageClass* Image_1 = new ImageClass;
	Image_1->Name = "Image_1";
	Image_1->Height = 200;
	Image_1->Width = 200;
	Image_1->Top = 100;
	Image_1->Left = 300;
	Image_1->m_Material.pathDiffuse = "Image_1";
	TexturePack texturePack;
	texturePack.m_Texture = new TextureClass;
	texturePack.m_Texture->SetTexture(m_RenderTexture->GetShaderResourceView());
	texturePack.m_Path = "Image_1";
	p_TextureManager->Add(texturePack);
	m_GuiManager->Add(p_D3D->GetDevice(), Image_1);


	return true;
}



