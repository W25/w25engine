////////////////////////////////////////////////////////////////////////////////
// Filename: statesaboutclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _STATEABOUTCLASS_H_
#define _STATEABOUTCLASS_H_


class D3DClass;
class MsgManagerClass;
class MeshManagerClass;
class TextureManagerClass;
class LightManagerClass;
class ShaderManagerClass;
class RenderTextureClass;
class GuiManagerClass;
class CameraClass;
class InputClass;

class StateAboutClass::pub{ 
public:
	StateClass();
	virtual ~StateClass();
	// ��������� ������������ ���������
	virtual bool InitializeState(void);
	// ������������� ���������
	virtual bool Resume();
	// ���������� ��������� � ������ �����
	virtual bool Pause();
	// ����� �� ���������
	virtual bool Exit();
	// ��������� �������� ������� ������ ������� �� �����, ������ ����������
	virtual bool RenderState();
	// ���������� ������
	virtual bool UpdateState(InputClass*, float);
	// ������������� GUI
	virtual bool InitializeComponentGUI( int screenWidth, int screenHeight);
	// ������ ������ ���������
	bool Initialize(HWND*, D3DClass*, MsgManagerClass*, MeshManagerClass*, ModelManagerClass*,
					TextureManagerClass*, LightManagerClass*, ShaderManagerClass*);
	bool Render();
	bool Update(InputClass*, float);
	bool RenderTexture();
	// ���������� �������� ���������
	WCHAR* GetNameState();
	// ���������� ����� ������ ���������
	float GetRunTime();
	// ���������� � ����� ������ ��������
	ModeStruct GetMode();
protected:
	// ����� ���������, �� �������� ���������� �������� ���������
	HWND* p_HWND;
	D3DClass* p_D3D;
	MsgManagerClass* p_MsgManager; 
	MeshManagerClass* p_MeshManager;
	TextureManagerClass* p_TextureManager;
	LightManagerClass* p_LightManager;
	ShaderManagerClass* p_ShaderManager;
	ModelManagerClass *p_ModelManager;
	// ��������� � ��������
	RenderTextureClass* m_RenderTexture;
	// �������� ����������
	GuiManagerClass* m_GuiManager;
	CameraClass *m_Camera;
	
	int m_ScreenWidth;
	int m_ScrenHeight;
	// ������������ ���� ��������
	virtual bool Shutdown();
	// �������� ���������
	WCHAR* nameState;
	// ����� ������ ���������
	float runTime;
	// ���������� ����������� ������
	int countRenderVertex;
	// ����� ��������� � ������ �����
	ModeStruct mode;
	// ����� ��� ����?
};
// � ����� ���������� ��� ��������� �������� ��������� �� ��������������� ����������
////////////////////////////////////////////////////////////////////////////////
// C�������� � ���������
////////////////////////////////////////////////////////////////////////////////
class AboutState: public StateClass {
// ������� �������� �������
public:
	static AboutState* GetInstance() {
		static AboutState m_Instance;
		return &m_Instance;
	}
	bool UpdateState(InputClass*, float);
	bool RenderState(void);
	bool InitializeState(void);
	bool InitializeComponentGUI(int screenWidth, int screenHeight);
	void ClickButtonDebug(int itemClick, int X, int Y);
private:
	
	// ������������ � �������� ������������ ���������� ��������
	AboutState() {};
	~AboutState() {};
	AboutState(const AboutState&) {};
	AboutState& operator=(AboutState&) {};
};


#endif