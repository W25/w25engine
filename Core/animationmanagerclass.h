////////////////////////////////////////////////////////////////////////////////
// Filename: meshmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _ANIMATIONMANAGERCLASS_H_
#define _ANIMATIONMANAGERCLASS_H_



// ����� ������� � �� ����� ������������ ��������
class AnimationManagerClass {
	// ������� �������� ������
public:
	static AnimationManagerClass* GetInstance() {
		static AnimationManagerClass m_Instance;
		return &m_Instance;
	}
public:
	std::map<TypeAnimation, AnimationClass> m_Animations;
	bool CamputeAnimation(AnimationClass *animation, double frame);
	void AddAnimation(AnimationClass animation);
	void PlayAnimation(TypeAnimation animation, double dTime);
	void RefreshAnimation(TypeAnimation animation);	
	int FindIndexRotation(double AnimationTime, const AnimationChannelClass pNodeAnim);
	int FindIndexPosition(double AnimationTime, const AnimationChannelClass pNodeAnim);
	int FindIndexScaling(double AnimationTime, const AnimationChannelClass pNodeAnim);
	void CamputeInterpolatedRotation(D3DXQUATERNION* outQuaternion, double AnimationTime, AnimationClass* animation, int indexChannel);
	void CamputeInterpolatedPosition(D3DXVECTOR3* outPosition, double AnimationTime, AnimationClass* animation, int indexChannel);
	void CamputeInterpolatedScaling(D3DXVECTOR3* outScaling, double AnimationTime, AnimationClass* animation, int indexChannel);
	void Shutdown(void);
private:
	// ������������ � �������� ������������ ���������� ��������
	AnimationManagerClass() {};
	~AnimationManagerClass() {};
	AnimationManagerClass(const AnimationManagerClass&){};
	AnimationManagerClass& operator=(AnimationManagerClass&) {};
};


#endif

