////////////////////////////////////////////////////////////////////////////////
// Filename: aboutstate.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _CAMPUTESTATE_H_
#define _CAMPUTESTATE_H_

#include "Core\stateclass.h"
#include "meshpolygonclass.h"

class MeshLineClass;
class TerrainClass;
class FrustumClass;
////////////////////////////////////////////////////////////////////////////////
// C�������� � ���������
////////////////////////////////////////////////////////////////////////////////
class CamputeState: public StateClass {
// ������� �������� ������
public:
	static CamputeState* GetInstance() {
		static CamputeState m_Instance;
		return &m_Instance;
	}
	bool ShutdownState(void);
	bool UpdateState(InputClass*, float);
	bool RenderState(void);
	bool InitializeState(void);
	bool InitializeComponentGUI(int screenWidth, int screenHeight);
	void ClickButtonDebug(int itemClick, int X, int Y);
private:
	MeshFlatClass m_Flat[2];
	MeshLineClass m_Line[100];
	// ������������ � �������� ������������ ���������� ��������
	CamputeState();
	~CamputeState() {};
	CamputeState(const CamputeState&) {};
	CamputeState& operator=(CamputeState&) {};
};


#endif