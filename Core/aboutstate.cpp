////////////////////////////////////////////////////////////////////////////////
// Filename: aboutstate.cpp
////////////////////////////////////////////////////////////////////////////////
#include "aboutstate.h"
#include "stdafx.h"

AboutState::AboutState() {
	m_UnitManager = nullptr;
	m_Terrain = nullptr;
	m_Frustum = nullptr;
	m_CameraFrustum = nullptr;
}

bool AboutState::ShutdownState() {

	if (m_UnitManager != nullptr) {
		m_UnitManager->Shutdown();
		delete m_UnitManager;
		m_UnitManager = nullptr;
	}
	if (m_ShotManager != nullptr) {
		m_ShotManager->Shutdown();
		delete m_ShotManager;
		m_ShotManager = nullptr;
	}
	if (m_Terrain != nullptr) {
		m_Terrain->Shutdown();
		delete m_Terrain;
		m_Terrain = nullptr;
	}
	if (m_Frustum != nullptr) {
		delete m_Frustum;
		m_Frustum = nullptr;
	}
	if (m_CameraFrustum != nullptr) {
		delete m_CameraFrustum;
		m_CameraFrustum = nullptr;
	}
	return true;
}

void AboutState::ClickButtonDebug(int itemClick, int X, int Y) {
	std::string text;
	text = std::to_string(runTime);
	m_GuiManager->SetText("Label2", text);
}

// �������� � ���� (�� ���� ������ �������)
bool AboutState::UpdateState(InputClass* p_Input, float dTime) {
	int dMouseX;
	int dMouseY;
	int MouseX;
	int MouseY;
	float speed = 15.0f;

	TimerClass* p_Time = TimerClass::GetInstance();
	signed __int64 tiksStart = p_Time->GetTiks();
	m_UnitManager->Update(dTime, m_Terrain);
	m_ShotManager->Update(dTime);
	EffectManagerClass* p_EffectManager = EffectManagerClass::GetInstance();
	p_EffectManager->Update(m_Camera, dTime);
	signed __int64 tiksEnd = p_Time->GetTiks();
	signed __int64 delta = tiksEnd - tiksStart;

	static int workUnitID = 1;

	p_Input->GetMouseMove(dMouseX, dMouseY);
	p_Input->GetMouseLocation(MouseX, MouseY);
	// �������� ������
	UnitClass* unit = m_UnitManager->GetUnit(workUnitID);
	if (p_Input->IsKeyPress(DIK_P) == true) {
		workUnitID++;
	}
	if (p_Input->IsKeyPress(DIK_O) == true) {
		workUnitID--;
	}
	if (unit != nullptr) {
		//unit->UpdateMove(dTime);
		std::string text = std::to_string((unit->debug)) + ":" + std::to_string((unit->debug));
		m_GuiManager->SetText("Label2", text);
		//unit->unitModel.SetTypeAnimation(TypeAnimation::IDLE);
	}


	if (p_Input->IsKeyDown(DIK_UPARROW) == true) {
		UnitClass *unit = m_UnitManager->GetUnit(workUnitID);
		if (unit != nullptr) {
			D3DXVECTOR3 pos = unit->unitModel.position;
			unit->unitModel.MovePosition(unit->SpeedMove * dTime);
			unit->unitModel.SetTypeAnimation(TypeAnimation::MOVE_UP);
			D3DXVECTOR3 deltaPos = unit->unitModel.position - pos;
			m_Camera->AddPosition(deltaPos);
		}
	}
	if (p_Input->IsKeyDown(DIK_DOWNARROW) == true) {
		UnitClass* unit = m_UnitManager->GetUnit(workUnitID);
		if (unit != nullptr) {
			D3DXVECTOR3 pos = unit->unitModel.position;
			unit->unitModel.MovePosition(-unit->SpeedMove * dTime);
			unit->unitModel.SetTypeAnimation(TypeAnimation::MOVE_DOWN);
			D3DXVECTOR3 deltaPos = unit->unitModel.position - pos;
			m_Camera->AddPosition(deltaPos);
		}
	}
	if (p_Input->IsKeyDown(DIK_RIGHTARROW) == true) {
		UnitClass* unit = m_UnitManager->GetUnit(workUnitID);
		if (unit != nullptr) {	
			unit->unitModel.AddRotation(D3DXVECTOR3(unit->SpeedRotation * dTime, 0.0f, 0.0f));
			unit->unitModel.SetTypeAnimation(TypeAnimation::TURN_L);
		}
	}
	if (p_Input->IsKeyDown(DIK_LEFTARROW) == true) {
		UnitClass* unit = m_UnitManager->GetUnit(workUnitID);
		if (unit != nullptr) {
			unit->unitModel.AddRotation(D3DXVECTOR3(-unit->SpeedRotation * dTime, 0.0f, 0.0f));
			unit->unitModel.SetTypeAnimation(TypeAnimation::TURN_R);
		}
	}
	int scroll;
	p_Input->GetMouseMoveZ(scroll);
	if (scroll != 0) {
		m_Camera->MovePosition(scroll * dTime);

	}
	if (p_Input->IsMousePress(MOUSE_RIGHT) == true) {

		D3DXVECTOR3 mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 end(0.0f, 0.0f, 0.0f);
		mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		start = m_Camera->GetPosition();
		// ���������� ����� �����
		//m_Terrain->GetIntersects(start, mouseDir, &end);
		//m_Terrain->CreateLine(p_D3D->GetDevice(), start - D3DXVECTOR3(0.0f, -1.0f, 0.0f), end);

		UnitClass* unit = m_UnitManager->GetUnitSelection(m_Camera->GetPosition(), mouseDir);
		//unit->unitModel.scale *= 1.2f;
	}

	if (p_Input->IsMousePress(MOUSE_LEFT) == true) {
		D3DXVECTOR3 mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 end(0.0f, 0.0f, 0.0f);
		mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		start = m_Camera->GetPosition();
		// ���������� ����� �����
		m_Terrain->GetIntersects(start, mouseDir, &end);
		//m_Terrain->CreateLine(p_D3D->GetDevice(), start - D3DXVECTOR3(0.0f, -1.0f, 0.0f), end);
		UnitClass* unit = m_UnitManager->GetUnitSelection(m_Camera->GetPosition(), mouseDir);
		if (unit != nullptr) {
			m_Terrain->CreateRound(p_D3D->GetDevice(), unit->unitModel.position, 2.0f);
			workUnitID = unit->ID;
		}else {
			//m_Terrain->CreateRound(p_D3D->GetDevice(), end, 0.10f);
			unit = m_UnitManager->GetUnit(workUnitID);
			if (unit != nullptr) {
				unit->SetMovePoint(end);
				p_EffectManager->SetPositionToID(2, end);
				p_EffectManager->SetDirectionToID(2, D3DXVECTOR3(0.0f, -1.0f, 0.0f));
			}
		}
	}
	UnitClass* unitSelected = m_UnitManager->GetUnit(workUnitID);
	if (unitSelected != nullptr) {
		m_Terrain->CreateRound(p_D3D->GetDevice(), unitSelected->unitModel.position, 2.0f);
	}

	if (p_Input->IsKeyDown(DIK_Z) == true) {
		D3DXVECTOR3 mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 end(0.0f, 0.0f, 0.0f);
		start = m_Camera->GetPosition();
		m_Terrain->GetIntersects(start, mouseDir, &end);
		m_Terrain->AlterArea(p_D3D->GetDevice(), end, -1.5f*dTime, 8, alter_argument::POSSITION);
	}
	if (p_Input->IsKeyDown(DIK_X) == true) {
		D3DXVECTOR3 mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 end(0.0f, 0.0f, 0.0f);
		start = m_Camera->GetPosition();
		m_Terrain->GetIntersects(start, mouseDir, &end);
		m_Terrain->AlterArea(p_D3D->GetDevice(), end, 1.5f*dTime, 8, alter_argument::POSSITION);
	}
	if (p_Input->IsKeyDown(DIK_C) == true) {
		D3DXVECTOR3 mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 end(0.0f, 0.0f, 0.0f);
		start = m_Camera->GetPosition();
		m_Terrain->GetIntersects(start, mouseDir, &end);
		m_Terrain->AlterArea(p_D3D->GetDevice(), end, 10.0f * dTime, 9, alter_argument::MASK1);
	}
	if (p_Input->IsKeyDown(DIK_V) == true) {
		D3DXVECTOR3 mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 end(0.0f, 0.0f, 0.0f);
		start = m_Camera->GetPosition();
		m_Terrain->GetIntersects(start, mouseDir, &end);
		m_Terrain->AlterArea(p_D3D->GetDevice(), end, 10.0f * dTime, 9, alter_argument::MASK2);
	}
	if (p_Input->IsKeyDown(DIK_B) == true) {
		D3DXVECTOR3 mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 end(0.0f, 0.0f, 0.0f);
		start = m_Camera->GetPosition();
		m_Terrain->GetIntersects(start, mouseDir, &end);
		m_Terrain->AlterArea(p_D3D->GetDevice(), end, 10.0f * dTime, 9, alter_argument::MASK3);
	}


	if (p_Input->IsKeyPress(DIK_1) == true) {
		D3DXVECTOR3 mouseDir = m_Camera->GetCursorView((float)MouseX, (float)MouseY, m_Camera->GetPosition());
		D3DXVECTOR3 start(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 end(0.0f, 0.0f, 0.0f);
		start = m_Camera->GetPosition();
		m_Terrain->GetIntersects(start, mouseDir, &end);
		UnitClass* unit = m_UnitManager->GetUnit(workUnitID);
		if (unit != nullptr) {
			ShotBaseClass shot;
			shot.effectID = 2;
			shot.targetPos = unit->markPoint;
			shot.speed = 5.0f;
			shot.position = unit->unitModel.position;
			m_ShotManager->AddShot(shot);
		}
	}

	if (p_Input->IsKeyDown(DIK_F5) == true) {
		m_Terrain->SaveTerrainMap("./Data/terrain/map.w25map");
	}

	if (p_Input->IsKeyDown(DIK_F9) == true) {
		m_Terrain->Shutdown();
		delete m_Terrain;
		m_Terrain = nullptr;
		m_Terrain = new TerrainClass();
		m_Terrain->Initialize(p_D3D->GetDevice(), p_ShaderManager, p_TextureManager, p_LightManager, "./Data/terrain/map.w25map");
	}

	static int typeRenderTerrain = 0;
	if (p_Input->IsKeyPress(DIK_3) == true) {
		ModelClass* model = p_ModelManager->GetModel(workUnitID);
		typeRenderTerrain--;
		m_Terrain->SetMethodRender(typeRenderTerrain);
		model->modeRender = 0;
	}
	if (p_Input->IsKeyPress(DIK_4) == true) {
		ModelClass* model = p_ModelManager->GetModel(workUnitID);
		typeRenderTerrain++;
		m_Terrain->SetMethodRender(typeRenderTerrain);
		model->modeRender = 1;
	}
	if (p_Input->IsKeyDown(DIK_5) == true) {
		ModelClass* model = p_ModelManager->GetModel(workUnitID);
		m_Terrain->SetMethodRender(2);
		model->modeRender = 2;
	}
	//========================================================
	// ���������� �������
	//========================================================
	if (p_Input->IsMouseDown(MOUSE_RIGHT) == true) {
		m_Camera->AddRotation(D3DXVECTOR3((float)dMouseY / 2.13f, (float)dMouseX / 3.8f, 0.0f));
	}
	if (p_Input->IsKeyDown(DIK_W) == true) {
		m_Camera->MovePosition(speed*dTime);
	}
	if (p_Input->IsKeyDown(DIK_A) == true) {
		m_Camera->ShiftPosition(-speed * dTime, 0.0f);
	}
	if (p_Input->IsKeyDown(DIK_D) == true) {
		m_Camera->ShiftPosition(speed*dTime, 0.0f);
	}
	if (p_Input->IsKeyDown(DIK_S) == true) {
		m_Camera->MovePosition(-speed * dTime);
	}
	if (p_Input->IsKeyDown(DIK_Q) == true) {
		m_Camera->AddRotation(D3DXVECTOR3(1.0f, 0.0f, 0.0f));
	}
	if (p_Input->IsKeyDown(DIK_E) == true) {
		m_Camera->AddRotation(D3DXVECTOR3(-1.0f, 0.0f, 0.0f));
	}

	if (m_CameraFrustum->flagUpdate == true) {
		m_CameraFrustum->Clone(m_Camera);
	}
	// ������������
	if (p_Input->IsKeyDown(DIK_NUMPAD8) == true) {
		m_CameraFrustum->MovePosition(speed * dTime);
	}
	if (p_Input->IsKeyDown(DIK_NUMPAD2) == true) {
		m_CameraFrustum->ShiftPosition(-speed * dTime, 0.0f);
	}
	if (p_Input->IsKeyDown(DIK_NUMPAD4) == true) {
		m_CameraFrustum->ShiftPosition(-speed * dTime, 0.0f);
	}
	if (p_Input->IsKeyDown(DIK_NUMPAD6) == true) {
		m_CameraFrustum->ShiftPosition(speed * dTime, 0.0f);
	}

	// �������
	if (p_Input->IsKeyDown(DIK_NUMPAD7) == true) {
		m_CameraFrustum->AddRotation(D3DXVECTOR3(0.0f, -1.5f, 0.0f));
	}
	if (p_Input->IsKeyDown(DIK_NUMPAD9) == true) {
		m_CameraFrustum->AddRotation(D3DXVECTOR3(0.0f, 1.5f, 0.0f));
	}
	// ������
	if (p_Input->IsKeyDown(DIK_NUMPAD1) == true) {
		m_CameraFrustum->AddRotation(D3DXVECTOR3(0.0f, 0.0f, 0.5f));
	}
	if (p_Input->IsKeyDown(DIK_NUMPAD3) == true) {
		m_CameraFrustum->AddRotation(D3DXVECTOR3(0.0f, 0.0f, -0.5f));
	}
	if (p_Input->IsKeyPress(DIK_NUMPAD5) == true) {
		m_CameraFrustum->flagUpdate = !m_CameraFrustum->flagUpdate;
		//RenderTexture();
	}
	static int itemDebug = 0;
	static float timeCtrlFrame = 0.0f;

	if (p_Input->IsKeyDown(DIK_SUBTRACT) == true) {
		itemDebug--;
		LightClass* light = p_LightManager->GetLight();
		light->AddRotationDirection(1, 0, 0);
	}

	if (p_Input->IsKeyDown(DIK_ADD) == true) {
		itemDebug++;
		LightClass* light = p_LightManager->GetLight();
		light->AddRotationDirection(-1, 0, 0);
	}

	// ����� ��������
	if (p_Input->IsKeyPress(DIK_BACKSPACE) == true){
		
	}
	m_GuiManager->SetText("Label2", std::to_string(m_Terrain->GetMethodRender()));
	m_GuiManager->SetText("Label1", std::to_string(m_Fps.GetFps()));
	return true;
}

bool AboutState::RenderState() {
	D3DXMATRIX orthoMatrix, worldMatrix, viewMatrix, projectionMatrix, translateMatrix, guiMatrix;
	D3DXMATRIX viewMatrixFrustum, projectionMatrixFrustum;
	TimerClass* p_Time = TimerClass::GetInstance();
	signed __int64 tiksStart = p_Time->GetTiks();
	// ��������� ��������� ������
	m_Camera->Render();
	
	m_Camera->GetWorldMatrix(&worldMatrix);
	m_Camera->GetOrthoMatrix(&orthoMatrix);
	m_Camera->GetProjectionMatrix(&projectionMatrix);
	m_Camera->GetViewMatrix(&viewMatrix);
	m_Camera->GetMatrixGUI(&guiMatrix);

	m_CameraFrustum->Render();
	m_CameraFrustum->GetProjectionMatrix(&projectionMatrixFrustum);
	m_CameraFrustum->GetViewMatrix(&viewMatrixFrustum);

	m_Frustum->ConstructFrustum(100.0f, projectionMatrixFrustum, viewMatrixFrustum);
	m_Terrain->Render(p_D3D->GetDevice(), worldMatrix, viewMatrix, projectionMatrix, m_Frustum);

	m_UnitManager->Render(m_Camera);
	p_D3D->DisableWriteZBuffer();
	EffectManagerClass* p_EffectManager = EffectManagerClass::GetInstance();
	p_EffectManager->Render(m_Camera);

	p_D3D->EnableWriteZBuffer();

	signed __int64 tiksEnd = p_Time->GetTiks();
	signed __int64 delta = tiksEnd - tiksStart;

	return true;
}

bool AboutState::InitializeState(void) {

	// ������� ���������
	m_Frustum = new FrustumClass();

	// ��������
	m_Terrain = new TerrainClass();
	m_Terrain->Initialize(p_D3D->GetDevice(), p_ShaderManager, p_TextureManager, p_LightManager, "./Data/terrain/map.w25map");
	
	// ������� ������ �����
	LightClass* light = new LightClass();

	// ������������ ������� �����
	light->SetAmbientColor(1.0f, 1.0f, 1.0f, 1.0f);
	light->SetDiffuseColor(1.0f, 1.0f, 1.0f, 1.0f);
	light->SetDirection(45.0f, 0.0f, 0.0f);
	light->SetSpecularColor(1.0f, 1.0f, 1.0f, 1.0f);
	light->SetSpecularPower(1.0f);
	p_LightManager->SetDirectionalLight(light);

	// ������������� ��������� ��������� ������
	m_Camera->SetPosition(D3DXVECTOR3(12.0f, 8.0f, 12.0f));
	m_Camera->SetRotation(D3DXVECTOR3(30.0f, 45.0f, 0.0f));
	m_Camera->Render();

	// ������ ������� ���������
	m_CameraFrustum = new CameraClass();
	m_CameraFrustum->Clone(m_Camera);

	// �������� ���� ������
	m_UnitManager = new UnitManagerClass();
	m_UnitManager->Initialize();

	// �������� ��������
	m_ShotManager = new ShotManagerClass();
	m_ShotManager->Initialize(m_Terrain);

	// �������� ������
	UnitClass *unit = new UnitClass();
	unit->unitModel.CreatePartModel(TypeBody::LEGS, "./Data/PartModel/legs_1.w25pm");
	unit->unitModel.CreatePartModel(TypeBody::BODY, "./Data/PartModel/body_1.w25pm");
	unit->unitModel.CreatePartModel(TypeBody::HEAD, "./Data/PartModel/head_1.w25pm");
	unit->unitModel.position = D3DXVECTOR3(16.0f+5.0f, 0.0f, 16.0f);
	unit->SpeedMove = 1.0f;
	unit->SpeedRotation = 45.0f*unit->SpeedMove;
	m_UnitManager->AddUnit(unit);

	unit = new UnitClass();
	unit->unitModel.CreatePartModel(TypeBody::LEGS, "./Data/PartModel/legs_1.w25pm");
	unit->unitModel.CreatePartModel(TypeBody::BODY, "./Data/PartModel/body_1.w25pm");
	unit->unitModel.CreatePartModel(TypeBody::HEAD, "./Data/PartModel/head_1.w25pm");
	unit->unitModel.position = D3DXVECTOR3(16.0f + 1.0f, 0.0f, 16.0f);
	unit->SpeedMove = 2.0f;
	unit->SpeedRotation = 45.0f * unit->SpeedMove;
	m_UnitManager->AddUnit(unit);

	unit = new UnitClass();
	unit->unitModel.CreatePartModel(TypeBody::LEGS, "./Data/PartModel/legs_1.w25pm");
	unit->unitModel.CreatePartModel(TypeBody::BODY, "./Data/PartModel/body_1.w25pm");
	unit->unitModel.CreatePartModel(TypeBody::HEAD, "./Data/PartModel/head_1.w25pm");
	unit->unitModel.position = D3DXVECTOR3(16.0f + 4.0f, 0.0f, 16.0f + 9.0f);
	unit->SpeedMove = 4.0f;
	unit->SpeedRotation = 45.0f * unit->SpeedMove;
	m_UnitManager->AddUnit(unit);

	unit = new UnitClass();
	unit->unitModel.CreatePartModel(TypeBody::LEGS, "./Data/PartModel/legs_1.w25pm");
	unit->unitModel.CreatePartModel(TypeBody::BODY, "./Data/PartModel/body_1.w25pm");
	unit->unitModel.CreatePartModel(TypeBody::HEAD, "./Data/PartModel/head_1.w25pm");
	unit->unitModel.position = D3DXVECTOR3(16.0f - 2.0f, 0.0f, 16.0f - 3.0f);
	unit->SpeedMove = 8.0f;
	unit->SpeedRotation = 45.0f * unit->SpeedMove;
	m_UnitManager->AddUnit(unit);

	unit = new UnitClass();
	unit->unitModel.CreatePartModel(TypeBody::LEGS, "./Data/PartModel/legs_1.w25pm");
	unit->unitModel.CreatePartModel(TypeBody::BODY, "./Data/PartModel/head_1.w25pm");
	unit->unitModel.position = D3DXVECTOR3(16.0f+5.0f, 0.0f, 16.0f+5.0f);
	unit->SpeedMove = 1.0f;
	unit->SpeedRotation = 45.0f;
	m_UnitManager->AddUnit(unit);


	EffectManagerClass* p_EffectManager = EffectManagerClass::GetInstance();

	EffectClass effect1;
	BaseParticleSystemClass* particleSystem = new SmokeParticleSystemClass();
	particleSystem->Initialize();
	effect1.SetPosition(D3DXVECTOR3(16.0f + 5.0f, 0.0f, 16.0f + 5.0f));
	effect1.AddParticleSystem(particleSystem);
	effect1.SetEmitting(true);
	p_EffectManager->AddEffect(effect1);
	
	EffectClass effect2;
	particleSystem = new BaseParticleSystemClass();
	particleSystem->Initialize();
	effect2.SetPosition(D3DXVECTOR3(16.0f + 5.0f, 2.0f, 16.0f + 15.0f));
	effect2.AddParticleSystem(particleSystem);
	effect2.SetEmitting(true);
	effect2.SetDirection(D3DXVECTOR3(0, 1 , -1));
	p_EffectManager->AddEffect(effect2);


	return true;
}

bool AboutState::InitializeComponentGUI(int screenWidth, int screenHeight) {
	LabelClass* Label_1 = new LabelClass;
	Label_1->Name = "Label1";
	Label_1->Left = 10;
	Label_1->Top = 50;
	Label_1->fontSize = 12;
	Label_1->Text = L"Text in Label";
	m_GuiManager->Add(p_D3D->GetDevice(), Label_1);

	LabelClass* Label_2 = new LabelClass;
	Label_2->Name = "Label2";
	Label_2->Left = 70;
	Label_2->Top = 50;
	Label_2->fontSize = 12;
	Label_2->Text = L"Text in Label";
	m_GuiManager->Add(p_D3D->GetDevice(), Label_2);

	using std::placeholders::_1;
	using std::placeholders::_2;
	using std::placeholders::_3;
	// ������ Debug
	ButtonClass* ButtonDebug = new ButtonClass;
	// ����������� ������� � ��������
	ButtonDebug->Name = "Button1";
	ButtonDebug->eventOnClick = std::bind(&AboutState::ClickButtonDebug, AboutState::GetInstance(), _1, _2, _3);
	ButtonDebug->Height = 50;
	ButtonDebug->Width = 200;
	ButtonDebug->Top = 160;
	ButtonDebug->Left = 25;
	ButtonDebug->Text = L"Debug";
	m_GuiManager->Add(p_D3D->GetDevice(), ButtonDebug);

	ImageClass* Image_1 = new ImageClass;
	Image_1->Name = "Image_1";
	Image_1->Height = 200;
	Image_1->Width = 200;
	Image_1->Top = 100;
	Image_1->Left = 300;
	Image_1->m_Material.pathDiffuse = "Image_1";
	TexturePack texturePack;
	texturePack.m_Texture = new TextureClass;
	texturePack.m_Texture->SetTexture(m_RenderTexture->GetShaderResourceView());
	texturePack.m_Path = "Image_1";
	p_TextureManager->Add(texturePack);
	m_GuiManager->Add(p_D3D->GetDevice(), Image_1);

	return true;
}



