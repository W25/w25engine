////////////////////////////////////////////////////////////////////////////////
// Filename: objectmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "objectmanagerclass.h"
#include "stdafx.h"

ObjectManagerClass::ObjectManagerClass() {
	p_MeshManager = 0;
	p_ShaderManager = 0;
	p_LightManager = 0;
	p_Device = 0;
	m_Object.clear();
}
bool ObjectManagerClass::Initialize(ID3D10Device* device, MeshManagerClass* p_MeshMng, TextureManagerClass *p_TextureMng,
									ShaderManagerClass* p_ShaderMng, LightManagerClass* p_LightMng) {
	p_MeshManager = p_MeshMng;
	p_TextureManager = p_TextureMng;
	p_ShaderManager = p_ShaderMng;
	p_LightManager = p_LightMng;
	p_Device = device;
	return true;
}
// ����������� ����������� ��������
bool ObjectManagerClass::SaveFile(std::string path) {
	std::ofstream fileSave;
	fileSave.open(path, std::ios::binary);
	// ���������, ��� ���� ������� �������
	if (fileSave.fail()) {
		return false;
	}
	// ���������� ���������� ��������
	int countObject = (int)m_Object.size();
	fileSave.write((char*)&countObject, sizeof(int));
	// ��������� ������ � ������� ��� � �������
	for (int i = 0; i < (int)m_Object.size(); i++) {
		int sizeObject = sizeof(ObjectModelClass);
		fileSave.write((char*)&sizeObject, sizeof(int));
		ObjectModelClass writeObject = m_Object[i];
		fileSave.write((char*)&writeObject, sizeObject);
	}
	return true;
}
bool ObjectManagerClass::LoadFile(std::string path) {
	// ����� ������
	std::ifstream fileLoad;
	//��������� ���� � �������� ������ ������ ��� ������
	fileLoad.open(path, std::ios::binary);
	// ��������� �� ����������� ������ ��� ��������
	if (fileLoad.fail()) {
		return false;
	}
	// ������� ������ �������
	this->Clear();
	// ��������� ���������� �������� � �����
	int countObject;
	fileLoad.read((char*)&countObject, sizeof(int));
	// ��������� ������ � ������� ��� � �������
	for (int i = 0; i < countObject; i++) {
		int sizeObject = sizeof(ObjectModelClass);
		fileLoad.read((char*)&sizeObject, sizeof(int));
		ObjectModelClass readObject;
		//// ��������� ������ � ��������� ��� ������� ��� �������������
		this->AddObject(readObject);
	}
	return true;
}
// ������ ������ ����� ���������
void  ObjectManagerClass::fileReadString(std::ifstream& file, std::string& text) {
	unsigned int length;
	// ������ ���������� �������� � ������
	file.read((char*)&length, sizeof(unsigned int));
	char* line = new char[length];
	// ������ ������
	file.read(line, length);
	text = line;
	// ������ ���������� ����� �� �������
	text.resize(length);
	delete[] line;
}
// ������ ������, ����� ���������
void ObjectManagerClass::fileWriteString(std::ofstream& file, std::string text) {
	// �������� ������ ��� ������
	// ������ ���������� �������� � ������
	unsigned int count = text.length();
	// ������ ���������� ��������

	file.write((char*)&count, sizeof(unsigned int));
	// ������ ������
	file.write(text.c_str(), count);
}
// ��������� ������ � ������� � ���������� ��� ID � �������
int ObjectManagerClass::AddObject(ObjectModelClass object){
	// ���������� ���������� ID
	if (m_Object.size() > 0){
		object.ID = m_Object.back().ID + 1;
	} else {
		object.ID = 0;
	}
	m_Object.push_back(object);
	return 0;
}

// ������� ������ �� �������, ���� ��� ������ � ������ � ��������
bool ObjectManagerClass::DeleteObject(int ID){

	for (unsigned int i = 0; i < m_Object.size(); i++) {
		if (m_Object[i].ID == ID) {
			// ������� ������ �� �������
			m_Object[i].Shutdown();
			m_Object.erase(m_Object.begin() + i);
			break;
		}
	}
	return true;	
}
// ������� ��� ������� �� �������
void ObjectManagerClass::Clear(void) {
	for (unsigned int i = 0; i<m_Object.size(); i++) {
		m_Object[i].Shutdown();
	}
	m_Object.clear();
}
// ���������� ��� ������� � ������� � ������������� �������� ��� ���������
void ObjectManagerClass::Shutdown(void){
	Clear();
	p_MeshManager = 0;
	p_ShaderManager = 0;
	p_LightManager = 0;
	p_Device = 0;
}
// ���������� ���������� ��������
int ObjectManagerClass::GetCountObject(){
	return m_Object.size();
}
// ����������� ���� � �������� � ����������� �����
int ObjectManagerClass::GetIndexIntersection(D3DXVECTOR3 RayPosition,D3DXVECTOR3 RayDirection) {
	for (unsigned int i = 0; i < m_Object.size(); i++) {
		D3DXVECTOR3 spherePosition = m_Object[i].position;
		float sphereRadius = m_Object[i].radius*m_Object[i].scale.x; 
		// ������� ����������� ���� �� ������
		D3DXVECTOR3 vect(RayPosition.x - spherePosition.x,
			RayPosition.y - spherePosition.y,
			RayPosition.z - spherePosition.z);
		// at^2+bt+c = 0
		float a = RayDirection.x*RayDirection.x +
			RayDirection.y*RayDirection.y +
			RayDirection.z*RayDirection.z;
		float b = 2 * (vect.x*RayDirection.x +
			vect.y*RayDirection.y +
			vect.z*RayDirection.z);
		float c = pow(vect.x, 2) + pow(vect.y, 2) + pow(vect.z, 2) - pow(sphereRadius, 2);
		float D = pow(b, 2) - 4.0f * a*c;
		// ����� �����������
		if (D > 0) {
			float intersect = (-b + sqrt(D)) / (2.0f*a);
			// ����� ����������� ����� �� ����
			if (intersect >= 0) {
				return i;
			}
		}
	}
	// ������� ������ �������
	return -1;
}
// ������ ��������� ����� � ������
int ObjectManagerClass::GetIndexIntersection(D3DXVECTOR3 position) {
	int result = -1;
	vector<int> resultID;
	// ������� index ���������� ��������
	for (unsigned int i = 0; i < m_Object.size(); i++) {
		D3DXVECTOR3 spherePosition = m_Object[i].position;
		float sphereRadius = m_Object[i].radius*m_Object[i].scale.x;
		D3DXVECTOR3 vec = (position - spherePosition);
		float length = D3DXVec3Length(&vec);
		if ( length <= sphereRadius) {
			resultID.push_back(i);
		}
	}
	float lengthMin = 0.0f;
	// ������ ��������� ������
	if (resultID.size() > 0) {
		D3DXVECTOR3 vec = position - m_Object[resultID[0]].position;
		lengthMin = D3DXVec3Length(&vec);
		result = resultID[0];
	}
	// ������� ����� ��������q ������
	for (unsigned int i = 0; i < resultID.size(); i++) {
		D3DXVECTOR3 vec = (position - m_Object[resultID[i]].position);
		float length = D3DXVec3Length(&vec);
		if (length < lengthMin) {
			lengthMin = length;
			result = resultID[i];
		}
	}
	// ������� ������ �������*/
	return result;
}

// ���������� ���������� �������
D3DXVECTOR3 ObjectManagerClass::GetPosition(int index){
	if (index < (int)m_Object.size() && index >= 0){
		return m_Object[index].position;
	}
	return D3DXVECTOR3(0.0f,0.0f,0.0f);
}
// ���������� ������ ��������
D3DXVECTOR3 ObjectManagerClass::GetRotation(int index) {
	if (index < (int)m_Object.size() && index >= 0) {
		return m_Object[index].rotation;
	}
	return D3DXVECTOR3(0.0f, 0.0f, 0.0f);
}
// ���������� ������ ��������
D3DXVECTOR3 ObjectManagerClass::GetScale(int index) {
	if (index < (int)m_Object.size() && index >= 0) {
		return m_Object[index].scale;
	}
	return D3DXVECTOR3(1.0f, 1.0f, 1.0f);
}
// ���������� ������ � �������� ID
ObjectModelClass ObjectManagerClass::GetObjectToID(int ID) {
	for (unsigned int i = 0; i < m_Object.size(); i++) {
		if (m_Object[i].ID == ID) {
			return m_Object[i];
		}
	}
	return ObjectModelClass();
}
// �������� ������ � �������� ID
void ObjectManagerClass::SetObjectToID(ObjectModelClass object) {
	for (unsigned int i = 0; i < m_Object.size(); i++) {
		if (m_Object[i].ID == object.ID) {
			m_Object[i] = object;
		}
	}
}

// ��������� ��������� �������
bool ObjectManagerClass::Render(D3DXMATRIX , D3DXMATRIX, D3DXMATRIX,
								FrustumClass*, CameraClass* ){

	return true;
}