////////////////////////////////////////////////////////////////////////////////
// Filename: callbackclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _CALLBACKCLASS_H_
#define _CALLBACKCLASS_H_


class Callback {
public:
	bool IsNull(){
		if (function == nullptr) {
			return true;
		}
		return false;
	}
	Callback() {
		function = nullptr;
	}
	~Callback() {
		if (function != nullptr) { delete function; }
	}
	template<class T>
	void operator=(T func) {
		if (function != nullptr) { delete function; }
		// ����� � ��������������� �������� Call, ���������� func
		class NewFuncClass :public FuncClass {
		public:
			T func;
			NewFuncClass(T f) :func(f) {
			}
			void Call(int itemClick, int X, int Y) {
				func(itemClick, X, Y);
			}
		};
		// ������ ��������� ������ � ��������� ���
		function = new NewFuncClass(func);
	}
	void operator()(int itemClick, int X, int Y) {
		if (function) function->Call(itemClick, X, Y);
	}
private:
	// �����, �������������� ����� ������� � �� �������������
	class FuncClass {
	public:
		// ���������������� �������
		virtual void Call(int, int, int) = 0;
	};

	// ��������� �� ����������� �����
	FuncClass *function;

};




#endif
