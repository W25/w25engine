////////////////////////////////////////////////////////////////////////////////
// Filename: aboutstate.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _ABOUTSTATE_H_
#define _ABOUTSTATE_H_

#include "Core\stateclass.h"
#include "meshpolygonclass.h"

class MeshLineClass;
class TerrainClass;
class FrustumClass;
class UnitManagerClass;
class ShotManagerClass;
////////////////////////////////////////////////////////////////////////////////
// C�������� � ���������
////////////////////////////////////////////////////////////////////////////////
class AboutState: public StateClass {
// ������� �������� ������
public:
	static AboutState* GetInstance() {
		static AboutState m_Instance;
		return &m_Instance;
	}
	bool ShutdownState(void);
	bool UpdateState(InputClass*, float);
	bool RenderState(void);
	bool InitializeState(void);
	bool InitializeComponentGUI(int screenWidth, int screenHeight);
	void ClickButtonDebug(int itemClick, int X, int Y);
private:
	TerrainClass* m_Terrain;
	FrustumClass* m_Frustum;
	CameraClass* m_CameraFrustum;
	UnitManagerClass* m_UnitManager;
	ShotManagerClass* m_ShotManager;
	// ������������ � �������� ������������ ���������� ��������
	AboutState();
	~AboutState() {};
	AboutState(const AboutState&) {};
	AboutState& operator=(AboutState&) {};
};


#endif