////////////////////////////////////////////////////////////////////////////////
// Filename: modelclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MODELCLASS_H_
#define _MODELCLASS_H_

enum class TypeAnimation;
enum class TypeBody;

class MeshManagerClass;
class MaterialClass;
class SkeletonClass;
class BoneTreeClass;

struct StructPartModelID {
	int id;
	int type;
};

struct KeyBoneClass {
	BoneTreeClass* p_Bone;
	TypeBody Key;
	string Name;
};

struct KeyAnimationClass {
	TypeAnimation Key;
	string path;
};

class ModelClass {
public:
	ModelClass();	
	SkeletonClass* m_Skeleton;
	std::vector<KeyBoneClass> m_KeyBone;
	int meshID;
	int modeRender;
	TypeAnimation typeAnimation;
	MaterialClass material;
	D3DXVECTOR3 position;
	D3DXVECTOR3 rotation;
	D3DXVECTOR3 scale;
	D3DXMATRIX GetMatrixRoot(void);
	void CreateModel(FileInfoModelClass info);
	void Shutdown(void);
	void Update(double dTime, TerrainClass *p_Terrain);
	void RefreshAnimation(TypeAnimation animation);
	std::vector<D3DXMATRIX> GetMatrixBones(void);
};



#endif