////////////////////////////////////////////////////////////////////////////////
// Filename: objectmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _OBJECTMANAGERCLASS_H_
#define _OBJECTMANAGERCLASS_H_

class ObjectManagerClass{
public:
	ObjectManagerClass();
	bool Initialize(ID3D10Device* device, MeshManagerClass* p_MeshMng, TextureManagerClass *p_TextureMng,
					ShaderManagerClass* p_ShaderMng, LightManagerClass* p_LightMng);
	int AddObject(ObjectModelClass);
	bool DeleteObject(int);
	void Shutdown(void);
	int GetIndexIntersection(D3DXVECTOR3,D3DXVECTOR3);
	int GetIndexIntersection(D3DXVECTOR3 position);
	int GetCountObject();
	bool SaveFile(std::string path);
	bool LoadFile(std::string path); 
	void Clear(void);
	D3DXVECTOR3 GetPosition(int);
	D3DXVECTOR3 GetRotation(int);
	D3DXVECTOR3 GetScale(int);
	ObjectModelClass GetObjectToID(int);
	bool Render(D3DXMATRIX worldMatrix, D3DXMATRIX projectionMatrix, D3DXMATRIX viewMatrix,
				FrustumClass* p_Frustum, CameraClass* camera);
	void SetObjectToID(ObjectModelClass);
	int countRenderVertex;
private:
	std::vector<ObjectModelClass> m_Object;
	MeshManagerClass* p_MeshManager;
	TextureManagerClass *p_TextureManager;
	ShaderManagerClass* p_ShaderManager;
	LightManagerClass* p_LightManager;
	ID3D10Device* p_Device;
	void fileReadString(std::ifstream& file, std::string& text);
	void fileWriteString(std::ofstream& file, std::string text);
};

#endif

