////////////////////////////////////////////////////////////////////////////////
// Filename: objectmodelclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _OBJECTMODELCLASS_H_
#define _OBJECTMODELCLASS_H_

class ObjectModelClass {
public:
	ObjectModelClass();
	void Shutdown();
	std::vector<int> meshID;
	SkeletonClass* m_Skeleton;
	int m_MethodRender;
	int m_ShaderRender;
	int ID;
	int modelID;
	MaterialClass material;
	D3DXVECTOR3 position;
	D3DXVECTOR3 rotation;
	D3DXVECTOR3 scale;
	float radius;

	
};

#endif