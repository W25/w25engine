#include "modelclass.h"
#include "stdafx.h"

ModelClass::ModelClass() {
	meshID = -1;
	modeRender = 0;
	m_Skeleton = nullptr;
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	typeAnimation = TypeAnimation::IDLE;
	//D3DXQuaternionIdentity(&rotation);
}

void ModelClass::RefreshAnimation(TypeAnimation animation) {
	m_Skeleton->RefreshAnimation(animation);
}
void ModelClass::Update(double dTime, TerrainClass* p_Terrain) {
	
	MeshManagerClass* p_MeshManager = MeshManagerClass::GetInstance();
	// ���� ������ � ������� ������� ��������� ������ ������
	SkeletonClass* p_Skeleton = p_MeshManager->GetSkeleton(meshID);
	if (p_Skeleton == nullptr) {
		return;
	}
	std::map<TypeAnimation, AnimationClass>::iterator iter;
	iter = p_Skeleton->m_Animation.find(typeAnimation);
	if (iter == p_Skeleton->m_Animation.end()) {
		return;
	}
	m_Skeleton->fractionFrame += dTime;
	int countFrame = (int)(m_Skeleton->fractionFrame / iter->second.widthFrame);

	if (countFrame != 0) {
		m_Skeleton->indexFrame += countFrame;
		m_Skeleton->fractionFrame -= countFrame * iter->second.widthFrame;
		// FIFO
		if (m_Skeleton->indexFrame > iter->second.lastIndexFrame) {
			m_Skeleton->indexFrame = iter->second.reloadIndexFrame;
		}
		if (m_Skeleton->indexFrame < iter->second.firstIndexFrame) {
			m_Skeleton->indexFrame = iter->second.lastIndexFrame;
		}
	}
	// �������� � �������� �������
	if (m_Skeleton->fractionFrame < 0.0) {
		m_Skeleton->fractionFrame = iter->second.widthFrame - abs(m_Skeleton->fractionFrame);
		m_Skeleton->indexFrame -= 1;
	}
	// FIFO
	if (m_Skeleton->indexFrame > iter->second.lastIndexFrame) {
		m_Skeleton->indexFrame = iter->second.reloadIndexFrame;
	}
	if (m_Skeleton->indexFrame < iter->second.firstIndexFrame) {
		m_Skeleton->indexFrame = iter->second.lastIndexFrame;
	}
	// ����� ����� � ����� ����������
	double frame = (double)m_Skeleton->indexFrame + m_Skeleton->fractionFrame / iter->second.widthFrame;
	
	m_Skeleton->CamputeAnimation(&iter->second, frame);

	
}

D3DXMATRIX ModelClass::GetMatrixRoot(void) {

	if (m_Skeleton != nullptr) {
		if (m_Skeleton->p_RootBone != nullptr) {
			return m_Skeleton->p_RootBone->GlobalTransform;
		}
	}
	D3DXMATRIX result;
	D3DXMatrixIdentity(&result);
	return result;
}

void ModelClass::Shutdown(void) {
	if (m_Skeleton != nullptr) {
		m_Skeleton->Shutdown();
		delete m_Skeleton;
		m_Skeleton = nullptr;
	}

}

void ModelClass::CreateModel(FileInfoModelClass info){
	meshID = info.meshID;
	MeshManagerClass* p_MeshManager = MeshManagerClass::GetInstance();
	SkeletonClass* skeleton = p_MeshManager->GetSkeleton(info.meshID);
	if (skeleton != nullptr) {
		m_Skeleton = new SkeletonClass();
		m_Skeleton->CopySkeleton(p_MeshManager->GetSkeleton(info.meshID));
	}
}



std::vector<D3DXMATRIX> ModelClass::GetMatrixBones(void) {
	std::vector<D3DXMATRIX> result;
	if (m_Skeleton != nullptr) {
		result = m_Skeleton->GetMatrixBones();
	}
	return result;
}

