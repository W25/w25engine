////////////////////////////////////////////////////////////////////////////////
// Filename: animationmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "animationmanagerclass.h"
#include "stdafx.h"


void AnimationManagerClass::RefreshAnimation(TypeAnimation animation) {
	std::map<TypeAnimation, AnimationClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_Animations.find(animation);
	if (iter == m_Animations.end()) {
		return;
	}
	iter->second.pauseFrame = 0.0;
	iter->second.indexFrame = 0;
}
void AnimationManagerClass::PlayAnimation(TypeAnimation animation, double dTime) {
	std::map<TypeAnimation, AnimationClass>::iterator iter;
	// ���� ���� � ����������
	iter = m_Animations.find(animation);
	if (iter == m_Animations.end()) {
		return;
	}
	iter->second.pauseFrame += dTime;
	int countFrame = (int)(iter->second.pauseFrame / iter->second.widthFrame);

	if (countFrame != 0) {
		iter->second.indexFrame += countFrame;
		iter->second.pauseFrame -= countFrame * iter->second.widthFrame;
		// FIFO
		if (iter->second.indexFrame > iter->second.lastIndexFrame) {
			iter->second.indexFrame = iter->second.reloadIndexFrame;
		}
		if (iter->second.indexFrame < iter->second.firstIndexFrame) {
			iter->second.indexFrame = iter->second.lastIndexFrame;
		}
	}
	// �������� � �������� �������
	if (iter->second.pauseFrame < 0.0) {
		iter->second.pauseFrame = iter->second.widthFrame - abs(iter->second.pauseFrame);
		iter->second.indexFrame -= 1;
	}
	// FIFO
	if (iter->second.indexFrame > iter->second.lastIndexFrame) {
		iter->second.indexFrame = iter->second.reloadIndexFrame;
	}
	if (iter->second.indexFrame < iter->second.firstIndexFrame) {
		iter->second.indexFrame = iter->second.lastIndexFrame;
	}
	// ����� ����� � ����� ����������
	double frame = (double)iter->second.indexFrame + iter->second.pauseFrame / iter->second.widthFrame;
	CamputeAnimation(&iter->second, frame);
}




void AnimationManagerClass::CamputeInterpolatedPosition(D3DXVECTOR3* outPosition, double AnimationTime, AnimationClass* animation, int indexChannel) {
/*	AnimationChannelClass *pNodeAnim = &animation->mAnimationChannel[indexChannel];
	// ������������� �� ���� ���������
	if (pNodeAnim->m_PositionKeys.size() == 1) {
		(*outPosition) = pNodeAnim->m_PositionKeys[0].value;
		return;
	}
	int PositionIndex = FindIndexPosition(AnimationTime, (*pNodeAnim));
	int NextPositionIndex;
	double factor;
	if (PositionIndex == animation->lastIndexFrame) {
		NextPositionIndex = animation->reloadIndexFrame;
		factor = (AnimationTime - pNodeAnim->m_PositionKeys[PositionIndex].time);
	} else {
		NextPositionIndex = (PositionIndex + 1);
		if (NextPositionIndex > animation->lastIndexFrame) {
			NextPositionIndex = animation->reloadIndexFrame;
		}
		factor = (AnimationTime - pNodeAnim->m_PositionKeys[PositionIndex].time);
	}

	if (factor < 0.0) {
		factor = 0.0;
	}
	if (factor > 1.0) {
		factor = 1.0;
	}
	const D3DXVECTOR3& StartPosition = pNodeAnim->m_PositionKeys[PositionIndex].value;
	const D3DXVECTOR3& EndPosition = pNodeAnim->m_PositionKeys[NextPositionIndex].value;
	D3DXVec3Lerp(outPosition, &StartPosition, &EndPosition, (float)factor);*/
}
void AnimationManagerClass::CamputeInterpolatedScaling(D3DXVECTOR3* outScaling, double AnimationTime, AnimationClass* animation, int indexChannel) {
	/*AnimationChannelClass *pNodeAnim = &animation->mAnimationChannel[indexChannel];
	// ������������� �� ���� ���������
	if (pNodeAnim->m_ScalingKeys.size() == 1) {
		(*outScaling) = pNodeAnim->m_ScalingKeys[0].value;
		return;
	}
	int ScalingIndex = FindIndexScaling(AnimationTime, (*pNodeAnim));
	int NextScalingIndex;
	double factor;
	if (ScalingIndex == animation->lastIndexFrame) {
		NextScalingIndex = animation->reloadIndexFrame;
		factor = (AnimationTime - pNodeAnim->m_ScalingKeys[ScalingIndex].time);
	} else {
		NextScalingIndex = (ScalingIndex + 1);
		if (NextScalingIndex > animation->lastIndexFrame) {
			NextScalingIndex = animation->reloadIndexFrame;
		}
		factor = (AnimationTime - pNodeAnim->m_ScalingKeys[ScalingIndex].time);
	}

	if (factor < 0.0) {
		factor = 0.0;
	}
	if (factor > 1.0) {
		factor = 1.0;
	}
	const D3DXVECTOR3& StartScaling = pNodeAnim->m_ScalingKeys[ScalingIndex].value;
	const D3DXVECTOR3& EndScaling = pNodeAnim->m_ScalingKeys[NextScalingIndex].value;
	D3DXVec3Lerp(outScaling, &StartScaling, &EndScaling, (float)factor);*/
}

void AnimationManagerClass::CamputeInterpolatedRotation(D3DXQUATERNION* outQuaternion, double AnimationTime, AnimationClass* animation, int indexChannel) {
	/*AnimationChannelClass *pNodeAnim = &animation->mAnimationChannel[indexChannel];
	// ������������� �� ���� ���������
	if (pNodeAnim->m_RotationKeys.size() == 1) {
		(*outQuaternion) = pNodeAnim->m_RotationKeys[0].value;
		return;
	}
	int RotationIndex = FindIndexRotation(AnimationTime, (*pNodeAnim));
	int NextRotationIndex;
	double factor;
	if (RotationIndex == animation->lastIndexFrame) {
		NextRotationIndex = animation->reloadIndexFrame;
		factor = (AnimationTime - pNodeAnim->m_RotationKeys[RotationIndex].time);
	} else {
		NextRotationIndex = (RotationIndex + 1);
		if (NextRotationIndex > animation->lastIndexFrame) {
			NextRotationIndex = animation->reloadIndexFrame;
		}
		factor = (AnimationTime - pNodeAnim->m_RotationKeys[RotationIndex].time);;
	}

	if (factor < 0.0) {
		factor = 0.0;
	}
	if (factor > 1.0) {
		factor = 1.0;
	}
	const D3DXQUATERNION& StartRotationQ = pNodeAnim->m_RotationKeys[RotationIndex].value;
	const D3DXQUATERNION& EndRotationQ = pNodeAnim->m_RotationKeys[NextRotationIndex].value;

	D3DXQuaternionSlerp(outQuaternion, &StartRotationQ, &EndRotationQ, (float)factor);
	D3DXQuaternionNormalize(outQuaternion, outQuaternion);*/
}

bool AnimationManagerClass::CamputeAnimation(AnimationClass *animation, double frame) {
	/*//animChannel
	BoneTreeClass *outBone;
	// �������� ����������� ����� ������ ����� ������ �� ��������� 
	flagDoneAnimation = true;
	for (unsigned int i = 0; i < animation->mAnimationChannel.size(); i++) {
		if (FindBoneByName(animation->mAnimationChannel[i].Name, outBone) == true) {
			D3DXVECTOR3 position;
			D3DXVECTOR3 scale;
			D3DXQUATERNION rotation;
			CamputeInterpolatedPosition(&position, frame, animation, i);
			CamputeInterpolatedScaling(&scale, frame, animation, i);
			CamputeInterpolatedRotation(&rotation, frame, animation, i);
			D3DXMATRIX translateMatrix;
			D3DXMATRIX rotationMatrix;
			D3DXMATRIX scaleMatrix;
			D3DXMATRIX transformMatrix;
			// ������� ��������
			D3DXMatrixScaling(&scaleMatrix, scale.x, scale.y, scale.z);
			// ������� ��������
			D3DXMatrixRotationQuaternion(&rotationMatrix, &rotation);
			// ������� �����������
			D3DXMatrixTranslation(&translateMatrix, position.x, position.y, position.z);
			// �������� ������� �������� � ����������
			D3DXMatrixMultiply(&transformMatrix, &scaleMatrix, &rotationMatrix);
			// �������� ������ �������� � (�������� � ��������)
			D3DXMatrixMultiply(&transformMatrix, &transformMatrix, &translateMatrix);
			outBone->LocalTransform = transformMatrix;
			int PositionIndex = FindIndexPosition(frame, animation->mAnimationChannel[i]);
			int RotationIndex = FindIndexRotation(frame, animation->mAnimationChannel[i]);
			int ScalingIndex = FindIndexScaling(frame, animation->mAnimationChannel[i]);
			if (PositionIndex + 1 != (int)animation->mAnimationChannel[i].m_PositionKeys.size() ||
				RotationIndex + 1 != (int)animation->mAnimationChannel[i].m_RotationKeys.size() ||
				ScalingIndex + 1 != (int)animation->mAnimationChannel[i].m_ScalingKeys.size()) {
				flagDoneAnimation = false;
			}
		}
	}
	CamputeGlobalMatrix();*/
	return true;
}

void AnimationManagerClass::AddAnimation(AnimationClass animation) {
	m_Animations.insert(std::pair<TypeAnimation, AnimationClass>(animation.Type, animation));
	//animation.mAnimationChannel[0].Name
}
// ��������� ������� � ���������� ������� �������
// ����� ��������������, �� �����


void AnimationManagerClass::Shutdown() {

}