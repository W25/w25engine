// stdafx.h: включаемый файл для стандартных системных включаемых файлов
// или включаемых файлов для конкретного проекта, которые часто используются, но
// не часто изменяются
//

#pragma once

#include "targetver.h"
 // Исключите редко используемые компоненты из заголовков Windows
#define WIN32_LEAN_AND_MEAN            
// Файлы заголовков Windows:
#include <windows.h>

// Файлы заголовков C RunTime
#include <WinUser.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <mmsystem.h>

#pragma warning( once : 26495)
//=========================================================
// DLL
//=========================================================
// Assimp
#pragma comment(lib, "assimp-vc140-mt.lib")
// Directx 10
#pragma comment(lib, "d3d10.lib")
#pragma comment(lib, "d3dx10.lib")
#pragma comment(lib, "dxgi.lib")
// DirectInput
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
// CPU
#pragma comment(lib, "pdh.lib")
#include <pdh.h>
// FPS
#pragma comment(lib, "winmm.lib")

//=========================================================
// PRE-PROCESSING DIRECTIVES 
//=========================================================
#define WIN32_LEAN_AND_MEAN

#pragma warning(suppress: 28251)

#define DIRECTINPUT_VERSION 0x0800

#define MOUSE_LEFT 0
#define MOUSE_RIGHT 1
#define MOUSE_MIDDLE 2

//=========================================================
// INCLUDES 
//=========================================================
// Контейнеры
#include <string>
#include <vector>
#include <list>
#include <map>
// Делегаты
#include <functional>
// Считывание файла данных
#include <fstream>
#include <string>
#include <sstream>
// Directx 10
#include <d3d10.h>
#include <d3dx10.h>
#include <d3dx10math.h>
// Ввод
#include "dinput.h"
// Таймер
#include "profileapi.h"

#include <algorithm>
#include <vector3.h>
#include <matrix4x4.h>



#include "Core\cameraclass.h"
#include "Core\frustumclass.h"

#include "Core\systemclass.h"
#include "Core\d3dclass.h"
#include "Core\inputclass.h"

#include "Core\timerclass.h"
#include "Core\cpuclass.h"
#include "Core\fpsclass.h"

#include "Core\texturemanagerclass.h"
#include "Core\textureclass.h"
#include "Core\materialclass.h"

#include "Core\rendertextureclass.h"

#include "Core\lightmanagerclass.h"
#include "Core\lightclass.h"

#include "Core\shadermanagerclass.h"
#include "Core\textureshaderclass.h"
#include "Core\fontshaderclass.h"
#include "Core\lightshaderclass.h"
#include "Core\bumpmapshaderclass.h"
#include "Core\multitextureshaderclass.h"
#include "Core\terrainshaderclass.h"
#include "Core\particleshaderclass.h"

#include "Core\msgmanagerclass.h"

#include "Core\statemanagerclass.h"
#include "Core\stateclass.h"
#include "Core\aboutstate.h"
#include "Core\camputestate.h"

#include <assimp\Importer.hpp>       // Интерфейс импортера C++ 
#include <assimp\scene.h>            // Структура выходных данных 
#include <assimp\postprocess.h>      // Почтовая обработка fla

#include "Core\meshmanagerclass.h"
#include "Core\resourcemeshclass.h"
#include "Core\meshpolygonclass.h"
#include "Core\meshclass.h"

#include "Core\boneclass.h"
#include "Core\animationmanagerclass.h"

#include "Core\modelmanagerclass.h"
#include "Core\modelclass.h"
#include "Core\modelunitclass.h"

#include "Core\unitmanagerclass.h"

#include "Core\frustumclass.h"

#include "Core\effectmanagerclass.h"
#include "effectclass.h"
// Система частиц
#include "Core\baseparticlesystemclass.h"
#include "Core\smokeparticlesystemclass.h"


#include "Core\shotbaseclass.h"
#include "Core\shotmanagerclass.h"

#include "Core\terrainclass.h"
#include "Core\terraintileclass.h"

//#include "Core\rendertextureclass.h"

#include "Core\statemanagerclass.h"
#include "Core\stateclass.h"

#include "Core\guimanagerclass.h"
#include "Core\widgetclass.h"
#include "Core\buttonclass.h"
#include "Core\textclass.h"
#include "Core\labelclass.h"
#include "Core\imageclass.h"
#include "Core\panelclass.h"
#include "Core\cursorclass.h"
#include "Core\fontclass.h"
